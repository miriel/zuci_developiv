//Cambiando status del registro
$('.editar').click(function() {

    var id = $(this).attr('rel');
    var route = path + "tipoArchivo/" + id + "/edit";

    //Creando llamada para poder cambiar el status
    $.get(route, function(result) {

        $('#catvalue').val(result.catvalue);
        $('#catacronym').val(result.catacronym);
        $('#catvalue').attr('rel', id)

        var status = (result.catstatus == 1) ? 'A' : 'I';

        $('[name=status]').val(status);
    });

});

//Limpiando mesajes de alerta de la ventana modal de edicion
$('.editar').click(function() {
    $("#msjEd1").html("");
    $("#msj-errorEdit1").fadeOut();
    $("#msjEd2").html("");
    $("#msj-errorEdit1").fadeOut();
});

//Haciendo llamada AJAX para editar los datos
$('#btnEditArchivo').click(function() {

    var token = $('#token').val();
    var id = $('#catvalue').attr('rel');
    var acronym = $('#catacronym').val();
    var textoNuevo = $('#catvalue').val();
    var route = path + "tipoArchivo/update";
    var status = $("#catstatus option:selected").val();

    $.ajax({
        url: route,
        headers: {
            'X-CSRF-TOKEN': token
        },
        data: {
            idTipo: id,
            catvalue: textoNuevo,
            catacronym: acronym,
            status: status
        },
        type: 'PUT',
        dataType: 'json',
        success: function(data) {

          if(data){
            var route = path + "tipoArchivo";
            window.location.href = route;
          }

        },
        error: function(msj) {
          //Obteniendo mensajes de error de la validacion de los campos
            $("#msjEd1").html(msj.responseJSON.errors.catvalue);
            $("#msj-errorEdit1").fadeIn();

            $("#msjEd2").html(msj.responseJSON.errors.catacronym);
            $("#msj-errorEdit2").fadeIn();
        }
    });
});

//Limpiando mesajes de alerta de la ventana modal de insercion
$('.agregar').click(function() {
    $("#msj1").html("");
    $("#msj-errorAlta1").fadeOut();
    $("#msj2").html("");
    $("#msj-errorAlta2").fadeOut();
});

//Haciendo llamada AJAX para crear un registro
$('#btnCreateArchivo').click(function() {
    var token = $('#token').val();
    var catValueAlta = $('#catValueAlta').val();
    var catCronymAlta = $('#catCronymAlta').val();
    var status = $("#catstatus option:selected").val();
    var route = path + "tipoArchivo/create";

    $.ajax({
        url: route,
        headers: {
            'X-CSRF-TOKEN': token
        },
        data: {
            catValueAlta: catValueAlta,
            catCronymAlta: catCronymAlta,
            status: status
        },
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            window.location.href = path + "tipoArchivo";
        },
        error: function(msj) {
          //Obteniendo mensajes de error de la validacion de los campos
            $("#msj1").html(msj.responseJSON.errors.catCronymAlta);
            $("#msj-errorAlta1").fadeIn();

            $("#msj2").html(msj.responseJSON.errors.catCronymAlta);
            $("#msj-errorAlta2").fadeIn();
        }
    });
});

//Validando en que pestaña se encuentra para poder activarla
$(document).ready(function(e) {
    var status = $("#status").val();

    if (status == 1) {
        $('.nav-tabs a[href="#altas"]').tab('show')
    }
    if (status == 0) {
        $('.nav-tabs a[href="#bajas"]').tab('show')
    }
});
