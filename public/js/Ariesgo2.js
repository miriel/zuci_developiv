function consecuencias(valConcat,idConsecuencia,idCatalogo,folio){
      //console.log("idActividad:" + idActividad  /*+"idConsecuencia"+idConsecuencia+" idCatalogo" + idCatalogo*/);
      var valores = valConcat.split("|");
      var idActividad = valores[0];
      var valTot = valores[1];

      var totPreg = $("#totPreg").val();
      var totalGeneral; // Calculo del total general
      var valJsonGupos, leeJsonGupos ; // Variable que obtiene

      // Resultado de los grupos
      var resultGRupo, separaGrupos, siglaAsignada, sigla;

      //console.log(valTot);
      if ( idCatalogo == '50' ){
        $("#"+idConsecuencia+"_resultado").val(valTot);
        totalGeneral = calculaTotalGenrl(idConsecuencia);
        // Obtiene el JSON de los grupos
        valJsonGupos = $("#jsonGrupos").val();

        // Parse el json de los grupos de Json
        leeJsonGupos = JSON.parse(valJsonGupos);
        //console.log(leeJsonGupos);
        var menos, mas;

        // Cuenta el total de elementos de un arreglo
        var countJSONGroup = leeJsonGupos.length;

        // Obtiene le ultimo valor del JSON decrementando una unidad para obtener el ultimo valor de tal
        var countJSNGRoupK =  --countJSONGroup;

        var sigla = obtieneSigla(totalGeneral, leeJsonGupos );
        $("#"+idConsecuencia+"_grado").val(sigla);
        var grado = $("#"+idConsecuencia+"_grado").val();

        //console.log("sigla asignada: "+ sigla);
        var valor = $("#"+idConsecuencia+"_resultado").val();
        guardaRespuesta(idCatalogo, idActividad, valor, idConsecuencia, sigla, totalGeneral,totPreg,folio, grado);
      }

      if ( idCatalogo == '51' ){
        $("#"+idConsecuencia+"_exposicion").val(valTot);
        totalGeneral = calculaTotalGenrl(idConsecuencia);
        // Obtiene el JSON de los grupos
        valJsonGupos = $("#jsonGrupos").val();

        // Parse el json de los grupos de Json
        leeJsonGupos = JSON.parse(valJsonGupos);
        //console.log(leeJsonGupos);
        var menos, mas;

        // Cuenta el total de elementos de un arreglo
        var countJSONGroup = leeJsonGupos.length;

        // Obtiene le ultimo valor del JSON decrementando una unidad para obtener el ultimo valor de tal
        var countJSNGRoupK =  --countJSONGroup;

        var sigla = obtieneSigla(totalGeneral, leeJsonGupos );
        $("#"+idConsecuencia+"_grado").val(sigla);
        var grado = $("#"+idConsecuencia+"_grado").val();

        //console.log("sigla asignada: "+ sigla);
        var valor = $("#"+idConsecuencia+"_exposicion").val();


        guardaRespuesta(idCatalogo, idActividad, valor, idConsecuencia, sigla, totalGeneral,totPreg,folio, grado);
      }

      if ( idCatalogo == '52' ){
        $("#"+idConsecuencia+"_probabilidad").val(valTot);
        totalGeneral = calculaTotalGenrl(idConsecuencia);
        // Obtiene el JSON de los grupos
        valJsonGupos = $("#jsonGrupos").val();

        // Parse el json de los grupos de Json
        leeJsonGupos = JSON.parse(valJsonGupos);
        //console.log(leeJsonGupos);
        var menos, mas;

        // Cuenta el total de elementos de un arreglo
        var countJSONGroup = leeJsonGupos.length;

        // Obtiene le ultimo valor del JSON decrementando una unidad para obtener el ultimo valor de tal
        var countJSNGRoupK =  --countJSONGroup;

        var sigla = obtieneSigla(totalGeneral, leeJsonGupos );
        //console.log("sigla asignada: "+ sigla);
        $("#"+idConsecuencia+"_grado").val(sigla);
        var grado = $("#"+idConsecuencia+"_grado").val();

        var valor = $("#"+idConsecuencia+"_probabilidad").val();
        guardaRespuesta(idCatalogo, idActividad, valor, idConsecuencia, sigla, totalGeneral,totPreg,folio, grado);

      }

      // Muestra la opcion de la sigla correspondiente.




}

function guardaRespuesta(catalogo, idActiv, valor, idConsecuencia, sigla, total, totPreg, folio, gradoT ){

   var route = path+"ariesgo/"+idConsecuencia;
   var token = $("#token").val();

   // Variable que obtiene el id del usuario que se logueo en el sistema
   var idUserLogin = $("#idUserLogin").val();

   $.ajax({
       url: route,
       headers: {'X-CSRF-TOKEN':token},
       type: 'PUT',
       dataType: 'json',
       data: { catalogo:catalogo, descripcion: idActiv, valor: valor, total:total, idConsecuencia:idConsecuencia,
               folio:folio, totalPreg:totPreg, gradoT:gradoT, idUserLogin:idUserLogin }
   });
}

function obtieneSigla(totalGeneral, leeJsonGupos){
  $.each(leeJsonGupos, function(i,item){
      menos=i;
      menos--;

      if( i == 0 ){
        if( totalGeneral <= leeJsonGupos[i].cmval ){
          sigla = leeJsonGupos[i].cmabbr;
          return false;
        }
      }else{
        menos=i;
        menos--;
        //console.log("Primer barrido" + i + " - "+leeJsonGupos[i].cmval+" - "+leeJsonGupos[i].cmabbr+" - "+leeJsonGupos[i].cmfk+" Total: "+totalGeneral+ " Una menos: "+leeJsonGupos[menos].cmval+ " real: "+leeJsonGupos[i].cmval);
        // Compara si esta en el rango de los valores con el anterior indice del JSON
        if( totalGeneral >= leeJsonGupos[menos].cmval && totalGeneral < leeJsonGupos[i].cmval ){
          sigla = leeJsonGupos[menos].cmabbr;
          menos++;
          return false;
        }else
        // Si no esta en ningun rango obtiene el ultimo valor del JSON
        if( totalGeneral >= leeJsonGupos[i].cmval ){
          sigla = leeJsonGupos[i].cmabbr;
        }
      }
  });

  return sigla;
}


function calculaTotalGenrl(idConsecuencia){
  // Obtiene todos los valores de los totales
  var  valor1 = $("#"+idConsecuencia+"_resultado").val();
  var  valor2 = $("#"+idConsecuencia+"_exposicion").val();
  var  valor3 = $("#"+idConsecuencia+"_probabilidad").val();

  // Iguala a 0 cuando los totales sean iguales a nulo
  if(valor1 === null){ valor1=0; }
  if(valor2 === null){ valor2=0; }
  if(valor3 === null){ valor3=0; }

  // Se guardan los valores a temporales
  $("#valor1Temp").val(valor1);
  $("#idConsecuenTemp").val(idConsecuencia);

  var totalGeneral = valor1 * valor2  * valor3;
  $("#"+idConsecuencia+"_total").val(totalGeneral);
  return totalGeneral;
}

function obtieneGrupo(grado){

  var resultVal;
  var token = $("#token").val();
  var route = path+"/grad/"+grado;

  $.ajax({
      async: false,
       url: route,
       headers: {'X-CSRF-TOKEN':token},
       type: 'GET',
       //data: { anname:anname, anatfk:anatfk, anvalue:anvalue,status:status},
       success: function(data){
            if(data == 'undefined' || data == ''){
              resultVal = data[0].cmfk+"|"+data[0].cmval+"|"+data[0].cmabbr;
            }else{
              resultVal = data[0].cmfk+"|"+data[0].cmval+"|"+data[0].cmabbr;
            }
       }
   });
  return resultVal;
}

function guardarTextarea(idActividad,id,id2){
  var route = path+"ariesgo/"+id;
  var token = $("#token").val();
  var caja = idActividad;
  var id2 = id2;
  var totPreg = $("#totPreg").val();

  // Variable que obtiene el id del usuario que se logueo en el sistema
  var idUserLogin = $("#idUserLogin").val();

  $.ajax({
      url: route,
      headers: {'X-CSRF-TOKEN':token},
      type: 'PUT',
      dataType: 'json',
      data: { caja:caja , id2: id2, catalogo:'updtcom', idUserLogin:idUserLogin, totPreg:totPreg }
  });
}


function guardarTextareaEjec(valorCaja,id,id2,tipoCaja){
  // arplanacc = Plan de acción
  // ardepresp = Plan de acción
  // arconsemp = Consecuencias para la empresa

  var route = path+"ariesgoEjec/"+id;
  var token = $("#token").val();
<<<<<<< HEAD

=======
  
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
  // Variable que obtiene el id del usuario que se logueo en el sistema
  var idUserLogin = $("#idUserLogin").val();

  $.ajax({
      url: route,
      headers: {'X-CSRF-TOKEN':token},
      type: 'PUT',
      dataType: 'json',
      data: { caja:valorCaja , id2: id2, tipoCaja:tipoCaja, idUserLogin:idUserLogin }
  });

}
