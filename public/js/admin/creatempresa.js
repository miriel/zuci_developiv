// Crea un cuestionario desde la ventana modal al editar el empresa
$("#btnCreateEmpresa").click(function(){

		var route = path+"empresa";
		var ctname = $("#ctname").val();
		var token = $("#token").val();

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'POST',
					dataType: 'json',
					data: { ctname:ctname},
					success: function(){
						$("#msj-success").fadeIn();
						carga();
					},
					error:function(msj){
          $("#msj").html(msj.responseJSON.errors.ctname);
          $("#msj-error").fadeIn();
          }
			});

});

// Funcio  que envia datos de la ventana modal y actualiza un registro
$("#btnEditEmpresa").click(function(){

		var id = $("#ctfk").val();
		var route = path+"empresa/"+id;
		var ctname = $("#ctnamed").val();
		var token = $("#token").val();

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'PUT',
					dataType: 'json',
					data: { ctname: ctname },
					success: function(){
						$("#msj-successEd").fadeIn();
						carga();
					},
					error:function(msj){
					//console.log(msj);
					//console.log(msj.responseJSON.errors.qtname);
          $("#msjEd").html(msj.responseJSON.errors.ctname);
          $("#msj-errorEd").fadeIn();
          }
			});

});


// Vuelve a actualizar la pagina
function carga(){
	location.reload();
}
