function mostrar(id) {
	// alert(id);
    if (id == "50") {
      $("#mostar").show();

    }else {
    	 $("#mostar").hide();
    }
	}


// Crea un cuestionario desde la ventana modal para agregar catalogos
$("#agregarcatalogos").click(function(){

		var route = path+"catalogos";
		var token = $("#token").val();
		var cmtpcat = $("#cmtpcat").val();
		var cmdesc = $("#cmdesc").val();
	  var cmabbr = $("#cmabbr").val();
	  var cmval = $("#cmval").val();
		var cmmin = $("#cmmin").val();
		var cmmax = $("#cmmax").val();
		var cmmoneda = $("#cmmoneda").val();
		// alert(cmval);
 	// alert( cmtpcat + "descripcion"+ cmdesc + " abre" + cmabbr + "valor" + cmval + "conto min" + cmmin + "monto max" + cmmax + "moneda" + cmmoneda);

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'POST',
					dataType: 'json',
					data: {cmdesc:cmdesc, cmabbr:cmabbr, cmval:cmval, cmmin:cmmin, cmmax:cmmax, cmmoneda:cmmoneda, cmtpcat:cmtpcat},
					success: function(){
						$("#msj-success").fadeIn();
						carga();
					},
					error:function(msj){
						// console.log(msj);
						$("#msjd").html(msj.responseJSON.errors.cmdesc);
						$("#msj-errord").fadeIn();
						$("#msjt").html(msj.responseJSON.errors.cmtpcat);
						$("#msj-errort").fadeIn();
						$("#msjb").html(msj.responseJSON.errors.cmabbr);
						$("#msj-errorb").fadeIn();
						$("#msjv").html(msj.responseJSON.errors.cmval);
						$("#msj-errorv").fadeIn();
						$("#msji").html(msj.responseJSON.errors.cmmin);
						$("#msj-errori").fadeIn();
						$("#msja").html(msj.responseJSON.errors.cmmax);
						$("#msj-errora").fadeIn();
          }
			});
});

// Crea un cuestionario desde la ventana modal para agregar Exposicion
$("#agregaexposicion").click(function(){

		var route = path+"catalogos";
		var token = $("#token").val();
		var cmtpcat = "51";
		var cmdesc = $("#cmdesc").val();
	  var cmabbr = $("#cmabbr").val();
	  var cmval = $("#cmval").val();
		var cmmin = $("#cmmin").val();
		var cmmax = $("#cmmax").val();
		var cmmoneda = $("#cmmoneda").val();

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'POST',
					dataType: 'json',
					data: {cmdesc:cmdesc, cmabbr:cmabbr, cmval:cmval, cmmin:cmmin, cmmax:cmmax, cmmoneda:cmmoneda, cmtpcat:cmtpcat},
					success: function(){
						$("#msj-success").fadeIn();
						carga();
					},
					error:function(msj){
						// console.log(msj);
						$("#msjd").html(msj.responseJSON.errors.cmdesc);
						$("#msj-errord").fadeIn();
						$("#msjb").html(msj.responseJSON.errors.cmabbr);
						$("#msj-errorb").fadeIn();
						$("#msjv").html(msj.responseJSON.errors.cmval);
						$("#msj-errorv").fadeIn();
						$("#msji").html(msj.responseJSON.errors.cmmin);
						$("#msj-errori").fadeIn();
						$("#msja").html(msj.responseJSON.errors.cmmax);
						$("#msj-errora").fadeIn();
          }
			});
});



// Funcion  que envia datos de la ventana modal y actualiza un registro
$("#EditActividad").click(function(){
	var id = $("#cmfkEd").val();
	var route = path+"catalogos/"+id;
	var cmdesc = $("#cmdescEd").val();
  var cmabbr = $("#cmabbrEd").val();
  var cmval = $("#cmvalEd").val();
	var cmmin = $("#cmminEd").val();
	var cmmax = $("#cmmaxEd").val();
	var cmmoneda = $("#cmmodenaEd").val();
	var token = $("#token").val();
 	// alert( cmdesc + cmabbr + cmval + cmmin + cmmax + cmmoneda);
	$.ajax({
		 url: route,
		 headers: {'X-CSRF-TOKEN':token},
		 type: 'PUT',
		 dataType: 'json',
		 data: { cmdesc:cmdesc, cmabbr:cmabbr, cmval:cmval, cmmin:cmmin,cmmax:cmmax, cmmoneda:cmmoneda},
		 success: function(){
			 $("#msj-successEd").fadeIn();
			 		carga();
		 },error:function(msj){
				// console.log(msj);
			 $("#msjEd").html(msj.responseJSON.errors.cmdesc);
			 $("#msj-errorEd").fadeIn();
			 $("#msj1").html(msj.responseJSON.errors.cmabbr);
			 $("#msj-error1").fadeIn();
			 $("#msj2").html(msj.responseJSON.errors.cmval);
			 $("#msj-error2").fadeIn();
			 $("#msj3").html(msj.responseJSON.errors.cmmin);
			 $("#msj-error3").fadeIn();
			 $("#msj4").html(msj.responseJSON.errors.cmmax);
			 $("#msj-error4").fadeIn();

		 }
 });

});


// Vuelve a actualizar la pagina
function carga(){
	location.reload();
}
