
// Crea un cuestionario desde la ventana modal al editar el empresa
$("#CreateDescripcion").click(function(){
		var route = path+"descripcion";
    var qtfk= $("#idoperacion").val();
    var cmdesc = $("#cmdescCre").val();
    var cuest = $("#cuestionarios").val();
		var token = $("#token").val();
	// alert(qtfk + cmdesc + cuest);
   $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN':token},
        type: 'POST',
        dataType: 'json',
        data: { qtfk:qtfk, cmdesc:cmdesc ,cuest:cuest},
        success: function(){
          $("#msj-success").fadeIn();
          carga();
        },
        error:function(msj){
          $("#msjCre").html(msj.responseJSON.errors.cmdesc);
          $("#msj-errorCre").fadeIn();
         }
    });

});



// Funcion  que envia datos de la ventana modal y actualiza un registro
$("#EditDescripcion").click(function(){
	var id = $("#cmfkEd").val();
	var route = path+"descripcion/"+id;
	var cmdesc = $("#cmdescEd").val();
	var token = $("#token").val();


	 $.ajax({
			url: route,
			headers: {'X-CSRF-TOKEN':token},
			type: 'PUT',
			dataType: 'json',
			data: { cmdesc:cmdesc },
			success: function(){
				$("#msj-successEd").fadeIn();
				carga();
			},error:function(msj){
         console.log(msj);
 				$("#msjEd").html(msj.responseJSON.errors.cmdesc);
 				$("#msj-errorEd").fadeIn();

			}
	});

});


// Vuelve a actualizar la pagina
function carga(){
	location.reload();
}
