$(document).ready(function(e){
  var status = $("#status").val();

    // Carga el ID del tipo de cuestionario en el modal de acuerdo al seleccionado con respecto al modal de Boostrap
    $('#editActividad').on('show.bs.modal', function(e){
      // Obtiene el id de la lista de registros en el index
      var id = $(e.relatedTarget).data().id;
      var dato = id.split("|");
      var idP= dato[0]; // Id
      var idC = dato[1]; // Catalogo
      // alert( idP + idC );

      // Ruta que trae el registro de la BD para llenar las cajas de texto en el modal
      var route = path+"actividad/"+idP+"/edit";
      // alert(route);

      // Llena el ID del registro de la lista del index en la ventana modal
      $(e.currentTarget).find('#cmfkEd').val(idP);
      // Obtiene y llena las cajas de la ventana modal con los valores que corresponden al ID del registro
        	$.get(route, function(res){
            if (res.cmtpcat != "50") {
              $("#Ocultar").hide();
              }
              else {
                 $("#Ocultar").show();
              }

          $(e.currentTarget).find('#cmdescEd').val(res.cmdesc);
          $(e.currentTarget).find('#cmabbrEd').val(res.cmabbr);
          $(e.currentTarget).find('#cmvalEd').val(res.cmval);
          $(e.currentTarget).find('#cmminEd').val(res.cmmin);
          $(e.currentTarget).find('#cmmaxEd').val(res.cmmax);
          $(e.currentTarget).find('#cmmodenaEd').val(res.cmmoneda);
    	});

    });


    // Redirecciona y muestra pestaña de tipo de cuestionario depende del estatus que se encuentre el registro
    if(status == 1){
      $('.nav-tabs a[href="#altas"]').tab('show')
    }
    if(status == 0){
      $('.nav-tabs a[href="#bajas"]').tab('show')
    }


});
