$(document).ready(function(e){
  var status = $("#status").val();

    // Carga el ID del tipo de cuestionario en el modal de acuerdo al seleccionado con respecto al modal de Boostrap
    $('#editCiudad').on('show.bs.modal', function(e){
      // Obtiene el id de la lista de registros en el index
      var id = $(e.relatedTarget).data().id;

      // Ruta que trae el registro de la BD para llenar las cajas de texto en el modal
      var route = path+"ciudad/"+id+"/edit";

      // Llena el ID del registro de la lista del index en la ventana modal
      $(e.currentTarget).find('#cifk').val(id);

      // Obtiene y llena las cajas de la ventana modal con los valores que corresponden al ID del registro
    	$.get(route, function(res){
          $(e.currentTarget).find('#cinamed').val(res.ciname);
            $(e.currentTarget).find('#ciladad').val(res.cilada);
    	});

    });





    // Redirecciona y muestra pestaña de tipo de cuestionario depende del estatus que se encuentre el registro
    if(status == 1){
      $('.nav-tabs a[href="#altas"]').tab('show')
    }
    if(status == 0){
      $('.nav-tabs a[href="#bajas"]').tab('show')
    }


});
