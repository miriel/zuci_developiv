// Crea un cuestionario desde la ventana modal al editar el cuestionario
$("#btnCrePreg").click(function(){
		var route = path+"pregunta";
		var qsname = $("#qsnameCr").val();
		var token = $("#token").val();

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'POST',
					dataType: 'json',
					data: { qsname: qsname },
					success: function(){
						$("#msj-success").fadeIn();
						carga();
					},
					error:function(msj){
          //console.log(msj.responseJSON.errors.qsname);
          $("#msjCr").html(msj.responseJSON.errors.qsname);
          $("#msj-errorCr").fadeIn();
          }
			});
});

// Funcio  que envia datos de la ventana modal y actualiza un registro
$("#btnEditPreg").click(function(){

		var id = $("#idPregCst").val(); // ID de pregunta
		var idPregResp = $("#idPregRespCst").val(); // ID de Pregunta Respuesta
		var qsname = $("#qsname").val();
		var token = $("#token").val();
		var annameAddRsp = $("#annameAddRsp").val();
		var idTipoCuestionario = $("#idTipoCuestionario").val();
		var modulo = "cuestionarios";
		var qtnameRespCst = $("#qtnameRespCst").val();
		var qunameRespCst = $("#qunameRespCst").val();
		var annameAddRsp = $("#annameAddRsp").val();
		var idSeccionRespCst = $("#idSeccionRespCst").val();
		var idSubseccionRespCst = $("#idSubseccionRespCst").val();

		// Ruta donde se enviaran los datos
		var route = path+"pregunta/"+idPregResp;

		$("#msj").html("");
		$("#msj-error").fadeOut();

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'PUT',
					dataType: 'json',
					data: { qaqsfk:qsname, idPregResp:idPregResp, idTipoCuestionario:idTipoCuestionario,modulo:modulo, annameAddRsp:annameAddRsp,
					 				qunameRespCst: qunameRespCst, qtnameRespCst:qtnameRespCst, annameAddRsp:annameAddRsp, idSubseccionRespCst:idSubseccionRespCst, idSeccionRespCst:idSeccionRespCst  },
					success: function(){
						$("#msj-success").fadeIn();
						carga();
					},
					error:function(msj){
					//console.log(msj);
					//console.log(msj.responseJSON.errors.qtname);
          $("#msj").html(msj.responseJSON.errors.qaqsfk);
          $("#msj-error").fadeIn();
          }
			});

});


// Funcion  que envia datos de la ventana modal y actualiza un registro en el modulo de preguntas
$("#btnEditPregModPrg").click(function(){

		var id = $("#idPregUpCst").val(); // ID de pregunta
		var route = path+"pregunta/"+id;
		var qsname = $("#qsnameUpPreg").val();
		var token = $("#token").val();
		var idTipoCuestionario = $("#idTipoCuestionario").val();
		var modulo = "preguntas"; // Modulo donde se realiza la accion

		$("#msj").html("");
		$("#msj-error").fadeOut();

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'PUT',
					dataType: 'json',
					data: { qsname:qsname, idTipoCuestionario:idTipoCuestionario, modulo:modulo },
					success: function(){
						$("#msj-success").fadeIn();
						carga();
					},
					error:function(msj){
					//console.log(msj);
					//console.log(msj.responseJSON.errors.qtname);

          }
			});

});

// Vuelve a actualizar la pagina
function carga(){
	location.reload();
}
