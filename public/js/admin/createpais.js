// Crea un cuestionario desde la ventana modal al editar el pais
$("#btnCreatePais").click(function(){

		var route = path+"pais";
		var cnname = $("#cnname").val();
		var token = $("#token").val();

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'POST',
					dataType: 'json',
					data: { cnname:cnname},
					success: function(){
						$("#msj-success").fadeIn();
						carga();
					},
					error:function(msj){
          $("#msj").html(msj.responseJSON.errors.cnname);
          $("#msj-error").fadeIn();
          }
			});

});

// Funcio  que envia datos de la ventana modal y actualiza un registro
$("#btnEditPais").click(function(){

		var id = $("#id").val();
		var route = path+"pais/"+id;
		var cnname = $("#cnnamed").val();
		var token = $("#token").val();

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'PUT',
					dataType: 'json',
					data: { cnname: cnname },
					success: function(){
						$("#msj-successEd").fadeIn();
						carga();
					},
					error:function(msj){
					//console.log(msj);
					//console.log(msj.responseJSON.errors.qtname);
          $("#msjEd").html(msj.responseJSON.errors.cnname);
          $("#msj-errorEd").fadeIn();
          }
			});

});


// Vuelve a actualizar la pagina
function carga(){
	location.reload();
}
