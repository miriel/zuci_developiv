$("#descargaAll").click(function(){

  // Crea un array de los chcked
  var datos = [];
  $("input:checkbox:checked").each(function() {
    var arreglo = $(this).val();
    datos.push(arreglo);
  });

  // Enviar los parametros y descarga el archivo comprimido con las opciones seleccionadas
  window.location.href = path+"file/create?fileCheck="+datos;

});

$("#eliminaFilesM").click(function(){

  // Crea un array de los chcked
  var datos = [];
  $("input:checkbox:checked").each(function() {
    var arreglo = $(this).val();
    datos.push(arreglo);
  });

 var route = path+"file/1";
 var token = $("#token").val();
 var deleteAllFiles=1;

  $.ajax({
       url: route,
       headers: {'X-CSRF-TOKEN':token},
       type: 'DELETE',
       dataType: 'json',
       data: { datos:datos, deleteAllFiles:deleteAllFiles },
       success: function(){
         $("#msj-success").fadeIn();
         carga();
       },
       error:function(msj){
       //console.log(msj);
       //console.log(msj.responseJSON.errors.qtname);
       //$("#msj").html(msj.responseJSON.errors.qaqsfk);
       //$("#msj-error").fadeIn();
       }
   });

  // Enviar los parametros y descarga el archivo comprimido con las opciones seleccionadas


});


function deleteFile(id, status){

  var token = $("#token").val();
  var comentario = $("#cometarios_"+id).val();
  var calificacion = $("#calificacion_"+id).val();

  $("#msjcom_"+id).html("");
  $("#msj-errorcom_"+id).fadeOut();

  $("#msjcal_"+id).html("");
  $("#msj-errorcal_"+id).fadeOut();

  var countErr = 0;
  if(status == 3 || status == 5){
      if(comentario == ""){
          $("#msjcom_"+id).html("Falta cometario");
          $("#msj-errorcom_"+id).fadeIn();
          ++countErr;
      }
      if(calificacion == ""){
        $("#msjcal_"+id).html("Falta calificacion");
        $("#msj-errorcal_"+id).fadeIn();
        ++countErr;
      }

      // VAlida si no tiene los comentarios y califiacion asignada al archivo
      if(countErr>0){
        return;
      }
  }

  // Ruta
  var route = path+"file/"+id;
  var deleteAllFiles=0;

   $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN':token},
        type: 'DELETE',
        dataType: 'json',
        data: { status:status, deleteAllFiles:deleteAllFiles, comentario:comentario, calificacion:calificacion },
        success: function(){
          carga();
        },
        error:function(msj){}
    });
}

function nextFilter(id, status){
  var token = $("#token").val();

  var comentario = $("#cometarios_"+id).val();
  var calificacion = $("#calificacion_"+id).val();

  $("#msjcom_"+id).html("");
  $("#msj-errorcom_"+id).fadeOut();

  $("#msjcal_"+id).html("");
  $("#msj-errorcal_"+id).fadeOut();

  var countErr = 0;

  // En caso de que esten en el estado de revision de auditor y de reenvio a auditor validar que esten los comentarios y calificacion obligatorios
  if(status == 3 || status == 5){
      if(comentario == ""){
          $("#msjcom_"+id).html("Falta cometario");
          $("#msj-errorcom_"+id).fadeIn();
          ++countErr;
      }
      if(calificacion == ""){
        $("#msjcal_"+id).html("Falta calificacion");
        $("#msj-errorcal_"+id).fadeIn();
        ++countErr;
      }

      // VAlida si no tiene los comentarios y califiacion asignada al archivo
      if(countErr>0){
        return;
      }
  }

  // Ruta
  var route = path+"signiv1/"+id;

   $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN':token},
        type: 'GET',
        dataType: 'json',
        data: { status:status },
        success: function(){
          carga();
        },
        error:function(msj){}
    });

}


// Mdulo de Adminsitrador para cargar templates
$("#descargaAllTemplates").click(function(){

  // Crea un array de los chcked
  var datos = [];
  $("input:checkbox:checked").each(function() {
    var arreglo = $(this).val();
    datos.push(arreglo);
  });

  // Enviar los parametros y descarga el archivo comprimido con las opciones seleccionadas
  window.location.href = path+"fileTemplate/create?fileCheck="+datos;

});

// Mdulo de Adminsitrador para cargar templates por cuestionario
$("#descargaAllTemplatesCuest").click(function(){
<<<<<<< HEAD
=======

>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
  // Crea un array de los chcked
  var datos = [];
  $("input:checkbox:checked").each(function() {
    var arreglo = $(this).val();
    datos.push(arreglo);
  });
  // Enviar los parametros y descarga el archivo comprimido con las opciones seleccionadas
  window.location.href = path+"tempcuest/create?fileCheck="+datos;

});


//$("#fileCheckAll").change(function () {
//$(document).ready(function(){
$("#fileCheckAll").click(function(){
    var val;
    if ( $("input[name=fileCheckAll]").is(":checked") ) {
      $("input[name=fileCheck]").each(function (index) {
        val = $(this).val();
        if( val > 0){
          if($("#fileCheck_"+val).prop('checked')){
            $("#fileCheck_"+val).prop("checked",true);
          }else{
            $("#fileCheck_"+val).prop("checked",true);
          }
        }
      });
    }else{
      $("input[name=fileCheck]").each(function (index) {
        val = $(this).val();
        if( val > 0){
          if($("#fileCheck_"+val).prop('checked')){
              $("#fileCheck_"+val).prop("checked",false);
          }else{
              $("#fileCheck_"+val).prop("checked",false);
          }
        }
      });
    }
});

// Selecciona todos los checkbox del modulo archivos de administrador templates
$("#fileCheckAllTempl").change(function () {
      //$("input:checkbox").prop('checked', $(this).prop("checked"));
      var val;
      if ( $("input[name=fileCheckAllTempl]").is(":checked") ) {
        $("input[name=fileCheckTemplates]").each(function (index) {
          val = $(this).val();
          if( $(this).val() > 0){
            $("#fileCheckTemplates_"+val).attr("checked",true);
          }
          //console.log("checado"+"---"+val);
        });

      }else{
        $("input[name=fileCheckTemplates]").each(function (index) {
          val = $(this).val();
          if( $(this).val() > 0){
            $("#fileCheckTemplates_"+val).attr("checked",false);
          }
        });
        //console.log("no checado"+val);
      }
});


// Funcion que gaurda el comentario del archivo
function guardarComentFile(id){
  var route = path+"file/"+id;
  var token = $("#token").val();
<<<<<<< HEAD
  var comentario = $("#cometariosUser_"+id).val();
=======
  var comentario = $("#cometarios_"+id).val();
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
  var tipoUpdate = "comentario";

  $.ajax({
      url: route,
      headers: {'X-CSRF-TOKEN':token},
      type: 'PUT',
      dataType: 'json',
      data: {
        comentario:comentario, idFile:id, tipoUpdate:tipoUpdate
      }
  });
}

// Funcion que guarda el comentario del usuario
<<<<<<< HEAD
function guardarComentFileAudit(id){
  var route = path+"file/"+id;
  var token = $("#token").val();
  var comentarioUser = $("#cometariosAudit_"+id).val();
=======
function guardarComentFile(id){
  var route = path+"file/"+id;
  var token = $("#token").val();
  var comentarioUser = $("#cometariosUser_"+id).val();
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
  var tipoUpdate = "comentarioUser";

  $.ajax({
      url: route,
      headers: {'X-CSRF-TOKEN':token},
      type: 'PUT',
      dataType: 'json',
      data: {
        comentarioUser:comentarioUser, idFile:id, tipoUpdate:tipoUpdate
      }
  });
}

// Funcion que gaurda el comentario del archivo
function guardarCalificFile(id, valSel){
  var route = path+"file/"+id;
  var token = $("#token").val();
  var tipoUpdate = "calificacion";

  $.ajax({
      url: route,
      headers: {'X-CSRF-TOKEN':token},
      type: 'PUT',
      dataType: 'json',
      data: {
        calif:valSel, idFile:id, tipoUpdate:tipoUpdate
      }
  });
}

// Rechaza
$("#rechazaFiles").click(function(){

  // Crea un array de los chcked
  var datos = [];
  $("input:checkbox:checked").each(function() {
    var arreglo = $(this).val();
    datos.push(arreglo);
  });

  // Ruta
  var route = path+"file/"+id;
  var deleteAllFiles=1;

   $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN':token},
        type: 'DELETE',
        dataType: 'json',
        data: { status:status, deleteAllFiles:deleteAllFiles, comentario:comentario, calificacion:calificacion },
        success: function(){
          carga();
        },
        error:function(msj){}
    });
});


// Deshbilitar templete de los cuestionariosAl
function deleteFileTempCuest(id, status){

  var token = $("#token").val();

  // Ruta
  var route = path+"tempcuest/"+id;

   $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN':token},
        type: 'DELETE',
        dataType: 'json',
        data: { status:status },
        success: function(){
          carga();
        },
        error:function(msj){}
    });
}


// Activar templete de los cuestionarios templates
function activarFileTempCuest(id, status){

  var token = $("#token").val();

  // Ruta
  var route = path+"tempcuest/"+id;

   $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN':token},
        type: 'DELETE',
        dataType: 'json',
        data: { status:status },
        success: function(){
          carga();
        },
        error:function(msj){}
    });
}

// Inactiva templates de cuestinarios de forma masiva
$("#inactivaFlsTempCuestAll").click(function(){
  // Crea un array de los chcked
  var datos = [];
  $("input:checkbox:checked").each(function() {
    var arreglo = $(this).val();
    datos.push(arreglo);
  });

 var route = path+"tempcuest/1";
 var token = $("#token").val();
 var deleteAllFiles=1;

  $.ajax({
       url: route,
       headers: {'X-CSRF-TOKEN':token},
       type: 'DELETE',
       dataType: 'json',
       data: { datos:datos, deleteAllFiles:deleteAllFiles },
       success: function(){
         $("#msj-success").fadeIn();
         carga();
       },
       error:function(msj){
       }
   });
});


$("#fileCheckAllTempCuest").click(function(){
var val;
if ( $("input[name=fileCheckAllTempCuest]").is(":checked") ) {
  $("input[name=fileCheckTemplates]").each(function (index) {
    val = $(this).val();
    if( val > 0){
      if( $("#fileCheckTemplates_"+val).prop('checked') ){
        $("#fileCheckTemplates_"+val).prop('checked',true);
      }else{
        $("#fileCheckTemplates_"+val).prop('checked',true);
      }
    }
    //console.log("checado"+"---"+val);
  });

}else{
  $("input[name=fileCheckTemplates]").each(function (index) {
    val = $(this).val();
    if( val > 0){
      if( $("#fileCheckTemplates_"+val).prop('checked') ){
        $("#fileCheckTemplates_"+val).prop("checked",false);
      }else{
        $("#fileCheckTemplates_"+val).prop("checked",false);
      }
    }
  });
  //console.log("no checado"+val);
}
});


// Selecciona todos los selects para el aministrador de templates por carga de archivos por pregunta->respuesta
$("#fileCheckAllTempXResp").click(function(){
var val;
if ( $("input[name=fileCheckAllTempXResp]").is(":checked") ) {
  $("input[name=fileCheckTemplates]").each(function (index) {
    val = $(this).val();
    if( val > 0){
      //$("#fileCheckTemplates_"+val).attr("checked",true);
      if( $("#fileCheckTemplates_"+val).prop('checked') ) {
        $("#fileCheckTemplates_"+val).prop("checked",true);
      }else{
        $("#fileCheckTemplates_"+val).prop("checked",true);
      }
    }
    //console.log("checado"+"---"+val);
  });

}else{
  $("input[name=fileCheckTemplates]").each(function (index) {
    val = $(this).val();
    if( val > 0){
      if( $("#fileCheckTemplates_"+val).prop('checked') ) {
          $("#fileCheckTemplates_"+val).prop("checked",false);
      }else{
        //console.log("se checa en automtico cuando esta deshabliitado"+val);
        $("#fileCheckTemplates_"+val).prop("checked",false);
      }

    }
  });
  //console.log("no checado"+val);
}
});


// Deshbilitar templete de los cuestionarios por pregunta
function deleteFileTempXPreg(id, status){

  var token = $("#token").val();

  // Ruta
  var route = path+"fileTemplate/"+id;

   $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN':token},
        type: 'DELETE',
        dataType: 'json',
        data: { status:status },
        success: function(){
          carga();
        },
        error:function(msj){}
    });
}


// Inactiva templates de cuestinarios por pregunta de forma masiva
$("#inactivaFlsTempCuestXPregAll").click(function(){
  // Crea un array de los chcked
  var datos = [];
  $("input:checkbox:checked").each(function() {
    var arreglo = $(this).val();
    datos.push(arreglo);
  });

 var route = path+"fileTemplate/1";
 var token = $("#token").val();
 var deleteAllFiles=1;

  $.ajax({
       url: route,
       headers: {'X-CSRF-TOKEN':token},
       type: 'DELETE',
       dataType: 'json',
       data: { datos:datos, deleteAllFiles:deleteAllFiles },
       success: function(){
         $("#msj-success").fadeIn();
         carga();
       },
       error:function(msj){
       }
   });
});


$("#filesCheckAllCuest").click(function(){
var val;
if ( $("input[name=filesCheckAllCuest]").is(":checked") ) {
  $("input[name=fileCheckCuest]").each(function (index) {
    val = $(this).val();
    if( val > 0){
      if( $("#fileCheckCuest_"+val).prop('checked') ){
        $("#fileCheckCuest_"+val).prop('checked',true);
      }else{
        $("#fileCheckCuest_"+val).prop('checked',true);
      }
    }
    //console.log("checado"+"---"+val);
  });

}else{
  $("input[name=fileCheckCuest]").each(function (index) {
    val = $(this).val();
    if( val > 0){
      if( $("#fileCheckCuest_"+val).prop('checked') ){
        $("#fileCheckCuest_"+val).prop("checked",false);
      }else{
        $("#fileCheckCuest_"+val).prop("checked",false);
      }
    }
  });
  //console.log("no checado"+val);
}
});


$("#eliminaFilesCuest").click(function(){

  // Crea un array de los chcked
  var datos = [];
  $("input:checkbox:checked").each(function() {
    var arreglo = $(this).val();
    datos.push(arreglo);
  });

 var route = path+"filecuest/1";
 var token = $("#token").val();
 var deleteAllFiles=1;

  $.ajax({
       url: route,
       headers: {'X-CSRF-TOKEN':token},
       type: 'DELETE',
       dataType: 'json',
       data: { datos:datos, deleteAllFiles:deleteAllFiles },
       success: function(){
         $("#msj-success").fadeIn();
         carga();
       },
       error:function(msj){
       }
   });
   });


// Borra o deshabilita archivos archivos de cuestionarios
   function deleteFileCuest(id, status){

     var token = $("#token").val();
     var comentario = $("#cometarios_"+id).val();
     var calificacion = $("#calificacion_"+id).val();

     $("#msjcom_"+id).html("");
     $("#msj-errorcom_"+id).fadeOut();

     $("#msjcal_"+id).html("");
     $("#msj-errorcal_"+id).fadeOut();

     var countErr = 0;
     if(status == 3 || status == 5){
         if(comentario == ""){
             $("#msjcom_"+id).html("Falta cometario");
             $("#msj-errorcom_"+id).fadeIn();
             ++countErr;
         }
         if(calificacion == ""){
           $("#msjcal_"+id).html("Falta calificacion");
           $("#msj-errorcal_"+id).fadeIn();
           ++countErr;
         }

         // VAlida si no tiene los comentarios y califiacion asignada al archivo
         if(countErr>0){
           return;
         }
     }

     // Ruta
     var route = path+"filecuest/"+id;
     var deleteAllFiles=0;

      $.ajax({
           url: route,
           headers: {'X-CSRF-TOKEN':token},
           type: 'DELETE',
           dataType: 'json',
           data: { status:status, deleteAllFiles:deleteAllFiles, comentario:comentario, calificacion:calificacion },
           success: function(){
             carga();
           },
           error:function(msj){}
       });
   }


// Descarga todos los archivos seleccionados para el modulo de carga de archivos por cuestionarios
   $("#descargaAllFilsCuest").click(function(){

     // Crea un array de los chcked
     var datos = [];
     $("input:checkbox:checked").each(function() {
       var arreglo = $(this).val();
       datos.push(arreglo);
     });

     // Enviar los parametros y descarga el archivo comprimido con las opciones seleccionadas
     window.location.href = path+"filecuest/create?fileCheck="+datos;

   });


// Nivel siguiente para los archivos de cada cuestionario
function nextFilterCuest(id, status){
  var token = $("#token").val();

  var comentario = $("#cometarios_"+id).val();
  var calificacion = $("#calificacion_"+id).val();

  $("#msjcom_"+id).html("");
  $("#msj-errorcom_"+id).fadeOut();

  $("#msjcal_"+id).html("");
  $("#msj-errorcal_"+id).fadeOut();

  var countErr = 0;

  // En caso de que esten en el estado de revision de auditor y de reenvio a auditor validar que esten los comentarios y calificacion obligatorios
  if(status == 3 || status == 5){
      if(comentario == ""){
          $("#msjcom_"+id).html("Falta cometario");
          $("#msj-errorcom_"+id).fadeIn();
          ++countErr;
      }
      if(calificacion == ""){
        $("#msjcal_"+id).html("Falta calificacion");
        $("#msj-errorcal_"+id).fadeIn();
        ++countErr;
      }

      // VAlida si no tiene los comentarios y califiacion asignada al archivo
      if(countErr>0){
        return;
      }
  }

  // Ruta
  var route = path+"signiv1Cuest/"+id;

   $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN':token},
        type: 'GET',
        dataType: 'json',
        data: { status:status },
        success: function(){
          carga();
        },
        error:function(msj){}
    });

}

var cajas = {'.limiteAuditor':'.comentarioAuditor','.limiteUser':'.comentarioUser'};

$.each(cajas, function(key, value){

  $(key).each(function(){
    var comentarioAuditor = 0;
    totalInicial = $(this).find(value).val().length;
    var totalRestante = (100 - totalInicial);
    var elemento = $(this).find(value);
    $('#'+elemento.attr('rel')).text(totalRestante);
  });

  $(value).keyup(function(){
    var numPalabras = $(this).val().length;
    var cantidadActual = (100 - numPalabras);
    var id = $(this).attr('rel');
    $('#'+id).text(cantidadActual);
  });

});
