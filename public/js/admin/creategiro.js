// Crea un cuestionario desde la ventana modal al editar el giro
$("#btnCreateGiro").click(function(){

		var route = path+"giro";
		var cotname = $("#cotname").val();
		var token = $("#token").val();

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'POST',
					dataType: 'json',
					data: { cotname:cotname },
					success: function(){
						$("#msj-success").fadeIn();
						carga();
					},
					error:function(msj){
						// console.log(msj);
          $("#msj").html(msj.responseJSON.errors.cotname);
          $("#msj-error").fadeIn();
          }
			});
});



// Funcio  que envia datos de la ventana modal y actualiza un registro
$("#btnEditGiro").click(function(){
	var id = $("#cotfk").val();
	var route = path+"giro/"+id;
	var cotname = $("#cotnamed").val();
	var token = $("#token").val();

	 $.ajax({
			url: route,
			headers: {'X-CSRF-TOKEN':token},
			type: 'PUT',
			dataType: 'json',
			data: { cotname: cotname},
			success: function(){
				$("#msj-successEd").fadeIn();
				carga();
			},
			error:function(msj){
				$("#msjEd").html(msj.responseJSON.errors.cotname);
				$("#msj-errorEd").fadeIn();
			}
	});

});


// Vuelve a actualizar la pagina
function carga(){
	location.reload();
}
