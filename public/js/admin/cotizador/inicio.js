$(document).ready(function(){
  // Deshabilita boton para agregar respuetas al cargar la pagina
  $("#modal").attr("disabled", true);
  $("#borrar").attr("disabled", true);
  $("#guardar").css("display", "none");
  $(".userc").attr("disabled", true);
  // $("#disabled2").css("display", "none");
  // $("#disabled3").css("display", "none");
});

function ocultar(){
  // $("#ocultar").attr("disabled", true);
  document.getElementById("cmmoneda").disabled = true;
}

 function mostar(id){
   $.get("div/"+id+"",function(id){
       $("#cmmoneda").val(id[0].cmmax);
         $("#modal").attr("disabled", false);
         // funcion para cambiar los precios dependieindo del tipo de mondena para usuarios
         var cantidad = $(".userc").val();
         var p = $(".userp").val();
         var moneda =  $("#cmmoneda").val();
         var mon = p / moneda;
         var r = mon.toFixed(2);
         // multiplica la cantidad * la cantidad puesta
         $(".usercon").val(r);
         var tot = cantidad * r ;
         // redonde el resultado a dos decimales
         var n = tot.toFixed(2);
          var total1 = $(".usertotal").val(n);
         // cambiar precios dependiendo de tipo de moneda para $cuestionarios
         var c1 = $(".primariosc").val();
         var p1 = $(".primariosp").val();
         var mon1 = p1 / moneda;
         var r1 = mon1.toFixed(2);
         // multiplica la cantidad * la cantidad puesta
         $(".primarioscon").val(r1);
         var tot1 = c1 * r1 ;
         // redonde el resultado a dos decimales
         var n1 = tot1.toFixed(2);
         var total2 = $(".primariostotal").val(n1);
          // cambiar precios dependiendo de tipo de moneda para cuestionarios secundarios
          // var c2 = $("#secundariosc").val();
          // var p2 = $("#secundariosp").val();
          // var mon2 = p2 / moneda;
          // var r2 = mon2.toFixed(2);
          // // multiplica la cantidad * la cantidad puesta
          // $("#secundarioscon").val(r2);
          // var tot2 = c2 * r2 ;
          // // redonde el resultado a dos decimales
          // var n2 = tot2.toFixed(2);
          // var total3 = $("#secundariostotal").val(n2);
          var suma = parseFloat(n) + parseFloat(n1);
          var l = suma.toFixed(2);
          // console.log(suma);
          var sub = $("#subtotal").val(l);
          var iva = parseFloat(l) * 16 / 100 ;
          // redondea el resultado del iva
          var re = iva.toFixed(2);
          $("#iva").val(re);
          var suma = parseFloat(l) + parseFloat(re);
          var su = suma.toFixed(2);
          $("#total").val(su);
          $("#audittotal").val(su);
   });


}
