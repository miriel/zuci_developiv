$(document).ready(function(e){

  // Tabs redireccionamiento
  var status = $("#status").val();
  // Redirecciona y muestra pestaña de tipo de cuestionario depende del estatus que se encuentre el registro
  if(status == 1){
    $('.nav-tabs a[href="#altas"]').tab('show')
  }
  if(status == 0){
    $('.nav-tabs a[href="#bajas"]').tab('show')
  }

    // Carga el ID del tipo de cuestionario en el modal de acuerdo al seleccionado con respecto al modal de Boostrap
    $('#editConfigCuest').on('show.bs.modal', function(e){
      // Obtiene el id de la lista de registros en el index
      var id = $(e.relatedTarget).data().id;

      // Ruta que trae el registro de la BD para llenar las cajas de texto en el modal
      var route = path+"configcuestionario/"+id+"/edit";

      // Llena el ID del registro de la lista del index en la ventana modal
      $(e.currentTarget).find('#idConfCuest').val(id);

      // Obtiene y llena las cajas de la ventana modal con los valores que corresponden al ID del registro
    	$.get(route, function(res){
          //console.log(res[0].cqeverybody);
          $(e.currentTarget).find('#typecompanyEd').val(res[0].tipoCompany);
          $(e.currentTarget).find('#questionnaireEd').val(res[0].IdCuestionario);
          $(e.currentTarget).find('#everybodyEd').val(res[0].cqeverybody);
    	});

    });
});


// Funcion  que envia datos de la ventana modal y crea un nuevo registro configuracion de cueationario
$("#btnCreaConfigCuest").click(function(){

		var route = path+"configcuestionario";
		var typecompany = $("#typecompanyCr").val();
    var questionnaire = $("#questionnaireCr").val();
    var everybody = $("#everybodyCr").val();
		var token = $("#token").val();

    $("#msjCr1").html("");
    $("#msj-errorCr1").fadeOut();
    $("#msjCr2").html("");
    $("#msj-errorCr2").fadeOut();

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'POST',
					dataType: 'json',
					data: { cqctfk:typecompany, cqqufk:questionnaire,everybody:everybody },
					success: function(){
						$("#msj-successCr").fadeIn();
						carga();
					},
					error:function(msj){
            //console.log(msj);
            $("#msjCr1").html(msj.responseJSON.errors.cqctfk);
            $("#msj-errorCr1").fadeIn();
            $("#msjCr2").html(msj.responseJSON.errors.cqqufk);
            $("#msj-errorCr2").fadeIn();
          }
			});

});


// Funcio  que envia datos de la ventana modal y actualiza un registro
$("#btnUpdtConfCuest").click(function(){

		var id = $("#idConfCuest").val();
		var route = path+"configcuestionario/"+id;
    var typecompany = $("#typecompanyEd").val();
    var questionnaire = $("#questionnaireEd").val();
    var everybody = $("#everybodyEd").val();
		var token = $("#token").val();

    $("#msjEd1").html("");
    $("#msj-errorEd1").fadeOut();
    $("#msjEd2").html("");
    $("#msj-errorEd2").fadeOut();

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'PUT',
					dataType: 'json',
					data: { cqctfk:typecompany, cqqufk:questionnaire,everybody:everybody },
					success: function(){
						$("#msj-successEd").fadeIn();
						carga();
					},
					error:function(msj){
					//console.log(msj);
					//console.log(msj.responseJSON.errors.qtname);
            $("#msjEd1").html(msj.responseJSON.errors.cqctfk);
            $("#msj-errorEd1").fadeIn();
            $("#msjEd2").html(msj.responseJSON.errors.cqqufk);
            $("#msj-errorEd2").fadeIn();
          }
			});

});
