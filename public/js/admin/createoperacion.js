
// Crea un cuestionario desde la ventana modal al editar el empresa
$("#btnCreateOperacion").click(function(){

		var route = path+"operacion";
    var qtfk= $("#qtfk").val();
		var cmdesc = $("#cmdescCre").val();
		var token = $("#token").val();


		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'POST',
					dataType: 'json',
					data: { cmdesc:cmdesc ,qtfk:qtfk},
					success: function(){
						$("#msj-success").fadeIn();
						carga();
					},
					error:function(msj){
           $("#msjCre").html(msj.responseJSON.errors.cmdesc);
           $("#msj-errorCre").fadeIn();
          }
			});

});



// Funcion  que envia datos de la ventana modal y actualiza un registro
$("#EditOperacion").click(function(){
	var id = $("#cmfkEd").val();
	var route = path+"operacion/"+id;
	var cmdesc = $("#cmdescEd").val();
	var token = $("#token").val();


	 $.ajax({
			url: route,
			headers: {'X-CSRF-TOKEN':token},
			type: 'PUT',
			dataType: 'json',
			data: { cmdesc:cmdesc },
			success: function(){
				$("#msj-successEd").fadeIn();
				carga();
			},error:function(msj){
         console.log(msj);
 				$("#msjEd").html(msj.responseJSON.errors.cmdesc);
 				$("#msj-errorEd").fadeIn();

			}
	});

});


// Vuelve a actualizar la pagina
function carga(){
	location.reload();
}
