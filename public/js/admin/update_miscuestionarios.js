/// ################################# FUNCIONES PARA CUESTIONARIOS SIN SECCIONES #################################3
function guardaRespuesta(id, idPregunta,noPregunta){
   var route = path+"miscuestionarios/"+id;
   var value = $("#value_"+id).val();
   var token = $("#token").val();
   var totPreguntas = $("#totPreguntas").val();
   var idCuestion = $("#idCuestion").val();
   var secciones = "0"; // cuestionario sin secciones
   //alert(" idcuestion" + idCuestion + " idPregunta  " + idPregunta);

  $.ajax({
       url: route,
       headers: {'X-CSRF-TOKEN':token},
       type: 'PUT',
       dataType: 'json',
       data: { value: value, totPreguntas:totPreguntas, idCuestion:idCuestion, idPregunta:idPregunta, secciones:secciones,noPregunta:noPregunta }
   });

}

function guardaRespuestaRadio(id, valor, idPregunta, idCuestion,noPregunta){
   var route = path+"miscuestionarios/"+id;
   var value = $("#value_"+id).val();
   var token = $("#token").val();
   var tipoRespuesta = "4"; // Tipo de respesta radiobuton  se encuentran los tipo den la tabla answers y type_answers
   var totPreguntas = $("#totPreguntas").val();
   var secciones = "0"; // cuestionario sin secciones
   //alert(" idcuestion" + idCuestion + " idPregunta  " + idPregunta);

   $.ajax({
       url: route,
       headers: {'X-CSRF-TOKEN':token},
       type: 'PUT',
       dataType: 'json',
       data: { value: valor, tipoRespuesta: tipoRespuesta, idPregunta: idPregunta, totPreguntas:totPreguntas, idCuestion:idCuestion, secciones:secciones,
                noPregunta:noPregunta}
   });

}

function guardaRespuestaCheck(id, valor, idPregunta, idCuestion,noPregunta){
   var route = path+"miscuestionarios/"+id;
   var value = $("#value_"+id).val();
   var token = $("#token").val();
   var tipoRespuesta = "6"; // Tipo de respesta radiobuton  se encuentran los tipo den la tabla answers y type_answers
   var totPreguntas = $("#totPreguntas").val();
   var secciones = "0"; // cuestionario sin secciones
  // alert(" idcuestion" + idCuestion + " idPregunta  " + idPregunta);

   $.ajax({
       url: route,
       headers: {'X-CSRF-TOKEN':token},
       type: 'PUT',
       dataType: 'json',
       data: { value: valor, tipoRespuesta: tipoRespuesta, idPregunta: idPregunta, totPreguntas:totPreguntas, idCuestion:idCuestion, secciones:secciones,
               noPregunta:noPregunta }
   });

}

/// ################################# FUNCIONES PARA CUESTIONARIOS CON SECCIONES #################################3


function guardaRespuestaRadioCS(id, valor, idPregunta, idCuestion, idSeccion, noPregunta,idSubseccion){

   var route = path+"miscuestionarios/"+id;
   //var value = $("#value_"+id).val();
   var token = $("#token").val();
   var tipoRespuesta = "4"; // Tipo de respesta radiobuton  se encuentran los tipo den la tabla answers y type_answers
   var totPreguntas = $("#totPreguntas").val();
   var secciones = "1"; // cuestionario con secciones
   //alert(" idcuestion" + idCuestion + " idPregunta  " + idPregunta);

   $.ajax({
       url: route,
       headers: {'X-CSRF-TOKEN':token},
       type: 'PUT',
       dataType: 'json',
       data: { value: valor, tipoRespuesta: tipoRespuesta, idPregunta: idPregunta, totPreguntas:totPreguntas, idCuestion:idCuestion, secciones:secciones,
               idSeccion:idSeccion, noPregunta:noPregunta,idSubseccion:idSubseccion}
   });

}


function guardarRespuestaSelectCS(id, valor, idPregunta, idCuestion, noPregunta, idSubseccion){

  var route = path+"miscuestionarios/"+id;
  //var value = $("#value_"+id).val();
  var token = $("#token").val();
  var tipoRespuesta = "3"; // Tipo de respesta radiobuton  se encuentran los tipo den la tabla answers y type_answers
  var totPreguntas = $("#totPreguntas").val();
  var secciones = "1"; // cuestionario con secciones
  //alert(" idcuestion" + idCuestion + " idPregunta  " + idPregunta);

  $.ajax({
      url: route,
      headers: {'X-CSRF-TOKEN':token},
      type: 'PUT',
      dataType: 'json',
      data: { value: valor, tipoRespuesta: tipoRespuesta, idPregunta: idPregunta, totPreguntas:totPreguntas, idCuestion:idCuestion,
              secciones:secciones, noPregunta:noPregunta, idSubseccion:idSubseccion }
  });
}

function guardarRespuestaSelectCSAudit(id, valor, idPregunta, idCuestion, noPregunta, idSubseccion){

  var route = path+"miscuestionarios/"+id;
  //var value = $("#value_"+id).val();
  var token = $("#token").val();
  var tipoRespuesta = "3"; // Tipo de respesta radiobuton  se encuentran los tipo den la tabla answers y type_answers
  var totPreguntas = $("#totPreguntas").val();
  var secciones = "1"; // cuestionario con secciones
  //alert(" idcuestion" + idCuestion + " idPregunta  " + idPregunta);

  $.ajax({
      url: route,
      headers: {'X-CSRF-TOKEN':token},
      type: 'PUT',
      dataType: 'json',
      data: { value: valor, tipoRespuesta: tipoRespuesta, idPregunta: idPregunta, totPreguntas:totPreguntas, idCuestion:idCuestion,
              secciones:secciones, noPregunta:noPregunta, idSubseccion:idSubseccion }
  });
}

function guardarRespuestaTextareaCS(id, valor, idPregunta, idCuestion, noPregunta, idSubseccion){

  var route = path+"miscuestionarios/"+id;
  var token = $("#token").val();
  var tipoRespuesta = "7"; // Tipo de respesta radiobuton  se encuentran los tipo den la tabla answers y type_answers
  var totPreguntas = $("#totPreguntas").val();
  var secciones = "1"; // cuestionario con secciones

  $.ajax({
      url: route,
      headers: {'X-CSRF-TOKEN':token},
      type: 'PUT',
      dataType: 'json',
      data: {
        value: valor, tipoRespuesta: tipoRespuesta, idPregunta: idPregunta,
        totPreguntas:totPreguntas, idCuestion:idCuestion, secciones:secciones, noPregunta:noPregunta,
        idSubseccion:idSubseccion
      }
  });
}

function guardarRespuestaTextCS(id, valor, idPregunta, idCuestion, noPregunta){

  var route = path+"miscuestionarios/"+id;
  var token = $("#token").val();
  var tipoRespuesta = "7"; // Tipo de respesta radiobuton  se encuentran los tipo den la tabla answers y type_answers
  var totPreguntas = $("#totPreguntas").val();
  var secciones = "1"; // cuestionario con secciones

  $.ajax({
      url: route,
      headers: {'X-CSRF-TOKEN':token},
      type: 'PUT',
      dataType: 'json',
      data: {
        value: valor, tipoRespuesta: tipoRespuesta, idPregunta: idPregunta,
        totPreguntas:totPreguntas, idCuestion:idCuestion, noPregunta:noPregunta,
        noPregunta:noPregunta
      }
  });
}
