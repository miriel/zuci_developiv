// Crea un cuestionario desde la ventana modal al editar el cuestionario
$("#btnCreCuest").click(function(){

		var route = path+"cuestionario";
		var quname = $("#quname").val();
		var atqqtfk = $("#atqqtfk").val();
		var token = $("#token").val();

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'POST',
					dataType: 'json',
					data: { quname: quname, atqqtfk:atqqtfk },
					success: function(){
						$("#msj-success").fadeIn();
						carga();
					},
					error:function(msj){
          $("#msj").html(msj.responseJSON.errors.quname);
          $("#msj-error").fadeIn();
          }
			});

});

// Vuelve a actualizar la pagina
function carga(){
	location.reload();
}

// Selecciona el tipo de respuesta y determina si acepta insetar el valor de la respuesta
$(document).ready(function(){	

	// Setea los contenedores de errores al cargar la entana modal
	$("#createCuest").click(function(){
		 $("#msj").html("");
		 $("#msj-error").css("display","none");
	});


});
