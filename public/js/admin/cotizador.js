$(document).ready(function(e){
  var status = $("#status").val();
    // Carga el ID del tipo de cuestionario en el modal de acuerdo al seleccionado con respecto al modal de Boostrap
    $('#editRole').on('show.bs.modal', function(e){
      // Obtiene el id de la lista de registros en el index
      var id = $(e.relatedTarget).data().id;
      // console.log(id);
      // Ruta que trae el registro de la BD para llenar las cajas de texto en el modal
      var route = path+"admincotizador/"+id+"/edit";
      // funcion para separar las variables
      var idR = id.split(",");
      // Llena el ID del registro de la lista del index en la ventana modal con el id separado
      $(e.currentTarget).find('#id').val(idR[0]);
      // Obtiene y llena las cajas de la ventana modal con los valores que corresponden al ID del registro
      // condicion si tiene una variable R llena los campos de rol
        if(idR[1] == 'R'){
          $.get(route, function(res){
            $(e.currentTarget).find('#rol').val(idR[1]);
            $(e.currentTarget).find('#name').val(res.name);
            $(e.currentTarget).find('#cost').val(res.cost);
          });
        }
        // conndicion si tiene una P es cuestionario y llena los campos 
        if(idR[1] == 'P'){
          $.get(route, function(res){
            $(e.currentTarget).find('#rol').val(idR[1]);
            $(e.currentTarget).find('#name').val(res.quname);
            $(e.currentTarget).find('#cost').val(res.qucost);
          });
        }

    });





    // Redirecciona y muestra pestaña de tipo de cuestionario depende del estatus que se encuentre el registro
    if(status == 1){
      $('.nav-tabs a[href="#altas"]').tab('show')
    }
    if(status == 0){
      $('.nav-tabs a[href="#bajas"]').tab('show')
    }


});




// Funcio  que envia datos de la ventana modal y actualiza un registro
$("#btnEditRole").click(function(){
	var id = $("#id").val();
  var letra = $("#rol").val();
	var route = path+"admincotizador/"+id;
	var cost = $("#cost").val();
	var token = $("#token").val();
   // console.log(cost + id + letra);
	 $.ajax({
			url: route,
			headers: {'X-CSRF-TOKEN':token},
			type: 'PUT',
			dataType: 'json',
			data: { cost:cost , id:id ,letra:letra},
			success: function(){
				$("#msj-successEd").fadeIn();
				carga();
			},
			error:function(msj){
				$("#msjEd").html(msj.responseJSON.errors.cost);
				$("#msj-errorEd").fadeIn();
			}
	});

});


// Vuelve a actualizar la pagina
function carga(){
	location.reload();
}
