$(document).ready(function(e){
  var status = $("#status").val();

    // Carga el ID del tipo de cuestionario en el modal de acuerdo al seleccionado con respecto al modal de Boostrap
    $('#editOperacion').on('show.bs.modal', function(e){
      // Obtiene el id de la lista de registros en el index
      var id = $(e.relatedTarget).data().id;

      // Ruta que trae el registro de la BD para llenar las cajas de texto en el modal
      var route = path+"operacion/"+id+"/edit";
      // alert(route);

      // Llena el ID del registro de la lista del index en la ventana modal
      $(e.currentTarget).find('#cmfkEd').val(id);

      // Obtiene y llena las cajas de la ventana modal con los valores que corresponden al ID del registro
    	$.get(route, function(res){
          $(e.currentTarget).find('#cmdescEd').val(res.cmdesc);
    	});
    });





    // Redirecciona y muestra pestaña de tipo de cuestionario depende del estatus que se encuentre el registro
    if(status == 1){
      $('.nav-tabs a[href="#altas"]').tab('show')
    }
    if(status == 0){
      $('.nav-tabs a[href="#bajas"]').tab('show')
    }


});
