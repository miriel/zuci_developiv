
$(document).ready(function(){

		// Carga de cuestionarios de acuerdo al tipo de cuestionario seleccionado y configurado
		$("#qtname").change(function(event){
			var route = path+"tipocuest/"+event.target.value;
			var token = $("#token").val();
			$.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'GET',
					dataType: 'json',
					data: {  },
					success: function(res){
							$("#quname").empty();
							if(res.length == 0){
									$("#quname").append("<option  value=''>Ninguno asignado</option>");
							}else{
									for(i=0; i< res.length; i++){
										$("#quname").append("<option  value='"+res[i].idCues+"'>"+res[i].nameCuest+"</option>");
									}
							}

					}
			});
		});


		//######################## Carga el ID de la pregunta en el modal de acuerdo al seleccionado con respecto al modal de Boostrap ############
    $('#editPregnts').on('show.bs.modal', function(e){

      // Obtiene el id de la lista de registros en el index
      var id = $(e.relatedTarget).data().id;
			var dato = id.split("|");
			var idPregResp = dato[0]; // Id de pregunta y respuesta
			var idPregunta = dato[1]; // ID de pegunta
			var idSeccion = dato[2]; // ID seccion
			var idSubseccion = dato[3]; // ID subseccion

      // Ruta que trae el registro de la BD para llenar las cajas de texto en el modal
      var route = path+"pregunta/"+idPregunta+"/edit";

      // Llena el ID del registro de la lista del index en la ventana modal
      $(e.currentTarget).find('#idPregCst').val(idPregunta);
			$(e.currentTarget).find('#idPregRespCst').val(idPregResp);


      // Obtiene y llena las cajas de la ventana modal con los valores que corresponden al ID del registro
    	$.get(route, function(res){
         	$(e.currentTarget).find('#qsname').val(res.qsfk);
    	});


				// Id del tipo de la tabla  (answer_type_questionnaires)
				var idTipoCuestionario = $("#idTipoCuestionario").val();
				var route = path+"asingapregunta/"+idTipoCuestionario+"/edit";

				$.get(route, function(res){
						$("#qtnameRespCst").val(res[0].idTipoCuest);
						$("#qunameRespCst").val(res[0].idCuest);
						$("#idSeccionRespCst").val(idSeccion);
						$("#idSubseccionRespCst").val(idSubseccion);
				});

    });

		//######################## Carga el ID de las respuestas en el modal de acuerdo al seleccionado con respecto al modal de Boostrap ############
    $('#editRespsts').on('show.bs.modal', function(e){
			// Carga esta funcion al cargar la paginate
			// Determina el valor de ponderacion de acuerdo al tipo de respuesta que seleccionan

      // Obtiene el id de la lista de registros en el index
      var id = $(e.relatedTarget).data().id;
			var dato = id.split("|");
			var idPregResp = dato[0]; // Id de pregunta y respuesta
			var idResp = dato[1]; // ID de respuesta
			var idPregunta = dato[2]; // ID de respuesta

      // Ruta que trae el registro de la BD para llenar las cajas de texto en el modal
      var route = path+"respuesta/"+idPregResp+"/edit";

      // Llena el ID del registro de la lista del index en la ventana modal
      $('#idPregRespAspRs').val(idPregResp);
			$('#idRespAspRs').val(idResp);
			$('#idPregAspRs').val(idPregunta);
			//alert(idPregResp+" - "+ idResp+" - "+idPregunta);
      // Obtiene y llena las cajas de la ventana modal con los valores que corresponden al ID del registro
    	$.get(route, function(res){

					console.log(res);
				 	$(e.currentTarget).find('#annameUpdRespPre').val(res[0].idRespuesta);
					$(e.currentTarget).find('#anatfkUpdRespPre').val(res[0].nameResp);
					$(e.currentTarget).find('#anvalueUpdRespPre').val(res[0].anvalue);
					$(e.currentTarget).find('#statusUpdRespPre').val(res[0].status);

					var anatfk = $("#anatfk").val();  // Obtiene el valor del tipo de respuesta
					// Bloquea las cajas (valor de ponderacion) de acuerdo al tipo de respuesta seleccionada
					muestraValores(anatfk,'edit');

    	});
    });

		/////############# Valida el valor de ponderacion de acuerdo al tipo de respuesta que seleccionan ##############
		$("#anatfk").change(function(event){
	    var idTipoResp = event.target.value;
	    muestraValores(idTipoResp);
		});

});

//############################## INICIO DE FUNCIONES PARA AGREGAR RESPUESTAS Y PREGUNTAS (Modulo: asignar preguntas y respuestas ) ####################################3
$(document).ready(function(){
		// Deshabilita boton para agregar respuetas al cargar la pagina
		$("#addResp").attr("disabled", true);
});

// Agrega la pregunta seleccionada
$("#addPreg").click(function(event){
		var valPreg = $("#qsnameAdPrRsp").val();  // ID de Pregunta
		var contPreg = $("#contPreg").val(); // Contador de preguntas
		$("#msj-error5").fadeOut(); // Oculta el mensaje de error
		if(valPreg == ""){  // Valida el error si es igual a nulo
				$("#msj-error5").fadeIn();
				$("#msj5").html("Debe seleccionar una pregunta");
				return;
		}
		var idPregunta = $('select[name=qsnameAdPrRsp]').val(); // Obtiene el nombre de la pregunta del select
		var valorPregunta =  $("#qsnameAdPrRsp option:selected").text(); // El valor de texto de la pregunta
 		var pregHidd = "<th>"+
											 "<input type='hidden' id='preguntaHidden' name='preguntaHidden' value='"+idPregunta+"'>"+valorPregunta+" "+
										"</th>"+
								    "<th>"+
										"<input value='Eliminar' type='button'  onclick='javascript:eliminarPregunta("+idPregunta+")' id='btnDeletPreg-"+idPregunta+"'  class='btn btn-danger'>"+
										"<th>";
		nuevaPregunta(pregHidd, idPregunta);
		$("#addResp").attr("disabled", false);
		$("#addPreg").attr("disabled", true);
		$("#contPreg").val(++contPreg);

});

// Agrega la respuesta
  $("#addResp").click(function(event){
			var valResp = $("#annameAdPrRsp").val();
			var contResp = $("#contResp").val();

			// Valida que se halla agregado una respuesta
			$("#msj-error6").fadeOut();
			if(valResp == ""){
				$("#msj-error6").fadeIn();
				$("#msj6").html("Debe seleccionar una respuesta");
				return;
			}

			// Obtiene todas las respuestas
			var datos = [];
			var countVal=0;
		  $(".respuestaHidden").each(function() {
		      //var arreglo = $(this).val();
		      if( $(this).val()  ==  valResp){
						++countVal;
					}
		  });

			if(countVal > 0){
				$("#msj-error6").fadeIn();
				$("#msj6").html("Respuesta repetida");
				return;
			}
			// Valida que no se halla repetido una respuesta para la pregunta

			var idResp = $('select[name=annameAdPrRsp]').val();
			var valorRespuesta =  $("#annameAdPrRsp option:selected").text();
			var respHidd = "<td>"+
												 "<input type='hidden' id='respuestaHidden[]' name='respuestaHidden[]' class='respuestaHidden' value='"+idResp+"'>"+valorRespuesta+" "+
											"</td>"+
									    "<td>"+
											"<input value='Eliminar' type='button' onclick='javascript:eliminarRespuesta("+idResp+")' id='btnDeletResp-"+idResp+"' class='btn btn-danger'>"+
											"<td>";
			nuevaRespuesta(respHidd, idResp);
		  $("#contResp").val(++contResp);
	});


function eliminarPregunta(idPregunta){
		// Variable asginada para el valor del contador
		var resConR = "";
		var countPreg = $("#contPreg").val();
		if(countPreg == 1){
			 resConR = "";
			 	$("#addPreg").attr("disabled", false);
				$("#addResp").attr("disabled", true);
		}else{
			resConR = --countPreg;
		}
		$("#thead-"+idPregunta).remove();
		$("#contPreg").val(resConR);
}

function eliminarRespuesta(idRespuesta){
		var resConP = "";
		var contResp = $("#contResp").val();
		if(contResp == 1){
			 resConP = "";
			 $("#addPreg").attr("disabled", true);
			 $("#addResp").attr("disabled", false);
		}else{
			resConP = --contResp;
		}
		$("#tbody-"+idRespuesta).remove();
		$("#contResp").val(resConP);
}

// Agrea una nuevo pregunta de la BD
function nuevaPregunta(pregH, idPreg){
		$("#cuestionMezcla").append("<thead id='thead-"+idPreg+"'><tr>"+pregH+"</tr></thead>");
}

// Agrea una nuevo respuesta de la BD
function nuevaRespuesta(respH, idResp){
		$("#cuestionMezcla").append("<tbody id='tbody-"+idResp+"'><tr>"+respH+"</tr><tbody>");
}

// Agrega o crea una nueva asignacion de pregunta y respuesta a un cuestionario
$("#btnCrePregCuest").click(function(){

	var route = path+"asingapregunta";
	var token = $("#token").val();
  var qtname = $("#qtname").val(); // Tipo de cuestionario
	var qsname = $("#qsnameAdPrRsp").val();  // Pregunta
	var quname = $("#quname").val(); // Cuestionario
	var anname = $("#annameAdPrRsp").val(); // Id de Respuesta
	var contPreg = $("#contPreg").val();
	var contResp = $("#contResp").val();
	var seccion = $("#seccion").val();
	var subseccion = $("#subseccion").val();
	var idTipoCuestionario = $("#idTipoCuestionario").val();

	// Obtiene todas las respuestas
	var datos = [];
  $(".respuestaHidden").each(function() {
     // var description = $(this).val();
      var arreglo = $(this).val();
      datos.push(arreglo);
  });

	$("#msj-error1").fadeOut();
	$("#msj-errorPr2").fadeOut();
	$("#msjPr2").html("");
	$("#msj-error3").fadeOut();
	$("#msj-error4").fadeOut();
	$("#msj-error5").fadeOut();
	$("#msj-error6").fadeOut();

	$.ajax({
			 url: route,
			 headers: {'X-CSRF-TOKEN':token},
			 type: 'POST',
			 dataType: 'json',
			 data: { respuestaHidden: datos, qtname:qtname, qaqsfk:qsname, quname:quname, anname:anname, contPreg:contPreg, contResp:contResp,
				 			seccion:seccion, subseccion:subseccion,
			 				 idTipoCuestionario: idTipoCuestionario	},
			 success: function(){
			 carga();
			 },
			 error: function(msj){
				 				console.log(msj);

							 if(msj.responseJSON.errors.qtname != undefined ){
								 $("#msj-error1").fadeIn();
								 $("#msj1").html(msj.responseJSON.errors.qtname);
							 }

							 if(msj.responseJSON.errors.quname != undefined ){
								 $("#msj-error3").fadeIn();
								 $("#msj3").html(msj.responseJSON.errors.quname);
						   }

							 if(msj.responseJSON.errors.qaqsfk != undefined ){
								 $("#msj-errorPr2").fadeIn();
								 $("#msjPr2").html(msj.responseJSON.errors.qaqsfk); alert('ssdsd');
								 eliminarPregunta(qsname);
								 eliminarRespuesta(anname);
								 $("#addPreg").attr("disabled", false); // Habilita boton para agregar preguntas
								 $("#addResp").attr("disabled", true); // Inhabilita boton para agregar respuestas
							 }

							 if(msj.responseJSON.errors.anname != undefined ){
								 $("#msj-error4").fadeIn();
								 $("#msj4").html(msj.responseJSON.errors.anname);
						   }

							 if(msj.responseJSON.errors.contPreg != undefined ){
								 $("#msj-error5").fadeIn();
								 $("#msj5").html(msj.responseJSON.errors.contPreg);
						   }

							 if(msj.responseJSON.errors.contResp != undefined ){
								 $("#msj-error6").fadeIn();
								 $("#msj6").html(msj.responseJSON.errors.contResp);
						   }
		  }
	 });
});



//############################## FIN DE FUNCIONES PARA AGREGAR RESPUESTAS Y PREGUNTAS ####################################3




// Vuelve a actualizar la pagina
function carga(){
	location.reload();
}

// Boton para agregar respuestas y preguntas al cuestionario seleccionado al presinar guardar dentro de la ventana modal
$("#btnCreateAsignPregnt").on("click",function() {
	// Id del tipo de la tabla  (answer_type_questionnaires)
	var idTipoCuestionario = $("#idTipoCuestionario").val();
	var route = path+"asingapregunta/"+idTipoCuestionario+"/edit";

	$.get(route, function(res){
			$("#qtname").val(res[0].idTipoCuest);
			$("#txtqtname").val(res[0].nameTipoCuest);
			$("#quname").val(res[0].idCuest);
			$("#txtquname").val(res[0].nameCuest);
	});

});


// Funcio  que envia datos de la ventana modal y actualiza un registro de la pregunta
$("#btnGrdPreg").click(function(){

		var id = $("#idPregCst").val();
		var route = path+"pregunta/"+id;
		var qsname = $("#qsname").val();
		var token = $("#token").val();

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'PUT',
					dataType: 'json',
					data: { qsname: qsname },
					success: function(){
						$("#msj-success").fadeIn();
						carga();
					},
					error:function(msj){
					//console.log(msj);
					//console.log(msj.responseJSON.errors.qtname);
          $("#msj").html(msj.responseJSON.errors.qsname);
          $("#msj-error").fadeIn();
          }
			});

});

// Funcio  que envia datos de la ventana modal y actualiza un registro de la Respuesta
$("#btnUpdtAsResp").click(function(){

		var id = $("#idRespAspRs").val();
		var idPregResp = $("#idPregRespAspRs").val();
		var idPregunta = $("#idPregAspRs").val();
		var modulo = "cuestionarios";
		var anname = $("#annameUpdRespPre").val(); // ID de respuesta
		var idTipoCuestionario = $("#idTipoCuestionario").val();
		var status = $("#statusUpdRespPre").val();
		var token = $("#token").val();

		var route = path+"respuesta/"+id;

		$("#msj1").html("");
		$("#msj-error1").fadeOut();

		$.ajax({
				url: route,
				headers: {'X-CSRF-TOKEN':token},
				type: 'PUT',
				dataType: 'json',
				data: { qaanfk:anname, idTipoCuestionario:idTipoCuestionario, modulo:modulo, idPregunta:idPregunta, idPregResp:idPregResp, status:status  },
				success: function(r){
					$("#msj-success").fadeIn();
					carga();
				} ,
				error:function(msj){
					//console.log(msj);
					//console.log(msj.responseJSON.errors.qaanfk[0]);
					$("#msj-error1UpRsp").fadeIn();
					$("#msj1UpRsp").html(msj.responseJSON.errors.qaanfk[0]);
				}
		});


});


function cargaTemplate(idPregResp, idRespuesta, idPregunta){
	window.open(path+'fileTemplate/'+idPregResp,'_blank');
}


//######################## Carga el ID de la pregunta en el modal de acuerdo al seleccionado con respecto al modal de Boostrap ############
$('#addRespPreg').on('show.bs.modal', function(e){

	// Obtiene el id de la lista de registros en el index
	var id = $(e.relatedTarget).data().id;
	var dato = id.split("|");
	var idPregResp = dato[0]; // Id de pregunta y respuesta
	var idPregunta = dato[1]; // ID de pegunta

	// Ruta que trae el registro de la BD para llenar las cajas de texto en el modal
	var route = path+"pregunta/"+idPregunta+"/edit";

	// Llena el ID del registro de la lista del index en la ventana modal
	$(e.currentTarget).find('#idPregCstAddRsp').val(idPregunta);
	$(e.currentTarget).find('#idPregRespCstAddRsp').val(idPregResp);


	// Obtiene y llena las cajas de la ventana modal con los valores que corresponden al ID del registro
	$.get(route, function(res){
			$(e.currentTarget).find('#qsnameAddRsp').val(res.qsfk);
	});

});
