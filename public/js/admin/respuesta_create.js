// Funcio  que envia datos de la ventana modal y crea un nuevo registro
$("#btnCreateResp").click(function(){

		var route = path+"respuesta";
		var anname = $("#anname").val();
		var anatfk = $("#anatfk").val();
		var anvalue = $("#anvalue").val();
		var status = $("#status").val();
		var token = $("#token").val();

		$("#msj1").html("");
		$("#msj-error1").fadeOut();
		$("#msj2").html("");
		$("#msj-error2").fadeOut();

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'POST',
					dataType: 'json',
					data: { anname:anname, anatfk:anatfk, anvalue:anvalue,status:status},
					success: function(){
						$("#msj-success").fadeIn();
						carga();
					},
					error:function(msj){
						//console.log(msj.responseJSON.errors.qtname);
						$("#msj1").html(msj.responseJSON.errors.anname);
						$("#msj-error1").fadeIn();
						$("#msj2").html(msj.responseJSON.errors.anatfk);
						$("#msj-error2").fadeIn();
          }
			});
});

// Funcio  que envia datos de la ventana modal y actualiza un registro
$("#btnUpdResp").click(function(){

		var id = $("#idPreg").val();
		var anname = $("#annameEd").val();
		var anatfk = $("#anatfkEd").val();
		var anvalue = $("#anvalueEd").val();
		var status = 'N';
		var token = $("#token").val();
		var modulo = "respuestas"; // Modulo donde se envian los parametros al controlador

		// Ruta donde se enviar los parametros
		var route = path+"respuesta/"+id;

		$("#msjPr1").html("");
		$("#msjPr-error1").fadeOut();
		$("#msjPr2").html("");
		$("#msjPr-error2").fadeOut();

		$.ajax({
				url: route,
				headers: {'X-CSRF-TOKEN':token},
				type: 'PUT',
				dataType: 'json',
				data: { anname:anname, anatfk:anatfk, anvalue:anvalue,status:status, modulo:modulo },
				success: function(){
					$("#msj-successEd").fadeIn();
					carga();
				},
				error:function(msj){
					//console.log(msj.responseJSON.errors.qtname);
					$("#msjPr1").html(msj.responseJSON.errors.anname);
					$("#msjPr-error1").fadeIn();
					$("#msjPr2").html(msj.responseJSON.errors.anatfk);
					$("#msjPr-error2").fadeIn();
				 }
		});

});

/////############# Valida el valor de ponderacion de acuerdo al tipo de respuesta que seleccionan ##############
$("#anatfk").change(function(event){
	var idTipoResp = event.target.value;
	muestraValores(idTipoResp);
});

$("#anatfkEd").change(function(event){
	var idTipoResp = event.target.value;
	muestraValores(idTipoResp);
});
