$(document).ready(function(e){

  var status = $("#status").val();

    // Carga el ID del tipo de cuestionario en el modal de acuerdo al seleccionado con respecto al modal de Boostrap
    $('#editCatalog').on('show.bs.modal', function(e){
      // Obtiene el id de la lista de registros en el index
      var id = $(e.relatedTarget).data().id;

      // Ruta que trae el registro de la BD para llenar las cajas de texto en el modal
      var route = path+"catalogo/"+id+"/edit";

      // Llena el ID del registro de la lista del index en la ventana modal
      $(e.currentTarget).find('#idEdCat').val(id);

      // Obtiene y llena las cajas de la ventana modal con los valores que corresponden al ID del registro
    	$.get(route, function(res){
          $(e.currentTarget).find('#catvalueEd').val(res.catvalue);
          $(e.currentTarget).find('#catnamtablEd').val(res.catnamtabl);
    	});

    });

    // Redirecciona y muestra pestaña de tipo de cuestionario depende del estatus que se encuentre el registro
    if(status == 1){
      $('.nav-tabs a[href="#altas"]').tab('show')
    }
    if(status == 0){
      $('.nav-tabs a[href="#bajas"]').tab('show')
    }
});


// Funcio  que envia datos de la ventana modal y crea un nuevo registro
$("#btnCreaRegCat").click(function(){

		var route = path+"catalogo";
		var catvalue = $("#catvalueCr").val();
		var catnamtabl = $("#catnamtablCr").val();
    var token = $("#token").val();
		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'POST',
					dataType: 'json',
					data: { catvalue: catvalue, catnamtabl:catnamtabl},
					success: function(){
						$("#msj-successCr").fadeIn();
						carga();
					},
					error:function(msj){
          $("#msjCr").html(msj.responseJSON.errors.catvalue);
          $("#msj-errorCr").fadeIn();
          }
			});
});


$("#btnEditCatlg").click(function(){

		var id = $("#idEdCat").val();
		var route = path+"catalogo/"+id;
		var catvalue = $("#catvalueEd").val();
    var catnamtabl = $("#catnamtablEd").val();
		var token = $("#token").val();

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'PUT',
					dataType: 'json',
					data: { catvalue: catvalue, catnamtabl:catnamtabl },
					success: function(){
						$("#msj-successEd").fadeIn();
						carga();
					},
					error:function(msj){
					//console.log(msj);
					//console.log(msj.responseJSON.errors.qtname);
          $("#msjEd").html(msj.responseJSON.errors.qtname);
          $("#msj-errorEd").fadeIn();
          }
			});

});
