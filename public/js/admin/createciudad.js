
$("#btnCreateCiudad").click(function(){

  var route = path+"ciudad";
  var ciname = $("#ciname").val();
  var cilada = $("#cilada").val();
  var token = $("#token").val();

  $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN':token},
    type: 'POST',
    dataType: 'json',
    data: { ciname:ciname, cilada:cilada},
    success: function(){
      $("#msj-success").fadeIn();
      carga();
    },
    error:function(msj){
      $("#msj").html(msj.responseJSON.errors.ciname)
      $("#msj-error").fadeIn();

      $("#msj2").html(msj.responseJSON.errors.cilada)
      $("#msj-error2").fadeIn();
    }
  });
});

// Funcio  que envia datos de la ventana modal y actualiza un registro
$("#btnEditCiudad").click(function(){

		var id = $("#cifk").val();
		var route = path+"ciudad/"+id;
    var ciname = $("#cinamed").val();
    var cilada = $("#ciladad").val();
		var token = $("#token").val();

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'PUT',
					dataType: 'json',
					data: { ciname: ciname, cilada:cilada },
					success: function(){
						$("#msj-successEd").fadeIn();
						carga();
					},
					error:function(msj){
					console.log(msj);
					//console.log(msj.responseJSON.errors.qtname);
          // $("#msjEd").html(msj.responseJSON.errors.ciname);
          // $("#msj-errorEd").fadeIn();
          // $("#msjEdd").html(msj.responseJSON.errors.cilada);
          // $("#msj-errorEdd").fadeIn();
          }
			});

});


// Vuelve a actualizar la pagina
function carga(){
	location.reload();
}
