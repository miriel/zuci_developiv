// Funcio  que envia datos de la ventana modal y crea un nuevo registro
$("#btnCreaTipoCuest").click(function(){

		var route = path+"tipocuestionario";
		var qtname = $("#qtname").val();
		var token = $("#token").val();

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'POST',
					dataType: 'json',
					data: { qtname: qtname},
					success: function(){
						$("#msj-success").fadeIn();
						carga();
					},
					error:function(msj){
          $("#msj").html(msj.responseJSON.errors.qtname);
          $("#msj-error").fadeIn();
          }
			});

});


// Funcio  que envia datos de la ventana modal y actualiza un registro
$("#btnEditTipoCuest").click(function(){

		var id = $("#id").val();
		var route = path+"tipocuestionario/"+id;
		var qtname = $("#qtnameEd").val();
		var token = $("#token").val();

		 $.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN':token},
					type: 'PUT',
					dataType: 'json',
					data: { qtname: qtname },
					success: function(){
						$("#msj-successEd").fadeIn();
						carga();
					},
					error:function(msj){
					//console.log(msj);
					//console.log(msj.responseJSON.errors.qtname);
          $("#msjEd").html(msj.responseJSON.errors.qtname);
          $("#msj-errorEd").fadeIn();
          }
			});

});


// Actualizar la pagina
function carga(){
	location.reload();
}
