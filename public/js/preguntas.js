// Muestra el id de la pregunta respuesta en la ventana modal para cargar las imagenes para posteriormente guardar la imagen con el ID de la respuesta y
// guardar el valor de la imagen en la respuesta que corresponde (tabla BD)
$(document).ready(function(e) {
    $('#subeFilesM').on('show.bs.modal', function(e) {
        var id = $(e.relatedTarget).data().id;
        $(e.currentTarget).find('#id').val(id);
    });
});

function procentaje() {
    var totalPreguntas = $(".nivelCumplimiento").length;
    var totalNivelCumplimiento = 0;
    var datoNivelCumplimiento = 0;

    $(".nivelCumplimiento").each(function() {
        datoNivelCumplimiento = $(this).val();
        if ($.isNumeric(datoNivelCumplimiento)) {
            totalNivelCumplimiento += parseInt(datoNivelCumplimiento);
        }
    });

    var porcentaje = (totalNivelCumplimiento / totalPreguntas)
    $('#porcentaje').text('Promedio: ' + porcentaje.toFixed(2));
}

procentaje();
procentajeAudit();

$('.nivelCumplimiento').change(function() {
    procentaje();
});

$('.nivelCumplimientoAudit').change(function(){
 procentajeAudit();
});

function habilitarCampos(rol, idCuestionario) {
    if (rol == 3) {
        $('.a').prop("disabled", true);
    }

    if (rol == 3 && idCuestionario == 8) {
        $('.nivelCumplimiento').prop("disabled", false);
        $('.a').prop("disabled", false);
    }
}

$('#selectAll').click(function() {
    var seleccionados = $('.donwnload').prop('checked');
    if (seleccionados) {
        $('.donwnload').prop("checked", false);
    } else {
        $('.donwnload').prop("checked", true);
    }
});

$('.downloadButtom').click(function() {
    var nameFiles = [];
    var totalRadios = $('.donwnload').length;

    var totalNulos = 0;
    $('.donwnload').each(function() {
        if ($(this).prop('checked')) {
            nameFiles.push($(this).attr('rel'));
        }else{
          totalNulos++;
        }
    });

    if(totalNulos !== totalRadios){
      var route = path + "downloadTemplateCues/" + nameFiles;
      window.location.href = route;
    }
});


function procentajeAudit(){
    var totalPreguntas = $(".nivelCumplimientoAudit").length;
    var totalNivelCumplimiento = 0;
    var datoNivelCumplimiento = 0;

    $(".nivelCumplimientoAudit").each(function(){
      datoNivelCumplimiento = $(this).val();
      if($.isNumeric(datoNivelCumplimiento)){
        totalNivelCumplimiento += parseInt(datoNivelCumplimiento);
      }
    });

    var porcentaje = (totalNivelCumplimiento / totalPreguntas)
    $('#porcentajeAudit').text('PromAudit: '+porcentaje.toFixed(2));
}

$('.limite').each(function(){
<<<<<<< HEAD
  console.log(value);
=======
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
  var comentarioAuditor = 0;
  totalInicial = $(this).find(value).val().length;
  var totalRestante = (100 - totalInicial);
  //var elemento = $(this).find(value);
  $('#'+$(this).find(value).attr('rel')).text(totalRestante);
});

<<<<<<< HEAD
// $(value).keyup(function(){
//   var numPalabras = $(this).val().length;
//   var cantidadActual = (100 - numPalabras);
//   var id = $(this).attr('rel');
//   $('#'+id).text(cantidadActual);
// });
=======
$(value).keyup(function(){
  var numPalabras = $(this).val().length;
  var cantidadActual = (100 - numPalabras);
  var id = $(this).attr('rel');
  $('#'+id).text(cantidadActual);
});
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
