//Validando en que pestaña se encuentra para poder activarla
$(document).ready(function(e) {
    var status = $("#status").val();

    if (status == 1) {
        $('.nav-tabs a[href="#altas"]').tab('show')
    }
    if (status == 0) {
        $('.nav-tabs a[href="#bajas"]').tab('show')
    }
});

//Editando calificación
$('#btnEditCalificacion').click(function(){

    var cmfk = $('#cmfk').val();
    var token = $('#token').val();
    var cmdesc = $('#cmdesc').val();
    var cmabbr = $('#cmabbr').val();
    var cmstatus = $('#cmstatus').val();
    var route = path + "calificacionArchivo/update";

    $.ajax({
        url: route,
        headers: {
            'X-CSRF-TOKEN': token
        },
        data: {
            cmfk: cmfk,
            cmdesc: cmdesc,
            cmabbr: cmabbr,
            cmstatus: cmstatus
        },
        type: 'PUT',
        dataType: 'json',
        success: function(data) {

          if(data){
            var route = path + "calificacionArchivo";
            window.location.href = route;
          }

        },
        error: function(msj) {
          //Obteniendo mensajes de error de la validacion de los campos
            $("#msjEd1").html(msj.responseJSON.errors.cmdesc);
            $("#msj-errorEdit1").fadeIn();

            $("#msjEd2").html(msj.responseJSON.errors.cmabbr);
            $("#msj-errorEdit2").fadeIn();
        }
    });
});

//Llenando los campos de la ventana modal de edit
$('.editar').click(function() {

    var id = $(this).attr('rel');
    var route = path + "calificacionArchivo/" + id + "/edit";

    $.get(route, function(result) {

        $('#cmdesc').val(result.cmdesc);
        $('#cmabbr').val(result.cmabbr);
        $('#cmfk').val(result.cmfk);

        var status = (result.cmstatus == 1) ? 'A' : 'I';

        $('[name=status]').val(status);
    });

});

//Haciendo llamada AJAX para crear un registro
$('#btnCreateTipoCalificacion').click(function() {
    var token = $('#token').val();
    var cmdesc = $('#cmdescA').val();
    var cmabbr = $('#cmabbrA').val();
    var cmstatus = $("#cmstatusA").val();
    var route = path + "calificacionArchivo/create";

    $.ajax({
        url: route,
        headers: {
            'X-CSRF-TOKEN': token
        },
        data: {
            cmdesc: cmdesc,
            cmabbr: cmabbr,
            cmstatus: cmstatus
        },
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            window.location.href = path + "calificacionArchivo";
        },
        error: function(msj) {
          //Obteniendo mensajes de error de la validacion de los campos
            $("#msj1").html(msj.responseJSON.errors.cmdesc);
            $("#msj-errorAlta1").fadeIn();

            $("#msj2").html(msj.responseJSON.errors.cmabbr);
            $("#msj-errorAlta2").fadeIn();
        }
    });
});
