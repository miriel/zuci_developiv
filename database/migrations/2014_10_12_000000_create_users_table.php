<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable($value = false);
            $table->string('apaternal')->nullable($value = false);
            $table->string('amaternal')->nullable($value = false);
            $table->string('email')->unique()->nullable($value = false);
            $table->string('code', 30);
            $table->integer('active')->nullable($value = false)->default(0);
            $table->text('folio');
            $table->string('wkrkarea', 100);
            $table->string('job', 100);
            $table->bigInteger('phone_cel')->nullable($value = true);
            $table->bigInteger('phone_fixed');
            $table->string('lada_cel', 20);
            $table->timestamp('expiredcode');
            $table->string('password');
            $table->integer('status')->default(1);
            $table->rememberToken();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
