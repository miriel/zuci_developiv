<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('pgfk');
            $table->string('pgname');
            $table->string('pgdesc');
            $table->decimal('pgprice', 8, 2);
            $table->string('pgperiod');
            $table->boolean('pghowprice');
            $table->integer('pgquantity');
            $table->integer('pgstatus')->default(1);
            $table->ipAddress('pgip');
            $table->integer('pguserpk');
            $table->timestamp('pginsertdt')->nullable($value = false);
            $table->timestamp('pgupddt')->nullable($value = false);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
