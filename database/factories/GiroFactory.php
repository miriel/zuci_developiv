<?php

use Faker\Generator as Faker;

$factory->define(App\Company_Turn::class, function (Faker $faker) {
    return [
      'cotname' => $faker->text(rand(32, 64)),
     'cotstatus' => $faker->text(rand(256, 512))
    ];
});
