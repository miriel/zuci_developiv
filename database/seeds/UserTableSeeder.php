<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
       // Role::truncate();
       User::truncate();
       DB::statement('TRUNCATE roles CASCADE');

        $adminRole = Role::create(['name' => 'Admin']);
        $userRole = Role::create(['name' => 'User']);

         $admin = new User();
         $admin->name = 'pedro';
         $admin->apaternal = 'meza';
         $admin->amaternal = 'arellano';
         $admin->email = 'pedro.meza@zuma-ti.mx';
         $admin->code = 'p4i2m2';
         $admin->active = '1';
         $admin->folio = '14620180619212932';
         $admin->wkrkarea = 'zuma';
         $admin->job = 'programador';
         $admin->phone_cel = '5555555555';
         $admin->phone_fixed = '5555555555';
         $admin->lada_cel = '+555';
         $admin->expiredcode = '2018-06-20';
         $admin->password = bcrypt('123456');
         $admin->status = '1';
         $admin->save();
         $admin->assignRole($adminRole);

         $admin = new User();
         $admin->name = 'omar miriel';
         $admin->apaternal = 'hernandez';
         $admin->amaternal = 'hernandez';
         $admin->email = 'miriel.hernandez@zuma-ti.mx';
         $admin->code = 'c81n1o';
         $admin->active = '1';
         $admin->folio = '14420180619184906';
         $admin->wkrkarea = 'zuma';
         $admin->job = 'programador';
         $admin->phone_cel = '5555555555';
         $admin->phone_fixed = '5555555555';
         $admin->lada_cel = '+555';
         $admin->expiredcode = '2018-06-20';
         $admin->password = bcrypt('123456');
         $admin->status = '1';
         $admin->save();
         $admin->assignRole($adminRole);

         $user = new User();
         $user->name = 'user';
         $user->apaternal = 'user';
         $user->amaternal = 'user';
         $user->email = 'user@user.com';
         $user->code = '9bgdif';
         $user->active = '1';
         $user->folio = '14720180619214146';
         $user->wkrkarea = 'zuma';
         $user->job = 'zuma';
         $user->phone_cel = '5555555555';
         $user->phone_fixed = '5555555555';
         $user->lada_cel = '+555';
         $user->expiredcode = '2018-06-20';
         $user->password = bcrypt('123456');
         $user->status = '1';
         $user->save();
         $user->assignRole($userRole);


      }
}
