<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.

idioma para portugues
-->
<?php
 
return [
    'inici' => 'Home',
    'acerca' => 'Sobre',
    'login' => 'Iniciar sessão',
    'idiomas' => 'l&iacutenguas',
    'home' => 'Bem-vindo aos nossos serviços de certificação OEA & C-TPAT ',
    'inicio' => 'Serviços de certificação OEA & C-TPAT',
    'prevalidador' => 'Pré-validador',
    'texto1' => 'Para acessar o questionário pré-validador para avaliar sua viabilidade, a certificação ',
    'ingreso' => 'Digite seu fólio',    
    'texto2' => 'Para acesso a qualquer ator ou parceiro de negócios que já tenha um número de fólio pré-validador para certificação',
    'ingresar' => 'entrar',
    'texto3' => 'Requisitos para certificação',
    'txto4' => 'Requisitos gerais',
    'texto5' => 'Manual de Certificação',
    'texto6' => 'Treinamento',
    'texto7' => 'Avaliação',
    'Tecnicos' => 'Técnicos especializados',
    'Servicios' => 'Serviços profissionais',
    'Asistencia' => 'Assistência Telefônica',
    'Altamente' => 'Altamente recomendado',
    'Planes'  => 'Planos personalizados',
    'Como' => 'Como nossos clientes nos tratam',
    'Nuestros' => 'Nossos clientes de certificação OEA e C-TPAT',
    'Contactanos' => 'Entre em contato conosco',
    'Llama' => 'Ligue para nossos escritórios para mais informações.',
    'Hoja' => 'Boletim Informativo',
    'Puedes' => 'Você pode confiar em nós, enviamos apenas ofertas, nem mesmo um único e-mail indesejado.',
    'texto8' => 'Prevalidator for Certification',
	'texto9' => 'Correio comercial',
	'texto10' => 'Questionário',
    
];