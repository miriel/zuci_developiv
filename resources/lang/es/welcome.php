<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.


Idioma para español
-->
<?php
 
return [
    'inici' => 'Inicio',
    'acerca' => 'Acerca de',
    'login' => 'Iniciar Sesión',
    'idiomas' => 'Idiomas',
    'home' => 'Bienvenidos a Nuestro Servicios de Certificacion OEA & C-TPAT ',
    'inicio' => 'Servicios de Certificación OEA & C-TPAT',
    'prevalidador' => 'Pre-Validador',
    'texto1' => 'Para el acceso a cuestionario de pre-validado para evaluar tu viabilidad la certificación ',
    'ingreso' => 'Ingrese su Folio',    
    'texto2' => 'Para el acceso a todo actor o socio comercial que ya tine un numero de folio del Pre-Validador para la certificación',
    'ingresar' => 'ingresar',
    'texto3' => 'Requerimientos para certificación',
    'texto4' => 'Requerimientos Generales',
    'texto5' => 'Manual de Certificación',
    'texto6' => 'Capacitación',
    'texto7' => 'Evaluación',
    'Tecnicos' => 'Técnicos expertos',
    'Servicios' => 'Servicios Profesionales',
    'Asistencia' => 'Asistencia Telefónica',
    'Altamente' => 'Altamente recomendado',
    'Planes'  => 'Planes a medida',
    'Como' => 'Cómo nos tratan nuestros clientes',
    'Nuestros' => 'Nuestros Clientes de Certificación OEA & C-TPAT',
    'Contactanos' => 'Contactanos',
    'Llama' => 'Llama a nuestras oficinas para mayor información.',
    'Hoja' => 'Hoja informativa',
    'Puedes' => 'Puedes confiar en nosotros solo enviamos ofertas, ni un solo correo no deseado.',
    'texto8' => 'Prevalidador para la Certificación ',
	'texto9' => 'Correo empresarial',
	'texto10' => 'Cuestionario',
	
    
];