<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.


Idioma para Ingles
-->
<?php
 
return [
    'inici' => 'Start',
    'acerca' => 'About',
    'login' => 'Login', 
    'idiomas' => 'Languages',
    'home' => 'Welcome to our Certification Services OEA & C-TPAT',
    'inicio' => 'Requirements for certification Certification Services OEA & C-TPAT',
    'prevalidador' => 'Pre-Validator',
    'texto1' => 'To access the pre-validator questionnaire to evaluate your viability the certification ',
    'ingreso' => 'Enter your Folio',
    'texto2' => 'For access to any actor or business partner that already has a Pre-Validator folio number for certification',
    'ingresar' => 'enter',
    'texto3' => 'Requirements for certification',
    'texto4' => 'General Requirements',
    'texto5' => 'Certification Manual',
    'texto6' => 'Training',
    'texto7' => 'Evaluation',
    'Tecnicos' => 'Expert technicians',
    'Servicios' => 'Professional Services',
    'Asistencia' => 'Telephone Assistance',
    'Altamente' => 'Highly recommended',
    'Planes'  => 'Plans to measure',
    'Como' => 'How our clients treat us',
    'Nuestros' => 'Our OEA & C-TPAT Certification Clients',
    'Contactanos' => 'Contact us',
    'Llama' => 'Call our offices for more information.',
    'Hoja' => 'Informational page',
    'Puedes' => 'You can trust us we only send offers, not even a single unwanted email.',
     'texto8' => 'Prevalidator for Certification',
	 'texto9' => 'Business mail',
	 'texto10' => 'Questionnaire',
    
    
];