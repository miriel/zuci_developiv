@extends('layouts.principal')
@section('content')


			<div role="main" class="main">
				<div class="slider-container rev_slider_wrapper" style="height: 1000px;">

					<div id="revolutionSlider" class="slider rev_slider " data-plugin-revolution-slider data-plugin-options="{'sliderLayout': 'fullscreen', 'responsiveLevels': [4096,1200,992,500]}">
						<ul>
							<li data-transition="fade" class="booking-area">
								<img class="img booking-area" src="inicio/img/Portadaprincipal.png"
									alt=""
									data-bgposition="center center" width="auto"
									data-bgfit="cover"
									data-bgrepeat="no-repeat"
									class="rev-slidebg responsive">

								<!-- <div class="tp-caption"
									data-x="center" data-hoffset="['-150','-150','-150','-220']"
									data-y="180"
									data-start="1000"
									data-transform_in="x:[-300%];opacity:0;s:500;"><img src="img/slides/slide-title-border.png" alt=""></div> -->

								<div class="tp-caption top-label" data-x="center"data-y="12"data-fontsize="['35']" data-start="500"
									data-transform_in="y:[-300%];opacity:0;s:500;">
								</div>

								<!-- <div class="tp-caption"
									data-x="center" data-hoffset="['150','150','150','220']"
									data-y="180"
									data-start="1000"
									data-transform_in="x:[300%];opacity:0;s:500;"><img src="img/slides/slide-title-border.png" alt=""></div> -->

								<div class="tp-caption main-label text-center fuente"
									data-x="center"
									data-y="['-30','-30','-30','-100']"
									data-start="1000"
									data-whitespace="nowrap"
									data-fontsize="['45','45','45','52']"
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									data-mask_in="x:0px;y:0px;">Bienvenido a Nuestros Servicios de Certificación  <br> OEA & C-TPAT</div>

								 <div class="tp-caption bottom-label"
									data-x="left"
									data-y="['110','110','110','145']"
									data-start="2000"
									data-fontsize="['20','20','20','40']"
									data-transform_in="y:[100%];opacity:0;s:500;"> <img src="inicio/img/logo_general.png" alt=""> </div>

								<!-- <a class="tp-caption btn btn-lg btn-primary btn-slider-action"
									data-hash
									data-hash-offset="85"
									href="#home-intro"
									data-x="center" data-hoffset="0"
									data-y="center" data-voffset="['100','100','100','190']"
									data-start="2200"
									data-whitespace="nowrap"
									data-paddingtop="['20','20','20','40']"
									data-paddingbottom="['20','20','20','40']"
									data-paddingleft="['25','25','25','45']"
									data-paddingright="['25','25','25','45']"
									data-fontsize="['22','22','22','52']"
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">Get Started Now!</a>  -->

							</li>
						</ul>
						<div class="container">
							@include('flash::message')
					@if(Session::has('flash_registro'))
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						{{Session::get('flash_registro')}}
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					@endif
					@include('flash::message')
					@if(Session::has('flas_salir'))
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						{{Session::get('flas_salir')}}
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					@endif

						</div>
					</div>
				</div>
				<div class="home-intro" id="home-intro">
					<div class="container">

						<div class="row align-items-center">
							<div class="col-lg-8">
								<p>
									The fastest way to <em>certify</em> yourself in your business.
									<span>Check out our options and features included.</span>
								</p>
							</div>
							<div class="col-lg-4">
								<div class="get-started text-left text-lg-right">
									<a href="{{ route('register') }}"  class="btn btn-lg btn-primary">Register now!</a>
									<div class="learn-more tamano">or <a href="{{ route('login') }}" style="color:#fff;">Log in.</a></div>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="container">

					<div class="row text-center">
						<div class="col">
							<h1 class="mb-2 word-rotator-title">
								{{ trans('welcome.texto3') }}
								<strong class="inverted">
									<span class="word-rotator" data-plugin-options="{'delay': 2000, 'animDelay': 300}">
										<span class="word-rotator-items">
											<span>incredibly</span>
											<span>especially</span>
											<span>extremely</span>
										</span>
									</span>
								</strong>
							</h1>
							<p class="lead">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce elementum, nulla vel pellentesque consequat, ante nulla hendrerit arcu, ac tincidunt mauris lacus sed leo. vamus suscipit molestie vestibulum.
							</p>
						</div>
					</div>

				</div>

				<div class="home-concept mt-5">
					<div class="container">

						<div class="row text-center">
							<span class="sun"></span>
							<span class="cloud"></span>
							<div class="col-lg-2 ml-lg-auto">
								<div class="process-image">
									<img src="inicio/img/requeri.png" alt="" />
									<strong>Requerimientos </strong><strong>Generales</strong>
								</div>
							</div>
							<div class="col-lg-2">
								<div class="process-image">
									<img src="inicio/img/manual.png" alt="" />
									<strong>Manual de</strong>	<strong> Certificación</strong>
								</div>
							</div>
							<div class="col-lg-2">
								<div class="process-image">
									<img src="inicio/img/training.png" alt="" />
									<strong>{{ trans('welcome.texto6') }}</strong>
								</div>
							</div>
							<div class="col-lg-2">
								<div class="process-image">
									<img src="inicio/img/evaluacion.png" alt="" />
									<strong>{{ trans('welcome.texto7') }}</strong>
								</div>
							</div>
							<div class="col-lg-4 ml-lg-auto">
								<div class="project-image">
									<div id="fcSlideshow" class="fc-slideshow">
										<ul class="fc-slides">
											<li><a href="portfolio-single-small-slider.html"><img class="img-responsive" src="inicio/img/ejemplo.png" alt="" /></a></li>
											<li><a href="portfolio-single-small-slider.html"><img class="img-responsive" src="inicio/img/ejemplo1.png" alt="" /></a></li>
											<li><a href="portfolio-single-small-slider.html"><img class="img-responsive" src="inicio/img/ejemplo2.png" alt="" /></a></li>
											<li><a href="portfolio-single-small-slider.html"><img class="img-responsive" src="inicio/img/ejemplo3.png" alt="" /></a></li>
										</ul>
									</div>
									<strong class="our-work">Our Work</strong>
								</div>
							</div>
						</div>

					</div>
				</div>

				<!-- <div class="container">

					<div class="row">
						<div class="col">
							<hr class="tall mt-4">
						</div>
					</div>

					<div class="row">
						<div class="col-lg-8">
							<h2>Our <strong>Features</strong></h2>
							<div class="row">
								<div class="col-sm-6">
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-group"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="heading-primary mb-0">Customer Support</h4>
											<p class="mb-4">Lorem ipsum dolor sit amet, consectetur.</p>
										</div>
									</div>
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-file"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="heading-primary mb-0">HTML5 / CSS3 / JS</h4>
											<p class="mb-4">Lorem ipsum dolor sit amet,.</p>
										</div>
									</div>
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-google-plus"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="heading-primary mb-0">500+ Google Fonts</h4>
											<p class="mb-4">Lorem ipsum dolor sit amet, consectetur.</p>
										</div>
									</div>
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-adjust"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="heading-primary mb-0">Colors</h4>
											<p class="mb-4">Lorem ipsum dolor sit amet, consectetur.</p>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-film"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="heading-primary mb-0">Sliders</h4>
											<p class="mb-4">Lorem ipsum dolor sit amet, consectetur.</p>
										</div>
									</div>
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-user"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="heading-primary mb-0">Icons</h4>
											<p class="mb-4">Lorem ipsum dolor sit amet, consectetur.</p>
										</div>
									</div>
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-bars"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="heading-primary mb-0">Buttons</h4>
											<p class="mb-4">Lorem ipsum dolor sit, consectetur.</p>
										</div>
									</div>
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-desktop"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="heading-primary mb-0">Lightbox</h4>
											<p class="mb-4">Lorem sit amet, consectetur.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<h2>and more...</h2>

							<div class="accordion" id="accordion">
								<div class="card card-default">
									<div class="card-header">
										<h4 class="card-title m-0">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
												<i class="fa fa-usd"></i>
												Pricing Tables
											</a>
										</h4>
									</div>
									<div id="collapseOne" class="collapse show">
										<div class="card-body">
											Donec tellus massa, tristique sit amet condim vel, facilisis quis sapien. Praesent id enim sit amet odio vulputate eleifend in in tortor.
										</div>
									</div>
								</div>
								<div class="card card-default">
									<div class="card-header">
										<h4 class="card-title m-0">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
												<i class="fa fa-comment"></i>
												Contact Forms
											</a>
										</h4>
									</div>
									<div id="collapseTwo" class="collapse">
										<div class="card-body">
											Donec tellus massa, tristique sit amet condimentum vel, facilisis quis sapien.
										</div>
									</div>
								</div>
								<div class="card card-default">
									<div class="card-header">
										<h4 class="card-title m-0">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
												<i class="fa fa-laptop"></i>
												Portfolio Pages
											</a>
										</h4>
									</div>
									<div id="collapseThree" class="collapse">
										<div class="card-body">
											Donec tellus massa, tristique sit amet condimentum vel, facilisis quis sapien.
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<hr class="tall">

					<div class="row text-center pt-4">
						<div class="col">
							<h2 class="mb-2 word-rotator-title">
								We're not the only ones
								<strong>
									<span class="word-rotator" data-plugin-options="{'delay': 3500, 'animDelay': 400}">
										<span class="word-rotator-items">
											<span>excited</span>
											<span>happy</span>
										</span>
									</span>
								</strong>
								about Porto Template...
							</h2>
							<h4 class="heading-primary lead tall">25,000 customers in 100 countries use Porto Template. Meet our customers.</h4>
						</div>
					</div>

					<div class="row text-center">
						<div class="owl-carousel owl-theme" data-plugin-options="{'items': 6, 'autoplay': true, 'autoplayTimeout': 3000}">
							<div>
								<img class="img-fluid" src="img/logos/logo-1.png" alt="">
							</div>
							<div>
								<img class="img-fluid" src="img/logos/logo-2.png" alt="">
							</div>
							<div>
								<img class="img-fluid" src="img/logos/logo-3.png" alt="">
							</div>
							<div>
								<img class="img-fluid" src="img/logos/logo-4.png" alt="">
							</div>
							<div>
								<img class="img-fluid" src="img/logos/logo-5.png" alt="">
							</div>
							<div>
								<img class="img-fluid" src="img/logos/logo-6.png" alt="">
							</div>
							<div>
								<img class="img-fluid" src="img/logos/logo-4.png" alt="">
							</div>
							<div>
								<img class="img-fluid" src="img/logos/logo-2.png" alt="">
							</div>
						</div>
					</div>

				</div> -->

				<!-- <section class="section section-custom-map">
					<section class="section section-default section-footer">
						<div class="container">
							<div class="row">
								<div class="col-lg-6">
									<div class="recent-posts mb-5">
										<h2>Latest <strong>Blog</strong> Posts</h2>
										<div class="owl-carousel owl-theme mb-0" data-plugin-options="{'items': 1}">
											<div class="row">
												<div class="col-lg-6">
													<article>
														<div class="date">
															<span class="day">15</span>
															<span class="month">Jan</span>
														</div>
														<h4 class="heading-primary"><a href="blog-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h4>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
													</article>
												</div>
												<div class="col-lg-6">
													<article>
														<div class="date">
															<span class="day">15</span>
															<span class="month">Jan</span>
														</div>
														<h4 class="heading-primary"><a href="blog-post.html">Lorem ipsum dolor</a></h4>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
													</article>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6">
													<article>
														<div class="date">
															<span class="day">12</span>
															<span class="month">Jan</span>
														</div>
														<h4 class="heading-primary"><a href="blog-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h4>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
													</article>
												</div>
												<div class="col-lg-6">
													<article>
														<div class="date">
															<span class="day">11</span>
															<span class="month">Jan</span>
														</div>
														<h4 class="heading-primary"><a href="blog-post.html">Lorem ipsum dolor</a></h4>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
													</article>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6">
													<article>
														<div class="date">
															<span class="day">15</span>
															<span class="month">Jan</span>
														</div>
														<h4 class="heading-primary"><a href="blog-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h4>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
													</article>
												</div>
												<div class="col-lg-6">
													<article>
														<div class="date">
															<span class="day">15</span>
															<span class="month">Jan</span>
														</div>
														<h4 class="heading-primary"><a href="blog-post.html">Lorem ipsum dolor</a></h4>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
													</article>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<h2><strong>What</strong> Client’s Say</h2>
									<div class="row">
										<div class="owl-carousel owl-theme mb-0" data-plugin-options="{'items': 1}">
											<div>
												<div class="col">
													<div class="testimonial testimonial-primary">
														<blockquote>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat.  Donec hendrerit vehicula est, in consequat.  Donec hendrerit vehicula est, in consequat.</p>
														</blockquote>
														<div class="testimonial-arrow-down"></div>
														<div class="testimonial-author">
															<div class="testimonial-author-thumbnail img-thumbnail">
																<img src="img/clients/client-1.jpg" alt="">
															</div>
															<p><strong>John Smith</strong><span>CEO & Founder - Okler</span></p>
														</div>
													</div>
												</div>
											</div>
											<div>
												<div class="col">
													<div class="testimonial testimonial-primary">
														<blockquote>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat.</p>
														</blockquote>
														<div class="testimonial-arrow-down"></div>
														<div class="testimonial-author">
															<div class="testimonial-author-thumbnail img-thumbnail">
																<img src="img/clients/client-1.jpg" alt="">
															</div>
															<p><strong>John Smith</strong><span>CEO & Founder - Okler</span></p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</section> -->
			</div>
			@stop
