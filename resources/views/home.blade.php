@extends('layouts.adm')

@section('content')

		<section class="body">
			<div class="inner-wrapper">
				<!-- start: sidebar -->

				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<!-- <header class="page-header">
						<h2><i class="fa fa-home"> Home </i></h2>
					</header> -->
          <div class="col-lg-12 col-xl-12">

            <div class="panel-body">
                    <div class="content-wrapper">
                      <div class="container-fluid">
                        <!-- Area Chart Example-->
                        <div class="col-lg-12">
                        <div class="card mb-3">
                          <div class="card-header blanco">
                            <i class="fa fa-area-chart"></i> Info General</div>
                          <div class="card-body">
                            <div class="col-md-12">
                              <div class="h-100 g-brd-around g-brd-gray-light-v7 g-rounded-4 g-pa-15 g-pa-20--md">
                                    <header>
                                    <h2 class="text-uppercase g-font-size-12 g-font-size-default--md g-color-black mb-0 fuente">Información General</h2>
                                  </header>
                                  <hr class="d-flex g-brd-gray-light-v7 g-my-15 g-my-30--md">
                                  <div class="row ">
                                    <div class="col-lg-2">   </div>
                                    <div class="col-lg-3 ">
                                      <label class="" for="#firstName">Nombre</label><br>
                                      {{Auth::user()->name }}
                                    </div>

                                    <div class="col-lg-3 ">
                                      <label class="" for="#lastName">Apellido Paterno</label><br>
                                        {{Auth::user()->apaternal }}
                                    </div>
                                    <div class="col-lg-3 ">
                                      <label class="" for="#lastName">Apellido Materno</label><br>
                                        {{Auth::user()->amaternal }}
                                    </div>
                                  </div><br>
                                  <div class="row">
                                    <div class="col-lg-2"> </div>
                                    <div class="col-lg-3">
                                      <label class="" for="#lastName">Correo Electrónico</label><br>
                                        {{Auth::user()->email }}
                                    </div>
                                    <div class="col-lg-3">
                                      <label class="" for="#lastName">Teléfono Cel</label><br>
                                        {{Auth::user()->lada_cel }} {{Auth::user()->phone_cel }}
                                    </div>
                                    <div class="col-lg-3">
                                      <label class="" for="#lastName">Teléfono Fijo</label><br>
                                        {{Auth::user()->phone_fixed }}
                                    </div>

                                  </div><br>
                                  <div class="row">
                                    <div class="col-lg-2"> </div>
                                    <div class="col-lg-3">
                                      <label class="" for="#lastName">Folio</label><br>
                                        {{Auth::user()->folio }}
                                    </div>
                                    <div class="col-lg-3">
                                      <label class="" for="#lastName">Empresa</label><br>
                                        {{Auth::user()->job }}
                                    </div>
                                    <div class="col-lg-3">
                                      <label class="" for="#lastName">Área</label><br>
                                        {{Auth::user()->wkrkarea }}
                                    </div>
                                  </div>
                                  <br>

                                  <div class="row">
                                    <div class="col-lg-5"></div>
                                    <div class="col-lg-4">
                                      <img src="img/logo.png" class="text-center">
                                    </div>
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.panel-body -->
              </div>
          </div>
				</section>
			</div>




		</section>

@endsection
