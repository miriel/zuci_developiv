
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar Giro</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Editar giro
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                          {!! Form::open()!!}
                              <div class="form-group">
                                {!! Form::label('description','Nombre') !!}
                                {!! Form ::text('cotname', $giro->cotname, ['class'=>'form-control'])!!}
                                <span class="help-block">
                                  <p class="alert-danger">{{$errors->first('cotname')}}</p>
                                </span>
                              </div>
                              <div class="form-group">
                                {!! Form::submit('Editar',['class'=>'btn bt-primary']) !!}
                              </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
