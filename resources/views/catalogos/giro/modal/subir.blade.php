
<!-- Modal -->
<div class="modal fade" id="descargargiro" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite ">
      <div class="modal-header modalcolor">
        <h4 class="letracolor">descargar giro</h4>
        <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <a href="{{ route('export.file',['type'=>'xls']) }}"><button class="btn btn-success">Download Excel xls</button></a>
        <a href="{{ route('export.file',['type'=>'xlsx']) }}"><button class="btn btn-success">Download Excel xlsx</button></a>
        <!-- <a href="{{ route('export.file',['type'=>'csv']) }}"><button class="btn btn-success">Download CSV</button></a> -->
            {!! Form::open(array('route' => 'import.file','method'=>'POST','files'=>'true')) !!}
       				 <div class="col-xs-12 col-sm-12 col-md-12">
       							<div class="form-group">
       									{!! Form::label('sample_file','Select File to Import:',['class'=>'col-md-3']) !!}
       									<div class="col-md-9">
       									{!! Form::file('sample_file', array('class' => 'form-control')) !!}
       									{!! $errors->first('sample_file', '<p class="alert alert-danger">:message</p>') !!}
       									</div>
       							</div>
       					</div>
       							<div class="col-xs-12 col-sm-12 col-md-12 text-center">
       							{!! Form::submit('Upload',['class'=>'btn btn-primary']) !!}
       							</div>
       					</div>
       				 {!! Form::close() !!}
       			 </div>
      </div>
    </div>
  </div>
</div>
