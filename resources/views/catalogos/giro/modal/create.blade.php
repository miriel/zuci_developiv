@extends('layouts.adm')
@section('content')

<div id="page-wrapper">

    <!-- /.r-ow -->

    <br>
    <div class="row">
        <div class="col-lg-12">
          <!-- /.panel-heading -->

                        <!-- /.panel-body -->

                    <!-- /.panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i>   Agregar Giro
                            <!-- <div class="pull-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Actions
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><a href="#">Action</a>
                                        </li>
                                        <li><a href="#">Another action</a>
                                        </li>
                                        <li><a href="#">Something else here</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a>
                                        </li>
                                    </ul>
                                </div>
                            </div> -->
                        </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                              {!! Form::open(['route'=>'giro.store','method'=>'POST'])!!}
                              <div class="form-group">
                                {!! Form::label('cotname','Nombre giro') !!}
                                {!! Form ::text('cotname',null, ['class'=>'form-control'])!!}
                                <span class="help-block">
                                  <p class="alert-danger">{{$errors->first('cotname')}}</p>
                                </span>
                              </div>

                                <div class="form-group">
                                  {!! Form::submit('Registrar',['class'=>'btn btn-default']) !!}

                                </div>
                          {!! Form::close() !!}
                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
      </div>
    <!-- /.row -->
</div>


@stop
