@extends('layouts.adm')
	@section('content')
			@include('catalogos.giro.modal')
			@include('catalogos.giro.modal.edit')

			<div class="inner-wrapper">
				<section role="main" class="content-body pb-0">
					<!-- <header class="page-header">
						<h2>Giros</h2>

						<div class="right-wrapper text-right">
							<ol class="breadcrumbs">
								<li>
									<a href="{{url("home")}}">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span class="blanco"></span></li>
							</ol>

						</div>
					</header> -->
					@include('flash::message')
					@if(Session::has('message'))
							<div class="alert alert-success" role="alert">
								{{Session::get('message')}}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
					@endif
					@if(Session::has('flash_delete'))
							<div class="alert alert-warning " role="alert">
								{{Session::get('flash_delete')}}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
					@endif
					<br>
					@include('flash::message')
					@if(Session::has('flash_success'))
							<div class="alert alert-success" role="alert">
								{{Session::get('flash_success')}}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
					@endif
					<div class="col-md-12">
								<section class="card card-featured card-featured-primary mb-4">
									<header class="card-header">
										<div class="card-actions">

											   @can('create',new App\Company_Turn)
												<button type="button" class="btn btn-outline btn-link" data-toggle='modal'
												data-target='#agregargiro' style="color:#fff;">	<i class="fa fa-plus"> 	Agregar</i></button>
												  @endcan
													<!-- <button type="button" class="btn btn-outline btn-link" data-toggle='modal'
													data-target='#descargargiro' style="color:#fff;">	<i class="fa fa-plus"> 	Descargar layaout</i></button> -->

										</div>
										<h2 class="card-title blanco">Giros</h2>
									<!--	<h2>	<a target="_blank" href="{{ route('products.pdf') }}" class="btn btn-lg btn-primary">
															 Descargar Giro en PDF
													 </a>
													<a href="{{ route('products.excel') }}" class="btn btn-sm btn-primary">
								            Descargar productos en Excel
								        </a>
											</h2> -->
											<input type="hidden" name="status" id="status" value="{{  $status }}" />
									</header>
									<div class="card-body">
<<<<<<< HEAD
										<div class="row">
											<div class="col-md-4">
											</div>
											<div class="col-md-4" id="serach">
													<input class="form-control" name="search" type="text" id="search">
											</div>
											<div class="col-md-4">
											</div>
										</div>
										<!-- <code>.card-featured.card-featured-primary</code> -->
=======
										<!-- <code>.card-featured.card-featured-primary</code> -->
										<div class="	">
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003

											<!-- <div class="container">
											<form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ URL::to('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
												<input type="file" name="import_file" />
												<button class="btn btn-primary">Import File</button>
											</form>
										</div> -->
											<div class="tabs tabs-primary">
												<ul class="nav nav-tabs ">
													<li class="nav-item active">
														<a class="nav-link active" href="#altas" data-toggle="tab"><i class="fa fa-star"></i> Altas</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" href="#bajas" data-toggle="tab">Bajas</a>
													</li>
												</ul>
												<div class="tab-content">
													<div  id="altas" class="tab-pane active">
														<table width="100%" class="table table-responsive-md table-sm  ">
															<thead>
																	<tr>
																		<td><b>ID</b></td>
																		<td><b>Nombre</b></td>
																		<td><b>Estatus</b></td>
																		<td><b>Acción</b></td>
																	</tr>
															</thead>
															@foreach($giro as $giros)
																<tbody>
																		<tr class="odd gradeX">
																				<td>{{$giros->cotfk }}</td>
																				<td>{{ $giros->cotname }}</td>
																				<td>
																					<img src="{{ url('admin/img/success.png') }}"alt="">
																					<!-- <button type="button" class="btn btn-success btn-circle"><i class="fa fa-check"></i></button> -->
																				</td>
																				<td class="">
																						<div class="row">
																							 @can('delete',new App\Company_Turn)
																								<div class="col-md- col-md-offset-1">
																									{!!Form::open(['route'=> ['giro.destroy',$giros->cotfk, 'estatus' => $giros->cotstatus],'method'=>'DELETE'])!!}
																										{{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-xs btn-default','title'=>'Inactivar']) }}
																										{!!Form::close()!!}
																								</div>
																								  @endcan
																								 @can('update',new App\Company_Turn)
																								<div class="col-md-1 col-md-offset-1">
																									<a data-toggle='modal' id="editGiro" data-target='#editGiro'
																									 data-id='{{ $giros->cotfk }}' class="btn btn-xs btn-default ">  <i class="fa fa-edit"></i></a>
																								</div>
																								  @endcan
																					</div>
																				</td>
																		</tr>
																	</tbody>
																@endforeach
														</table>
														<div class=" pull-right">
																{{ $giro->appends(['status' => 1])}}
														</div>
													</div>
													<div id="bajas" class="tab-pane fade">
														<table width="100%" class="table table-responsive-md table-sm 	">
															<thead>
																	<tr>
																		<td><b>ID</b></td>
																		<td><b>Nombre</b></td>
																		<td><b>Estatus</b></td>
																		<td><b>Acción</b></td>
																	</tr>
															</thead>
															@foreach($girobajas as $girosBa)
																<tbody>
																		<tr class="odd gradeX">
																				<td>{{$girosBa->cotfk }}</td>
																				<td>{{ $girosBa->cotname }}</td>
																				<td>
																					<img src="{{ url('admin/img/danger.png') }}"alt="">
																				</td>
																				<td>
																						<div class="row">
																							@can('delete',new App\Company_Turn)
																								<div class="col-md- col-md-offset-">
																									{!!Form::open(['route'=> ['giro.destroy',$girosBa->cotfk, 'estatus' => $girosBa->cotstatus],'method'=>'DELETE'])!!}
																										{{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-xs btn-default','title'=>'Inactivar']) }}
																										{!!Form::close()!!}
																								</div>
																								@endcan
																								@can('update',new App\Company_Turn)
																								<div class="col-md-1 col-md-offset-1">
																									<a data-toggle='modal' id="editGiro" data-target='#editGiro'
																							 data-id='{{ $girosBa->cotfk }}' class="btn btn-xs btn-default ">  <i class="fa fa-edit"></i></a>
																							</div>
																							@endcan
																					</div>
																				</td>
																		</tr>
																	</tbody>
																@endforeach
														</table>
														<div class=" pull-right">
																{{ $girobajas->appends(['status' => 0])}}
														</div>
												</div>
											</div>
											</div>
<<<<<<< HEAD
=======

									</div>
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
								</section>
							</div>



					<!-- end: page -->
				</section>
			</div>

			@stop
@section('scripts')
	{!!Html::script('js/Generics.js')!!}
	{!!Html::script('js/admin/creategiro.js')!!}
	{!!Html::script('js/admin/giro.index.js')!!}
<<<<<<< HEAD
	<script>
		$('#search').keypress(function(){
			$.ajax({
		    url : 'post.php',

		    // la información a enviar
		    // (también es posible utilizar una cadena de datos)
		    data : { id : 123 },

		    // especifica si será una petición POST o GET
		    type : 'GET',

		    // el tipo de información que se espera de respuesta
		    dataType : 'json',

		    // código a ejecutar si la petición es satisfactoria;
		    // la respuesta es pasada como argumento a la función
		    success : function(json) {
		        $('<h1/>').text(json.title).appendTo('body');
		        $('<div class="content"/>')
		            .html(json.html).appendTo('body');
		    },

		    // código a ejecutar si la petición falla;
		    // son pasados como argumentos a la función
		    // el objeto de la petición en crudo y código de estatus de la petición
		    error : function(xhr, status) {
		        alert('Disculpe, existió un problema');
		    },

		    // código a ejecutar sin importar si la petición falló o no
		    complete : function(xhr, status) {
		        alert('Petición realizada');
		    }
		});
		});
	</script>
=======
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
@endsection
