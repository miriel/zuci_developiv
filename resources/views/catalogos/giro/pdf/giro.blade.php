<!DOCTYPE html>
<html>
<head>
	<title>HTML to API - Invoice</title>
	<!-- <link rel="stylesheet" href="sass/main.css" media="screen" charset="utf-8"/> -->
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
	<meta http-equiv="content-type" content="text-html; charset=utf-8">
{!! Html::style('assets/css/pdf.css') !!}
</head>

<body>
	<header class="clearfix">
		<div class="container">
			<figure>
				<img src="{{ url('assets/img/logo.png') }}"alt="">
				</figure>
			<div class="company-info">
				<h2 class="title">Analisis De Riesgo</h2>
				<!-- <span>455 Foggy Heights, AZ 85004, US</span>
				<span class="line"></span>
				<a class="phone" href="tel:602-519-0450">(602) 519-0450</a>
				<span class="line"></span>
				<a class="email" href="mailto:company@example.com">company@example.com</a> -->
			</div>
		</div>
	</header>

	<section>

		<table class="table-wrappe">
			<thead>
				<tr>
					<th class="service">ID</th>
					<th class="desc">Producto</th>
				</tr>
			</thead>
			<tbody>
				@foreach($giro as $giros)
				<tr>
					<td class="service">{{$giros->cotfk }}</td>
					<td class="desc">{{ $giros->cotname }}</td>
				</tr>
				@endforeach
			</tbody>
		</table><br>
	</section>

	<!-- <footer>
		<div class="container">
			<div class="thanks">Thank you!</div>
			<div class="notice">
				<div>NOTICE:</div>
				<div>A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
			</div>
			<div class="end">Invoice was created on a computer and is valid without the signature and seal.</div>
		</div>
	</footer> -->

</body>

</html>
