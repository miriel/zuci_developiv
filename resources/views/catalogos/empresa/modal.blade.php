<!-- Modal -->
<div class="modal fade" id="agregaEmpresa" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class="letracolor">Agregar Empresa</h4>
        <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <div class="form-group">
              <!-- Mensajes exitosos -->
              <div id="msj-success" class="alert alert-success" role="alert" style="display:none">
                  Empresa agregado correctamente
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>

              {!! Form::label('ctname','Nombre') !!}
              {!! Form ::text('ctname',null, ['class'=>'form-control','placeholder'=>'Nombre del la Empresa'])!!}
              <div id="msj-error" class=" alert-danger" role="alert" style="display:none">
                  <strong class="help-block blanco" id="msj"></strong>
              </div>

            </div>
      </div>
      <div class="modal-footer">
        <input type="button" class="btn btn-outline btn-success" id="btnCreateEmpresa" value="Guardar" />                                                        
      </div>
    </div>

  </div>
</div>

<!-- Large modal -->
