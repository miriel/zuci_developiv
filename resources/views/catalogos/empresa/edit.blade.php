@extends('layouts.adm')
@section('content')


<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar Empresa</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Editar Empresa
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            {!! Form::open(['route'=>['empresa.update',$empresa],'method'=>'PUT'])!!}
                              <div class="form-group">
                                {!! Form::label('ctname','Nombre') !!}
                                {!! Form ::text('ctname',$empresa->ctname, ['class'=>'form-control','required'])!!}
                                <span class="help-block">
                                  <p class="alert-danger">{{$errors->first('ctname')}}</p>
                                </span>
                              </div>
                              <div class="form-group">
                                {!! Form::submit('Editar',['class'=>'btn bt-primary']) !!}
                              </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>



@stop
