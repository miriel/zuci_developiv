@extends('layouts.adm')
	@section('content')
	@include('catalogos.cotizador.modal.edit')
		@include('catalogos.cotizador.modal.editp')

			<div class="inner-wrapper">
				<section role="main" class="content-body pb-0">
          @include('flash::message')
            @if(Session::has('flash_success'))
            <div class="alert alert-success" role="alert">
              {{Session::get('flash_success')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            @include('flash::message')
            @if(Session::has('flash_delete'))
            <div class="alert alert-warning " role="alert">
              {{Session::get('flash_delete')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
					<div class="col-md-12">
								<section class="card card-featured card-featured-primary mb-4">
									<header class="card-header">
										<div class="card-actions">
                  	</div>
										<h2 class="card-title blanco">Administrador de Precios Cotizador</h2>

									</header>
									<div class="card-body">
										<!-- <code>.card-featured.card-featured-primary</code> -->
										<div class="	">
							<div class="tabs tabs-primary">
								<ul class="nav nav-tabs">
									<li class="nav-item active">
										<a class="nav-link active" href="#altas" data-toggle="tab"> Roles</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#primarios" data-toggle="tab">Servicios Primarios</a>
									</li>
                  <li class="nav-item">
                    <a class="nav-link" href="#secundarios" data-toggle="tab">Servicios Secundarios</a>
                  </li>
								</ul>
								<div class="tab-content">
									<div  id="altas" class="tab-pane active">
										<table width="100%" class="table table-responsive-md table-sm  ">
											<thead>
													<tr>
                            <td>ID</td>
                            <td>Nombre</td>
                            <td>Prec. Unitario</td>
														<td>Acciones</td>
													</tr>
											</thead>
                      @foreach($roles as $role)
                      <tr class="odd gradeX">
                        <td>{{ $role->id }}</td>
                        <td>{{ $role->name }}</td>
                        <td>{{ $role->cost }}</td>
                        <td>
                          <a  data-toggle='modal' id="editRole" data-target='#editRole'
													 data-id='{{ $role->id }},R' class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
                        </td>
                      </tr>
												@endforeach
										</table>
									</div>
									<div id="primarios" class="tab-pane ">
										<table width="100%" class="table table-responsive-md table-sm  ">
											<thead>
													<tr>
														<td>ID</td>
														<td>Nombre</td>
														<td>Prec. Unitario</td>
														<td>Acciones</td>
													</tr>
											</thead>
											@foreach($primarios as $prima)
											<tr class="odd gradeX">
												<td>{{ $prima->qufk }}</td>
												<td>{{ $prima->quname }}</td>
												<td>{{ $prima->qucost }}</td>
												<td>
														<!-- <a href="{{ route('admincotizador.edit',$role)}}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a> -->
													<a data-toggle='modal' id="editRole" data-target='#editRole'
													 data-id='{{ $prima->qufk }},P' class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
												</td>
											</tr>
												@endforeach
										</table>
									</div>

									<div id="secundarios" class="tab-pane ">
										<table width="100%" class="table table-responsive-md table-sm  ">
											<thead>
													<tr>
														<td>ID</td>
														<td>Nombre</td>
														<td>Prec. Unitario</td>
														<td>Acciones</td>
													</tr>
											</thead>
											@foreach($secundarios as $secun)
											<tr class="odd gradeX">
												<td>{{ $secun->qufk }}</td>
												<td>{{ $secun->quname }}</td>
												<td>{{ $secun->qucost }}</td>
												<td>
														<!-- <a href="{{ route('admincotizador.edit',$role)}}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a> -->
													<a  data-toggle='modal' id="editRole" data-target='#editRole'
													 data-id='{{ $secun->qufk }},P' class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
												</td>
											</tr>
												@endforeach
										</table>
									</div>

								</div>

							</div>
						</div>
									</div>
								</section>
							</div>



					<!-- end: page -->
				</section>
			</div>
		@stop
		@section('scripts')
			{!!Html::script('js/Generics.js')!!}
			{!!Html::script('js/admin/cotizador.js')!!}
		@endsection
