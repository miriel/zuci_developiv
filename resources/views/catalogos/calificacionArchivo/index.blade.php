@extends('layouts.adm')
@section('content')
    @include('catalogos.calificacionArchivo.modal.edit')
    @include('catalogos.calificacionArchivo.modal.create')
    <div class="inner-wrapper">
      <section role="main" class="content-body pb-0">

            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            @include('flash::message')
            {{Session::has('message')}}
            @if(Session::has('message'))
                <div class="alert alert-success" role="alert">
                  {{Session::get('message')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
            @endif
            @if(Session::has('flash_delete'))
                <div class="alert alert-warning " role="alert">
                  {{Session::get('flash_delete')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
            @endif
        <br>
        @include('flash::message')
        @if(Session::has('flash_success'))
            <div class="alert alert-success" role="alert">
              {{Session::get('flash_success')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
        <div class="col-md-12">
              <section class="card card-featured card-featured-primary mb-4">
                <header class="card-header">
                  <div class="card-actions">
                    @can('create',new App\Company_Type)
                      <button type="button" class="btn btn-outline btn-link agregar" data-toggle='modal'
                        data-target='#agregarTipoCalificacion' style="color:#fff;">	<i class="fa fa-plus"> 	Agregar</i></button>
                        @endcan
                  </div>

                  <h2 class="card-title blanco">Calificación de archivos</h2>
                    <input type="hidden" name="status" id="status" value="{{ $status }}" />
                </header>
                <div class="card-body">
                  <!-- <code>.card-featured.card-featured-primary</code> -->
                  <div>
                    <div class="tabs tabs-primary">
                      <ul class="nav nav-tabs">
                        <li class="nav-item active">
                          <a class="nav-link active" href="#altas" data-toggle="tab"><i class="fa fa-star"></i> Altas</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#bajas" data-toggle="tab">Bajas</a>
                        </li>
                      </ul>
                      <div class="tab-content">
                        <div  id="altas" class="tab-pane active">
                          <table width="100%" class="table table-responsive-md table-sm  ">
                            <thead>
                                <tr>
                                  <td><b>ID</b></td>
                                  <td><b>Descripción larga</b></td>
                                  <td><b>Descripción corta</b></td>
                                  <td><b>Estatus</b></td>
                                  <td><b>Acción</b></td>
                                </tr>
                            </thead>
                            <tbody>
                          <?php $a = 1 ?>
                          @foreach($tiposCalificacionActivos as $tiposActivos)
                          <?php $status = ($tiposActivos->cmstatus == 1) ? 'Activo' : 'Inactivo'; ?>
                                  <tr class="odd gradeX">
                                    <td>{{ $a }}</td>
                                    <td>{{ $tiposActivos->cmdesc}}</td>
                                    <td>{{ $tiposActivos->cmabbr }}</td>
                                    <td>{{ $status }}</td>
                                    <td>
                                      <div class="row">
                                      @can('delete',new App\Company_Type)
                                      <div class="col-md-1 col-md-offset-1">
                                        {!!Form::open(['route'=>['calificacionArchivo.destroy',$tiposActivos->cmfk,'status'=>$tiposActivos->cmstatus],'method'=>'DELETE'])!!}
                                        {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-xs btn-default eliminar','title'=>'Inactivar', 'style'=>'cursor:pointer']) }}
                                        {!!Form::close()!!}
                                      </div>
                                      @endcan
                                      @can('update',new App\Company_Type)
                                      <div class="col-md-1 col-md-offset-1">
                                        <a data-toggle='modal' id='editCalificacionArchivo' data-target='#editCalificacionArchivo' rel="{{ $tiposActivos->cmfk }}" style='cursor:pointer' class='btn btn-xs btn-default editar text-center'>
                                          <i class="fa fa-edit"></i>
                                        </a>
                                      </div>
                                      @endcan
                                    </div>
                                    </td>
                                  </tr>
                                <?php  $a++ ?>
                              @endforeach
                              </tbody>
                          </table>
                          <div class=" pull-right">

                          </div>
                        </div>
                        <div id="bajas" class="tab-pane fade">
                          <table width="100%" class="table table-responsive-md table-sm">
                            <thead>
                                <tr>
                                  <td><b>ID</b></td>
                                  <td><b>Descripción larga</b></td>
                                  <td><b>Descripción corta</b></td>
                                  <td><b>Estatus</b></td>
                                  <td><b>Acción</b></td>
                                </tr>
                            </thead>
                            <tbody>
                              <?php $a = 1 ?>
                              @foreach($tiposCalificacionInactivo as $tiposInactivos)
                              <?php $status = ($tiposInactivos->cmstatus == 1) ? 'Activo' : 'Inactivo'; ?>
                                      <tr class="odd gradeX">
                                        <td>{{ $a }}</td>
                                        <td>{{ $tiposActivos->cmdesc}}</td>
                                        <td>{{ $tiposActivos->cmabbr }}</td>
                                        <td>{{ $status }}</td>
                                        <td>
                                          <div class="row">
                                            @can('delete',new App\Company_Type)
                                            <div class="col-md-1 col-md-offset-1">
                                              {!!Form::open(['route'=>['calificacionArchivo.destroy',$tiposInactivos->cmfk,'status'=>$tiposInactivos->cmstatus],'method'=>'DELETE'])!!}
                                              {{ Form::button('<i class="fa fa-check"></i>', ['type' => 'submit', 'class' => 'btn btn-xs btn-default','title'=>'Activar','style'=>'cursor:pointer']) }}
                                              {!!Form::close()!!}
                                            </div>
                                            @endcan
                                            @can('update',new App\Company_Type)
                                            <div class="col-md-1 col-md-offset-1">
                                              <a data-toggle='modal' id="editCalificacionArchivo" rel="{{ $tiposInactivos->cmfk }}" data-target='#editCalificacionArchivo' style='cursor:pointer' class="btn btn-xs btn-default editar text-center">
                                                <i class="fa fa-edit"></i>
                                              </a>
                                            </div>
                                            @endcan
                                          </div>
                                        </td>
                                      </tr>
                                    <?php  $a++ ?>
                                  @endforeach
                            </tbody>
                          </table>
                          <div class=" pull-right">

                          </div>
                      </div>
                    </div>
                    </div>
                </div>
              </section>
            </div>
      </section>
    </div>
@stop
@section('scripts')
    {!!Html::script('js/Generics.js')!!}
    {!!Html::script('js/calificacionArchivo.js')!!}
@endsection
