<!-- Modal -->
<div class="modal fade" id="editCalificacionArchivo" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class=" letracolor">Editar calificación de archivo</h4>
          <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <!-- Mensajes exitosos -->
            <div id="msj-successEd" class="alert alert-success" role="alert" style="display:none">
                Calificación actualizada correctamente.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <input type="hidden" id="ctfk" />
            <input type="hidden" id="cmfk" value=""/>
            <?php  ?>
            {!! Form::label('cmdesc','Descripción larga') !!}
            {!! Form ::text('cmdesc', null, ['id'=> 'cmdesc','class'=>'form-control','placeholder'=>'Agrega una descripción corta'])!!}
            <div id="msj-errorEdit1" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjEd1"></strong>
            </div>
            <br/>
            {!! Form::label('cmabbr','Descripción corta') !!}
            {!! Form ::text('cmabbr', null, ['id'=> 'cmabbr','class'=>'form-control','placeholder'=>'Agrega una descripción larga'])!!}
            <div id="msj-errorEdit2" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjEd2"></strong>
            </div>
            <br/>
            {!! Form::label('cmstatus','Estatus') !!}
            <br/>
            {!! Form ::select('cmstatus', array('A' => 'Activo', 'I' => 'Inactivo'), null,['id'=>'cmstatus'])!!}
            <div class="modal-footer">
              <input type="button" class="btn btn-primary" id="btnEditCalificacion" value="Actualizar" />
            </div>
      </div>
    </div>
  </div>
</div>
