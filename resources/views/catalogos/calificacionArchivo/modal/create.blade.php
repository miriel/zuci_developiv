<!-- Modal -->
<div class="modal fade" id="agregarTipoCalificacion" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class="letracolor">Agregar tipo de calificación</h4>
        <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <div class="form-group">
              <!-- Mensajes exitosos -->
              <div id="msj-success" class="alert alert-success" role="alert" style="display:none">
                  Calificación creada correctamente.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>

              {!! Form::label('cmdesc','Descripción larga') !!}
              {!! Form ::text('cmdesc',null, ['id'=>'cmdescA','class'=>'form-control','placeholder'=>'Agrega una descripción corta'])!!}
              <div id="msj-errorAlta1" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msj1">La respuesta es requerida</strong>
              </div>
              </br>
              {!! Form::label('cmabbr','Descripción corta') !!}
              {!! Form ::text('cmabbr',null, ['id'=>'cmabbrA','class'=>'form-control','placeholder'=>'Agrega una descripción larga'])!!}
              <div id="msj-errorAlta2" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msj2">La respuesta es requerida</strong>
              </div>
              </br>
              {!! Form::label('cmstatus','Estatus') !!}
              <br/>
              {!! Form ::select('cmstatus', array('A' => 'Activo', 'I' => 'Inactivo'), 'A',['id'=>'cmstatusA'])!!}
          </div>
      </div>
      <div class="modal-footer">
        <input type="button" class="btn btn-outline btn-success" id="btnCreateTipoCalificacion" value="Guardar" />
      </div>
    </div>

  </div>
</div>

<!-- Large modal -->
