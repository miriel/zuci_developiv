@extends('layouts.adm')
@section('content')


<div id="page-wrapper">
  <br>

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>Editar Plan</h3>
                </div>
                <!-- {!!Form::button('<i class="fa fa-plus"></i> Agregar description', ['id' => 'addDescripcion','type' => 'submit','class' => 'btn btn-success', 'title'=>'Agregar descripccion'])!!} -->

                <div class="panel-body">
                    <div class="row">
                        @foreach($planes as  $planes )
                      {!! Form::open(['route'=>['planes.update',$planes->pgfk],'method'=>'PUT'])!!}

                        <div class="col-lg-10">
                              <div class="form-group">
                                {!! Form::label('nombre','Nombre') !!}
                                {!! Form ::text('pgname',$planes->pgname, ['class'=>'form-control','required'])!!}
                                <span class="help-block">
                                  <p class="alert-danger">{{$errors->first('pgname')}}</p>
                                </span>
                              </div>
                        </div>
                        <div class="col-lg-10">
                              <div class="form-group">
                                <?php
                                     $descrPlan = explode(",",$planes->pgdesc);
                                     foreach($descrPlan as $desc){ ?>
                                       {!! Form::label('description','Descripcion del Plan') !!}
                                       {!! Form ::text('pgdesc', $desc  ,['name'=>'pgdesc[]','class'=>'form-control'])!!}
                                       <?PHP
                                     }
                                     ?>
                                <span class="help-block">
                                  <p class="alert-danger">{{$errors->first('pgdesc')}}</p>
                                </span>
                                <table class="table" id="tablaPlanes">
                                      <thead>
                                          <tr>
                                              <th>

                                              </th>
                                           </tr>
                                      </thead>
                                </table>
                              </div>
                        </div>
                        <div class="col-lg-10">
                          <div class="form-group">
                              {!! Form::label('price','Precio del Plan') !!}
                              <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
                              <div class="input-group">
                                <div class="input-group-addon">$</div>
                                <input type="text" class="form-control" id="pgprice" name="pgprice" value="{{ $planes->pgprice}}" placeholder="Amount">
                                <div class="input-group-addon">.00</div>
                              </div>
                              @if($planes->pghowprice == 1)
                                <input  class="form-group"type="radio" id="pghowprice" name="pghowprice"value="true" checked>Habilitar</input>
                                <input type="radio" name="pghowprice"id="pghowprice" value="false">Deshabilitar</input>
                              @else
                                <input  class="form-group"type="radio" id="pghowprice"name="pghowprice"  value="true">Habilitar</input>
                                <input type="radio" name="pghowprice"id="pghowprice"  value="false"checked>Deshabilitar</input>
                              @endif
                              <span class="help-block">
                                <p class="alert-danger">{{$errors->first('pghowprice')}}</p>
                              </span>
                              </div>
                        </div>
                        <div class="col-lg-4">
                          <div class="form-group">
                            {!! Form::submit('Editar Plan',['class'=>'btn bt-success ']) !!}
                          </div>
                        </div>

                        {!! Form::close() !!}
                        @endforeach
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>

@stop
@section('scripts')
    {!!Html::script('js/admin/addpaquetes.js')!!}
@endsection
