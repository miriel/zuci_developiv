<div class="row">
    <div class="col-lg-10">
<div class="form-group">
  {!! Form::label('pgname','Nombre del Plan') !!}
  {!! Form ::text('pgname',null, ['id'=>'pgname','class'=>'form-control'])!!}
  <div id="valida">
    <span class="msj-error">
      <p class="alert-danger" id="msj"></p>
    </span>
  </div>

</div>
<div class="form-group">
  <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
  <div class="input-group">
    <div class="input-group-addon">$</div>
    <input type="text" class="form-control" id="pgprice" name="pgprice" placeholder="Amount">
    <div class="input-group-addon">.00</div>
  </div>
  <div id="valida">
    <span class="msj-error2">
      <p class="alert-danger" id="msj2"></p>
    </span>
  </div>
     <input  class="form-group"type="radio" id="radioButton"name="pghowprice" value="true">Habilitar</input>
     <input type="radio" name="pghowprice"id="radioButton"value="false">Deshabilitar</input>
     <div id="valida">
       <span class="msj-error3">
         <p class="alert-danger" id="msj3"></p>
       </span>
     </div>
  </div>
  <div class="form-group">
    {!! Form::label('pgdesc','Descripcion del Plan') !!}
    {!! Form ::text('pgdesc',null, ['id'=>'pgdesc[]','name'=>'pgdesc[]','class'=>'pgdesc form-control'])!!}
    <div id="valida">
      <span class="msj-error4">
        <p class="alert-danger" id="msj4"></p>
      </span>
    </div>
  </div>
    </div>
      </div>
