@extends('layouts.adm')
@section('content')

<div id="page-wrapper">

    <!-- /.r-ow -->
    <br>
    <div class="row">
      <div class="alert alert-success"id="msj-success" role="alert" style="display:none">
      <strong> Ha sido creada con exito!</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>

  

        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>Agregar nuevo Plan</h3>
                </div>
                <div class="panel-body">

                    <!-- {!!Form::button('<i class="fa fa-plus"></i> Agregar Nombre', ['id' => 'addNombre','type' => 'submit', 'class' => 'btn btn-success', 'title'=>'Agregar nombre'])!!} -->
                    {!!Form::button('<i class="fa fa-plus"></i> Agregar description', ['id' => 'addDescripcion','type' => 'submit','class' => 'btn btn-success', 'title'=>'Agregar descripccion'])!!}

                    <div class="row">
                        <div class="col-lg-12" id="miFormulario">
                          {!! Form::open()!!}

                        <input type="hidden" name="_token" id="token"value="{{ csrf_token() }}">
                          @include('catalogos.planes.form.preguntas')
                          <table class="table" id="tablaPlanes" >
                                <thead>
                                    <tr>
                                        <th >

                                        </th>
                                     </tr>
                                </thead>
                          </table>
                          <div class="form-group">
                            {!!link_to('#', $title='Registrar',$attributes=['id'=>'registro','class'=>'btn btn-primary'],$secure = null)!!}
                          </div>
                            {!! Form::close() !!}
                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>


@stop
@section('scripts')
    {!!Html::script('js/admin/addpaquetes.js')!!}
    {!! Html::script('js/admin/crearplan.js') !!}
@endsection
