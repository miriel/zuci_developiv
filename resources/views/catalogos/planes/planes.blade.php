@extends('layouts.adm')

@section('content')


    <div id="page-wrapper">
		<div  class="row">
      <div class="col-lg-12 mayusculas">
          <h3 class="page-header">planes</h3>

      <div class="row mayusculas ">
        @foreach( $planes as $dt )
        <div class="col-lg-3">
          <div class="panel panel-primary color">
            <div class="panel-heading border">
              <h4 style="color: #ffffff;">{{ $dt->pgname}} </h4>
             </div>
              <div class="panel-body color1">
                <?php
                $des = explode(",",$dt->pgdesc);
                foreach($des as $descri){ ?>
                  <p class="blanco"><i class="fa fa-check"></i> {{ $descri}} </p>
                  <?php
                }
                 ?>
                <hr>
                @if ($dt->pghowprice == 1)
                <p class="blanco"><i class="fa fa-dollar-sign"></i> $ {{ $dt->pgprice}} /por mes/año </p>
                @else
                  <p class="blanco"><i class="fa fa-dollar-sign"></i></p>
                @endif
                 <a class="btn btn-success before  btn-block border"  href="#" role="button"> COMPRAR</a>
              </div>
           </div>
         </div>
          @endforeach
      </div>
    </div>
      </div>
  	</div>
@stop
