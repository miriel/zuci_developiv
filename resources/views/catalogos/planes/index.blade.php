@extends('layouts.adm')
@section('content')


      <div id="page-wrapper">
              <!-- /.col-lg-12 -->
              <div class="row"><br>
                @include('flash::message')
                  @if(Session::has('flash_success'))
                  <div class="alert alert-success" role="alert">
                    {{Session::get('flash_success')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  @endif
                  @include('flash::message')
                  @if(Session::has('flash_delete'))
                  <div class="alert alert-warning " role="alert">
                    {{Session::get('flash_delete')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  @endif

                <br>
                  <div class="col-lg-12">
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              Tipos de planes
                          </div>
                          <!-- /.panel-heading -->
                          <div class="panel-body">
                              <!-- {!! Form::open(['route'=> 'planes.index','method'=>'GET','class'=>'navbar-form navbar-left pull-right','role'=>'search'])!!}
                              <div class="form-group">
                                {!! Form::text('name', null, ['class'=>'form-control','placeholder'=>'Search'])!!}
                              </div>
                              <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                              {!! Form::close() !!} -->
                            <div class="table">
                              <table class="table table-striped">
                                <thead>
                                    <tr>
                                      <td>ID</td>
                                      <td>Nombre</td>
                                      <td>Estatus</td>
                                      <td>Accion</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($planes as $plan)
                                    <tr class="odd gradeX">
                                      <td>{{ $plan->pgfk }}</td>
                                      <td>{{ $plan->pgname}}</td>
                                      <td>
                                      @if( $plan->pgstatus == 1)
                                          <button type="button" class="btn btn-outline btn-success">Activo</button>
                                      @else
                                          <button type="button" class="btn btn-outline btn-danger">Inactivo</button>
                                      @endif
                                      </td>
                                      <td>
                                        <div class="row">
                                            <div class="col-lg-1 col-lg-offset-1">
                                                {!!Form::open(['route'=> ['planes.destroy',$plan->pgfk, 'status' => $plan->pgstatus],'method'=>'DELETE'])!!}
                                                    @if( $plan->pgstatus == 1)
                                                        {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger','title'=>'Inactivar']) }}
                                                    @else
                                                        {{ Form::button('<i class="fa fa-check"></i>', ['type' => 'submit', 'class' => 'btn btn-success','title'=>'Activar']) }}
                                                    @endif
                                                {!!Form::close()!!}
                                            </div>
                                            <div class="col-lg-1 col-lg-offset-1">
                                              <a href="{{ route('planes.edit',$plan->pgfk)}}"class="btn btn-primary"> <i class="fa fa-edit"></i> </a>
                                            </div>
                                      </div>
                                      </td>
                                    </tr>
                                        @endforeach
                                </tbody>
                              </table>
                              <div class=" pull-right">
                                  {{ $planes->render() }}
                              </div>

                            </div>


                              <!-- <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                  <thead>
                                      <tr>
                                        <td>ID</td>
                                        <td>Nombre</td>
                                        <td>Estatus</td>
                                        <td>Accion</td>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      @foreach($planes as $plan)
                                      <tr class="odd gradeX">
                                        <td>{{ $plan->pgfk }}</td>
                                        <td>{{ $plan->nombre}}</td>
                                        <td>
                                        @if( $plan->pgstatus == 1)
                                            <button type="button" class="btn btn-outline btn-success">Activo</button>
                                        @else
                                            <button type="button" class="btn btn-outline btn-danger">Inactivo</button>
                                        @endif
                                        </td>
                                        <td>
                                          <div class="row">
                                              <div class="col-lg-1 col-lg-offset-1">
                                                  {!!Form::open(['route'=> ['planes.destroy',$plan->pgfk, 'status' => $plan->pgstatus],'method'=>'DELETE'])!!}
                                                      @if( $plan->pgstatus == 1)
                                                          {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger','title'=>'Inactivar']) }}
                                                      @else
                                                          {{ Form::button('<i class="fa fa-check"></i>', ['type' => 'submit', 'class' => 'btn btn-success','title'=>'Activar']) }}
                                                      @endif
                                                  {!!Form::close()!!}
                                              </div>
                                              <div class="col-lg-1 col-lg-offset-1">
                                                <a href="{{ route('planes.edit',$plan->pgfk)}}"class="btn btn-primary"> <i class="fa fa-edit"></i> </a>
                                              </div>
                                        </div>
                                        </td>
                                      </tr>
                                          @endforeach
                                  </tbody>
                              </table> -->
                          </div>
                          <!-- /.panel-body -->
                      </div>
                      <!-- /.panel -->
                  </div>
                  <!-- /.col-lg-12 -->
              </div>
          </div>
        </div>



@stop
