<!-- Modal -->
<div class="modal fade" id="agregarTipo" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class="letracolor">Agregar tipo de archivo</h4>
        <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <div class="form-group">
              <!-- Mensajes exitosos -->
              <div id="msj-success" class="alert alert-success" role="alert" style="display:none">
                  Empresa agregado correctamente
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>

              {!! Form::label('catvalue','Nombre del archivo') !!}
              {!! Form ::text('catValueAlta',null, ['id'=>'catValueAlta','class'=>'form-control','placeholder'=>'Nombre del archivo'])!!}
              <div id="msj-errorAlta1" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msj1">La respuesta es requerida</strong>
              </div>
              </br>
              {!! Form::label('catcronym','Acrónimo') !!}
              {!! Form ::text('catCronymAlta',null, ['id'=>'catCronymAlta','class'=>'form-control','placeholder'=>'Acrónimo del archivo'])!!}
              <div id="msj-errorAlta2" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msj2">La respuesta es requerida</strong>
              </div>
              </br>
              {!! Form::label('catstatus','Estatus') !!}
              <br/>
              {!! Form ::select('status', array('A' => 'Activo', 'I' => 'Inactivo'), 'A',['id'=>'catstatus'])!!}
          </div>
      </div>
      <div class="modal-footer">
        <input type="button" class="btn btn-outline btn-success" id="btnCreateArchivo" value="Guardar" />
      </div>
    </div>

  </div>
</div>

<!-- Large modal -->
