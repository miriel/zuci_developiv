<!-- Modal -->
<div class="modal fade" id="editArchivo" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class=" letracolor">Editar tipo de archivo</h4>
          <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <!-- Mensajes exitosos -->
            <div id="msj-successEd" class="alert alert-success" role="alert" style="display:none">
                Empresa actializada correctamente
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <input type="hidden" id="ctfk" />
            <?php  ?>
            {!! Form::label('catvalue','Nombre del archivo') !!}
            {!! Form ::text('catvalue', null, ['id'=> 'catvalue','class'=>'form-control'])!!}
            <div id="msj-errorEdit1" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjEd1"></strong>
            </div>
            <br/>
            {!! Form::label('catvalue','Acrónimo') !!}
            {!! Form ::text('catvalue', null, ['id'=> 'catacronym','class'=>'form-control'])!!}
            <div id="msj-errorEdit2" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjEd2"></strong>
            </div>
            <br/>
            {!! Form::label('catvalue','Estatus') !!}
            <br/>
            {!! Form ::select('status', array('A' => 'Activo', 'I' => 'Inactivo'), null,['id'=>'catstatus'])!!}
            <div class="modal-footer">
              <input type="button" class="btn btn-primary" id="btnEditArchivo" value="Actualizar" />
            </div>
      </div>
    </div>
  </div>
</div>
