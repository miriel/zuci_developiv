@extends('layouts.adm')
@section('content')

<div id="page-wrapper">
    <br>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Crear tipo de archivo
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                              {!! Form::open(['route'=>'empresa.store','method'=>'POST'])!!}
                              <div class="form-group">
                                {!! Form::label('ctname','Nombre de la empresa') !!}
                                {!! Form ::text('ctname',null, ['class'=>'form-control','placeholder'=>'Nombre del la Empresa'])!!}
                                <span class="help-block">
                                  <p class="alert-danger">{{$errors->first('ctname')}}</p>
                                </span>
                              </div>

                                <div class="form-group">
                                  {!! Form::submit('Registrar',['class'=>'btn btn-default']) !!}

                                </div>
                          {!! Form::close() !!}
                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>


@stop
