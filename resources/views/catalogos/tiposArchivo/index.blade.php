@extends('layouts.adm')
@section('content')
    @include('catalogos.tiposArchivo.modal')
    @include('catalogos.tiposArchivo.modal.edit')
    <div class="inner-wrapper">
      <section role="main" class="content-body pb-0">

            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            @include('flash::message')
            {{Session::has('message')}}
            @if(Session::has('message'))
                <div class="alert alert-success" role="alert">
                  {{Session::get('message')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
            @endif
            @if(Session::has('flash_delete'))
                <div class="alert alert-warning " role="alert">
                  {{Session::get('flash_delete')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
            @endif
        <br>
        @include('flash::message')
        @if(Session::has('flash_success'))
            <div class="alert alert-success" role="alert">
              {{Session::get('flash_success')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
        <div class="col-md-12">
              <section class="card card-featured card-featured-primary mb-4">
                <header class="card-header">
                  <div class="card-actions">
                    @can('create',new App\Company_Type)
                      <button type="button" class="btn btn-outline btn-link agregar" data-toggle='modal'
                        data-target='#agregarTipo' style="color:#fff;">	<i class="fa fa-plus"> 	Agregar</i></button>
                        @endcan
                  </div>

                  <h2 class="card-title blanco">Tipos de archivos</h2>
                    <input type="hidden" name="status" id="status" value="{{ $status }}" />
                </header>
                <div class="card-body">
                  <!-- <code>.card-featured.card-featured-primary</code> -->
                  <div>
                    <div class="tabs tabs-primary">
                      <ul class="nav nav-tabs">
                        <li class="nav-item active">
                          <a class="nav-link active" href="#altas" data-toggle="tab"><i class="fa fa-star"></i> Altas</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#bajas" data-toggle="tab">Bajas</a>
                        </li>
                      </ul>
                      <div class="tab-content">
                        <div  id="altas" class="tab-pane active">
                          <table width="100%" class="table table-responsive-md table-sm  ">
                            <thead>
                                <tr>
                                  <td><b>ID</b></td>
                                  <td><b>Nombre</b></td>
                                  <td><b>Acrónimo</b></td>
                                  <td><b>Estatus</b></td>
                                  <td><b>Acción</b></td>
                                </tr>
                            </thead>
                            <?php $a = 1 ?>
                            <tbody>
                          @foreach($tiposArchivo as $tipos)
                              <?php $status = ($tipos->catstatus == 1) ? 'Activo' : 'Inactivo'; ?>
                                  <tr class="odd gradeX">
                                    <td>{{ $a }}</td>
                                    <td id="text">{{ $tipos->catvalue }}</td>
                                    <td>{{ $tipos->catacronym }}</td>
                                    <td>{{ $status }}</td>
                                      <td class="">
                                          <div class="row">
                                              @can('delete',new App\Company_Type)
                                              <div class="col-md- col-md-offset-1">
                                                {!!Form::open(['route'=>['tipoArchivo.destroy',$tipos->catfk,'status'=>$tipos->catstatus],'method'=>'DELETE'])!!}
                                                {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-xs btn-default','title'=>'Inactivar']) }}
                                                {!!Form::close()!!}
                                              </div>
                                              @endcan
                                              @can('update',new App\Company_Type)
                                              <div class="col-md-1 col-md-offset-1">
                                                <a data-toggle='modal' id="editArchivo" data-target='#editArchivo' rel="{{ $tipos->catfk }}" class="btn btn-xs btn-default editar">
                                                  <i class="fa fa-edit"></i>
                                                </a>
                                              </div>
                                              @endcan
                                        </div>
                                      </td>
                                  </tr>
                                <?php $a++ ?>
                              @endforeach
                              </tbody>
                          </table>
                          <div class=" pull-right">
                            {{ $tiposArchivo->appends(['status' => 1])}}
                          </div>
                        </div>
                        <div id="bajas" class="tab-pane fade">
                          <table width="100%" class="table table-responsive-md table-sm">
                            <thead>
                                <tr>
                                  <td><b>ID</b></td>
                                  <td><b>Nombre</b></td>
                                  <td><b>Acrónimo</b></td>
                                  <td><b>Estatus</b></td>
                                  <td><b>Acción</b></td>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($tiposArchivoBajas as $tipos)
                                  <?php $status = ($tipos->catstatus == 1) ? 'Activo' : 'Inactivo'; ?>
                                      <tr class="odd gradeX">
                                        <td>{{ $a }}</td>
                                        <td id="text">{{ $tipos->catvalue }}</td>
                                        <td>{{ $tipos->catacronym }}</td>
                                        <td>{{ $status }}</td>
                                        <td class="">
                                            <div class="row">
                                                @can('delete',new App\Company_Type)
                                                <div class="col-md- col-md-offset-1">
                                                  {!!Form::open(['route'=>['tipoArchivo.destroy',$tipos->catfk,'status'=>$tipos->catstatus],'method'=>'DELETE'])!!}
                                                  {{ Form::button('<i class="fa fa-check"></i>', ['type' => 'submit', 'class' => 'btn btn-xs btn-default','title'=>'Activar']) }}
                                                  {!!Form::close()!!}
                                                </div>
                                                @endcan
                                                @can('update',new App\Company_Type)
                                                <div class="col-md-1 col-md-offset-1">
                                                  <a data-toggle='modal' id="editArchivo" data-target='#editArchivo' rel="{{ $tipos->catfk }}" class="btn btn-xs btn-default editar">
                                                    <i class="fa fa-edit"></i>
                                                  </a>
                                                </div>
                                                @endcan
                                          </div>
                                        </td>
                                      </tr>
                                    <?php $a++ ?>
                                  @endforeach
                            </tbody>
                          </table>
                          <div class=" pull-right">
                            {{ $tiposArchivoBajas->appends(['status' => 0])}}
                          </div>
                      </div>
                    </div>
                    </div>
                </div>
              </section>
            </div>
      </section>
    </div>
@stop
@section('scripts')
    {!!Html::script('js/Generics.js')!!}
    {!!Html::script('js/tipoArchivo.js')!!}
@endsection
