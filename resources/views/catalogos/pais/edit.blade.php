@extends('layouts.adm')
@section('content')


<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar País</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Editar País
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            {!! Form::open(['route'=>['pais.update',$pais],'method'=>'PUT'])!!}
                              <div class="form-group">
                                {!! Form::label('cnname','Nombre') !!}
                                {!! Form ::text('cnname',$pais->cnname, ['class'=>'form-control','required'])!!}
                                <span class="help-block">
                                  <p class="alert-danger">{{$errors->first('cnname')}}</p>
                                </span>
                              </div>
                              <div class="form-group">
                                {!! Form::submit('Editar',['class'=>'btn bt-primary']) !!}
                              </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>


@stop
