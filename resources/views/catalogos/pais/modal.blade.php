<!-- Modal -->
<div class="modal fade" id="agregapais" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class=" letracolor">Agregar Pais</h4>
        <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <div class="form-group">
              <!-- Mensajes exitosos -->
              <div id="msj-success" class="alert alert-success" role="alert" style="display:none">
                  Pais agregado correctamente
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>

              {!! Form::label('cnname','Nombre Pais') !!}
              {!! Form ::text('cnname',null, ['class'=>'form-control'])!!}
              <div id="msj-error" class=" alert-danger" role="alert" style="display:none">
                  <strong class="help-block blanco" id="msj"></strong>
              </div>

            </div>
      </div>
      <div class="modal-footer">
        <input type="button" class="btn btn-outline btn-success" id="btnCreatePais" value="Guardar" />
      </div>
    </div>

  </div>
</div>

<!-- Large modal -->
