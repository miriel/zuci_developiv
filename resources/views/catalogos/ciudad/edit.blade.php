@extends('layouts.adm')
@section('content')


<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar Ciudad</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Editar ciudad
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            {!! Form::open(['route'=>['ciudad.update',  $ciudad ],'method'=>'PUT'])!!}
                              <div class="form-group">
                                {!! Form::label('description','Nombre') !!}
                                {!! Form ::text('ciname',$ciudad->ciname,['class'=>'form-control','ciudad'=>'id','required'])!!}
                                <span class="help-block">
                                  <p class="alert-danger">{{$errors->first('ciname')}}</p>
                                </span>
                              </div>
                              <div class="form-group">
                                {!! Form::label('cilada','Lada') !!}
                                {!! Form ::text('cilada',$ciudad->cilada,['class'=>'form-control','ciudad'=>'id','required'])!!}
                                <span class="help-block">
                                  <p class="alert-danger">{{$errors->first('cilada')}}</p>
                                </span>
                              </div>
                              <div class="form-group">
                                {!! Form::submit('Editar',['class'=>'btn bt-primary']) !!}
                              </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

</div>



@stop
