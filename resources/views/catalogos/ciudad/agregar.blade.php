@extends('layouts.adm')
	@section('content')

			<div class="inner-wrapper">
				<section role="main" class="content-body pb-0">

          @include('flash::message')
            @if(Session::has('flash_success'))
            <div class="alert alert-success" role="alert">
              {{Session::get('flash_success')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif

					<div class="col-md-12">
								<section class="card card-featured card-featured-primary mb-4">

									<div class="card-body">
										<!-- <code>.card-featured.card-featured-primary</code> -->
										<div class="row	">
                      <div class="col-lg-3">

                      </div>
                      <div class="col-md-6">
  							<section class="card card-primary mb-4">
  								<header class="card-header">
  									<!-- <div class="card-actions">
  										<a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
  										<a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
  									</div> -->

  									<h2 class="card-title text-center"> Agregar Ciudad a País</h2>
  								</header>
  								<div class="card-body">
                    <div class="panel-body">
                      {!! Form::open(['route'=>'agrega.store','method'=>'POST'])!!}

                          <div class="form-group">
                            <div class="row">
                              <div class="col-lg-6">
                                  <div class="form-group">
                              <label for="" class="control-label">País</label><br>
                              {!!Form::select('cccnfk', $arryVals['pais'], null, ['class'=>'nice-select form-control','id'=>'pais','placeholder'=>'SELECCIONA']) !!}
                              <span class="help-block">
                                  <p class=" alert-danger">{{$errors->first('country')}}</p>
                              </span>
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                              <label for="" class="form-group control-label">Ciudad</label><br>
                              {!!Form::select('cccifk', $arryVals['ciudad'], null, ['class'=>'nice-select form-control col-lg-6','id'=>'ciudad','placeholder'=>'SELECCIONA']) !!}
                              <span class="help-block">
                                  <p class=" alert-danger">{{$errors->first('city')}}</p>
                              </span>
                            </div>
                        </div>
                      </div>
                    </div>

                        <div class="form-group">
                          {!! Form::submit('Agregar',['class'=>'btn bt-success']) !!}
                        </div>
                      {!! Form::close() !!}

                      </div>
  								</div>
  							</section>
  						        </div>


						        </div>
									</div>
								</section>
							</div>



					<!-- end: page -->
				</section>
			</div>




			@stop
