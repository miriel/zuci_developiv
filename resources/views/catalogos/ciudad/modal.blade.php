<!-- Modal -->
<div class="modal fade" id="agregarCiudad" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class=" letracolor">Agregar Ciudad a País</h4>
        <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <div class="form-group">
              <!-- Mensajes exitosos -->
              <div id="msj-success" class="alert alert-success" role="alert" style="display:none">
                  Ciudad agregado correctamente
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>

              <div class="form-group">
                {!! Form::label('ciname','Nombre de la Ciudad') !!}
                {!! Form ::text('ciname',null, ['class'=>'form-control','required'])!!}
                <div id="msj-error" class=" alert-danger" role="alert" style="display:none">
                    <strong class="help-block blanco" id="msj"></strong>
                </div>
              </div>
              <div class="form-group">
                {!! Form::label('cilada','Lada de la Ciudad') !!}
                {!! Form ::text('cilada',null, ['class'=>'form-control','required'])!!}
                <div id="msj-error2" class=" alert-danger" role="alert" style="display:none">
                    <strong class="help-block blanco" id="msj2"></strong>
                </div>
              </div>

            </div>
      </div>
      <div class="modal-footer">
        <input type="button" class="btn btn-outline btn-success" id="btnCreateCiudad" value="Guardar" />
      </div>
    </div>
  </div>
</div>

<!-- Large modal -->
