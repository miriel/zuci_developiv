@extends('layouts.adm')
@section('content')

<div id="page-wrapper">
    <br>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Crear Ciudad
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                              {!! Form::open(['route'=>'ciudad.store','method'=>'POST'])!!}
                              <div class="form-group">
                                {!! Form::label('ciname','Nombre de la Ciudad') !!}
                                {!! Form ::text('ciname',null, ['class'=>'form-control','required'])!!}
                                <span class="help-block">
                                  <p class="alert-danger">{{$errors->first('ciname')}}</p>
                                </span>
                              </div>
                              <div class="form-group">
                                {!! Form::label('cilada','Lada de la Ciudad') !!}
                                {!! Form ::text('cilada',null, ['class'=>'form-control','required'])!!}
                                <span class="help-block">
                                  <p class="alert-danger">{{$errors->first('cilada')}}</p>
                                </span>
                              </div>

                                <div class="form-group">
                                  {!! Form::submit('Registrar',['class'=>'btn btn-default']) !!}

                                </div>
                          {!! Form::close() !!}
                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>


@stop
