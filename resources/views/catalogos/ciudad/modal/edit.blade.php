<!-- Modal -->
<div class="modal fade" id="editCiudad" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class=" letracolor">Editar Ciudad</h4>
          <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <!-- Mensajes exitosos -->
            <div id="msj-successEd" class="alert alert-success" role="alert" style="display:none">
                Ciudad actializada correctamente
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <input type="hidden" id="cifk" />
            {!! Form::label('cinamed','Nombre') !!}
            {!! Form ::text('cinamed', null, ['id'=> 'cinamed','class'=>'form-control'])!!}
            <div id="msj-errorEd" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjEd"></strong>
            </div>
            {!! Form::label('ciladad','Lada') !!}
            {!! Form ::text('ciladad', null, ['id'=> 'ciladad','class'=>'form-control'])!!}
            <div id="msj-errorEdd" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjEdd"></strong>
            </div>
            <div class="modal-footer">
              <input type="button" class="btn btn-primary" id="btnEditCiudad" value="Actualizar" />
            </div>
      </div>
    </div>
  </div>
</div>
