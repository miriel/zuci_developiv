@extends('layouts.admin')
	@section('content')
	<div class="inner-wrapper">
		<section role="main" class="content-body pb-0">
			<div class="col-md-12">
				<section class="card card-featured card-featured-primary mb-12">
					<div class="card-body">
						<div class="row">
	            <div class="col">
	              <section class="card form-wizard" id="w4">

	                <div class="card-body">
	                  <div class="wizard-progress wizard-progress-lg">
	                    <div class="steps-progress">
	                      <div class="progress-indicator" style="width: 0%;"></div>
	                    </div>
	                    <ul class="nav wizard-steps">
	                      <li class="nav-item active">
	                        <a class="nav-link" href="#w4-account" data-toggle="tab"><span>1</span>Datos Personales</a>
	                      </li>
	                      <li class="nav-item">
	                        <a class="nav-link" href="#w4-profile" data-toggle="tab"><span>2</span>Datos de Empresa</a>
	                      </li>
	                      <li class="nav-item">
	                        <a class="nav-link" href="#w4-billing" data-toggle="tab"><span>3</span>Tipo de Usuario</a>
	                      </li>
												<li class="nav-item">
	                        <a class="nav-link" href="#w5-billing" data-toggle="tab"><span>4</span>Rol</a>
	                      </li>
	                      <li class="nav-item">
	                        <a class="nav-link" href="#w4-confirm" data-toggle="tab"><span>5</span>Confirmation</a>
	                      </li>
	                    </ul>
	                  </div>

	                  <form class="form-horizontal p-3" action="{{ route('user.store')}}" method="POST" novalidate="novalidate">
											  {{ csrf_field()}}
	                    <div class="tab-content">
	                      <div id="w4-account" class="tab-pane active">
	                        <div class="form-group row has-error">
	                          <label class="col-sm-4 control-label text-sm-right pt-1" for="w4-username">Nombre <span style="color:red;">*</span> </label>
	                          <div class="col-sm-4">
	                            <input type="text" class="form-control" name="name" id="w4-username" value="{{ old('name')}}" required="" aria-invalid="true">
																<label id="w4-username-error" class="error" for="w4-username"></label>
																	<label  class="error" >{{$errors->first('name')}}</label>
														</div>
														<div class="col-sm-4">
																<label  class="error" >{{$errors->first('email')}}</label>
																<label  class="error" >{{$errors->first('wkrkarea')}}</label>
																<label  class="error" >{{$errors->first('job')}}</label>
																<label  class="error" >{{$errors->first('lada')}}</label>
																<label  class="error" >{{$errors->first('phone1')}}</label>
																<label  class="error" >{{$errors->first('codigoGestor')}}</label>
														</div>
	                        </div>
													<div class="form-group row has-error">
	                          <label class="col-sm-4 control-label text-sm-right pt-1" for="w4-username">Apellido Paterno <span style="color:red;">*</span></label>
	                          <div class="col-sm-4">
	                            <input type="text" class="form-control" name="apaternal" id="w4-username" value="{{ old('apaternal')}}" required="" aria-invalid="true">
																<label id="w4-username-error" class="error" for="w4-username"></label>
																	<label  class="error" >{{$errors->first('apaternal')}}</label>
														</div>
	                        </div>
													<div class="form-group row has-error">
	                          <label class="col-sm-4 control-label text-sm-right pt-1" for="w4-username">Apellido Materno <span style="color:red;">*</span></label>
	                          <div class="col-sm-4">
	                            <input type="text" class="form-control" name="amaternal" id="w4-username" value="{{ old('amaternal')}}" required="" aria-invalid="true">
																<label id="w4-username-error" class="error" for="w4-username"></label>
																	<label  class="error" >{{$errors->first('amaternal')}}</label>
														</div>
	                        </div>
	                      </div>
	                      <div id="w4-profile" class="tab-pane">
													<div class="form-group row has-error">
	                          <label class="col-sm-4 control-label text-sm-right pt-1" for="w4-username">&Aacuterea a la que pertenece <span style="color:red;">*</span></label>
	                          <div class="col-sm-4">
	                            <input type="text" class="form-control" name="wkrkarea" id="w4-username" value="{{ old('wkrkarea')}}" required="" aria-invalid="true">
																<label id="w4-username-error" class="error" for="w4-username"></label>
																<span class="help-block">
																		<label  class="error" >{{$errors->first('wkrkarea')}}</label>
																</span>
														</div>
	                        </div>
													<div class="form-group row has-error">
	                          <label class="col-sm-4 control-label text-sm-right pt-1" for="w4-username">Puesto que desempeña <span style="color:red;">*</span></label>
	                          <div class="col-sm-4">
	                            <input type="text" class="form-control" name="job" id="w4-username" value="{{ old('job')}}" required aria-invalid="true">
																<label id="w4-username-error" class="error" for="w4-username"></label>
																<label  class="error" >{{$errors->first('job')}}</label>
														</div>
	                        </div>
													<div class="form-group row has-error">
														<label class="col-sm-4 control-label text-sm-right pt-1" for="w4-username">Lada / Telefono Fijo de la empresa <span style="color:red;">*</span></label>
														<div class="col-sm-4">
															<div class="input-group">
															{!!Form::text('lada', null, ['id'=>'lada','class'=>'col-lg-2  nice-select form-control ','placeholder'=>'Lada','required']) !!}
															 {!!Form::text('phone1', null, ['name'=>'phone1','id'=>'phone1','class'=>'  nice-select form-control ','placeholder'=>'Telefono','required']) !!}
																 </div>
																 <label  class="error" >{{$errors->first('phone1')}}</label>
																 <label  class="error" >{{$errors->first('lada')}}</label>
																 	<label id="w4-username-error" class="error" for="w4-username"></label>
															</div>
														</div>
	                      </div>

	                      <div id="w4-billing" class="tab-pane">
													<div class="form-group row has-error">
														<label class="col-sm-4 control-label text-sm-right pt-1" for="w4-username">Selecciona Tipo de usuario <span style="color:red;">*</span></label>
														<div class="col-sm-4">
																<select required class="form-control" name="tipouser" id="tipouser" >
																	<option value="">Selecciona</option>
																	<option value="1">Interno</option>
																	<option value="2">Externo</option>
																</select>
														</div>
	                      	</div>
													<div class="form-group row has-error">
														<label class="col-sm-4 control-label text-sm-right pt-1" for="w4-username">Codigo <span style="color:red;">*</span> <i class="fa fa-question margin" style="color:red;" data-toggle="tooltip" data-placement="right"
														title="El codigo puede ser alfanumerico (letras y numeros) tiene que contener minimo 4 caracteres maximo 8"></i></label>
														<div class="col-sm-4">
															<input type="text" class="form-control" name="codigoGestor" id="w4-username" value="{{ old('codigoGestor')}}" required="" aria-invalid="true">
																<label id="w4-username-error" class="error" for="w4-username"></label>
																<label  class="error" >{{$errors->first('codigoGestor')}}</label>
														</div>
													</div>
													<div class="form-group row has-error">
														<label class="col-sm-4 control-label text-sm-right pt-1" for="w4-username">Elige un supervisor <span style="color:red;">*</span></label>
														<div class="col-sm-4">
															<select name="supervisor" id="supervisor" class="form-control" required>
																	<option disabled selected>Elige el Supervisor</option>
																	@foreach($lista as $user)
																	<option value="{{$user->id}}">{{$user->name}} {{$user->apaternal}} {{$user->amaternal}}</option>
																	@endforeach
															</select>
														</div>
	                      	</div>
												</div>

												<div id="w5-billing" class="tab-pane">
													<div class="form-group row has-error">
														<label class="col-sm-4 control-label text-sm-right pt-1" for="w4-username">Selecciona un Rol <span style="color:red;">*</span></label>
														<div class="col-sm-4">
																@include('catalogos.user.checkboxes')
																	<label id="w4-username-error" class="error" for="w4-username"></label>
														</div>
	                      	</div>

											 </div>

	                      <div id="w4-confirm" class="tab-pane">
	                        <div class="form-group row">
	                          <label class="col-sm-3 control-label text-sm-right pt-1" for="w4-email">Email <span style="color:red;">*</span></label>
	                          <div class="col-sm-9">
	                            <input type="text" class="form-control" name="email" id="w4-email" value="{{ old('email')}}"required="">
															  <span class="help-block">la contraseña sera generada y  enviara por correo </span>
	                          </div>
														<label  class="error" >{{$errors->first('email')}}</label>
	                        </div>
	                        <div class="form-group row">
	                          <div class="col-sm-3"></div>
	                          <div class="col-sm-9">
	                            <div class="checkbox-custom">
	                              <input type="checkbox" name="terms" id="w4-terms" required="">
	                              <label for="w4-terms">Estoy de acuerdo con los términos de servicio</label>
	                            </div>
	                          </div>
	                        </div>
													<div class="form-group row">
														<div class="col-sm-9"></div>
	                          <div class="col-sm-3">
															<div class="col-sm-3 finish  float-right">
															<button type="submit" class="mb-1 mt-1 mr-1 btn btn-success" >Crear usuario</button>
															</div>
	                          </div>
	                        </div>
	                      </div>
	                    </div>
	                  </form>
	                </div>
	                <div class="card-footer">
	                  <ul class="pager">
	                    <li class="previous disabled">
	                      <a><i class="fa fa-angle-left"></i> Previous</a>
	                    </li>
	                    <li class="finish hidden float-right">
												  <!-- <button type="submit" class="btn btn-primary" >Crear usuario</button> -->
	                      <!-- <a>Finish</a> -->
	                    </li>
	                    <li class="next">
	                      <a>Next <i class="fa fa-angle-right"></i></a>
	                    </li>
	                  </ul>
	                </div>
	              </section>
	            </div>
	          </div>
					</div>
				</section>
			</div>
		</section>
	</div>


	@stop
	@section('scripts')
		{!!Html::script('js/Generics.js')!!}
	  {!!Html::script('admin/js/permisos.js')!!}

	@endsection
