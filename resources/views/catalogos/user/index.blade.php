@extends('layouts.adm')
	@section('content')


			<div class="inner-wrapper">
				<section role="main" class="content-body pb-0">

          @include('flash::message')
            @if(Session::has('flash_success'))
            <div class="alert alert-success" role="alert">
              {{Session::get('flash_success')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            @include('flash::message')
            @if(Session::has('flash_delete'))
            <div class="alert alert-warning " role="alert">
              {{Session::get('flash_delete')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
					<div class="col-md-12">
								<section class="card card-featured card-featured-primary mb-4">
									<header class="card-header">
										<div class="card-actions">
                      @can('create', $users->first())
                      <a href="{{ route('user.create') }}" class=" btn-outline btn-success">Crear Usuario</a>
                      <!-- <button type="" class="btn btn-outline btn-link" href="{{ route('user.create') }}"
                       style="color:#fff;">	<i class="fa fa-plus"> 	Agregar</i></button> -->
                      @endcan
										</div>
										<h2 class="card-title blanco">Usuarios</h2>

									</header>
									<div class="card-body">
										<!-- <code>.card-featured.card-featured-primary</code> -->
										<div class="	">
							<div class="tabs tabs-primary">
								<ul class="nav nav-tabs">
									<li class="nav-item active">
										<a class="nav-link active" href="#altas" data-toggle="tab"><i class="fa fa-star"></i> usuarios</a>
									</li>
									<!-- <li class="nav-item">
										<a class="nav-link" href="#bajas" data-toggle="tab">Bajas</a>
									</li> -->
								</ul>
								<div class="tab-content">
									<div  id="altas" class="tab-pane active">
										<table width="100%" class="table table-responsive-md table-sm  ">
											<thead>
													<tr>
                            <td>ID</td>
                            <td>Nombre</td>
                            <td>email</td>
                            <td>Roles</td>
                            <td>Acciones</td>
													</tr>
											</thead>
                      @foreach($users as $user)
                      @can('view',$user)
                      <tr class="odd gradeX">
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->roles->pluck('display_name')->implode(', ') }}</td>
                        <td>
                          @can('view', $user)
                          <a href="{{ route('user.show',$user)}}" class="btn btn-xs btn-default"> <i class="fa fa-eye"></i> </a>
                          @endcan

                          @can('update', $user)
                          <a href="{{ route('user.edit',$user)}}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
                          @endcan
													@can('update', $user)
													<a href="{{ route('miscuestionarios.index') }}" class="btn btn-xs btn-default"> <i class="fa fa-book"></i> </a>
													@endcan
                          @can('delete', $user)
                          <form class="" method="POST"
                          action="{{ route('user.destroy', $user) }}"
                          style="display:inline">
                          {{ csrf_field() }}{{ method_field('DELETE')}}
                          <button class="btn btn-xs btn-danger"
                          onclick="return confirm('Estas seguro de querer eliminar este usuario?')">
                        <i class="fa fa-times"></i> </button>
                          </form>
                          @endcan


                        </td>
                      </tr>
                        @endcan
                  </tbody>
												@endforeach
										</table>
										<div class=" pull-right">

										</div>
									</div>

								</div>
							</div>
						</div>
									</div>
								</section>
							</div>



					<!-- end: page -->
				</section>
			</div>
		@stop
