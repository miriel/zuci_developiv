@extends('layouts.adm')
	@section('content')

			<div class="loader"></div>
			<div class="inner-wrapper">
				<section role="main" class="content-body pb-0">

          @include('flash::message')
            @if(Session::has('flash_success'))
            <div class="alert alert-success" role="alert">
              {{Session::get('flash_success')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif

					<div class="col-md-12">
								<section class="card card-featured card-featured-primary mb-4">

									<div class="card-body">
										<!-- <code>.card-featured.card-featured-primary</code> -->
										<div class="row	">
                      <div class="col-md-6">
  							<section class="card card-primary mb-4">
  								<header class="card-header">
  									<!-- <div class="card-actions">
  										<a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
  										<a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
  									</div> -->

  									<h2 class="card-title"> Datos personales</h2>
  								</header>
  								<div class="card-body">
                    <div class="panel-body">
                          <div class="col-lg-3">
                              <p>Nombre del usuario:</p>
                               {{ $user->name }}
                          </div>
                          <div class="col-lg-3">
                            @if ($user->roles->count())
                              <p>Perfil del usuario: </p>
                              {{ $user->getRoleNames()->implode(', ') }}
                              @endif
                          </div>
                          <div class="col-lg-3">
                            <p>Email</p>
                            {{ $user->email }}
                          </div>
                      </div>
  								</div>
  							</section>
  						        </div>
                        <div class="col-md-3">
          							<section class="card card-primary mb-4">
          								<header class="card-header">
          									<!-- <div class="card-actions">
          										<a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
          										<a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
          									</div> -->

          									<h2 class="card-title">  Listado de roles y permisos</h2>
          								</header>
          								<div class="card-body">
                            <div class="panel-body">
                              @forelse ($user->roles as $role)
                                <strong>{{ $role->name }}</strong>
                                @if ($role->permissions->count())
                                <br>
                                <small class="text-muted">Permisos: {{ $role->permissions->pluck('name')->implode(', ')}} </small>
                                 @endif

                                @unless($loop->last)
                                <hr>
                                @endunless
                                @empty
                                <small class="text-muted">No tiene roles asociados</small>
                              @endforelse

                              </div>
          								</div>
          							</section>
  						          </div>
                          <div class="col-md-3">
                          <section class="card card-primary mb-4">
                            <header class="card-header">
                              <h2 class="card-title">   Permisos extra</h2>
                            </header>
                            <div class="card-body">
                              <div class="panel-body">
                                @forelse ($user->permissions as $permission)
                                  <strong>{{ $permission->name }}</strong>
                                   <br>
                                  @unless($loop->last)
                                  <hr>
                                  @endunless
                                  @empty
                                  <small class="text-muted">No tiene permisos adicionales</small>
                                @endforelse
                                </div>
                            </div>
                          </section>
                          </div>

						        </div>
									</div>
								</section>
							</div>



					<!-- end: page -->
				</section>
			</div>



			<script type="text/javascript">
			$(window).load(function() {
			    $(".loader").fadeOut("slow");
			});
			</script>

			@stop
