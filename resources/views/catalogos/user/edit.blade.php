@extends('layouts.adm')
@section('content')
<div class="inner-wrapper">
<section role="main" class="content-body pb-0">
   @include('flash::message')
   @if(Session::has('flash_success'))
   <div class="alert alert-success" role="alert">
      {{Session::get('flash_success')}}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
   </div>
   @endif
   <div class="col-md-12">
      <section class="card card-featured card-featured-primary mb-4">
         <div class="card-body">
            <div class="row">
               <div class="col-lg-12">
                  <div class="tabs tabs-vertical tabs-left">
                     <ul class="nav nav-tabs">
                        <li class="nav-item active">
                           <a class="nav-link" href="#editar" data-toggle="tab"><i class="fa fa-address-card "></i> Editar</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="#roles" data-toggle="tab"><i class="fa fa-low-vision "></i> Roles</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="#giro" data-toggle="tab"><i class="fa fa-wrench"></i> Permiso Generales</a>
                        </li>
                     </ul>
                     <div class="tab-content">
                        <div id="editar" class="tab-pane active">
                           <h4><strong>Datos personales</strong> </h4>
                           <form class="" action="{{ route('user.update', $user)}}" method="POST">
                              {{ csrf_field()}} {{ method_field('PUT')}}
                              <div class="row">
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                       <label for="name" class="control-label">Nombre:</label>
                                       <input type="text" name="name" class="form-control mayusculas" id="inputSuccess"value="{{ old('name',$user->name)}}" disabled>
                                       <span class="help-block">
                                          <p class="alert-danger">{{$errors->first('name')}}</p>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                       <label for="apaternal" class="control-label">Apellido Paterno:</label>
                                       <input type="text" name="apaternal" class="form-control mayusculas" id="inputSuccess"value="{{ old('name',$user->apaternal)}}" disabled>
                                       <span class="help-block">
                                          <p class="alert-danger">{{$errors->first('apaternal')}}</p>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                       <label for="amaternal" class="control-label">Apellido Materno:</label>
                                       <input type="text" name="amaternal" class="form-control mayusculas" id="inputSuccess"value="{{ old('name',$user->amaternal)}}" disabled>
                                       <span class="help-block">
                                          <p class="alert-danger">{{$errors->first('amaternal')}}</p>
                                       </span>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-8">
                                    <div class="form-group">
                                       <label for="name"  class="control-label">Email:</label>
                                       <input type="email" name="email" class="form-control mayusculas" value="{{ old('email',$user->email)}}"disabled>
                                       <span class="help-block">
                                          <p class="alert-danger">{{$errors->first('email')}}</p>
                                       </span>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-5">
                                    <div class="form-group">
                                       <label for="password"  class="control-label">Password:</label>
                                       <input type="password" name="password" class="form-control mayusculas" value="">
                                       <span class="help-block"> Deja en blanco para no cambiar la contraseña</span>
                                       <span class="help-block">
                                          <p class="alert-danger">{{$errors->first('password')}}</p>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="col-lg-5">
                                    <div class="form-group">
                                       <label for="password_confirmation"  class="control-label">Repite la contraseña:</label>
                                       <input type="password" name="password_confirmation" class="form-control mayusculas" value="">
                                       <span class="help-block">
                                          <p class="alert-danger">{{$errors->first('password')}}</p>
                                       </span>
                                    </div>
                                 </div>
                              </div>
                              <button class="btn btn-primary" name="button">Actualizar Información</button>
                           </form>
                        </div>
                        <div id="roles" class="tab-pane">
                           <h4><strong>Roles </strong></h4>
                           <div class="row">
                              <div class="col-lg-6">
                                 @role('Admin')
                                 <form class="" action="{{ route('user.roles.update',$user)}}" method="POST">
                                    {{ csrf_field() }} {{ method_field('PUT') }}
                                    @include('catalogos.user.checkboxes')
                                    <button class="btn btn-primary">Actualizar roles</button>
                                 </form>
                                 @else
                                 <ul class="list-group">
                                 @forelse ($user->roles as $role)
                                 <li class="list-group-item">{{ $role->name }}</li>
                                 @empty
                                 <li class="list-group-item">No tiene roles</li>
                                 @endforelse
                                 @endrole
                              </div>
                           </div>
                        </div>
                        <div id="giro"  class="tab-pane">
                           <div id="nameUser" style="padding-left:5px; background-color:lavender; height:39px; border-radius:3px; padding-top:1px; width:50%;">
                              <h5>Permisos del usuario <b>{{$user->name.' '.$user->apaternal.' '.$user->amaternal}}</b></h5>
                           </div>
                           </br>
                           		<div class="row">
																<div class="col-md-6">
																	<h4><strong>Permisos de Generales</strong> </h4>
																</div>
																<div class="col-md-6">
																	<div class="text-right" style="margin-top:15px;">
																		 <span class="selectAllAll" style="cursor:pointer; background-color: gainsboro; padding: 5px 5px 5px 5px; border-radius: 6px;">Seleccionar todos los permisos</span>
																	</div>
																</div>
															</div>
                              <form class="" action="{{ route('user.permissions.update',$user)}}" method="POST">
                                 {{ csrf_field() }} {{ method_field('PUT') }}
                                 @role('Admin')
                                 <div class="row">
                                    <div class="col-lg-12">
                                       <div class="accordion accordion-primary" id="accordion2Primary">
                                          <div class="card card-default">
                                             <div class="card-header">
                                                <h4 class="card-title m-0">
                                                   <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryOne">
                                                   Usuarios
                                                   </a>
                                                </h4>
                                             </div>
                                             <div id="collapse2PrimaryOne" class="collapse ">
                                                <div class="card-body">
                                                   <div class="text-right selectAll" rel="checkUser" style="cursor:pointer">
                                                      <span style=" background-color: gainsboro; padding: 5px 5px 5px 5px; border-radius: 6px;">Seleccionar todo</span>
                                                   </div>
                                                   <div class="row">
                                                      <div class="col-lg-3">
                                                         <h4> <strong>Usuarios</strong> </h4>
                                                         <input name="Todos" type="checkbox" value="1" class="check_todos checkUser checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                         @foreach ($permissions as $id	)
                                                         @if($id->tipo_catalogo == 1)
                                                         <div class="checkbox">
                                                            <label title="{{ $id->description }}">
                                                            <input name="permissions[]" class="ck checkUser checkAll"  title="" type="checkbox"value="{{ $id->name }}"
                                                            {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                            <strong>	{{ $id->name }}</strong>
                                                            </label>
                                                         </div>
                                                         @endif
                                                         @endforeach
                                                         </td>
                                                         </tr><br>
                                                      </div>
                                                      <div class="col-lg-3">
                                                         <h4> <strong>Roles</strong> </h4>
                                                         <input name="Todos" type="checkbox" value="1" class="check_todos1 checkUser checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                         @foreach ($permissions as $id	)
                                                         @if($id->tipo_catalogo == 3)
                                                         <div class="checkbox">
                                                            <label title="{{ $id->description }}">
                                                            <input name="permissions[]" class="ck3 checkUser checkAll"  type="checkbox"value="{{ $id->name }}"
                                                            {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                            <strong>	{{ $id->name }}</strong>
                                                            </label>
                                                         </div>
                                                         @endif
                                                         @endforeach
                                                         </td>
                                                         </tr><br>
                                                      </div>
                                                      <div class="col-lg-3">
                                                         <h4> <strong>Permisos</strong> </h4>
                                                         <input name="Todos" type="checkbox" value="1" class="check_4 checkUser checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                         @foreach ($permissions as $id	)
                                                         @if($id->tipo_catalogo == 4)
                                                         <div class="checkbox">
                                                            <label  title="{{ $id->description }}">
                                                            <input name="permissions[]" class="ck4 checkUser checkAll" type="checkbox"value="{{ $id->name }}"
                                                            {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                            <strong>	{{ $id->name }}</strong>
                                                            </label>
                                                         </div>
                                                         @endif
                                                         @endforeach
                                                         </td>
                                                         </tr><br>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="card card-default">
                                             <div class="card-header">
                                                <h4 class="card-title m-0">
                                                   <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryTwo">
                                                   Catálogos
                                                   </a>
                                                </h4>
                                             </div>
                                             <div id="collapse2PrimaryTwo" class="collapse">
                                                <div class="card-body">
                                                   <div class="text-right selectAll" rel="checkCatalogos" style="cursor:pointer">
                                                      <span style=" background-color: gainsboro; padding: 5px 5px 5px 5px; border-radius: 6px;">Seleccionar todo</span>
                                                   </div>
                                                   <div class="row">
                                                      <div class="col-lg-3">
                                                         <h4> <strong>Giro</strong> </h4>
                                                         <input name="Todos" type="checkbox" value="1" class="check_todo checkCatalogos checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                         @foreach ($permissions as $id	)
                                                         @if($id->tipo_catalogo == 2)
                                                         <div class="checkbox">
                                                            <label title="{{ $id->description }}">
                                                            <input name="permissions[]" class="ck2 checkCatalogos checkAll" type="checkbox"value="{{ $id->name }}"
                                                            {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                            <strong>	{{ $id->name }}</strong>
                                                            </label>
                                                         </div>
                                                         @endif
                                                         @endforeach
                                                         </td>
                                                         </tr><br>
                                                      </div>
                                                      <div class="col-lg-3">
                                                         <h4> <strong>Empresa</strong> </h4>
                                                         <input name="Todos" type="checkbox" value="1" class="check_5 checkCatalogos checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                         @foreach ($permissions as $id	)
                                                         @if($id->tipo_catalogo == 5)
                                                         <div class="checkbox">
                                                            <label title="{{ $id->description }}">
                                                            <input name="permissions[]" class="ck5 checkCatalogos checkAll"  type="checkbox"value="{{ $id->name }}"
                                                            {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                            <strong>	{{ $id->name }}</strong>
                                                            </label>
                                                         </div>
                                                         @endif
                                                         @endforeach
                                                         </td>
                                                         </tr><br>
                                                      </div>
                                                      <div class="col-lg-3">
                                                         <h4> <strong>Pais</strong> </h4>
                                                         <input name="Todos" type="checkbox" value="1" class="check_6 checkCatalogos checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                         @foreach ($permissions as $id	)
                                                         @if($id->tipo_catalogo == 6)
                                                         <div class="checkbox">
                                                            <label  title="{{ $id->description }}">
                                                            <input name="permissions[]" class="ck6 checkCatalogos checkAll" type="checkbox"value="{{ $id->name }}"
                                                            {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                            <strong>	{{ $id->name }}</strong>
                                                            </label>
                                                         </div>
                                                         @endif
                                                         @endforeach
                                                         </td>
                                                         </tr><br>
                                                      </div>
                                                      <div class="col-lg-3">
                                                         <h4> <strong>Ciudad</strong> </h4>
                                                         <input name="Todos" type="checkbox" value="1" class="check_7 checkCatalogos checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                         @foreach ($permissions as $id	)
                                                         @if($id->tipo_catalogo == 7)
                                                         <div class="checkbox">
                                                            <label title="{{ $id->description }}">
                                                            <input name="permissions[]" class="ck7 checkCatalogos checkAll" type="checkbox"value="{{ $id->name }}"
                                                            {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                            <strong>	{{ $id->name }}</strong>
                                                            </label>
                                                         </div>
                                                         @endif
                                                         @endforeach
                                                         </td>
                                                         </tr><br>
                                                      </div>
                                                      <div class="col-lg-3">
                                                         <h4> <strong>Divisas</strong> </h4>
                                                         <input name="Todos" type="checkbox" value="1" class="check_19 checkCatalogos checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                         @foreach ($permissions as $id	)
                                                         @if($id->tipo_catalogo == 19)
                                                         <div class="checkbox">
                                                            <label title="{{ $id->description }}">
                                                            <input name="permissions[]" class="ck19 checkCatalogos checkAll" type="checkbox"value="{{ $id->name }}"
                                                            {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                            <strong>	{{ $id->name }}</strong>
                                                            </label>
                                                         </div>
                                                         @endif
                                                         @endforeach
                                                         </td>
                                                         </tr><br>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="card card-default">
                                             <div class="card-header">
                                                <h4 class="card-title m-0">
                                                   <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryThree">
                                                   Administrador de cuestionarios
                                                   </a>
                                                </h4>
                                             </div>
                                             <div id="collapse2PrimaryThree" class="collapse">
                                                <div class="card-body">
                                                   <div class="text-right selectAll" rel="checkAdminCuestionarios" style="cursor:pointer">
                                                      <span style=" background-color: gainsboro; padding: 5px 5px 5px 5px; border-radius: 6px;">Seleccionar todo</span>
                                                   </div>
                                                   <div class="row">
                                                      <div class="col-lg-3">
                                                         <h4> <strong>Tipo de cuestionario</strong> </h4>
                                                         <input name="Todos" type="checkbox" value="1" class="check_8 checkAdminCuestionarios checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                         @foreach ($permissions as $id	)
                                                         @if($id->tipo_catalogo == 8)
                                                         <div class="checkbox" >
                                                            <label title="{{ $id->description }}">
                                                            <input name="permissions[]" class="ck8 checkAdminCuestionarios checkAll" type="checkbox"value="{{ $id->name }}"
                                                            {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                            <strong>	{{ $id->name }}</strong>
                                                            </label>
                                                         </div>
                                                         @endif
                                                         @endforeach
                                                         </td>
                                                         </tr><br>
                                                      </div>
                                                      <div class="col-lg-3">
                                                         <h4> <strong>Pregunta</strong> </h4>
                                                         <input name="Todos" type="checkbox" value="1" class="check_9 checkAdminCuestionarios checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                         @foreach ($permissions as $id	)
                                                         @if($id->tipo_catalogo == 9)
                                                         <div class="checkbox">
                                                            <label title="{{ $id->description }}">
                                                            <input name="permissions[]" class="ck9 checkAdminCuestionarios checkAll" type="checkbox"value="{{ $id->name }}"
                                                            {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                            <strong>	{{ $id->name }}</strong>
                                                            </label>
                                                         </div>
                                                         @endif
                                                         @endforeach
                                                         </td>
                                                         </tr><br>
                                                      </div>
                                                      <div class="col-lg-3">
                                                         <h4> <strong>Respuesta</strong> </h4>
                                                         <input name="Todos" type="checkbox" value="1" class="check_10 checkAdminCuestionarios checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                         @foreach ($permissions as $id	)
                                                         @if($id->tipo_catalogo == 10)
                                                         <div class="checkbox">
                                                            <label title="{{ $id->description }}">
                                                            <input name="permissions[]" class="ck10 checkAdminCuestionarios checkAll" type="checkbox"value="{{ $id->name }}"
                                                            {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                            <strong>	{{ $id->name }}</strong>
                                                            </label>
                                                         </div>
                                                         @endif
                                                         @endforeach
                                                         </td>
                                                         </tr><br>
                                                      </div>
                                                      <div class="col-lg-3">
                                                         <h4> <strong>Cuestionario</strong> </h4>
                                                         <input name="Todos" type="checkbox" value="1" class="check_11 checkAdminCuestionarios checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                         @foreach ($permissions as $id	)
                                                         @if($id->tipo_catalogo == 11)
                                                         <div class="checkbox">
                                                            <label title="{{ $id->description }}">
                                                            <input name="permissions[]" class="ck11 checkAdminCuestionarios checkAll" type="checkbox"value="{{ $id->name }}"
                                                            {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                            <strong>	{{ $id->name }}</strong>
                                                            </label>
                                                         </div>
                                                         @endif
                                                         @endforeach
                                                         </td>
                                                         </tr><br>
                                                      </div>
                                                      <div class="col-lg-3">
                                                         <h4> <strong>Funciones de cuestionario</strong> </h4>
                                                         <input name="Todos" type="checkbox" value="1" class="check_12 checkAdminCuestionarios checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                         @foreach ($permissions as $id	)
                                                         @if($id->tipo_catalogo == 12)
                                                         <div class="checkbox" >
                                                            <label title="{{ $id->description }}">
                                                            <input name="permissions[]" class="ck12 checkAdminCuestionarios checkAll" type="checkbox"value="{{ $id->name }}"
                                                            {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                            <strong>	{{ $id->name }}</strong>
                                                            </label>
                                                         </div>
                                                         @endif
                                                         @endforeach
                                                         </td>
                                                         </tr><br>
                                                      </div>
                                                      <div class="col-lg-3">
                                                         <h4> <strong>Configuración de cuestionarios</strong> </h4>
                                                         <input name="Todos" type="checkbox" value="1" class="check_13 checkAdminCuestionarios checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                         @foreach ($permissions as $id	)
                                                         @if($id->tipo_catalogo == 13)
                                                         <div class="checkbox">
                                                            <label title="{{ $id->description }}">
                                                            <input name="permissions[]" class="ck13 checkAdminCuestionarios checkAll" type="checkbox"value="{{ $id->name }}"
                                                            {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                            <strong>	{{ $id->name }}</strong>
                                                            </label>
                                                         </div>
                                                         @endif
                                                         @endforeach
                                                         </td>
                                                         </tr><br>
                                                      </div>
                                                      <div class="col-lg-3">
                                                         <h4> <strong>Secciones</strong> </h4>
                                                         <input name="Todos" type="checkbox" value="1" class="check_16 checkAdminCuestionarios checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                         @foreach ($permissions as $id	)
                                                         @if($id->tipo_catalogo == 16)
                                                         <div class="checkbox">
                                                            <label title="{{ $id->description }}">
                                                            <input name="permissions[]" class="ck16 checkAdminCuestionarios checkAll" type="checkbox"value="{{ $id->name }}"
                                                            {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                            <strong>	{{ $id->name }}</strong>
                                                            </label>
                                                         </div>
                                                         @endif
                                                         @endforeach
                                                         </td>
                                                         </tr><br>
                                                      </div>
                                                   </div>
                                                   <div class="row">
                                                      <div class="col-lg-12">
                                                         <div class="accordion accordion-primary" id="accordionSecond">
                                                            <div class="card card-default">
                                                               <div class="card-header sub-card-header" style="background-color:#0033AA !important">
                                                                  <h4 class="card-title m-0">
                                                                     <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionSecond" href="#collapseDAP">
                                                                     DAP
                                                                     </a>
                                                                  </h4>
                                                               </div>
                                                               <div id="collapseDAP" class="collapse">
                                                                  <div class="card-body">
                                                                     <div class="text-right selectAll" rel="checkDap" style="cursor:pointer">
                                                                        <span style=" background-color: gainsboro; padding: 5px 5px 5px 5px; border-radius: 6px;">Seleccionar todo</span>
                                                                     </div>
                                                                     <div class="row">
                                                                        <div class="col-lg-3">
                                                                           <h4> <strong>Menus</strong> </h4>
                                                                           <input name="Todos" type="checkbox" value="1" class="check_15 checkDap checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                                           @foreach ($permissions as $id	)
                                                                           @if($id->tipo_catalogo == 21)
                                                                           <div class="checkbox">
                                                                              <label title="{{ $id->description }}">
                                                                              <input name="permissions[]" class="ck15 checkDap checkAll" type="checkbox"value="{{ $id->name }}"
                                                                              {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                                              <strong>	{{ $id->name }}</strong>
                                                                              </label>
                                                                           </div>
                                                                           @endif
                                                                           @endforeach
                                                                           </td>
                                                                           </tr><br>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="card card-default">
                                                               <div class="card-header sub-card-header" style="background-color:#0033AA !important">
                                                                  <h4 class="card-title m-0">
                                                                     <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionSecond" href="#collapseAnalisis">
                                                                     Analisis
                                                                     </a>
                                                                  </h4>
                                                               </div>
                                                               <div id="collapseAnalisis" class="collapse">
                                                                  <div class="card-body">
                                                                     <div class="text-right selectAll" rel="checkAnalisis" style="cursor:pointer">
                                                                        <span style=" background-color: gainsboro; padding: 5px 5px 5px 5px; border-radius: 6px;">Seleccionar todo</span>
                                                                     </div>
                                                                     <div class="row">
                                                                        <div class="col-lg-3">
                                                                           <h4> <strong>Menus</strong> </h4>
                                                                           <input name="Todos" type="checkbox" value="1" class="check_15 checkAnalisis checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                                           @foreach ($permissions as $id	)
                                                                           @if($id->tipo_catalogo == 18)
                                                                           <div class="checkbox">
                                                                              <label title="{{ $id->description }}">
                                                                              <input name="permissions[]" class="ck15 checkAnalisis checkAll" type="checkbox"value="{{ $id->name }}"
                                                                              {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                                              <strong>	{{ $id->name }}</strong>
                                                                              </label>
                                                                           </div>
                                                                           @endif
                                                                           @endforeach
                                                                           </td>
                                                                           </tr><br>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="card card-default">
                                             <div class="card-header">
                                                <h4 class="card-title m-0">
                                                   <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryT4">
                                                   Administrador de menús desplegables
                                                   </a>
                                                </h4>
                                             </div>
                                             <div id="collapse2PrimaryT4" class="collapse">
                                                <div class="card-body">
                                                   <div class="text-right selectAll" rel="checkAdminMenusDesplegables" style="cursor:pointer">
                                                      <span style=" background-color: gainsboro; padding: 5px 5px 5px 5px; border-radius: 6px;">Seleccionar todo</span>
                                                   </div>
                                                   <div class="row">
                                                      <div class="col-lg-3">
                                                         <h4> <strong>Menus</strong> </h4>
                                                         <input name="Todos" type="checkbox" value="1" class="check_15 checkAdminMenusDesplegables checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                         @foreach ($permissions as $id	)
                                                         @if($id->tipo_catalogo == 15)
                                                         <div class="checkbox">
                                                            <label title="{{ $id->description }}">
                                                            <input name="permissions[]" class="ck15 checkAdminMenusDesplegables checkAll" type="checkbox"value="{{ $id->name }}"
                                                            {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                            <strong>	{{ $id->name }}</strong>
                                                            </label>
                                                         </div>
                                                         @endif
                                                         @endforeach
                                                         </td>
                                                         </tr><br>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="card card-default">
                                             <div class="card-header">
                                                <h4 class="card-title m-0">
                                                   <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapse2PrimaryT5">
                                                   Auditoria
                                                   </a>
                                                </h4>
                                             </div>
                                             <div id="collapse2PrimaryT5" class="collapse">
                                                <div class="card-body">
                                                   <div class="text-right selectAll" rel="checkaAuditoria" style="cursor:pointer">
                                                      <span style=" background-color: gainsboro; padding: 5px 5px 5px 5px; border-radius: 6px;">Seleccionar todo</span>
                                                   </div>
                                                   <div class="row">
                                                      <div class="col-lg-3">
                                                         <h4> <strong>Auditoria</strong> </h4>
                                                         <input name="Todos" type="checkbox" value="1" class="check_20 checkaAuditoria checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                         @foreach ($permissions as $id	)
                                                         @if($id->tipo_catalogo == 20)
                                                         <div class="checkbox">
                                                            <label title="{{ $id->description }}">
                                                            <input name="permissions[]" class="ck20 checkaAuditoria checkAll" type="checkbox"value="{{ $id->name }}"
                                                            {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                            <strong>	{{ $id->name }}</strong>
                                                            </label>
                                                         </div>
                                                         @endif
                                                         @endforeach
                                                         </td>
                                                         </tr><br>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="card card-default">
                                             <div class="card-header">
                                                <h4 class="card-title m-0">
                                                   <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#collapseCotizador">
                                                   Administrador del cotizador
                                                   </a>
                                                </h4>
                                             </div>
                                             <div id="collapseCotizador" class="collapse">
                                                <div class="card-body">
																									<div class="text-right selectAll" rel="checkCotizador" style="cursor:pointer">
																										 <span style=" background-color: gainsboro; padding: 5px 5px 5px 5px; border-radius: 6px;">Seleccionar todo</span>
																									</div>
                                                   <div class="row">
                                                      <div class="col-lg-3">
                                                         <h4> <strong>Cotizador</strong> </h4>
                                                         <input name="Todos" type="checkbox" value="1" class="check_20 checkCotizador checkAll"/><strong>Seleccionar todos</strong><br><br>
                                                         @foreach ($permissions as $id	)
                                                         @if($id->tipo_catalogo == 22)
                                                         <div class="checkbox">
                                                            <label title="{{ $id->description }}">
                                                            <input name="permissions[]" class="ck20 checkCotizador checkAll" type="checkbox"value="{{ $id->name }}"
                                                            {{ $user->permissions->contains($id) || collect(old('permissions'))->contains($id->name)? 'checked':'' }}>
                                                            <strong>	{{ $id->name }}</strong>
                                                            </label>
                                                         </div>
                                                         @endif
                                                         @endforeach
                                                         </td>
                                                         </tr><br>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <br>
                                 <button class="btn btn-primary">Actualizar permisos</button>
                              </form>
                              @else
                              <tr>
                                 <td class="col-xs-2">
                                    <ul class="list-group">
                                    @forelse ($user->permissions as $permission)
                                    <li class="list-group-item">{{ $permission->name }}</li>
                                    @empty
                                    <li class="list-group-item">No tiene permisos</li>
                                    @endforelse
                                    @endrole
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </section>
      </div>
      <!-- end: page -->
</section>
</div>
@stop
@section('scripts')
{!!Html::script('js/admin/checked.js')!!}
<script>
   $('.selectAll').click(function(){
   	var clase = '.'+$(this).attr('rel');
   	var totalCheck = $(clase).length;

   	var totalSeleccionados = 0;
   	$(clase).each(function(){
   		if($(this).prop('checked')){
   				totalSeleccionados++;
   		}
   	});

   	if(totalSeleccionados < totalCheck){
   			$(clase).prop('checked',true);
   	}else{
   			$(clase).prop('checked',false);
   			$(clase).removeAttr("checked");
   	}
   });

	 $('.selectAllAll').click(function(){
		 var totalCheck = $('.checkAll').length;

    	var totalSeleccionados = 0;
    	$('.checkAll').each(function(){
    		if($(this).prop('checked')){
    				totalSeleccionados++;
    		}
    	});

    	if(totalSeleccionados < totalCheck){
    			$('.checkAll').prop('checked',true);
    	}else{
    			$('.checkAll').prop('checked',false);
    			$('.checkAll').removeAttr("checked");
    	}
	 });
</script>
@endsection
