@extends('layouts.adm')
	@section('content')

			<div class="loader"></div>
			<div class="inner-wrapper">
				<section role="main" class="content-body pb-0">

          @include('flash::message')
            @if(Session::has('flash_success'))
            <div class="alert alert-success" role="alert">
              {{Session::get('flash_success')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif

					<div class="col-md-12">
								<section class="card card-featured card-featured-primary mb-4">

									<div class="card-body">
										<!-- <code>.card-featured.card-featured-primary</code> -->
										<div class="row	">
                      <div class="col-lg-3">

                      </div>
                      <div class="col-md-6">
  							<section class="card card-primary mb-4">
  								<header class="card-header">
  									<!-- <div class="card-actions">
  										<a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
  										<a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
  									</div> -->

  									<h2 class="card-title"> Crear Rol</h2>
  								</header>
  								<div class="card-body">
                    <div class="panel-body">
                      <form class="" action="{{ route('roles.store')}}" method="POST">
                        @include('catalogos.roles.form')
                        <div class="form-group">
                          <button class="btn btn-primary" >Crear rol</button>
                        </div>

                      </form>

                      </div>
  								</div>
  							</section>
  						        </div>

						        </div>
									</div>
								</section>
							</div>



					<!-- end: page -->
				</section>
			</div>



			<script type="text/javascript">
			$(window).load(function() {
			    $(".loader").fadeOut("slow");
			});
			</script>

			@stop
