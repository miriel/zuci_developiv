@extends('layouts.adm')
	@section('content')

			<div class="inner-wrapper">
				<section role="main" class="content-body pb-0">
          @include('flash::message')
            @if(Session::has('flash_success'))
            <div class="alert alert-success" role="alert">
              {{Session::get('flash_success')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            @include('flash::message')
            @if(Session::has('flash_delete'))
            <div class="alert alert-warning " role="alert">
              {{Session::get('flash_delete')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
					<div class="col-md-12">
								<section class="card card-featured card-featured-primary mb-4">
									<header class="card-header">
										<div class="card-actions">
                      @can('create', $roles->first())
                      <a href="{{ route('roles.create') }}" class=" btn-primary">Crear Rol</a>
                      @endcan

										</div>

										<h2 class="card-title blanco">Roles</h2>

									</header>
									<div class="card-body">
										<!-- <code>.card-featured.card-featured-primary</code> -->
										<div class="	">
							<div class="tabs tabs-primary">
								<ul class="nav nav-tabs">
									<li class="nav-item active">
										<a class="nav-link active" href="#altas" data-toggle="tab"><i class="fa fa-star"></i> Roles</a>
									</li>
									<!-- <li class="nav-item">
										<a class="nav-link" href="#bajas" data-toggle="tab">Bajas</a>
									</li> -->
								</ul>
								<div class="tab-content">
									<div  id="altas" class="tab-pane active">
										<table width="100%" class="table table-responsive-md table-sm  ">
											<thead>
													<tr>
                            <td>ID</td>
                            <td>Identificador</td>
                            <td>Permisos</td>
                            <td>Acciones</td>
													</tr>
											</thead>
                        <tbody>
                      @foreach($roles as $role)
                      <tr class="odd gradeX">
                        <td>{{ $role->id }}</td>
                        <td>{{ $role->name }}</td>
                        <td>{{ $role->permissions->pluck('display_name')->implode(', ') }}</td>
                        <td>
                          @can('update',$role)
                          <a href="{{ route('roles.edit',$role)}}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
                          @endcan
                          @can('delete', $role)
                              @if($role->id !== 1)
                                <form class="" method="post"
                                action="{{ route('roles.destroy', $role) }}"
                                style="display:inline">
                                {{ csrf_field() }}{{ method_field('DELETE')}}
                                <button  class="btn btn-xs btn-danger" name="button"
                                onclick="return confirm('Estas seguro de querer eliminar este rol?')">
                              <i class="fa fa-times"></i> </button>
                                </form>
                              @endif
                              @endcan
                        </td>
                      </tr>
                          @endforeach
                  </tbody>
										</table>
										<div class=" pull-right">

										</div>
									</div>

								</div>
							</div>
						</div>
									</div>
								</section>
							</div>



					<!-- end: page -->
				</section>
			</div>

			@stop
