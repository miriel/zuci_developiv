{{ csrf_field()}}
<div class="form-group">
  <label for="name" class="control-label">Identificador:</label>
  @if ($role->exists)
    <input type="text"  class="form-control mayusculas" value="{{ $role->name }}"disabled>
  @else
    <input name="name" class="form-control " value="{{ old('name', $role->name ) }}">
  @endif
  <span class="help-block">
    <p class="alert-danger">{{$errors->first('name')}}</p>
  </span>
</div>
<div class="form-group">
  <label for="name" class="control-label">Nombre:</label>
  <input type="text" name="display_name" class="form-control " value="{{ old('display_name', $role->display_name) }}">
  <span class="help-block">
    <p class="alert-danger">{{$errors->first('display_name')}}</p>
  </span>
</div>



<div class="form-group col-lg-6">
  <label>Permisos</label>
  @include('catalogos.user.permissions-checkboxes',['model'=>$role])

</div><br>
