@extends('layouts.adm')
	@section('content')

			<div class="inner-wrapper">
				<section role="main" class="content-body pb-0">

          @include('flash::message')
            @if(Session::has('flash_success'))
            <div class="alert alert-success" role="alert">
              {{Session::get('flash_success')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif

					<div class="col-md-12">
								<section class="card card-featured card-featured-primary mb-4">

									<div class="card-body">
										<!-- <code>.card-featured.card-featured-primary</code> -->
										<div class="row	">
                      <div class="col-lg-3">

                      </div>
                      <div class="col-md-6">
  							<section class="card card-primary mb-4">
  								<header class="card-header">
  									<!-- <div class="card-actions">
  										<a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
  										<a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
  									</div> -->

  									<h2 class="card-title"> ver Permisos</h2>
  								</header>
  								<div class="card-body">
                    <div class="panel-body">
                          <form class="" action="{{ route('permissions.update', $permission)}}" method="POST">

                            {{ method_field('PUT')}}{{ csrf_field()}}
                            <div class="form-group">
                              <label for="name" class="control-label">Nombre:</label>
                              <input type="text" disabled class="form-control " value="{{ old('name', $permission->name ) }}">
                            </div>
                            <div class="form-group">
                              <label for="name" class="control-label">Nombre:</label>
                              <input type="text" name="display_name" class="form-control " value="{{ old('display_name', $permission->display_name) }}">
                              <span class="help-block">
                                <p class="alert-danger blanco">{{$errors->first('display_name')}}</p>
                              </span>
                            </div>
                            <div class="form-group">
                              <button class="btn btn-primary" >Actualizar permissos</button>
                            </div>
                            </form>
                      </div>
  								</div>
  							</section>
  						        </div>

						        </div>
									</div>
								</section>
							</div>



					<!-- end: page -->
				</section>
			</div>


			@stop
