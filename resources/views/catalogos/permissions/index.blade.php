@extends('layouts.adm')
	@section('content')

			<div class="inner-wrapper">
				<section role="main" class="content-body pb-0">
					<div class="row">
						<div class="col-lg-1">
						</div>
						<div class="col-lg-5">
							<section class="card">
								<header class="card-header">
									<div class="card-actions">
										<a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
									</div>
									  <h2 class="card-title blanco">Permisos para Usuarios</h2>
								</header>
								<div class="card-body">
									<table class="table table-responsive-md mb-0">
										<thead>
											<tr>
												<th>First Name</th>
											  <th>Last Name</th>
											  <th>Acciones</th>
											</tr>
										</thead>
										<tbody>
											@foreach($permissions as $permission)
											  @if($permission->tipo_catalogo == 1)
												<tr>
													<td>{{ $permission->name }}</td>
												  <td>{{ $permission->display_name }}</td>
												  <td>
												    @can('update', $permission)
												    <a href="{{ route('permissions.edit', $permission) }}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
												    @endcan
												  </td>
												</tr>
												@endif
											@endforeach
										</tbody>
									</table>
								</div>
							</section>
						</div>
						<div class="col-lg-5">
							<section class="card">
								<header class="card-header">
									<div class="card-actions">
										<a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
									</div>
									  <h2 class="card-title blanco">Permisos para Giro</h2>
								</header>
								<div class="card-body">
									<table class="table table-responsive-md mb-0">
										<thead>
											<tr>
												<th>First Name</th>
											  <th>Last Name</th>
											  <th>Acciones</th>
											</tr>
										</thead>
										<tbody>
											@foreach($permissions as $permission)
											  @if($permission->tipo_catalogo == 2)
												<tr>
													<td>{{ $permission->name }}</td>
												  <td>{{ $permission->display_name }}</td>
												  <td>
												    @can('update', $permission)
												    <a href="{{ route('permissions.edit', $permission) }}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
												    @endcan
												  </td>
												</tr>
												@endif
											@endforeach
										</tbody>
									</table>
								</div>
							</section>
						</div>
					 </div>

					 <div class="row">
 						<div class="col-lg-1">
 						</div>
 						<div class="col-lg-5">
 							<section class="card">
 								<header class="card-header">
 									<div class="card-actions">
 										<a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
 									</div>
 									  <h2 class="card-title blanco">Permisos para Roles</h2>
 								</header>
 								<div class="card-body">
 									<table class="table table-responsive-md mb-0">
 										<thead>
 											<tr>
 												<th>First Name</th>
 											  <th>Last Name</th>
 											  <th>Acciones</th>
 											</tr>
 										</thead>
 										<tbody>
 											@foreach($permissions as $permission)
 											  @if($permission->tipo_catalogo == 3)
 												<tr>
 													<td>{{ $permission->name }}</td>
 												  <td>{{ $permission->display_name }}</td>
 												  <td>
 												    @can('update', $permission)
 												    <a href="{{ route('permissions.edit', $permission) }}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
 												    @endcan
 												  </td>
 												</tr>
 												@endif
 											@endforeach
 										</tbody>
 									</table>
 								</div>
 							</section>
 						</div>
 						<div class="col-lg-5">
 							<section class="card">
 								<header class="card-header">
 									<div class="card-actions">
 										<a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
 									</div>
 									  <h2 class="card-title blanco">Permisos para Permisos</h2>
 								</header>
 								<div class="card-body">
 									<table class="table table-responsive-md mb-0">
 										<thead>
 											<tr>
 												<th>First Name</th>
 											  <th>Last Name</th>
 											  <th>Acciones</th>
 											</tr>
 										</thead>
 										<tbody>
 											@foreach($permissions as $permission)
 											  @if($permission->tipo_catalogo == 4)
 												<tr>
 													<td>{{ $permission->name }}</td>
 												  <td>{{ $permission->display_name }}</td>
 												  <td>
 												    @can('update', $permission)
 												    <a href="{{ route('permissions.edit', $permission) }}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
 												    @endcan
 												  </td>
 												</tr>
 												@endif
 											@endforeach
 										</tbody>
 									</table>
 								</div>
 							</section>
 						</div>
 					 </div>

					 <div class="row">
						 <div class="col-lg-1">
						 </div>
						 <div class="col-lg-5">
							 <section class="card">
								 <header class="card-header">
									 <div class="card-actions">
										 <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
									 </div>
										 <h2 class="card-title blanco">Permisos para Empresa</h2>
								 </header>
								 <div class="card-body">
									 <table class="table table-responsive-md mb-0">
										 <thead>
											 <tr>
												 <th>First Name</th>
												 <th>Last Name</th>
												 <th>Acciones</th>
											 </tr>
										 </thead>
										 <tbody>
											 @foreach($permissions as $permission)
												 @if($permission->tipo_catalogo == 5)
												 <tr>
													 <td>{{ $permission->name }}</td>
													 <td>{{ $permission->display_name }}</td>
													 <td>
														 @can('update', $permission)
														 <a href="{{ route('permissions.edit', $permission) }}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
														 @endcan
													 </td>
												 </tr>
												 @endif
											 @endforeach
										 </tbody>
									 </table>
								 </div>
							 </section>
						 </div>
						 <div class="col-lg-5">
							 <section class="card">
								 <header class="card-header">
									 <div class="card-actions">
										 <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
									 </div>
										 <h2 class="card-title blanco">Permisos para Pais</h2>
								 </header>
								 <div class="card-body">
									 <table class="table table-responsive-md mb-0">
										 <thead>
											 <tr>
												 <th>First Name</th>
												 <th>Last Name</th>
												 <th>Acciones</th>
											 </tr>
										 </thead>
										 <tbody>
											 @foreach($permissions as $permission)
												 @if($permission->tipo_catalogo == 6)
												 <tr>
													 <td>{{ $permission->name }}</td>
													 <td>{{ $permission->display_name }}</td>
													 <td>
														 @can('update', $permission)
														 <a href="{{ route('permissions.edit', $permission) }}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
														 @endcan
													 </td>
												 </tr>
												 @endif
											 @endforeach
										 </tbody>
									 </table>
								 </div>
							 </section>
						 </div>
					 </div>

					 <div class="row">
						 <div class="col-lg-1">
						 </div>
						 <div class="col-lg-5">
							 <section class="card">
								 <header class="card-header">
									 <div class="card-actions">
										 <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
									 </div>
										 <h2 class="card-title blanco">Permisos para Ciudad</h2>
								 </header>
								 <div class="card-body">
									 <table class="table table-responsive-md mb-0">
										 <thead>
											 <tr>
												 <th>First Name</th>
												 <th>Last Name</th>
												 <th>Acciones</th>
											 </tr>
										 </thead>
										 <tbody>
											 @foreach($permissions as $permission)
												 @if($permission->tipo_catalogo == 7)
												 <tr>
													 <td>{{ $permission->name }}</td>
													 <td>{{ $permission->display_name }}</td>
													 <td>
														 @can('update', $permission)
														 <a href="{{ route('permissions.edit', $permission) }}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
														 @endcan
													 </td>
												 </tr>
												 @endif
											 @endforeach
										 </tbody>
									 </table>
								 </div>
							 </section>
						 </div>
						 <div class="col-lg-5">
							 <section class="card">
								 <header class="card-header">
									 <div class="card-actions">
										 <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
									 </div>
										 <h2 class="card-title blanco">Permisos para Cuestionarios</h2>
								 </header>
								 <div class="card-body">
									 <table class="table table-responsive-md mb-0">
										 <thead>
											 <tr>
												 <th>First Name</th>
												 <th>Last Name</th>
												 <th>Acciones</th>
											 </tr>
										 </thead>
										 <tbody>
											 @foreach($permissions as $permission)
												 @if($permission->tipo_catalogo == 8)
												 <tr>
													 <td>{{ $permission->name }}</td>
													 <td>{{ $permission->display_name }}</td>
													 <td>
														 @can('update', $permission)
														 <a href="{{ route('permissions.edit', $permission) }}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
														 @endcan
													 </td>
												 </tr>
												 @endif
											 @endforeach
										 </tbody>
									 </table>
								 </div>
							 </section>
						 </div>
					 </div>

					 <div class="row">
						 <div class="col-lg-1">
						 </div>
						 <div class="col-lg-5">
							 <section class="card">
								 <header class="card-header">
									 <div class="card-actions">
										 <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
									 </div>
										 <h2 class="card-title blanco">Permisos para Pregunta</h2>
								 </header>
								 <div class="card-body">
									 <table class="table table-responsive-md mb-0">
										 <thead>
											 <tr>
												 <th>First Name</th>
												 <th>Last Name</th>
												 <th>Acciones</th>
											 </tr>
										 </thead>
										 <tbody>
											 @foreach($permissions as $permission)
												 @if($permission->tipo_catalogo == 9)
												 <tr>
													 <td>{{ $permission->name }}</td>
													 <td>{{ $permission->display_name }}</td>
													 <td>
														 @can('update', $permission)
														 <a href="{{ route('permissions.edit', $permission) }}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
														 @endcan
													 </td>
												 </tr>
												 @endif
											 @endforeach
										 </tbody>
									 </table>
								 </div>
							 </section>
						 </div>
						 <div class="col-lg-5">
							 <section class="card">
								 <header class="card-header">
									 <div class="card-actions">
										 <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
									 </div>
										 <h2 class="card-title blanco">Permisos para Respuesta</h2>
								 </header>
								 <div class="card-body">
									 <table class="table table-responsive-md mb-0">
										 <thead>
											 <tr>
												 <th>First Name</th>
												 <th>Last Name</th>
												 <th>Acciones</th>
											 </tr>
										 </thead>
										 <tbody>
											 @foreach($permissions as $permission)
												 @if($permission->tipo_catalogo == 10)
												 <tr>
													 <td>{{ $permission->name }}</td>
													 <td>{{ $permission->display_name }}</td>
													 <td>
														 @can('update', $permission)
														 <a href="{{ route('permissions.edit', $permission) }}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
														 @endcan
													 </td>
												 </tr>
												 @endif
											 @endforeach
										 </tbody>
									 </table>
								 </div>
							 </section>
						 </div>
					 </div>

					 <div class="row">
						 <div class="col-lg-1">
						 </div>
						 <div class="col-lg-5">
							 <section class="card">
								 <header class="card-header">
									 <div class="card-actions">
										 <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
									 </div>
										 <h2 class="card-title blanco">Permisos para Cuestionario</h2>
								 </header>
								 <div class="card-body">
									 <table class="table table-responsive-md mb-0">
										 <thead>
											 <tr>
												 <th>First Name</th>
												 <th>Last Name</th>
												 <th>Acciones</th>
											 </tr>
										 </thead>
										 <tbody>
											 @foreach($permissions as $permission)
												 @if($permission->tipo_catalogo == 11)
												 <tr>
													 <td>{{ $permission->name }}</td>
													 <td>{{ $permission->display_name }}</td>
													 <td>
														 @can('update', $permission)
														 <a href="{{ route('permissions.edit', $permission) }}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
														 @endcan
													 </td>
												 </tr>
												 @endif
											 @endforeach
										 </tbody>
									 </table>
								 </div>
							 </section>
						 </div>
						 <div class="col-lg-5">
							 <section class="card">
								 <header class="card-header">
									 <div class="card-actions">
										 <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
									 </div>
										 <h2 class="card-title blanco">Permisos para Pregunta Respuesta</h2>
								 </header>
								 <div class="card-body">
									 <table class="table table-responsive-md mb-0">
										 <thead>
											 <tr>
												 <th>First Name</th>
												 <th>Last Name</th>
												 <th>Acciones</th>
											 </tr>
										 </thead>
										 <tbody>
											 @foreach($permissions as $permission)
												 @if($permission->tipo_catalogo == 12)
												 <tr>
													 <td>{{ $permission->name }}</td>
													 <td>{{ $permission->display_name }}</td>
													 <td>
														 @can('update', $permission)
														 <a href="{{ route('permissions.edit', $permission) }}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
														 @endcan
													 </td>
												 </tr>
												 @endif
											 @endforeach
										 </tbody>
									 </table>
								 </div>
							 </section>
						 </div>
					 </div>

					 <div class="row">
						<div class="col-lg-1">
						</div>
						<div class="col-lg-5">
							<section class="card">
								<header class="card-header">
									<div class="card-actions">
										<a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
									</div>
										<h2 class="card-title blanco">Permisos para Configuracion de Cuestionario</h2>
								</header>
								<div class="card-body">
									<table class="table table-responsive-md mb-0">
										<thead>
											<tr>
												<th>First Name</th>
												<th>Last Name</th>
												<th>Acciones</th>
											</tr>
										</thead>
										<tbody>
											@foreach($permissions as $permission)
												@if($permission->tipo_catalogo == 13)
												<tr>
													<td>{{ $permission->name }}</td>
													<td>{{ $permission->display_name }}</td>
													<td>
														@can('update', $permission)
														<a href="{{ route('permissions.edit', $permission) }}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
														@endcan
													</td>
												</tr>
												@endif
											@endforeach
										</tbody>
									</table>
								</div>
							</section>
						</div>
						<div class="col-lg-5">
							<section class="card">
								<header class="card-header">
									<div class="card-actions">
										<a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
									</div>
										<h2 class="card-title blanco">Permisos para Pregunta Menus</h2>
								</header>
								<div class="card-body">
									<table class="table table-responsive-md mb-0">
										<thead>
											<tr>
												<th>First Name</th>
												<th>Last Name</th>
												<th>Acciones</th>
											</tr>
										</thead>
										<tbody>
											@foreach($permissions as $permission)
												@if($permission->tipo_catalogo == 15)
												<tr>
													<td>{{ $permission->name }}</td>
													<td>{{ $permission->display_name }}</td>
													<td>
														@can('update', $permission)
														<a href="{{ route('permissions.edit', $permission) }}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
														@endcan
													</td>
												</tr>
												@endif
											@endforeach
										</tbody>
									</table>
								</div>
							</section>
						</div>
					</div>

					<div class="row">
					 <div class="col-lg-1">
					 </div>
					 <div class="col-lg-5">
						 <section class="card">
							 <header class="card-header">
								 <div class="card-actions">
									 <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
								 </div>
									 <h2 class="card-title blanco">Permisos para Configuracion de Secciones</h2>
							 </header>
							 <div class="card-body">
								 <table class="table table-responsive-md mb-0">
									 <thead>
										 <tr>
											 <th>First Name</th>
											 <th>Last Name</th>
											 <th>Acciones</th>
										 </tr>
									 </thead>
									 <tbody>
										 @foreach($permissions as $permission)
											 @if($permission->tipo_catalogo == 16)
											 <tr>
												 <td>{{ $permission->name }}</td>
												 <td>{{ $permission->display_name }}</td>
												 <td>
													 @can('update', $permission)
													 <a href="{{ route('permissions.edit', $permission) }}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
													 @endcan
												 </td>
											 </tr>
											 @endif
										 @endforeach
									 </tbody>
								 </table>
							 </div>
						 </section>
					 </div>
					 <div class="col-lg-5">
						 <section class="card">
							 <header class="card-header">
								 <div class="card-actions">
									 <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
								 </div>
									 <h2 class="card-title blanco">Permisos para Auditor</h2>
							 </header>
							 <div class="card-body">
								 <table class="table table-responsive-md mb-0">
									 <thead>
										 <tr>
											 <th>First Name</th>
											 <th>Last Name</th>
											 <th>Acciones</th>
										 </tr>
									 </thead>
									 <tbody>
										 @foreach($permissions as $permission)
											 @if($permission->tipo_catalogo == 17)
											 <tr>
												 <td>{{ $permission->name }}</td>
												 <td>{{ $permission->display_name }}</td>
												 <td>
													 @can('update', $permission)
													 <a href="{{ route('permissions.edit', $permission) }}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
													 @endcan
												 </td>
											 </tr>
											 @endif
										 @endforeach
									 </tbody>
								 </table>
							 </div>
						 </section>
					 </div>
				 </div>

				 <div class="row">
					<div class="col-lg-1">
					</div>
					<div class="col-lg-5">
						<section class="card">
							<header class="card-header">
								<div class="card-actions">
									<a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
								</div>
									<h2 class="card-title blanco">Permisos para Analisis</h2>
							</header>
							<div class="card-body">
								<table class="table table-responsive-md mb-0">
									<thead>
										<tr>
											<th>First Name</th>
											<th>Last Name</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>
										@foreach($permissions as $permission)
											@if($permission->tipo_catalogo == 18)
											<tr>
												<td>{{ $permission->name }}</td>
												<td>{{ $permission->display_name }}</td>
												<td>
													@can('update', $permission)
													<a href="{{ route('permissions.edit', $permission) }}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
													@endcan
												</td>
											</tr>
											@endif
										@endforeach
									</tbody>
								</table>
							</div>
						</section>
					</div>
					<div class="col-lg-5">
						<section class="card">
							<header class="card-header">
								<div class="card-actions">
									<a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
								</div>
									<h2 class="card-title blanco">Permisos para Divisas</h2>
							</header>
							<div class="card-body">
								<table class="table table-responsive-md mb-0">
									<thead>
										<tr>
											<th>First Name</th>
											<th>Last Name</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>
										@foreach($permissions as $permission)
											@if($permission->tipo_catalogo == 19)
											<tr>
												<td>{{ $permission->name }}</td>
												<td>{{ $permission->display_name }}</td>
												<td>
													@can('update', $permission)
													<a href="{{ route('permissions.edit', $permission) }}" class="btn btn-xs btn-default"> <i class="fa fa-pencil"></i> </a>
													@endcan
												</td>
											</tr>
											@endif
										@endforeach
									</tbody>
								</table>
							</div>
						</section>
					</div>
				</div>
				</section>
			</div>

@stop
