<!-- Modal -->
<div class="modal fade " id="info" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class="letracolor">Multas de Consecuencia de divisa </h4>
        <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <table  class="table table-striped">
          <thead>
            <tr>
              <td>Nombre</td>
              <td>Abreviacion</td>
              <td>Multa Minima</td>
              <td>Multa Maxima</td>
              <td>Tipo de cambio</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              @foreach($divisas1 as $divas)
                <tr>
                  @if($divas->cmtpcat == '50')
                  <td>  {{ $divas->cmdesc }}</td>
                  <td>  {{ $divas->cmabbr }}</td>
                  <td> ${{ $divas->cmmin }}</td>
                  <td> ${{ $divas->cmmax }}</td>
                  @foreach($moneda as $mon)
                   @if($divas->cmmoneda == $mon->cmfk)
                    <td>  {{ $mon->cmdesc }}</td>
                    @endif
                  @endforeach

                  @endif
                  @if($divas->cmtpcat == '51')
                    @if($divas->cmtpcat == '')
                    <td>Exposicion</td>
                  <td>  {{ $divas->cmdesc }}</td>
                  <td>  {{ $divas->cmabbr }}</td>
                  <td>  {{ $divas->cmmin }}</td>
                  <td>  {{ $divas->cmmax }}</td>
                  @foreach($moneda as $mon)
                   @if($divas->cmmoneda == $mon->cmfk)
                    <td>  {{ $mon->cmdesc }}</td>
                    @endif
                  @endforeach
                    @else
                    @endif
                  @endif
                  @if($divas->cmtpcat == '52')
                    @if($divas->cmtpcat == '')
                    <td>Probabilidad</td>
                  <td>  {{ $divas->cmdesc }}</td>
                  <td>  {{ $divas->cmabbr }}</td>
                  <td>  {{ $divas->cmmin }}</td>
                  <td>  {{ $divas->cmmax }}</td>
                  @foreach($moneda as $mon)
                   @if($divas->cmmoneda == $mon->cmfk)
                    <td>  {{ $mon->cmdesc }}</td>
                    @endif
                  @endforeach
                    @else
                    @endif
                  @endif

                </tr>
              @endforeach

          </tbody>
        </table>


    </div>

  </div>
</div>

<!-- Large modal -->
