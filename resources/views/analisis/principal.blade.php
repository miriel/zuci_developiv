@extends('layouts.adm')
@section('content')
<div class="inner-wrapper">
  <section role="main" class="content-body pb-0">

    <div class="col-md-12">
          <section class="card card-featured card-featured-primary mb-4">
            <header class="card-header">
              <div class="card-actions">
                <a class="" href="{{ URL::previous() }}">
                  <button type="button" class="btn btn-outline btn-success"  style="color:#fff;">	Regresar</button>
                </a>
              </div>
                <h2 class="card-title blanco">Analisis de Riesgo </h2>
            </header>
            <div class="card-body">
              <div class="row">
                    <div class="col-lg-12">
                      <div class="accordion accordion-primary " id="accordion2Primary">
                        <!--Foreach Operacion -->
                        @foreach($analisis as $idOperacion)
                          @if($idOperacion == '')
                          <!-- <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" content="{{ csrf_token() }}" /> -->
                          <div class="card card-default">
                            <div class="card-header">
                              <h4 class="card-title m-1">
                                <a class="text-center" href="{{ route('ariesgo.store' )}}">
                                </a>
                              </h4>
                            </div>

                          </div>
                          @else
                          <!-- <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" content="{{ csrf_token() }}" /> -->
                          <div class="card card-default">
                            <div class="card-header">
                              <h4 class="card-title m-1">
                                <a class="text-center" href="{{ route('ariesgo.show',['id' => $idOperacion->idOperacion ,'folio'=> $folio,'empresa'=>$empresa] )}}">
                                {{ $idOperacion->desOperacion }}
                                </a>
                              </h4>
                            </div>

                          </div>

                          @endif
                        @endforeach
                        <!-- fin de Foreach Operacion -->
                      </div>
                    </div>
          </section>
        </div>



    <!-- end: page -->
  </section>
</div>


@stop
