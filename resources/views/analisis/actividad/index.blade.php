@extends('layouts.adm')
	@section('content')
			@include('analisis.actividad.create')
			@include('analisis.actividad.edit')

			<div class="inner-wrapper">
				<section role="main" class="content-body pb-0">
					<!-- <header class="page-header">
						<h2>Giros</h2>

						<div class="right-wrapper text-right">
							<ol class="breadcrumbs">
								<li>
									<a href="{{url("home")}}">
										<i class="fa fa-home"></i>
									</a> 
								</li>
								<li><span class="blanco"></span></li>
							</ol>

						</div>
					</header> -->
					@include('flash::message')
					@if(Session::has('message'))
							<div class="alert alert-success" role="alert">
								{{Session::get('message')}}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
					@endif
					@if(Session::has('flash_delete'))
							<div class="alert alert-warning " role="alert">
								{{Session::get('flash_delete')}}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
					@endif
					<br>
					@include('flash::message')
					@if(Session::has('flash_success'))
							<div class="alert alert-success" role="alert">
								{{Session::get('flash_success')}}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
					@endif
					<div class="col-md-12">
								<section class="card card-featured card-featured-primary mb-4">
									<header class="card-header">
										<div class="card-actions">
                      <a href="{{ url('tipocuestionario' )}}" >
                        <button type="button"class="btn btn-outline btn-link"style="color:#fff;">	<i class="fa fa-undo">	Regresar</i></button>
                      </a>

												<button type="button" class="btn btn-outline btn-link" data-toggle='modal'
												data-target='#agregaractividad' style="color:#fff;" data-id='{{$idoperacion, $cuestionarios[0]->ctfk}}'>	<i class="fa fa-plus"> 	Agregar</i></button>
													<!-- <button type="button" class="btn btn-outline btn-link" data-toggle='modal'
													data-target='#descargargiro' style="color:#fff;">	<i class="fa fa-plus"> 	Descargar layaout</i></button> -->

										</div>

										<h2 class="card-title blanco">Análisis de Riesto ({{$cuestionarios[0]->ctname}})</h2>
											<input type="hidden" name="status" id="status" value="{{  $status }}" />
									</header>
									<div class="card-body">
										<!-- <code>.card-featured.card-featured-primary</code> -->
										<div class="	">
                      <h4 class="text-center">Sub estándar</h4>
											<!-- <div class="container">
											<form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ URL::to('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
												<input type="file" name="import_file" />
												<button class="btn btn-primary">Import File</button>
											</form>
										</div> -->
											<div class="tabs tabs-primary">
												<ul class="nav nav-tabs">
													<li class="nav-item active">
														<a class="nav-link active" href="#altas" data-toggle="tab"><i class="fa fa-star"></i> Altas</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" href="#bajas" data-toggle="tab">Bajas</a>
													</li>
												</ul>
												<div class="tab-content">
													<div  id="altas" class="tab-pane active">
														<table width="100%" class="table table-responsive-md table-sm  ">
															<thead>
																	<tr>
																		<td><b>ID</b></td>
																		<td><b>IdCatálogo</b> </td>
																		<td><b>Actividad</b></td>
																		<td><b>Estatus</b></td>
																		<td><b>Acción</b></td>
																	</tr>
															</thead>
															@foreach($actividad as $activi)
																<tbody>
																		<tr class="odd gradeX">
																				<td>{{$activi->idOperacion }}</td>
																				<td>{{$activi->catalogo }}</td>
																				<td>{{$activi->desOperacion }}</td>
																				<td>
																					<img src="{{ url('admin/img/success.png') }}"alt="">
																					<!-- <button type="button" class="btn btn-success btn-circle"><i class="fa fa-check"></i></button> -->
																				</td>
																				<td class="">
																						<div class="row">
																								<div class="col-md- col-md-offset-1">
																										{!!Form::open(['route'=> ['actividad.destroy',$activi->idOperacion, 'estatus' => $activi->cmstatus],'method'=>'DELETE'])!!}
																										{{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-xs btn-default','title'=>'Inactivar']) }}
																										{!!Form::close()!!}
																								</div>
																								<div class="col-md-1 col-md-offset-1">
																									<a data-toggle='modal' id="editActividad" data-target='#editActividad'
																									 data-id='{{ $activi->idOperacion }}' class="btn btn-xs btn-default ">  <i class="fa fa-edit"></i></a>
																								</div>
																								<div class="col-md-1 col-md-offset-1">
				                                          <a  href="{{ route('descripcion.index',['id' => $activi->idOperacion, 'id2'=>$cuestionarios[0]->ctfk] )}}" class="btn btn-xs btn-default "> <i class="fa fa-plus"> Descripción</i></a>
				                                        </div>
																					</div>
																				</td>
																		</tr>
																	</tbody>
																@endforeach
														</table>
														<div class=" pull-right">
																{{ $actividad->appends(['status' => 1,'id'=>$idoperacion,'id2' =>$cuestion])}}
														</div>
													</div>
													<div id="bajas" class="tab-pane fade">
														<table width="100%" class="table table-responsive-md table-sm 	">
															<thead>
																	<tr>
																		<td><b>ID</b></td>
																		<td><b>IdCatálogo</b> </td>
																		<td><b>Actividad</b></td>
																		<td><b>Estatus</b></td>
																		<td><b>Acción</b></td>
																	</tr>
															</thead>
																@foreach($actividadbaja as $activibaja)
																<tbody>
																		<tr class="odd gradeX">
                                      <td>{{$activibaja->idOperacion }}</td>
																			<td>{{$activibaja->catalogo }}</td>
                                      <td>{{ $activibaja->desOperacion }}</td>
																				<td>
																					<img src="{{ url('admin/img/danger.png') }}"alt="">
																				</td>
																				<td>
																						<div class="row">

																								<div class="col-md- col-md-offset-">
																									{!!Form::open(['route'=> ['actividad.destroy',$activibaja->idOperacion, 'estatus' => $activibaja->cmstatus],'method'=>'DELETE'])!!}
																										{{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-xs btn-default','title'=>'Inactivar']) }}
																										{!!Form::close()!!}
																								</div>
																								<div class="col-md-1 col-md-offset-1">
																									<a data-toggle='modal' id="editActividad" data-target='#editActividad'
																							 data-id='{{ $activibaja->idOperacion }}' class="btn btn-xs btn-default ">  <i class="fa fa-edit"></i></a>
																							</div>
																							<div class="col-md-1 col-md-offset-1">
																								<a  href="{{ route('descripcion.index',['id' => $activibaja->idOperacion, 'id2'=>$cuestionarios[0]->ctfk] )}}" class="btn btn-xs btn-default "> <i class="fa fa-plus"> Descripción</i></a>
																						  </div>
																					</div>
																				</td>
																		</tr>
																	</tbody>
																@endforeach
														</table>
														<div class=" pull-right">
																{{ $actividadbaja->appends(['status' => 0,'id'=>$idoperacion,'id2' =>$cuestion])}}
														</div>
												</div>
											</div>
											</div>

									</div>
								</section>
							</div>



					<!-- end: page -->
				</section>
			</div>

			@stop
@section('scripts')
	{!!Html::script('js/Generics.js')!!}
	{!!Html::script('js/admin/createactividad.js')!!}
	{!!Html::script('js/admin/actividad.js')!!}
@endsection
