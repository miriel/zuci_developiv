
<!-- Modal -->
<div class="modal fade" id="editActividad" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class="letracolor">Editar Actividad</h4>
        <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <!-- Mensajes exitosos -->
            <div id="msj-success" class="alert alert-success" role="alert" style="display:none">
                Operacion actializado correctamente
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <input type="hidden" id="cmfkEd" />
            {!! Form::label('cmdescEd','Nombre') !!}
            {!! Form ::text('cmdescEd', null, ['id'=> 'cmdescEd','class'=>'form-control'])!!}
            <div id="msj-errorEd" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjEd"></strong>
            </div>
            <div class="modal-footer">
              <input type="button" class="btn btn-primary" id="EditActividad" value="Actualizar" />
            </div>
      </div>
    </div>
  </div>
</div>
