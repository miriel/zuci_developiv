<!-- Modal -->
<div class="modal fade" id="agregaractividad" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class="letracolor">Agregar Actividad</h4>
        <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <div class="form-group">
              <!-- Mensajes exitosos -->
              <div id="msj-success" class="alert alert-success" role="alert" style="display:none">
                  actividad agregada correctamente
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <input type="hidden" id="idoperacion" value="{{$idoperacion}}" />
              <input type="hidden" id="cuestionarios" value="{{$cuestionarios[0]->ctfk}}" />
              {!! Form::label('cmdescCre','Nombre') !!}
              {!! Form ::text('cmdescCre',null, ['class'=>'form-control','id'=>'cmdescCre','placeholder'=>'Nombre del la Operacion'])!!}
              <div id="msj-errorCre" class=" alert-danger" role="alert" style="display:none">
                  <strong class="help-block blanco" id="msjCre"></strong>
              </div>

            </div>
      </div>
      <div class="modal-footer">
        <input type="button" class="btn btn-outline btn-success" id="btnCreateActividad" value="Guardar" />
      </div>
    </div>

  </div>
</div>

<!-- Large modal -->
