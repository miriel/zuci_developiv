@extends('layouts.adm')
	@section('content')
			@include('analisis.divisas.modal.create')
			@include('analisis.divisas.modal.edit')

			<div class="inner-wrapper">
				<section role="main" class="content-body pb-0">
					<!-- <header class="page-header">
						<h2>Giros</h2>

						<div class="right-wrapper text-right">
							<ol class="breadcrumbs">
								<li>
									<a href="{{url("home")}}">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span class="blanco"></span></li>
							</ol>

						</div>
					</header> -->
					@include('flash::message')
					@if(Session::has('message'))
							<div class="alert alert-success" role="alert">
								{{Session::get('message')}}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
					@endif
					@if(Session::has('flash_delete'))
							<div class="alert alert-warning " role="alert">
								{{Session::get('flash_delete')}}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
					@endif
					<br>
					@include('flash::message')
					@if(Session::has('flash_success'))
							<div class="alert alert-success" role="alert">
								{{Session::get('flash_success')}}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
					@endif
					<div class="col-md-12">
								<section class="card card-featured card-featured-primary mb-4">
									<header class="card-header">
										<div class="card-actions">

											   @can('createdivisa',new App\catalog_master)
												<button type="button" class="btn btn-outline btn-link" data-toggle='modal'
												data-target='#agregadivisa' style="color:#fff;">	<i class="fa fa-plus"> 	Agregar</i></button>
												  @endcan
													<!-- <button type="button" class="btn btn-outline btn-link" data-toggle='modal'
													data-target='#descargargiro' style="color:#fff;">	<i class="fa fa-plus"> 	Descargar layaout</i></button> -->

										</div>
										<h2 class="card-title blanco">Divisas</h2>
										<!-- <h2>	<a target="_blank" href="{{ route('products.pdf') }}" class="btn btn-lg btn-primary">
															 Descargar Giro en PDF
													 </a> -->
													<!-- <a href="{{ route('products.excel') }}" class="btn btn-sm btn-primary">
								            Descargar productos en Excel
								        </a>-->
												 <!-- </h2> -->
											<input type="hidden" name="status" id="status" value="{{  $status }}" />
									</header>
									<div class="card-body">
										<!-- <code>.card-featured.card-featured-primary</code> -->
										<div class="	">

											<!-- <div class="container">
											<form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ URL::to('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
												<input type="file" name="import_file" />
												<button class="btn btn-primary">Import File</button>
											</form>
										</div> -->
											<div class="tabs tabs-primary">
												<ul class="nav nav-tabs">
													<li class="nav-item active">
														<a class="nav-link active" href="#altas" data-toggle="tab"><i class="fa fa-star"></i> Altas</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" href="#bajas" data-toggle="tab">Bajas</a>
													</li>
												</ul>
												<div class="tab-content">
													<div  id="altas" class="tab-pane active">
														<table width="100%" class="table table-responsive-md table-sm  ">
															<thead>
																	<tr>
                                    <td><b>ID</b></td>
                                    <td><b>Nombre corto</b></td>
                                    <td><b>Descripción</b></td>
                                    <td><b>Estatus</b></td>
                                    <td><b>Acción</b></td>
																	</tr>
															</thead>
															@foreach($analisis as $ana)
																<tbody>
																		<tr class="odd gradeX">
                                      <td>{{$ana->cmfk }}</td>
                                      <td>{{$ana->cmabbr}}</td>
                                      <td>{{$ana->cmdesc }}</td>
																				<td>
																					<img src="{{ url('admin/img/success.png') }}"alt="">
																					<!-- <button type="button" class="btn btn-success btn-circle"><i class="fa fa-check"></i></button> -->
																				</td>
																				<td class="">
																						<div class="row">
																							 @can('deletedivisa',new App\catalog_master)
																								<div class="col-md- col-md-offset-1">
																									{!!Form::open(['route'=> ['divisas.destroy',$ana->cmfk, 'estatus' => $ana->cmstatus],'method'=>'DELETE'])!!}
																										{{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-xs btn-default','title'=>'Inactivar']) }}
																										{!!Form::close()!!}
																								</div>
																								  @endcan
																									@can('updatedivisa',new App\catalog_master)
																								<div class="col-md-1 col-md-offset-1">
																									<a data-toggle='modal' id="editDivisa" data-target='#editDivisa'
																									 data-id='{{ $ana->cmfk }}' class="btn btn-xs btn-default ">  <i class="fa fa-edit"></i></a>
																								</div>
																								@endcan
																					</div>
																				</td>
																		</tr>
																	</tbody>
																@endforeach
														</table>
														<div class=" pull-right">
																{{ $analisis->appends(['status' => 1])}}
														</div>
													</div>
													<div id="bajas" class="tab-pane fade">
														<table width="100%" class="table table-responsive-md table-sm 	">
															<thead>
																	<tr>
                                    <td><b>ID</b></td>
                                    <td><b>Nombre corto</b></td>
																		<td><b>Descripción</b></td>
																		<td><b>Estatus</b></td>
																		<td><b>Acción</b></td>
																	</tr>
															</thead>
															@foreach($analisisbajas as $anaBa)
																<tbody>
																		<tr class="odd gradeX">
                                      <td>{{$anaBa->cmfk }}</td>
                                      <td>{{$anaBa->cmabbr}}</td>
                                      <td>{{$anaBa->cmdesc }}</td>
																				<td>
																					<img src="{{ url('admin/img/danger.png') }}"alt="">
																				</td>
																				<td>
																						<div class="row">
																							@can('deletedivisa',new App\catalog_master)
																								<div class="col-md- col-md-offset-">
																									{!!Form::open(['route'=> ['divisas.destroy',$anaBa->cmfk, 'estatus' => $anaBa->cmstatus],'method'=>'DELETE'])!!}
																										{{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-xs btn-default','title'=>'Inactivar']) }}
																										{!!Form::close()!!}
																								</div>
																								@endcan
																								@can('updatedivisa',new App\catalog_master)
																								<div class="col-md-1 col-md-offset-1">
																									<a data-toggle='modal' id="editDivisa" data-target='#editDivisa'
																							 data-id='{{ $anaBa->cmfk }}' class="btn btn-xs btn-default ">  <i class="fa fa-edit"></i></a>
																							</div>
																								@endcan
																					</div>
																				</td>
																		</tr>
																	</tbody>
																@endforeach
														</table>
														<div class=" pull-right">
																{{ $analisisbajas->appends(['status' => 0])}}
														</div>
												</div>
											</div>
											</div>

									</div>
								</section>
							</div>
<<<<<<< HEAD
=======



>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
					<!-- end: page -->
				</section>
			</div>

<<<<<<< HEAD

=======
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
			@stop
@section('scripts')
	{!!Html::script('js/Generics.js')!!}
  {!!Html::script('js/admin/createdivisas.js')!!}
  {!!Html::script('js/admin/divisas.js')!!}

@endsection
