@extends('layouts.adm')

@section('content')

  <div class="inner-wrapper">
      <section role="main" class="content-body pb-0">
          	<section role="main" class="content-body pb-0">
                	<div class="col-md-12 ">
                    <section class="card card-featured card-featured-primary mb-4">
                      <header class="card-header">
                        <div class="card-actions">
                          <a class="" href="{{route("home")}}">
                            <button type="button" class="btn btn-outline btn-success"  style="color:#fff;">		Guardar</button>
                          </a>
                        </div>
                        <h2 class="card-title blanco">Analisis de Riesgo </h2>
                      </header>
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="card-body">
                            <div class="col-lg-12 col-xl-12">
                              <div class="accordion accordion-primary " id="accordion2Primary">
                                <input type="hidden" name="" id="valor1Temp" value="0">
                                <input type="hidden" name="" id="valor2Temp" value="0">
                                <input type="hidden" name="" id="valor3Temp" value="0">
                                <input type="hidden" name="" id="idConsecuenTemp" value="0">
                                <input type="hidden" name="" id="total" value="0">
                                <!--Foreach Operacion -->
                                @foreach($arrayIdOperacion as $idOperacion)
                                <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" content="{{ csrf_token() }}" />
                                <div class="card card-default">
                                  <div class="card-header">
                                    <h4 class="card-title m-0">
                                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#{{$idOperacion }}">
                                        {{ $arrayNameOperacion[$idOperacion] }}
                                      </a>
                                    </h4>
                                  </div>
                                  <div id="{{$idOperacion}}" class="collapse">
                                    <div class="card-body">
                                      <!--Foreach Actividad -->
																				@foreach($arrayIdActividad as $idActividad)
                                        <!--id de Actividad -->
																					@if(!isset($arrayNameActividad[$idOperacion][$idActividad]))
                                          @else
																				<h4 class=" letra2">{{$arrayNameActividad[$idOperacion][$idActividad]}}<br></h4>
                                        <table width="100%" class="table table-responsive-lg">
                                          <thead>
                                            <tr class="titulo">
                                              <td class=""><p>DESCRIPCION DEL PELIGRO</p></td>
                                              <td class=""><p>CONSECUENCIA</p></td>
                                              <td class=""><p>Total</p></td>
                                              <td class=""><p>EXPOSICION</p></td>
                                              <td class=""><p>Total</p></td>
                                              <td class=""><p>PROBABILIDAD</p></td>
                                              <td class=""><p>Total</p></td>
                                              <td class=""><p>Total General</p></td>
                                              <td class=""><p>GRADO DE RIESGO</p></td>
                                              <td class=""><p>REQISITOS LEGALES Y OTROS </p></td>
                                            </tr>
                                          </thead>
                                          <!--Foreach de descripcion del peligro -->
                                            @foreach($arrayIdDescripcion as $idDescripcion)
                                            <?php
                                             $respuesta =  DB::table('answered_analysis AS res')
                                              ->select('res.ararfk', 'res.aradsdanger','res.araconsequen','res.aratotconsec','res.araexpositio','res.aratotexposi',
                                                'res.araprobabili','res.aratotprobab','res.aragrndtotal','res.ararisklevel','res.araobservati')
                                              ->where('res.aradsdanger','=',$idDescripcion)
                                              ->get();
                                             ?>
                                             <tr class="odd gradeX">
                                               <!-- if de Descripcion del peligro -->
                                              @if(!isset($arrayNameDescripcion[$idOperacion][$idActividad][$idDescripcion]))
                                              @else
                                                <td>
                                                  {{$idDescripcion}}	{{$arrayNameDescripcion[$idOperacion][$idActividad][$idDescripcion]}}<br>
                                                </td>
                                                  <!--Foreach Consecuencias -->
                                                 @foreach($arrayIdCatalogo as $idCatalogo)
                                                      <!-- if de Consecuencias -->
                                                    @if(!isset($arrayNameCatalogo[$idOperacion][$idActividad][$idDescripcion][$idCatalogo]))
                                                    @else

                                                    <!-- if de Consecuencias select -->
                                                        @if($arrayNameCatalogo[$idOperacion][$idActividad][$idDescripcion][$idCatalogo] == '44')
                                                        <?php  foreach ($respuesta as $value) {  ?>
                                                          <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" content="{{ csrf_token() }}" />
                                                          <td>
                                                            <div class="form-group select" >
                                                              <select class="form-control" value="<?php echo $value->ararfk; ?>" onchange="consecuencias(this.value,{{$idDescripcion}},{{$idCatalogo}})" id="valorselect" >
                                                                <!-- Foreach de consecuencia del select 44 -->
                                                                @foreach($arrayIdConsecuencias as $idConsecuencia)
                                                                  <!-- if de consecuencias de select 44 -->
                                                                  @if(!isset($arrayNameConsecuencias[$idOperacion][$idActividad][$idDescripcion][$idCatalogo][$idConsecuencia]))
                                                                  @else
                                                                      <?php  if ($idConsecuencia == $value->araconsequen) {  ?>
                                                                        <option value="{{$idConsecuencia}}" selected="<?php echo $value->araconsequen; ?>" id="" placeholder="Consecuencias">{{ $arrayNameConsecuencias[$idOperacion][$idActividad][$idDescripcion][$idCatalogo][$idConsecuencia]}}</option>
                                                                      <?php }else { ?>
                                                                        <option value="{{$idConsecuencia}}" id="" placeholder="Consecuencias">{{ $arrayNameConsecuencias[$idOperacion][$idActividad][$idDescripcion][$idCatalogo][$idConsecuencia]}}</option>
                                                                    <?php   } ?>
                                                                  @endif
                                                                  <!-- fin de if de consecuencias de select 44 -->
                                                                @endforeach
                                                                <!-- fin deForeach de consecuencia del select 44 -->
                                                              </select>
                                                            </div>
                                                          </td>
                                                              <td>
                                                                <input type="text" class="form-control input "Onchange = "recibir($idDescripcion);" value="<?php echo $value->aratotconsec; ?>" id="{{$idDescripcion}}_resultado" name="resultado" disabled>
                                                              </td>
                                                            <?php  } ?>
                                                        @else
                                                        <!-- else de fin de if de Consecuencias select  -->
                                                            @if($arrayNameCatalogo[$idOperacion][$idActividad][$idDescripcion][$idCatalogo] == '45')
                                                            <?php   foreach ($respuesta as $valu2) {   ?>
                                                              <td>
                                                                <div class="form-group select2 ">
                                                                  <select class="form-control"  name="exposicion" onchange="consecuencias(this.value,{{$idDescripcion}},{{$idCatalogo}})" id="">
                                                                      <!--foreac de EXPOSICION -->
                                                                     @foreach($arrayIdConsecuencias as $value => $idConsecuencia)
                                                                        <!-- if de Conseciencia de catalogo 45 -->
                                                                       @if(!isset($arrayNameConsecuencias[$idOperacion][$idActividad][$idDescripcion][$idCatalogo][$idConsecuencia]))
                                                                       @else
                                                                       <!-- else de if de Conseciencia de catalogo 45 -->
                                                                         <?php  if ($idConsecuencia == $valu2->araexpositio) {  ?>
                                                                           <option value="{{$idConsecuencia}}" selected="<?php echo $valu2->araexpositio; ?>" placeholder="Consecuencias">{{ $arrayNameConsecuencias[$idOperacion][$idActividad][$idDescripcion][$idCatalogo][$idConsecuencia]}}</option>
                                                                          <?php }else { ?>
                                                                           <option value="{{$idConsecuencia}}" placeholder="Consecuencias">{{ $arrayNameConsecuencias[$idOperacion][$idActividad][$idDescripcion][$idCatalogo][$idConsecuencia]}}</option>
                                                                         <?php   } ?>
                                                                          @endif
                                                                         <!-- fin de if de Conseciencia de catalogo 45 -->
                                                                     @endforeach
                                                                     <!-- fin de foreach de EXPOSICION -->
                                                                  </select>
                                                               </div>
                                                              </td>
                                                                  <td> <input type="text" class="form-control input" value="<?php echo $valu2->aratotexposi; ?>" id="{{$idDescripcion}}_exposicion"name="exposicion" disabled> </td>
                                                                <?php  } ?>
                                                            @else
                                                              <!-- else de fin de if de Consecuencias select  -->
                                                                      <!-- if de Conseciencia de catalogo 46 -->
                                                                  @if($arrayNameCatalogo[$idOperacion][$idActividad][$idDescripcion][$idCatalogo] == '46')
                                                                  <?php   foreach ($respuesta as $valu3) {   ?>
                                                                    <td>
                                                                      <div class="form-group select3 ">
                                                                        <select class="form-control" name="probabilidad" onchange="consecuencias(this.value,{{$idDescripcion}},{{$idCatalogo}})" id="">
                                                                              <!--foreac de PROBABILIDAD -->
                                                                           @foreach($arrayIdConsecuencias as $value => $idConsecuencia)
                                                                              @if(!isset($arrayNameConsecuencias[$idOperacion][$idActividad][$idDescripcion][$idCatalogo][$idConsecuencia]))
                                                                              @else
                                                                               <?php  if ($idConsecuencia == $valu3->araprobabili) {  ?>
                                                                                <option value="{{$idConsecuencia}}"selected="<?php echo $valu3->araprobabili; ?>" placeholder="Consecuencias">{{ $arrayNameConsecuencias[$idOperacion][$idActividad][$idDescripcion][$idCatalogo][$idConsecuencia]}}</option>
                                                                                <?php }else { ?>
                                                                                <option value="{{$idConsecuencia}}" placeholder="Consecuencias">{{ $arrayNameConsecuencias[$idOperacion][$idActividad][$idDescripcion][$idCatalogo][$idConsecuencia]}}</option>
                                                                               <?php   } ?>
                                                                              @endif
                                                                           @endforeach
                                                                               <!-- fin del foreac de PROBABILIDAD -->
                                                                        </select>
                                                                      </div>
                                                                    </td>
                                                                        <td> <input type="text" class="form-control input" value="<?php echo $valu3->aratotprobab; ?>"name="probaresultado" id="{{$idDescripcion}}_probabilidad" value="" disabled> </td>
                                                                        <!-- fin del if de Conseciencia de catalogo 46 -->
                                                                          <?php  } ?>
                                                                  @else
                                                                    <!-- else de if de Conseciencia de catalogo 45 -->
                                                                  @endif

                                                            @endif
                                                              <!-- fin de if de Consecuencias select  -->
                                                        @endif
                                                        <!-- fin de if de Consecuencias select -->
                                                    @endif
                                                      <!-- if de Consecuencias -->
                                                 @endforeach
                                                 <!--fin de Foreach Consecuencias -->
                                                 <?php   foreach ($respuesta as $valu4) {
                                                   $val = $valu4->ararfk ?>
                                                   <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" content="{{ csrf_token() }}" />
                                                 <td> <input type="text" class="form-control  inputtotal" name="total" id="{{$idDescripcion}}_total"value="<?php echo $valu4->aragrndtotal; ?>"  disabled> </td>
                                                 <td> <input type="text" class="form-control inputtotal" name=""id="{{$idDescripcion}}_grado" value="<?php echo $valu4->ararisklevel; ?> " disabled> </td>

                                             <td> <textarea name="" onblur="guardarTextarea(this.value,{{$idDescripcion}},{{$valu4->ararfk}})" id="{{$idDescripcion}}_registro"rows="1"  cols="20"> <?php echo $valu4->araobservati; ?> </textarea> </td>
                                                 <?php  } ?>

                                              @endif
                                              <!-- id de Descripcio del peligro -->
                                            @endforeach
                                            <!--Foreach Descripcion del peligro -->
                                             </tr>
                                        </table>
																					@endif
                                          <!--fin de if Actividad -->
																			@endforeach
                                      <!-- fin de Foreach Actividad -->
                                    </div>
                                  </div>
                                </div>
                                @endforeach
                                <!-- fin de Foreach Operacion -->
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
            </section>
      </section>
  </div>

@stop
@section('scripts')
  {!!Html::script('js/Generics.js')!!}
  {!!Html::script('js/analisis.js')!!}
@endsection
