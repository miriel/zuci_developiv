@extends('layouts.adm')

@section('content')

      <div class="inner-wrapper">
          <section role="main" class="content-body pb-0">
                <section role="main" class="content-body pb-0">
                      <div class="col-md-12 ">
                        <section class="card card-featured card-featured-primary mb-4">
                          <header class="card-header">
                            <div class="row">
                              <div class="col-lg-7">
                                  <h2 class="card-title blanco">Analisis de Riesgo Ejecutivo</h2>
                              </div>
                              <div class="col-lg-5">
                                <a href="{{ URL::previous() }}" ><button type="button" class="btn btn-outline btn-link" style="color:#fff;">	<i class="fa fa-mail-reply-all"> 	Regresar</i></button></a>
                              </div>
                            </div>
                          </header>
                          <div class="row">
                              <div class="card-body">
                                <div class="col-lg-12 col-xl-12">
                                  <div class="accordion accordion-primary " id="accordion2Primary">
                                    <input type="hidden" name="" id="valor1Temp" value="0">
                                    <input type="hidden" name="" id="valor2Temp" value="0">
                                    <input type="hidden" name="" id="valor3Temp" value="0">
                                    <input type="hidden" name="" id="idConsecuenTemp" value="0">
                                    <input type="hidden" name="" id="total" value="0">
                                    <div class="row">
                                      </div>
                                    </div>
                                      <div class="col-lg-12">

                                      @foreach($nombre as $nom)

                                        @endforeach

                                        @if(count($analisis)== '0')
                                          <h4>No tiene Registros</h4>
                                        @else
                                        @foreach($arrayIdOperacion as $idOperacion)
                                        <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" content="{{ csrf_token() }}" />
                                          <h4 class=" letra2">{{ $arrayNameOperacion[$idOperacion] }}<br></h4>
                                            <table width="100%" class="table table-responsive-lg">
                                              <thead>
                                                <tr class="titulo">
                                                  <td class=""><p>DESCRIPCION DEL PELIGRO</p></td>
                                                  <td></td>
                                                  <td class=""><p>CONSECUENCIA</p></td>
                                                  <td class=""><p>Total</p></td>
                                                  <td class=""><p>EXPOSICION</p></td>
                                                  <td class=""><p>Total</p></td>
                                                  <td class=""><p>PROBABILIDAD</p></td>
                                                  <td class=""><p>Total</p></td>
                                                  <td class=""><p>Total General</p></td>
                                                  <td class=""><p>GRADO DE RIESGO</p></td>
                                                  <td class=""><p>REQISITOS LEGALES Y OTROS </p></td>
                                                </tr>
                                              </thead>
                                              @foreach($arrayIdActividad as $idActividad)
                                              <?php
                                               $respuesta =  DB::table('answered_analysis AS res')
                                                ->select('res.ararfk','arafoliofk','aractfk','res.aradsdanger','res.araconsequen','res.aratotconsec','res.araexpositio','res.aratotexposi',
                                                  'res.araprobabili','res.aratotprobab','res.aragrndtotal','res.ararisklevel','res.araobservati')
                                                ->where('res.aradsdanger','=',$idActividad)
                                                ->where('res.arafoliofk','=',$session)
                                                ->where('res.aractfk','=',$cuestionario)
                                                ->get();
                                               ?>
                                                 <tr class="odd gradeX">
                                              <!--id de Actividad -->
                                                @if(!isset($arrayNameActividad[$idOperacion][$idActividad]))
                                                @else
                                                <td>
                                                  {{$arrayNameActividad[$idOperacion][$idActividad]}}
                                                </td>
                                                <!--Foreach Consecuencias -->
                                                   @foreach($arrayIdCatalogo as $idCatalogo)
                                                         <!-- if de Consecuencias -->
                                                       @if(!isset($arrayNameCatalogo[$idOperacion][$idActividad][$idCatalogo]))
                                                       @else
                                                       <!-- if de Consecuencias select -->
                                                           @if($arrayNameCatalogo[$idOperacion][$idActividad][$idCatalogo] == '50')
                                                           <?php  foreach ($respuesta as $value) {  ?>
                                                             <td>
                                                                  <a  data-toggle='modal' data-target='#info' style="color:red;"><i class="fa fa-question margin"></i></a>
                                                             </td>

                                                             <td >
                                                               <div class="form-group select " >
                                                                 <select class="form-control select" value="<?php echo $value->ararfk; ?>" onchange="consecuencias(this.value,{{$idActividad}},{{$idCatalogo}})" id="valorselect" >
                                                                   <!-- Foreach de consecuencia del select 50 -->
                                                                   @foreach($arrayIdConsecuencias as $idConsecuencia)
                                                                     <!-- if de consecuencias de select 50 -->
                                                                     @if(!isset($arrayNameConsecuencias[$idOperacion][$idActividad][$idCatalogo][$idConsecuencia]))
                                                                     @else
                                                                         <?php  if ($idConsecuencia == $value->araconsequen) {  ?>
                                                                           <option value="{{$idConsecuencia}}" selected="<?php echo $value->araconsequen; ?>" id="" placeholder="Consecuencias">{{ $arrayNameConsecuencias[$idOperacion][$idActividad][$idCatalogo][$idConsecuencia]}}</option>
                                                                         <?php }else { ?>
                                                                           <option value="{{$idConsecuencia}}" id="" placeholder="Consecuencias">{{ $arrayNameConsecuencias[$idOperacion][$idActividad][$idCatalogo][$idConsecuencia]}}</option>
                                                                       <?php   } ?>
                                                                     @endif
                                                                     <!-- fin de if de consecuencias de select 50 -->
                                                                   @endforeach
                                                                   <!-- fin deForeach de consecuencia del select 50 -->
                                                                 </select>
                                                                </div>

                                                              </td>
                                                                 <td>
                                                                  <input type="text" class="form-control input" Onchange = "recibir($idActividad);" value="<?php echo $value->aratotconsec; ?>" id="{{$idActividad}}_resultado" name="resultado" disabled>
                                                                 </td>
                                                               <?php  } ?>
                                                           @else
                                                           @endif
                                                           <!-- else de fin de if de Consecuencias select  -->
                                                               @if($arrayNameCatalogo[$idOperacion][$idActividad][$idCatalogo] == '51')
                                                               <?php   foreach ($respuesta as $valu2) {   ?>
                                                                 <td>
                                                                   <div class="form-group select2 ">
                                                                     <select class="form-control"  name="exposicion" onchange="consecuencias(this.value,{{$idActividad}},{{$idCatalogo}})" id="">
                                                                         <!--foreac de EXPOSICION -->
                                                                        @foreach($arrayIdConsecuencias as $value => $idConsecuencia)
                                                                           <!-- if de Conseciencia de catalogo 51 -->
                                                                          @if(!isset($arrayNameConsecuencias[$idOperacion][$idActividad][$idCatalogo][$idConsecuencia]))
                                                                          @else
                                                                          <!-- else de if de Conseciencia de catalogo 51 -->
                                                                            <?php  if ($idConsecuencia == $valu2->araexpositio) {  ?>
                                                                              <option value="{{$idConsecuencia}}" selected="<?php echo $valu2->araexpositio; ?>" placeholder="Consecuencias">{{ $arrayNameConsecuencias[$idOperacion][$idActividad][$idCatalogo][$idConsecuencia]}}</option>
                                                                             <?php }else { ?>
                                                                              <option value="{{$idConsecuencia}}" placeholder="Consecuencias">{{ $arrayNameConsecuencias[$idOperacion][$idActividad][$idCatalogo][$idConsecuencia]}}</option>
                                                                            <?php   } ?>
                                                                             @endif
                                                                            <!-- fin de if de Conseciencia de catalogo 51 -->
                                                                        @endforeach
                                                                        <!-- fin de foreach de EXPOSICION -->
                                                                     </select>
                                                                  </div>
                                                                 </td>
                                                                     <td> <input type="text" class="form-control input" value="<?php echo $valu2->aratotexposi; ?>" id="{{$idActividad}}_exposicion"name="exposicion" disabled> </td>
                                                                   <?php  } ?>
                                                               @else
                                                                 <!-- else de fin de if de Consecuencias select  -->
                                                                         <!-- if de Conseciencia de catalogo 52 -->
                                                                     @if($arrayNameCatalogo[$idOperacion][$idActividad][$idCatalogo] == '52')
                                                                     <?php   foreach ($respuesta as $valu3) {   ?>
                                                                       <td>
                                                                         <div class="form-group select3 ">
                                                                           <select class="form-control" name="probabilidad" onchange="consecuencias(this.value,{{$idActividad}},{{$idCatalogo}})" id="">
                                                                                 <!--foreac de PROBABILIDAD -->
                                                                              @foreach($arrayIdConsecuencias as $value => $idConsecuencia)
                                                                                 @if(!isset($arrayNameConsecuencias[$idOperacion][$idActividad][$idCatalogo][$idConsecuencia]))
                                                                                 @else
                                                                                  <?php  if ($idConsecuencia == $valu3->araprobabili) {  ?>
                                                                                   <option value="{{$idConsecuencia}}"selected="<?php echo $valu3->araprobabili; ?>" placeholder="Consecuencias">{{ $arrayNameConsecuencias[$idOperacion][$idActividad][$idCatalogo][$idConsecuencia]}}</option>
                                                                                   <?php }else { ?>
                                                                                   <option value="{{$idConsecuencia}}" placeholder="Consecuencias">{{ $arrayNameConsecuencias[$idOperacion][$idActividad][$idCatalogo][$idConsecuencia]}}</option>
                                                                                  <?php   } ?>
                                                                                 @endif
                                                                              @endforeach
                                                                                  <!-- fin del foreac de PROBABILIDAD -->
                                                                           </select>
                                                                         </div>
                                                                       </td>
                                                                           <td> <input type="text" class="form-control input" value="<?php echo $valu3->aratotprobab; ?>"name="probaresultado" id="{{$idActividad}}_probabilidad" value="" disabled> </td>
                                                                           <!-- fin del if de Conseciencia de catalogo 52 -->
                                                                             <?php  } ?>
                                                                     @else
                                                                       <!-- else de if de Conseciencia de catalogo 51 -->
                                                                     @endif

                                                               @endif
                                                                 <!-- fin de if de Consecuencias select  -->
                                                       @endif
                                                   @endforeach
                                                   <?php   foreach ($respuesta as $valu4) {
                                                     $val = $valu4->ararfk ?>
                                                     <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" content="{{ csrf_token() }}" />
                                                   <td> <input type="text" class="form-control  inputtotal" name="total" id="{{$idActividad}}_total"value="<?php echo $valu4->aragrndtotal; ?>"  disabled> </td>
                                                   <td> <input type="text" class="form-control inputtotal" name=""id="{{$idActividad}}_grado" value="<?php echo $valu4->ararisklevel; ?> " disabled> </td>

                                               <td> <textarea name=""  onblur="guardarTextarea(this.value,{{$idActividad}},{{$valu4->ararfk}})" id="{{$idActividad}}_registro"rows="1"  cols="20"> <?php echo $valu4->araobservati; ?> </textarea>
                                                 <!-- <p id="contador">0/140</p>
                                                 <textarea name="text" id="text" rows="1" cols=""></textarea> -->
                                                   <?php  } ?>
                                                @endif


                                                </tr>

                                              @endforeach
                                              <!--fin de Foreach Consecuencias -->

                                        </table>
                                        @endforeach

                                        @endif



                                    <!--Foreach Operacion -->


                                    <!-- fin de Foreach Operacion -->
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </section>
                      </div>
                </section>
          </section>
      </div>


      @include('analisis.multas.info')

@stop

@section('scripts')
  {!!Html::script('js/Generics.js')!!}
  {!!Html::script('js/Ariesgo.js')!!}
  {!!Html::script('js/admin/actividad.js')!!}
@endsection
