<!DOCTYPE html>
<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
    .pie-chart {
        width: 1000px;
        height: 400px;
        margin: 0 auto;
    }
    .blanco{
		color: #fff;
		background-color: #003366;
		text-align: center;
		font-size: 25px;
		width: 100%;
		height: auto;
	}
	.letra3{
		background: #bfbfbf;
		font-size: 22px;
		margin-top: 3px;
	}
	.letra2{
	  background: #003366;
		color: #fff;
		font-size: 16px;
	  margin-top: 3px;
		text-align: center;
	}

	.letra{
		font-size: 11px;
	}
	.color1{
		color: #FFF;
		text-align: center;
    background: #003366;
		border-color: #FFF;
    font-size: 14px;
	}

	.color2{
		color: #FFF;
		text-align: center;
    background: #045FB4;
		border-color: #FFF;
    font-size: 11px;
	}
	.col{
		color: #655c5c;
	}
	.titulo{
		text-align: center;
		font-size: 17px;
	}

	.header,
	.footer {
    width: 100%;
    text-align: center;
    position: fixed;
}
.header {
    top: 0px;
}
.footer {
    bottom: 0px;
		text-align: right;
}
.pagenum:before {
    content: counter(page);
}


table{
  border-collapse: collapse;
	font-family: "Arial", serif;
}
    </style>
    {{-- make sure you are using http, and not https --}}
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>

    <script type="text/javascript">
        function init() {
            google.load("visualization", "1.3", {
                packages: ["corechart"],
                callback: 'drawCharts'
            });

            google.load("visualization", "1.3", {
                packages: ["corechart"],
                callback: 'drawBasic'
            });
        }

        function drawCharts(){
            var data = google.visualization.arrayToDataTable([
                ['Task', 'Vulnerabilidad','Probabilidad'],
                @foreach ($analisis as $analis)
                  ['{{ $analis->cmfk}}', {{ number_format($analis->promval,2) }}, {{ number_format($analis->promprob,2) }} ],
                @endforeach
            ]);
            var options = {
                title: 'Vulnerabilidad y Probabilidad',
                hAxis: {
                  title: 'Identificador'
                },
                vAxis: {
                  title: 'Cantidad'
                },
            };
            var chart = new google.visualization.LineChart(document.getElementById('piechart'));
            chart.draw(data, options);
        }

        function drawBasic(){
            var data = google.visualization.arrayToDataTable([
                ['Identificador', 'Grado de riesgo'],
                @foreach ($analisis as $analis)
                  	<?php $evRiesg = ($analis->promval + $analis->promprob) / 2; ?>
                  ['{{ $analis->cmfk}}', {{ number_format($evRiesg,2) }} ],
                @endforeach
            ]);
            var options = {
                title: 'Nivel de riesgo',
                chartArea: {width: '50%'},
                hAxis: {
                  title: 'Cantidad'
                },
                vAxis: {
                  title: 'Identificador'
                },
            };
            var chart = new google.visualization.BarChart(document.getElementById('piechart2'));
            chart.draw(data, options);
        }
    </script>
</head>

<body onload="init()">
    <table width="100%" border='0'>
        <tr>
            <td colspan="5" class="color1">Análisis de riesgo ejecutivo</td>
        </tr>
        <tr>
            <td class="color2">Identificador</td>
            <td class="color2">Posible riesgo o amenaza</td>
            <td class="color2">Vulnerabilidad</td>
            <td class="color2">Probabilidad</td>
            <td class="color2">Ev. Riesgo</td>
        </tr>
        @foreach ($analisis as $datos)
						<?php $evRiesg = ($datos->promval + $datos->promprob) / 2; ?>
            <tr>
						<td class="letra">{{ $datos->cmfk }}</td>
						<td class="letra">{{ $datos->cmdesc }}</td>
            <td class="letra">{{ number_format($datos->promval,2) }}</td>
						<td class="letra">{{ number_format($datos->promprob,2) }} </td>
						<td class="letra">{{ number_format($evRiesg,2) }}</td>
            </tr>
  				@endforeach
    </table>
    <br><br><br>
    <div id="piechart" class="pie-chart"></div>
    <br><br><br>
    <div id="piechart2" class="pie-chart"></div>
</body>

</html>
