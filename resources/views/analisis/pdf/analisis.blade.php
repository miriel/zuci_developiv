<!DOCTYPE html>
<html>
<head>
	<title>PDF - Analisis De Riesgo</title>
	<!-- <link rel="stylesheet" href="sass/main.css" media="screen" charset="utf-8"/> -->
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
	<meta http-equiv="content-type" content="text-html; charset=utf-8">
	<style media="screen">

	.blanco{
		color: #fff;
		background-color: #003366;
		text-align: center;
		font-size: 25px;
		width: 100%;
		height: auto;
	}
	.letra3{
		background: #bfbfbf;
		font-size: 22px;
		margin-top: 3px;
	}
	.letra2{
	  background: #003366;
		color: #fff;
		font-size: 28px;
	  margin-top: 3px;
		text-align: center;
	}

	.letra{
		font-size: 19px;
	}
	.color1{
		color: #655c5c;
		text-align: center;
	}
	.col{
		color: #655c5c;
	}
	.titulo{
		text-align: center;
		font-size: 40px;
	}

	.header,
	.footer {
    width: 100%;
    text-align: center;
    position: fixed;
}
.header {
    top: 0px;
}
.footer {
    bottom: 0px;
		text-align: right;
}
.pagenum:before {
    content: counter(page);

}

	</style>
</head>

<body>
			<div class="header">

		</div>
	<?php
		$table = "";
			if(count($analisis)== '0'){
				$table.="<h4>No tiene Registros</h4>";
			}
			else{
				foreach ($arrayIdAct as $arrayId ) {
					if(!isset($arrayNameAct[$arrayId])){
					}
					else{
							$table.= "<h1 class='titulo'><u>ANALISIS DE RIESGO</u></h1>";
						$table.= "<h3 class='letra2'><strong>".	$arrayNameAct[$arrayId]."</strong></h3>";
					}
				}

				foreach($arrayIdOperacion as $idOperacion) {
					$table.="<h4 class='letra3'>".$arrayNameOperacion[$idOperacion]."</h4>";
					foreach($arrayIdActividad as $idActividad){
						if(!isset($arrayNameActividad[$idOperacion][$idActividad])){
						}
						else{
							$table.="<div class='col-md-12 letra'><strong>".$arrayNameActividad[$idOperacion][$idActividad]."</strong></div>";
							$table.="<table border='0' width='100%'  >";
							foreach($analisis as $resul){
								if($idActividad == $resul->idRespuesta){
									$table.="<thead>";
									$table.="<tr class=''>";
									$table.="<td> <p class='color1'><strong>Consecuencia : </strong><br>".$resul->Consecuenias."</p></td>";
									$table.="<td> <p class='color1'><strong>Total : </strong><br>".$resul->aratotconsec."</p></td>";
									$table.="<td> <p class='color1'><strong>Exposicion : </strong><br>".$resul->Exposicion."</p></td>";
									$table.="<td> <p class='color1'><strong>Total : </strong><br>".$resul->aratotexposi."</p></td>";
									$table.="<td> <p class='color1'><strong>Probabilidad : </strong><br>".$resul->Probabilidad."</p></td>";
									$table.="<td> <p class='color1'><strong>Total : </strong><br>".$resul->aratotprobab."</p></td>";
									$table.="<td> <p class='color1'><strong>Total General : </strong><br>".$resul->aragrndtotal."</p></td>";
									$table.="<td> <p class='color1'><strong>Grado de Riesgo : </strong><br>".$resul->ararisklevel."</p></td>";
									$table.="<div class='col-md-12 col'><strong> Requisitos legales y otros: </strong>".$resul->araobservati."</div>";
									$table.="</tr>";
									$table.="</thead>";
								}
							}
			 				$table.="</table><br>";

						}
					}
				}
			}
			echo $table;
	 ?>
	 <div class="footer text-left">
	     Page <span class="pagenum"></span>
	 </div>


</body>

</html>
