
<!-- Modal -->
<div class="modal fade" id="editActividad" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class="letracolor">Editar Catálogos</h4>
        <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <!-- Mensajes exitosos -->
            <div id="msj-success" class="alert alert-success" role="alert" style="display:none">
                Operación actializado correctamente
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <input type="hidden" id="cmfkEd" />
            {!! Form::label('cmdescEd','Nombre') !!}
            {!! Form ::text('cmdescEd', null, ['id'=> 'cmdescEd','class'=>'form-control'])!!}
            <div id="msj-errorEd" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjEd"></strong>
            </div>

            {!! Form::label('cmabbrEd','Abreviación') !!}
            {!! Form ::text('cmabbrEd', null, ['id'=> 'cmabbrEd','class'=>'form-control'])!!}
            <div id="msj-error1" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msj1"></strong>
            </div>

            {!! Form::label('cmvalEd','Valor') !!}
            {!! Form ::text('cmvalEd', null, ['id'=> 'cmvalEd','class'=>'form-control'])!!}
            <div id="msj-error2" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msj2"></strong>
            </div><br>
              <div class="" style="display:none; "name="Ocultar" id="Ocultar">
                  <h4 class="text-center">Multas de divisas</h4><br>
                <div class="row">
                  <div class="col-lg-4">
                    <strong><label for="">Monto Mínimo de Multa</label></strong>
                    <div class="input-group" >
                      <div class="input-group-addon">$</div>
                      <input type="text" class="form-control" id="cmminEd" name="cmminEd" placeholder="Amount">
                      <div class="input-group-addon">.00</div>
                    </div>
                    <div id="msj-error3" class="alert-danger" role="alert" style="display:none">
                        <strong class="help-block blanco" id="msj3"></strong>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <strong><label for="">Monto Máximo de Multa</label></strong>
                    <div class="input-group ">
                      <div class="input-group-addon">$</div>
                      <input type="text" class="form-control" id="cmmaxEd" name="cmmaxEd" placeholder="Amount">
                      <div class="input-group-addon">.00</div>
                    </div>
                    <div id="msj-error4" class="alert-danger" role="alert" style="display:none">
                        <strong class="help-block blanco" id="msj4"></strong>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <strong><label  for="">Tipo de Moneda</label></strong>
                      <div class="form-group">
                           <select required class="form-control" name="cmmodenaEd" id="cmmodenaEd">
                                 @foreach($divisas as $div)
                               <option  value="{{$div->cmfk}}" >{{$div->cmdesc}}</option>
                               @endforeach
                           </select>
                       </div>
                  </div>
                </div>
              </div>


            <div class="modal-footer">
              <input type="button" class="btn btn-primary" id="EditActividad" value="Actualizar" />
            </div>
      </div>
    </div>
  </div>
</div>
