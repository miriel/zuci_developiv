<!-- Modal -->
  <div class="modal fade " id="agregaconsecuencia" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class=" letracolor">Agregar Consecuencia</h4>
          <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <div class="form-group">
              <!-- Mensajes exitosos -->
              <div id="msj-success" class="alert alert-success" role="alert" style="display:none">
                  Consecuencia agregado correctamente
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <!-- Mensajes de error -->
            </div>
            {!! Form::label('cmdesc','Nombre') !!}
            {!! Form ::text('cmdesc', null, ['class'=>'form-control'])!!}
            <div id="msj-errord" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjd"></strong>
            </div>
            {!! Form::label('cmabbr','Abreviación') !!}
            {!! Form ::text('cmabbr', null, ['class'=>'form-control'])!!}
            <div id="msj-errorb" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjb"></strong>
            </div>

          <div class="row">
            <div class="col-lg-4">
                {!! Form::label('cmval','Valor') !!}
              {!! Form ::text('cmval', null, ['class'=>'form-control'])!!}
            <div id="msj-errorv" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjv"></strong>
            </div><br>
            </div>
            <div class="col-lg-4">
              <label for="">Selecciona Tipo de Catálogo</label>
              <div class="form-group">
                <select required class="form-control" name="cmtpcat" id="cmtpcat"  onChange="mostrar(this.value);">
                  <option value="">Selecciona</option>
                  <option value="50">Consecuencia</option>
                  <option value="51">Exposición</option>
                  <option value="52">Probabilidad</option>
                </select>
              </div>
              <div id="msj-errort" class="alert-danger" role="alert" style="display:none">
                  <strong class="help-block blanco" id="msjt"></strong>
              </div><br>

            </div>

          </div>

            <div class="" style="display:none;" name="mostrar" id="mostar">
              <h4 class="text-center">Multas de divisas</h4><br>
              <div class="row">
                <div class="col-lg-4">
                  <strong><label for="">Monto Mínimo de Multa</label></strong>
                  <div class="input-group" >
                    <div class="input-group-addon">$</div>
                    <input type="text" class="form-control" id="cmmin" name="cmmin" placeholder="Amount">
                    <div class="input-group-addon">.00</div>
                  </div>
                  <div id="msj-errori" class="alert-danger" role="alert" style="display:none">
                      <strong class="help-block blanco" id="msji"></strong>
                  </div>
                </div>
                <div class="col-lg-4">
                  <strong><label for="">Monto Máximo de Multa</label></strong>
                  <div class="input-group ">
                    <div class="input-group-addon">$</div>
                    <input type="text" class="form-control" id="cmmax" name="cmmax" placeholder="Amount">
                    <div class="input-group-addon">.00</div>
                  </div>
                  <div id="msj-errora" class="alert-danger" role="alert" style="display:none">
                      <strong class="help-block blanco" id="msja"></strong>
                  </div>
                </div>
                <div class="col-lg-4">
                  <strong><label  for="">Tipo de Moneda</label></strong>
                    <div class="form-group">
                         <select required class="form-control" name="cmmoneda" id="cmmoneda">
                               @foreach($divisas as $div)
                             <option  value="{{$div->cmfk}}" >{{$div->cmdesc}}</option>
                             @endforeach
                         </select>
                     </div>
                </div>
              </div>
            </div>
      </div>
      <div class="modal-footer">
        <input type="button" class="btn btn-outline btn-success" id="agregarcatalogos" value="Guardar" />
      </div>
    </div>

  </div>
</div>

<!-- Large modal -->
