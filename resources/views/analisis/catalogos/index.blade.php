@extends('layouts.adm')
	@section('content')
	@include('analisis.catalogos.modal.create')
  @include('analisis.catalogos.modal.edit')

			<div class="inner-wrapper">
				<section role="main" class="content-body pb-0">
					<!-- <header class="page-header">
						<h2>Giros</h2>
						<div class="right-wrapper text-right">
							<ol class="breadcrumbs">
								<li>
									<a href="{{url("home")}}">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span class="blanco"></span></li>
							</ol>
						</div>
					</header> -->
					@include('flash::message')
					@if(Session::has('message'))
							<div class="alert alert-success" role="alert">
								{{Session::get('message')}}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
					@endif
					@if(Session::has('flash_delete'))
							<div class="alert alert-warning " role="alert">
								{{Session::get('flash_delete')}}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
					@endif
					<br>
					@include('flash::message')
					@if(Session::has('flash_success'))
							<div class="alert alert-success" role="alert">
								{{Session::get('flash_success')}}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
					@endif
					<div class="col-md-12">
								<section class="card card-featured card-featured-primary mb-4">
									<header class="card-header">
										<div class="card-actions">
											<button type="button" class="btn btn-outline btn-link" data-toggle='modal'
											data-target='#agregaconsecuencia' style="color:#fff;">	<i class="fa fa-plus"> 	Agregar</i></button>

										</div>

										<h2 class="card-title blanco">Catálogos de Análisis de Riesgo</h2>
											<input type="hidden" name="status" id="status" value="{{  $status }}" />

									</header>
									<div class="card-body">
										<!-- <code>.card-featured.card-featured-primary</code> -->
										<div class="	">

											<!-- <div class="container">
											<form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ URL::to('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
												<input type="file" name="import_file" />
												<button class="btn btn-primary">Import File</button>
											</form>
										</div> -->
											<div class="tabs tabs-primary">
												<ul class="nav nav-tabs">
													<li class="nav-item active">
														<a class="nav-link active" href="#altas" data-toggle="tab">Consecuencias</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" href="#baja" data-toggle="tab">Consecuencias Bajas</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" href="#exposicion" data-toggle="tab">Exposición</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" href="#exposicionbaja" data-toggle="tab">Exposición Bajas</a>
													</li>
                          <li class="nav-item">
                            <a class="nav-link" href="#probabilidad" data-toggle="tab">Probabilidad</a>
                          </li>
													<li class="nav-item">
														<a class="nav-link" href="#probabilidadbaja" data-toggle="tab">Probabilidad Bajas</a>
													</li>
                          <li class="nav-item">
                            <a class="nav-link" href="#grado" data-toggle="tab">Grado de Riesgo</a>
                          </li>
												</ul>
												<div class="tab-content">
													<div  id="altas" class="tab-pane active">
														<table width="100%" class="table table-responsive-md table-sm  ">
															<thead>
																	<tr>
																		<td><b>ID</b></td>
																		<td><b>Descripción</b></td>
                                    <td><b>Nombre corto</b></td>
																		<td><b>Estatus</b></td>
																		<td><b>Acción</b></td>
																	</tr>
															</thead>
															@foreach($analisis as $ana)
																<tbody>
																		<tr class="odd gradeX">
																				<td>{{$ana->cmfk }}</td>
                                        <td>{{$ana->cmabbr}}</td>
																				<td>{{ $ana->cmdesc }}</td>
																				<td>
																					<img src="{{ url('admin/img/success.png') }}"alt="">
																					<!-- <button type="button" class="btn btn-success btn-circle"><i class="fa fa-check"></i></button> -->
																				</td>
																				<td class="">
																						<div class="row">
																							<div class="col-md- col-md-offset-1">
																								{!!Form::open(['route'=> ['catalogos.destroy',$ana->cmfk, 'status' => $ana->cmstatus],'method'=>'DELETE'])!!}
																									{{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-xs btn-default','title'=>'Inactivar']) }}
																									{!!Form::close()!!}
																							</div>
																								<div class="col-md-1 col-md-offset-1">
																									<a data-toggle='modal' id="editActividad" data-target='#editActividad'
																									 data-id='{{ $ana->cmfk }}|{{$ana->cmtpcat}}' class="btn btn-xs btn-default ">  <i class="fa fa-edit"></i></a>
																								</div>
																					</div>
																				</td>
																		</tr>
																	</tbody>
																@endforeach
														</table>
														<div class=" pull-right">
																{{ $analisis->appends(['status' => 1])}}
														</div>
													</div>
													<div  id="baja" class="tab-pane fade">
														<table width="100%" class="table table-responsive-md table-sm  ">
															<thead>
																	<tr>
																		<td><b>ID</b></td>
																		<td><b>Descripción</b></td>
                                    <td><b>Nombre corto</b></td>
																		<td><b>Estatus</b></td>
																		<td><b>Acción</b></td>
																	</tr>
															</thead>
															@foreach($analisisBaja as $anabaja)
																<tbody>
																		<tr class="odd gradeX">
																				<td>{{$anabaja->cmfk }}</td>
                                        <td>{{$anabaja->cmabbr}}</td>
																				<td>{{$anabaja->cmdesc }}</td>
																				<td>
																					<img src="{{ url('admin/img/success.png') }}"alt="">
																					<!-- <button type="button" class="btn btn-success btn-circle"><i class="fa fa-check"></i></button> -->
																				</td>
																				<td class="">
																						<div class="row">
																							<div class="col-md- col-md-offset-1">
																								{!!Form::open(['route'=> ['catalogos.destroy',$anabaja->cmfk, 'status' => $anabaja->cmstatus],'method'=>'DELETE'])!!}
																									{{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-xs btn-default','title'=>'Inactivar']) }}
																									{!!Form::close()!!}
																							</div>
																								<div class="col-md-1 col-md-offset-1">
																									<a data-toggle='modal' id="editActividad" data-target='#editActividad'
																									 data-id='{{ $anabaja->cmfk }}|{{$anabaja->cmtpcat}}' class="btn btn-xs btn-default ">  <i class="fa fa-edit"></i></a>
																								</div>
																					</div>
																				</td>
																		</tr>
																	</tbody>
																@endforeach
														</table>
														<div class=" pull-right">
																{{ $analisisBaja->appends(['status' => 0])}}
														</div>
													</div>
                          <div id="exposicion" class="tab-pane fade">
                            <table width="100%" class="table table-responsive-md table-sm 	">
                              <thead>
                                  <tr>
                                    <td><b>ID</b></td>
                                    <td><b>Descripción</b></td>
                                    <td><b>Nombre corto</b></td>
                                    <td><b>Estatus</b></td>
                                    <td><b>Acción</b></td>
                                  </tr>
                              </thead>
                              @foreach($analisis51 as $ana51)
                                <tbody>
                                    <tr class="odd gradeX">
                                      <td>{{$ana51->cmfk }}</td>
                                      <td>{{$ana51->cmabbr}}</td>
                                      <td>{{$ana51->cmdesc }}</td>
                                      <td>
                                        <img src="{{ url('admin/img/success.png') }}"alt="">
                                        <!-- <button type="button" class="btn btn-success btn-circle"><i class="fa fa-check"></i></button> -->
                                      </td>
                                        <td>
                                            <div class="row">
																							<div class="col-md- col-md-offset-1">
																								{!!Form::open(['route'=> ['catalogos.destroy',$ana51->cmfk, 'status' => $ana51->cmstatus],'method'=>'DELETE'])!!}
																									{{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-xs btn-default','title'=>'Inactivar']) }}
																									{!!Form::close()!!}
																							</div>
																								@can('update',new App\catalog_master)
                                                <div class="col-md-1 col-md-offset-1">
                                                  <a data-toggle='modal' id="editActividad" data-target='#editActividad'
                                               data-id='{{ $ana51->cmfk }}|{{$ana51->cmtpcat}}' class="btn btn-xs btn-default ">  <i class="fa fa-edit"></i></a>
                                              </div>
																							@endcan
                                          </div>
                                        </td>
                                    </tr>
                                  </tbody>
                                @endforeach
                            </table>
                            <div class=" pull-right">
                                {{ $analisis51->appends(['status' => 1])}}
                            </div>
												 </div>
												 <div id="exposicionbaja" class="tab-pane fade">
													 <table width="100%" class="table table-responsive-md table-sm 	">
														 <thead>
																 <tr>
																	 <td><b>ID</b></td>
																	 <td><b>Descripción</b></td>
																	 <td><b>Nombre corto</b></td>
																	 <td><b>Estatus</b></td>
																	 <td><b>Acción</b></td>
																 </tr>
														 </thead>
														 @foreach($analisis51Baja as $ana51baja)
															 <tbody>
																	 <tr class="odd gradeX">
																		 <td>{{$ana51baja->cmfk }}</td>
																		 <td>{{$ana51baja->cmabbr}}</td>
																		 <td>{{$ana51baja->cmdesc }}</td>
																		 <td>
																			 <img src="{{ url('admin/img/success.png') }}"alt="">
																			 <!-- <button type="button" class="btn btn-success btn-circle"><i class="fa fa-check"></i></button> -->
																		 </td>
																			 <td>
																					 <div class="row">
																						<div class="col-md- col-md-offset-1">
																							{!!Form::open(['route'=> ['catalogos.destroy',$ana51baja->cmfk, 'status' => $ana51baja->cmstatus],'method'=>'DELETE'])!!}
																								{{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-xs btn-default','title'=>'Inactivar']) }}
																								{!!Form::close()!!}
																						</div>
																							@can('update',new App\catalog_master)
																							 <div class="col-md-1 col-md-offset-1">
																								 <a data-toggle='modal' id="editActividad" data-target='#editActividad'
																							data-id='{{ $ana51baja->cmfk }}|{{$ana51baja->cmtpcat}}' class="btn btn-xs btn-default ">  <i class="fa fa-edit"></i></a>
																						 </div>
																						@endcan
																				 </div>
																			 </td>
																	 </tr>
																 </tbody>
															 @endforeach
													 </table>
													 <div class=" pull-right">
															 {{ $analisis51Baja->appends(['status' => 0])}}
													 </div>
											 </div>
                         <div id="probabilidad" class="tab-pane fade">
                             <table width="100%" class="table table-responsive-md table-sm 	">
                               <thead>
                                   <tr>
                                     <td><b>ID</b></td>
                                     <td><b>Descripción</b></td>
                                     <td><b>Nombre corto</b></td>
                                     <td><b>Estatus</b></td>
                                     <td><b>Acción</b></td>
                                   </tr>
                               </thead>
                               @foreach($analisis52 as $ana52)
                                 <tbody>
                                     <tr class="odd gradeX">
                                       <td>{{$ana52->cmfk }}</td>
                                       <td>{{$ana52->cmabbr}}</td>
                                       <td>{{$ana52->cmdesc }}</td>
                                       <td>
                                         <img src="{{ url('admin/img/success.png') }}"alt="">
                                         <!-- <button type="button" class="btn btn-success btn-circle"><i class="fa fa-check"></i></button> -->
                                       </td>
                                         <td>
                                             <div class="row">
																							 <div class="col-md- col-md-offset-1">
																								 {!!Form::open(['route'=> ['catalogos.destroy',$ana52->cmfk, 'status' => $ana52->cmstatus],'method'=>'DELETE'])!!}
																									 {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-xs btn-default','title'=>'Inactivar']) }}
																									 {!!Form::close()!!}
																							 </div>
																							 	@can('update',new App\catalog_master)
                                                 <div class="col-md-1 col-md-offset-1">
                                                   <a data-toggle='modal' id="editActividad" data-target='#editActividad'
                                                data-id='{{ $ana52->cmfk }}|{{$ana52->cmtpcat}}' class="btn btn-xs btn-default ">  <i class="fa fa-edit"></i></a>
                                               </div>
																							 @endcan
                                           </div>
                                         </td>
                                     </tr>
                                   </tbody>
                                 @endforeach
                             </table>
                             <div class=" pull-right">
                                 {{ $analisis52->appends(['status' => 52])}}
                             </div>
                          </div>
                          <div id="grado" class="tab-pane fade">
                                  <table width="100%" class="table table-responsive-md table-sm 	">
                                    <thead>
                                        <tr>
                                          <td><b>ID</b></td>
                                          <td><b>Descripción</b></td>
                                          <td><b>Nombre corto</b></td>
                                          <td><b>Estatus</b></td>
                                          <td><b>Acción</b></td>
                                        </tr>
                                    </thead>
                                    @foreach($analisis53 as $ana53)
                                      <tbody>
                                          <tr class="odd gradeX">
                                            <td>{{$ana53->cmfk }}</td>
                                            <td>{{$ana53->cmabbr}}</td>
                                            <td>{{$ana53->cmdesc }}</td>
                                            <td>
                                              <img src="{{ url('admin/img/success.png') }}"alt="">
                                              <!-- <button type="button" class="btn btn-success btn-circle"><i class="fa fa-check"></i></button> -->
                                            </td>
                                              <td>
                                                  <div class="row">
                                                      <div class="col-md-1 col-md-offset-1">
                                                        <a data-toggle='modal' id="editActividad" data-target='#editActividad'
                                                     data-id='{{ $ana53->cmfk}}|{{$ana53->cmtpcat}}' class="btn btn-xs btn-default ">  <i class="fa fa-edit"></i></a>
                                                    </div>
                                                </div>
                                              </td>
                                          </tr>
                                        </tbody>
                                      @endforeach
                                  </table>
                                  <div class=" pull-right">
                                      {{ $analisis53->appends(['status' => 53])}}
                                  </div>
                              </div>
															<div id="probabilidadbaja" class="tab-pane fade">
		                              <table width="100%" class="table table-responsive-md table-sm 	">
		                                <thead>
		                                    <tr>
		                                      <td><b>ID</b></td>
		                                      <td><b>Descripción</b></td>
		                                      <td><b>Nombre corto</b></td>
		                                      <td><b>Estatus</b></td>
		                                      <td><b>Acción</b></td>
		                                    </tr>
		                                </thead>
		                                @foreach($analisis52Baja as $ana52Baja)
		                                  <tbody>
		                                      <tr class="odd gradeX">
		                                        <td>{{$ana52Baja->cmfk }}</td>
		                                        <td>{{$ana52Baja->cmabbr}}</td>
		                                        <td>{{$ana52Baja->cmdesc }}</td>
		                                        <td>
		                                          <img src="{{ url('admin/img/success.png') }}"alt="">
		                                          <!-- <button type="button" class="btn btn-success btn-circle"><i class="fa fa-check"></i></button> -->
		                                        </td>
		                                          <td>
		                                              <div class="row">
		 																							 <div class="col-md- col-md-offset-1">
		 																								 {!!Form::open(['route'=> ['catalogos.destroy',$ana52Baja->cmfk, 'status' => $ana52Baja->cmstatus],'method'=>'DELETE'])!!}
		 																									 {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-xs btn-default','title'=>'Inactivar']) }}
		 																									 {!!Form::close()!!}
		 																							 </div>
		 																							 	@can('update',new App\catalog_master)
		                                                  <div class="col-md-1 col-md-offset-1">
		                                                    <a data-toggle='modal' id="editActividad" data-target='#editActividad'
		                                                 data-id='{{ $ana52Baja->cmfk }}|{{$ana52Baja->cmtpcat}}' class="btn btn-xs btn-default ">  <i class="fa fa-edit"></i></a>
		                                                </div>
		 																							 @endcan
		                                            </div>
		                                          </td>
		                                      </tr>
		                                    </tbody>
		                                  @endforeach
		                              </table>
		                              <div class=" pull-right">
		                                  {{ $analisis52Baja->appends(['status' => 0])}}
		                              </div>
		                           </div>
		                           <div id="grado" class="tab-pane fade">
		                                   <table width="100%" class="table table-responsive-md table-sm 	">
		                                     <thead>
		                                         <tr>
		                                           <td><b>ID</b></td>
		                                           <td><b>Descripción</b></td>
		                                           <td><b>Nombre corto</b></td>
		                                           <td><b>Estatus</b></td>
		                                           <td><b>Acción</b></td>
		                                         </tr>
		                                     </thead>
		                                     @foreach($analisis53 as $ana53)
		                                       <tbody>
		                                           <tr class="odd gradeX">
		                                             <td>{{$ana53->cmfk }}</td>
		                                             <td>{{$ana53->cmabbr}}</td>
		                                             <td>{{$ana53->cmdesc }}</td>
		                                             <td>
		                                               <img src="{{ url('admin/img/success.png') }}"alt="">
		                                               <!-- <button type="button" class="btn btn-success btn-circle"><i class="fa fa-check"></i></button> -->
		                                             </td>
		                                               <td>
		                                                   <div class="row">
		                                                       <div class="col-md-1 col-md-offset-1">
		                                                         <a data-toggle='modal' id="editActividad" data-target='#editActividad'
		                                                      data-id='{{ $ana53->cmfk}}|{{$ana53->cmtpcat}}' class="btn btn-xs btn-default ">  <i class="fa fa-edit"></i></a>
		                                                     </div>
		                                                 </div>
		                                               </td>
		                                           </tr>
		                                         </tbody>
		                                       @endforeach
		                                   </table>
		                                   <div class=" pull-right">
		                                       {{ $analisis53->appends(['status' => 1])}}
		                                   </div>
		                               </div>
											</div>
											</div>

									</div>
								</section>
							</div>



					<!-- end: page -->
				</section>
			</div>

			@stop
      @section('scripts')
      	{!!Html::script('js/Generics.js')!!}
      	{!!Html::script('js/admin/createcatalogos.js')!!}
      	{!!Html::script('js/admin/catalogos.js')!!}
      @endsection
