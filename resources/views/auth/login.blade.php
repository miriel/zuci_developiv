@extends('layouts.principal')

@section('content')


        <div class="col-lg-12 col-lg-offset-12">
          @include('flash::message')
          @if(Session::has('flash_envio'))
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{Session::get('flash_envio')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
          @include('flash::message')
          @if(Session::has('tiempo'))
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{Session::get('tiempo')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
            </div>
            <div role="main" class="main ">
              <section class="page-header">
                <div class="container text-center">
                  <div class="row">
                    <div class="col">
                      <h1>Login</h1>
                    </div>
                  </div>
                </div>
              </section>

              <div class="container ">

                <div class="row">
                  <div class="col">

                    <div class="featured-boxes">
                      <div class="row">
                        <div class="col-lg-3">

                        </div>
                        <div class="col-md-6">
                          <div class="featured-box featured-box-primary text-left mt-5">
                            <div class="box-content">
                              <form class="form" method="POST" action="{{ route('login') }}">
                                  {{ csrf_field() }}
                                <div class="form-row">
                                  <div class="form-group col">
                                    <label>E-mail Address</label>
                                    <input id="email" type="email" class="form-control" name="email"  aria-describedby="emailHelp" value="{{ old('email') }}" required autofocus>
                                    <span class="help-block alert-danger">  <p style="color:red;">{{$errors->first('email')}}</p></span>
                                  </div>
                                </div>
                                <div class="form-row">
                                  <div class="form-group col">
                                    <a class="float-right" href="{{ route('password.request') }}">(Lost Password?)</a>
                                    <label>Password</label>
                                    <input id="password" type="password" class="form-control" name="password" required>
                                    <span class="help-block">  <p class="alert-danger">{{$errors->first('password')}}</p></span>
                                  </div>
                                </div>
                                <div class="form-row">
                                  <div class="form-group col-lg-6">
                                    <div class="form-check form-check-inline">
                                      <!-- <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" id="rememberme" name="rememberme"> Remember Me
                                      </label> -->
                                    </div>
                                  </div>
                                  <div class="form-group col-lg-6">
                                    <input type="submit" value="Login" class="btn btn-primary float-right mb-5" data-loading-text="Loading...">
                                  </div>
                                </div>
                              </form>
                              <div class="form-group ">
                                <div class="text-center" style="font-size: 1.8em;">
                                  <a class="d-block small" href="{{ route('register') }}">Register an Account</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>

                    </div>
                  </div>
                </div>

              </div>

            </div>


@endsection
