@extends('layouts.app')

@section('content')
        <div role="main" class="main">

          <section class="page-header ">
            <div class="container">
              <div class="row">
                <div class="col-lg-12 text-center">
                  <h1>ACTIVACION</h1>
                  @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                  @endif
                  @include('flash::message')
                  @if(Session::has('flash_expired'))
                  <div class="alert alert-success" role="alert">
                    {{Session::get('flash_expired')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  @endif
               @include('flash::message')
                 @if(Session::has('flash_errror'))
                 <div class="alert alert-danger alert-dismissible fade show" role="alert">
                   <div class="blanco">
                     {{Session::get('flash_errror')}}
                   </div>
                   <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span> </button>
                 </div>
                     @endif
                </div>
              </div>
            </div>
          </section>
            <div class="container text-center register">
            <div class="row ">
              <div class="col-lg-4"> </div>
              <div class="col-md-4">
                <div class="featured-box featured-box-primary  mt-5 ">
                  <div class="box-content">
                    <h4 class="heading-primary text-uppercase mb-3 text-center">Reenvio de activacion </h4>
                    <div class="panel-body">
                      <div class="text-center mt-4 mb-5">
                        <h4>You have not activated your account ?</h4>
                        <p>Enter your email address and we will send you instructions on how to activate your account.</p>
                      </div>
                          <form class="form" method="POST" action="{{ URL::asset('validacorreor') }}">
                          {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-6 control-label">E-Mail Address</label>
                          <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                          @if ($errors->has('email'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                        </div>
                        <button type="submit" class="btn  btn-primary "> Send link account activation</button>
                      </form>
                </div>
              </div>

          </div>
      </div>



@endsection
