@extends('layouts.principal')

@section('content')
        <div role="main" class="main">

          <section class="page-header text-center">
            <div class="container">
              <div class="row">
                <div class="col">
                  <h1>REGISTER</h1>
                </div>
              </div>
            </div>
          </section>
            <div class="container register booking-area">
            <div class="row ">
              <div class="col-md-6">
                <div class="featured-box featured-box-primary text-left mt-5 ">
                  <div class="box-content">
                    <h4 class="heading-primary text-uppercase mb-3">DATOS EMPRESARIALES </h4>
                      {!!Form::open(['route'=>'register', 'method'=>'POST'])!!}
                      <div class="form-row">
                        <div class="form-group col">
                          <label>Nombre de la empresa <strong style="color:#ff1515;">*</strong> </label>
                          <input id="nombreempresa" type="text" class="form-control mayusculas " name="nombreempresa" value="{{ old('nombreempresa') }}" >
                          <span class="help-block"><p class="alert-danger"> <strong class="">{{$errors->first('nombreempresa')}}</strong> </p></span>
                            </div>
                        </div>

                      <div class="form-row">
                        <div class="form-group col">
                          <label>Razón Social <strong style="color:#ff1515;">*</strong> </label>
                          <input id="razon" type="text" class="form-control mayusculas" name="razon" value="{{ old('razon') }}" >
                          <span class="help-block">  <p class="alert-danger">{{$errors->first('razon')}}</p></span>
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="form-group col">
                          <label>RFC de la empresa <strong style="color:#ff1515;">*</strong> </label>
                          <input id="burfc" type="text" class="form-control mayusculas" name="burfc" value="{{ old('burfc') }}">
                          <span class="help-block"><p class="alert-danger">{{$errors->first('burfc')}}</p></span>
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="form-group col-lg-6">
                          <label >Giro Comercial</label>
                              {!!Form::select('giro',$arryVals['giro'], null, ['id'=>'giro','class'=>'nice-select form-control','placeholder'=>'SELECCIONA']) !!}
                          <span class="help-block"><p class=" alert-danger">{{$errors->first('giro')}}</p></span>
                        </div>
                        <div class="form-group col-lg-6">
                          <label>Tipo de Empresa</label>
                              {!!Form::select('tipo', $arryVals['tipo'], null, ['id'=>'tipo','class'=>'nice-select form-control','placeholder'=>'SELECCIONA']) !!}
                            <span class="help-block">  <p class=" alert-danger">{{$errors->first('tipo')}}</p>  </span>
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="form-group col-lg-6">
                            <label>País</label>
                            {!!Form::select('countries', $arryVals['countries'], null, ['class'=>'nice-select form-control','id'=>'countries','placeholder'=>'SELECCIONA']) !!}
                            <span class="help-block"><p class=" alert-danger">{{$errors->first('countries')}}</p>  </span>
                          </div>
                          <div class="form-group col-lg-6">
                            <label >Ciudad</label>
                              {!!Form::select('city',['0'=>'SELECCIONA'], null, ['id'=>'city','class'=>'nice-select form-control']) !!}
                                  {!!Form::hidden('id_city',null, ['id'=>'id_city','class'=>'nice-select form-control ']) !!}
                            <span class="help-block">  <p class=" alert-danger">{{$errors->first('city')}}</p>  </span>
                          </div>
                      </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="featured-box featured-box-primary text-left mt-5">
                  <div class="box-content">
                    <h4 class="heading-primary text-uppercase mb-3">Datos Personales</h4>
                      <div class="form-row">
                        <div class="form-group col">
                          <label>Nombre <strong style="color:#ff1515;">*</strong> </label>
                              {!! Form::text('name', null, ['class' => 'form-control mayusculas','onkeypress'=>'return soloLetras(event);']) !!}
                              <p class="alert-danger">{{$errors->first('name')}}</p>
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="form-group col-lg-6">
                          <label >Apellido Paterno <strong style="color:#ff1515;">*</strong> </label>
                            <input id="ApellidoPaterno" type="text" class="form-control mayusculas" onkeypress="return soloLetras(event);" name="ApellidoPaterno" value="{{ old('ApellidoPaterno') }}" >
                          <p class="alert-danger">{{$errors->first('ApellidoPaterno')}}</p>
                        </div>
                        <div class="form-group col-lg-6">
                          <label >Apellido Materno <strong style="color:#ff1515;">*</strong> </label>
                            <input id="apellidomaterno" type="text" class="form-control mayusculas" onkeypress="return soloLetras(event);" name="apellidomaterno" value="{{ old('apellidomaterno') }}" >
                              <p class="alert-danger">{{$errors->first('apellidomaterno')}}</p>
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="form-group col-lg-6">
                          <label >Puesto que desempeña <strong style="color:#ff1515;">*</strong> </label>
                              <input id="puesto" type="text" class="form-control mayusculas" name="puesto" value="{{ old('puesto') }}" >
                              <p class="alert-danger">{{$errors->first('puesto')}}</p>
                        </div>
                        <div class="form-group col-lg-6">
                          <label>Área a la que pertenece <strong style="color:#ff1515;">*</strong>  </label>
                            <input id="area" type="text" class="form-control mayusculas" name="area" value="{{ old('area') }}" >
                            <p class="alert-danger">{{$errors->first('area')}}</p>
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="form-group col">
                          <label >E-Mail Address <strong style="color:#ff1515;">*</strong> </label>
                          <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" >
                            <p class="alert-danger">{{$errors->first('email')}}</p>
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="form-group col-lg-6">
                          <label >Lada /  Telefono Fijo de la empresa:</label>
                            <div class="input-group">
                            {!!Form::text('lada', null, ['id'=>'lada','class'=>'col-lg-2  nice-select form-control disabled','placeholder'=>'Lada','disabled']) !!}
                             {!!Form::text('phone1', null, ['name'=>'phone1','id'=>'phone1','class'=>'col-lg-10  nice-select form-control ','placeholder'=>'Telefono']) !!}
                               </div>
                               <p class="alert-danger">{{$errors->first('phone1')}}</p>
                        </div>
                        <div class="form-group col-lg-6">
                          <label >Lada /  Telefono Celular:</label>
                            <div class="input-group">
                                {!!Form::text('lada_cel', null, ['id'=>'lada_cel','class'=>'col-lg-2  nice-select form-control ','placeholder'=>'Lada']) !!}
                                {!!Form::text('phone2', null, ['name'=>'phone2','class'=>'col-lg-10  nice-select form-control','placeholder'=>'Telefono']) !!}
                            </div>
                            <p class=" alert-danger">{{$errors->first('lada_cel')}}</p>
                              <p class=" alert-danger">{{$errors->first('phone2')}}</p>
                            </div>
                      </div>
                      </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="featured-box featured-box-primary text-left mt-5">
                    <div class="box-content">
                      <h3 class="heading-primary text-uppercase mb-3 text-center">Aviso de Privacidad</h3>
                <div class="form-row">
                  <div class="form-groip col-lg-12">
                    <textarea class="form-control  text-black  alinear "  disabled id="" rows="5">
                            Zuma Tecnologías de Información S.C. (en lo sucesivo Zuma -Ti), con domicilio en calle Pilastra # 10, Col. Residencial Villa Coapa, México, D.F,
                            Deleg. Tlalpan, cp. 14390, y portal de internet www.zuma-ti.com.mx, es el responsable del uso y protección de sus datos personales, y al
                            respecto le informamos lo siguiente:
                            Finalidad.Los datos personales que recabamos de usted, los utilizaremos para las siguientes finalidades que son necesarias para el
                            servicio que solicita:.
                                Establecer una relación comercial
                                Prospección comercial
                                Conexión con alguna postulación de vacantes
                            En caso de que no desee que sus datos personales sean tratados para realizar contactos adicionales con usted que vayan más allá de
                            cumplir con su solicitud, desde este momento usted nos puede comunicar lo anterior a través de los siguientes medios:

                                Correo electrónico: privacidad@zuma-ti.com.mx
                                Teléfono: +52(55) 6724 0315
                                Contacto vía página web: http://zuma-ti.com.mx/es/contacto.php

                            La negativa para el uso de sus datos personales para estas finalidades no podrá ser un motivo para que le neguemos los servicios
                            y productos que solicita o contrata con nosotros.
                            Derecho de Acceso y Corrección Como titular de sus datos personales, tiene el derecho de solicitarnos los derechos de acceso,
                            rectificación, cancelación y oposición (derechos ARCO), establecidos en la Ley. Asimismo, podrá revocar, en todo momento,
                            el consentimiento que haya otorgado y que fuese necesario para llevar a cabo el tratamiento de sus datos personales,
                            así como solicitar que se limite el uso o divulgación de los mismos. A continuación se listan los siguientes medios
                            para enviar su solicitud:

                                Correo electrónico: privacidad@zuma-ti.com.mx
                                A la dirección Pilastra # 10, Col. Residencial Villa Coapa, México, D.F, Deleg. Tlalpan, cp. 14390
                                Comunicarse al número telefónico +52(55) 6724 0315

                            Zuma-Ti se reserva el derecho de hacer cambios en el presente Aviso de Privacidad, mismos que serán comunicados
                             vía la página web www.zuma-ti.com.mx

                          </textarea><br>
                          <div class="checkbox text-center">
                            <label><input type="checkbox" value="" required><strong> ACEPTAR</strong></label>
                            </div>
                  </div>

                </div>
                  </div>
                    </div>
                      </div>
              </div>


            <div class="form-row">
                <div class="form-group col-lg-9">
              <a class="btn btn-light mb-2"  href="{{URL::to('/')}}"><i class="fa fa-chevron-left"></i> Regresar</a>
              <button class="btn btn-light float-right mb-5  text-center" data-loading-text ="Loading..." name="button">Registrar <i class="fa fa-chevron-right"></i> </button>
              </div>
            </div>

          {!!Form::close()!!}

          </div>
      </div>



@endsection

@section('scripts')
  {!!Html::script('js/dropdown.js')!!}
@endsection
