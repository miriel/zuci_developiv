<!-- Modal -->
<div class="modal fade" id="createCatalogo" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class="letracolor">Agregar Sección /Subseccion</h4>
          <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <!-- Mensajes exitosos -->
            <div id="msj-successCr" class="alert alert-success" role="alert" style="display:none">
                Registro agregado correctamente
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            {!! Form::label('catvalueCr','Nombre') !!}
            {!! Form::text('catvalueCr',null, ['id'=>'catvalueCr','class'=>'form-control'])!!}
            <!-- Mensajes de error -->
            <div id="msj-errorCr" class=" alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjCr"></strong>
            </div>

            {!! Form::label('catnamtablCr','Tipo seccion') !!}
            {!! Form::select('catnamtablCr',[
                'questionnaries_section'=>'Seccion',
                'questionnaries_subsection'=>'SubSeccion',
                ],null,['id'=>'catnamtablCr','class'=>'form-control'])
            !!}

            <div class="modal-footer">
              <input type="button" class="btn btn-outline btn-success" id="btnCreaRegCat" value="Guardar" />
            </div>
      </div>
    </div>
  </div>
</div>
