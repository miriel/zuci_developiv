@extends('layouts.adm')
@section('content')
@include('cuestionarios.respuestas.modal.actualizar')
@include('cuestionarios.preguntas.modal.edit')
@include('cuestionarios.asignapreguntas.modal.create')


      <div class="inner-wrapper">
          <section role="main" class="content-body pb-0">
              @if(Session::has('message'))
                  <div class="alert alert-success" role="alert">
                    {{Session::get('message')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
              @endif
              <!-- /.row -->
              <div class="col-md-12">
                @foreach($arrayIdTipoCuestion as $idTipoCuestionario)
                    <input type="hidden" value="{{ $idTipoCuestionario }}" id="idTipoCuestionario" />
                @endforeach
                <section class="card card-featured card-featured-primary mb-4">
                    <header class="card-header">
                      <h2 class="card-title blanco">{{ $cuestionario[0]->quname }}

                        <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" content="{{ csrf_token() }}" />
                      </h2>
                    </header>
                      @can('question',new App\Questionnaire)
                    <div class="card-body">
                        {!!Form::model($cuestionario,['route'=> ['cuestionario.update',$cuestionario[0]->qufk],'method'=>'PUT'])!!}
                            <input type="hidden" name="atqbk" id="atqbk" value="{{ $cuestionario[0]->atqbk }}" />
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" >
                            <div class="form-group">
                            </div>
                            <div class="form-group">
                              {!!Form::label('quname','Nombre cuestionario')!!}
                              {!! Form::text('quname',$cuestionario[0]->quname, ['id'=>'quname','class'=>'form-control'])!!}
                              <!-- Mensajes de error -->
                              <div id="msj-error" class="alert alert-danger" role="alert" style="display:none">
                                  <strong class="help-block blanco" id="msj"></strong>
                              </div>
                              {!!Form::label('atqqtfk','Tipo Cuestionario')!!}
                              {!!Form::select('atqqtfk',$tipoCuestionario,$cuestionario[0]->atqqtfk, ['id'=>'atqqtfk','class'=>'form-control'])!!}
                            </div>
                            <div class="form-group">
                                {{ Form::button('Guardar', ['type' => 'submit', 'class' => 'btn btn-outline btn-success']) }}
                                <a class="btn btn-outline btn-success" href="{{ route('tempcuest.index',['id' => $cuestionario[0]->qufk, 'name' => $cuestionario[0]->quname  ] )}}">Templates</a>
                            </div>
                       {!!Form::close()!!}




                    </div>
                    @endcan


                </section>
                <section class="card card-featured card-featured-primary mb-4">
                      <header class="card-header">
                        <div class="card-actions">
                            @can('pregres',new App\Questionnaire)
                            <button type="button" class="btn btn-outline btn-link" data-toggle='modal' id="btnCreateAsignPregnt"
                            data-target='#createAsignPregnt' style="color:#fff;">	<i class="fa fa-plus"> 	Agregar</i></button>
                            @endcan
                        </div>
                        <h2 class="card-title blanco">Contenido
                          <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" content="{{ csrf_token() }}" />
                        </h2>
                      </header>
                      <div class="card-body">
                          <?php
                            $idPregResp="";
                            $result="";
                            $validSecc="";
                            $validSubSecc="";

                            foreach( $arrayIdSecciones as $idSeccion ){
                              // Tiene secciones
                              if($idSeccion != ""){
                                $validSecc=1;
                              // No tiene secciones
                              }else{
                                $validSecc=0;
                              }
                            }

                            foreach( $arrayIdSubSecciones as $idSubSeccion ){
                              // tiene secciones
                              if($idSubSeccion != ""){
                                $validSubSecc=1;
                              // No tiene secciones
                              }else{
                                $validSubSecc=0;
                              }
                            }

                            if( $validSecc == 1 || $validSubSecc ==  1 ){

                                    /* ###########  CON SECCIONES Y SUBSECCIONES  ###############*/
                                    foreach( $arrayIdSecciones as $idSeccion ){

                                        $result.="<div class='accordion accordion-primary' id='accordion2Primary'>";
                                          $result.="<div class='card card-default'>";
                                            $result.="<div class='card-header'>";
                                              $result.="<h4 class='card-title m-1'>";
                                                $result.="<a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion2Primary' href='#".$idSeccion."'>";
                                                    $result.=$arrayNameSeccion[$idSeccion];
                                                $result.="</a>";
                                                $result.="</h4>";
                                              $result.="</div>";

                                              $result.="<div id='".$idSeccion."' class='collapse'>";
                                              $result.="<div class='card-body'>";

                                                foreach($arrayIdSubSecciones as $idSubSeccion){
                                                  if(!isset($arrayNameSubseccion[$idSeccion][$idSubSeccion])){ }else{
                                                    $result.="<h4 class='letra2'>".$arrayNameSubseccion[$idSeccion][$idSubSeccion]."</h4>";
                                                  }

                                                  $result.="<table width='100%' border='0'>";
                                                      /// Seccion  de nombre de preguntas
                                                   foreach($arrayIdPregunta as $idPregunta){

                                                      foreach($arrayIdRespuesta as $idRespuesta){
                                                        if(!isset($arrayPregResp[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta])){
                                                        }else{
                                                            $idPregResp = $arrayPregResp[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta];
                                                        }
                                                      }

                                                      if(!isset($arrayNamePregunta[$idSeccion][$idSubSeccion][$idPregunta]) ){}else{

                                                          if(!isset($idSeccion) ){  }else{ }
                                                          if(!isset($idSubSeccion) ){  }else{ }

                                                          $result.="<tr bgcolor='#E6E6E6'>
                                                                      <td>
                                                                      <i class='fa fa-edit' data-toggle='modal' data-target='#editPregnts' id='editPregnts'
                                                                      data-id='".$idPregResp."|".$idPregunta."|".$idSeccion."|".$idSubSeccion."'></i>
                                                                          ".$arrayNamePregunta[$idSeccion][$idSubSeccion][$idPregunta]."
                                                                      </td>
                                                                      <td>
                                                                        Estatus
                                                                      </td>
                                                                      <td colspan='2'>
                                                                          Acciones
                                                                      </td>";
                                                          $result.="<tr>";


                                                          // Respuestas
                                                          foreach($arrayIdRespuesta as $idRespuesta){
                                                              $result.="<tr>";

                                                              if(!isset($arrayPregResp[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta])){
                                                              }else{
                                                                  // Obtiene el ID r de cada respuesta paa actualizarla postreiormente
                                                                  $idPregRespOk = $arrayPregResp[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta];
                                                              }

                                                              if(!isset( $arrayNameRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta] )){}else{

                                                                $result.="<td>";
                                                                $result.="".$arrayNameRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta]."
                                                                          </td>";

                                                                // Muestra el estatu de la respuesta
                                                                if(!isset($arrayStatusRespst[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta])){}else{
                                                                  if($arrayStatusRespst[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta] == 1){
                                                                    //<i class='fa fa-check'></i></span>
                                                                    $result.="<td>
                                                                                Activo
                                                                              </td>";
                                                                  }else{
                                                                    //<i class='fa fa-times'></i></span>
                                                                    $result.="<td>
                                                                                  Inactivo
                                                                              </td>";
                                                                  }
                                                                }

                                                                $result.="<td>
                                                                          <i class='fa fa-edit' data-toggle='modal' data-target='#editRespsts' id='editRespsts' data-id='".$idPregRespOk."|".$idRespuesta."|".$idPregunta."'></i>
                                                                          </td>";

                                                                // Muestra el tipo de respuesta archivo
                                                                if(!isset($arrayIdTipoRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta]) ){ }else{
                                                                    if($arrayIdTipoRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta] == 2){
                                                                      $result.="<td>
                                                                                  <span><a href='".route('fileTemplate.index',['id'=>$idPregRespOk,'name'=>$arrayNamePregunta[$idSeccion][$idSubSeccion][$idPregunta]])."'><i class='fa fa-cloud-upload'></i></a></span>
                                                                                </td>";
                                                                    }
                                                                }
                                                              }
                                                              $result.="</tr>";
                                                            }

                                                        }
                                                      }
                                                    $result.="</table>";
                                                }
                                                $result.="</div>";
                                              $result.="</div>";
                                            $result.="</div>";

                                        // SI TIENE SECCCIONES MUESTRA OTRO DESPLEGADO DE DIVS



                                    }

                        /// ####################### SIN SECCIONES NI SUBSECCIONES ##############################3
                        }else{






                          $result.="<table width='100%' border='0'  class='table'>";
                              /// Seccion  de nombre de preguntas
                           foreach($arrayIdPregunta as $idPregunta){

                              foreach($arrayIdRespuesta as $idRespuesta){
                                if(!isset($arrayPregResp[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta])){
                                }else{
                                    $idPregResp = $arrayPregResp[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta];
                                }
                              }

                              if(!isset($arrayNamePregunta[$idSeccion][$idSubSeccion][$idPregunta]) ){}else{

                                  if(!isset($idSeccion) ){  }else{ }
                                  if(!isset($idSubSeccion) ){  }else{ }

                                  $result.="<tr bgcolor='#E6E6E6'>
                                              <td>
                                              <i class='fa fa-edit' data-toggle='modal' data-target='#editPregnts' id='editPregnts'
                                              data-id='".$idPregResp."|".$idPregunta."|".$idSeccion."|".$idSubSeccion."'></i>
                                                  ".$arrayNamePregunta[$idSeccion][$idSubSeccion][$idPregunta]."
                                              </td>
                                              <td>
                                                Estatus
                                              </td>
                                              <td colspan='2'>
                                                  Acciones
                                              </td>";
                                  $result.="<tr>";


                                  // Respuestas
                                  foreach($arrayIdRespuesta as $idRespuesta){
                                      $result.="<tr>";

                                      if(!isset($arrayPregResp[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta])){
                                      }else{
                                          // Obtiene el ID r de cada respuesta paa actualizarla postreiormente
                                          $idPregRespOk = $arrayPregResp[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta];
                                      }

                                      if(!isset( $arrayNameRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta] )){}else{

                                        $result.="<td>";
                                        $result.="".$arrayNameRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta]."
                                                  </td>";

                                        // Muestra el estatu de la respuesta
                                        if(!isset($arrayStatusRespst[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta])){}else{
                                          if($arrayStatusRespst[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta] == 1){
                                            //<i class='fa fa-check'></i></span>
                                            $result.="<td>
                                                        Activo
                                                      </td>";
                                          }else{
                                            //<i class='fa fa-times'></i></span>
                                            $result.="<td>
                                                          Inactivo
                                                      </td>";
                                          }
                                        }

                                        $result.="<td>
                                                  <i class='fa fa-edit' data-toggle='modal' data-target='#editRespsts' id='editRespsts' data-id='".$idPregRespOk."|".$idRespuesta."|".$idPregunta."'></i>
                                                  </td>";

                                        // Muestra el tipo de respuesta archivo
                                        if(!isset($arrayIdTipoRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta]) ){ }else{
                                            if($arrayIdTipoRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta] == 2){
                                              $result.="<td>
                                                          <span><a href='".route('fileTemplate.index',['id'=>$idPregRespOk,'name'=>$arrayNamePregunta[$idSeccion][$idSubSeccion][$idPregunta]])."'><i class='fa fa-cloud-upload'></i></a></span>
                                                        </td>";
                                            }
                                        }
                                      }
                                      $result.="</tr>";
                                    }

                                }
                              }
                            $result.="</table>";










                        }

                        echo $result;

                      ?>
                      </div>
                </section>
              </div>
          </section>
       </div>
@stop
@section('scripts')
    {!!Html::script('js/Generics.js')!!}
    {!!Html::script('js/admin/Generales.js')!!}
    {!!Html::script('js/admin/asigna_preguntas.js')!!}
    {!!Html::script('js/admin/pregunta_create.js')!!}
@endsection
