@extends('layouts.adm')

@section('content')
  <div id="page-wrapper">
    <br>
    @include('flash::message')
    @if(Session::has('message'))
        <div class="alert alert-success" role="alert">
          {{Session::get('message')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    @endif
      <!-- /.row -->
      <div class="row">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Asingancion de preguntas a cuestionarios
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                        {!! Form::open(['route'=>'asingapregunta.store','method'=>'POST'])!!}
                                            <meta id="token" name="csrf-token" content="{{ csrf_token() }}">
                                            @include('cuestionarios.asignapreguntas.form.preguntasresp')
                                            <table class="table" id="cuestionMezcla">
                                                  <thead>
                                                      <tr>
                                                          <th>
                                                           PREGUNTAS Y RESPUESTAS ASIGNADAS
                                                           <input type="hidden" size="1" name="contPreg" id="contPreg" value=""  />
                                                           <input type="hidden" size="1" name="contResp" id="contResp" value=""  />
                                                           @if($errors->has('contPreg'))
                                                               <div class="panel-body">
                                                                     <div class="alert-danger">
                                                                           {{ $errors->first('contPreg') }}
                                                                     </div>
                                                               </div>
                                                           @endif
                                                           @if($errors->has('contResp'))
                                                               <div class="panel-body">
                                                                     <div class="alert-danger">
                                                                           {{ $errors->first('contResp') }}
                                                                     </div>
                                                               </div>
                                                           @endif
                                                           <th>
                                                       </tr>
                                                  </thead>
                                            </table>
                                            <div class="form-group">
                                                  {!! Form::submit('Guardar',['class'=>'btn btn-default'])!!}
                                            </div>
                                        {!! Form::close() !!}
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
   </div>
@stop

@section('scripts')
    {!!Html::script('js/Generics.js')!!}
    {!!Html::script('js/admin/Generales.js')!!}
    {!!Html::script('js/admin/asigna_preguntas.js')!!}
@endsection
