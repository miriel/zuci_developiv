@extends('layouts.adm')

@section('content')
<div id="page-wrapper">
    <div class="row">
      @include('flash::message')
      @if(Session::has('message'))
          <div class="alert alert-success" role="alert">
            {{Session::get('message')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
      @endif
      @if(Session::has('flash_delete'))
          <div class="alert alert-warning " role="alert">
            {{Session::get('flash_delete')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
      @endif
      <br>
      @include('flash::message')
      @if(Session::has('flash_success'))
          <div class="alert alert-success" role="alert">
            {{Session::get('flash_success')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
      @endif
        <!-- /.col-lg-12 -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <b>Catalogo cuestionarios</b>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table" id="">
                            <thead>
                                <tr>
                                  <td><b>ID</b></td>
                                  <td><b>Cuestionario</b></td>
                                  <td><b>Accion</b></td>
                                </tr>
                            </thead>
                            @foreach($preguntasResps as $pregunta)
                                <tbody>
                                    <tr class="odd gradeX">
                                        <td>{{ $pregunta->id }}</td>
                                        <td>{{ $pregunta->description }}</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-1 col-md-offset-1">
                                                  {!!Form::open(['route'=> ['asingapregunta.destroy',$pregunta->id, 'status' => $pregunta->status, 'description' => $pregunta->description],'method'=>'DELETE'])!!}
                                                      @if( $pregunta->status == 1)
                                                          {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger','title'=>'Inactivar']) }}
                                                      @else
                                                          {{ Form::button('<i class="fa fa-check"></i>', ['type' => 'submit', 'class' => 'btn btn-success','title'=>'Activar']) }}
                                                      @endif
                                                  {!!Form::close()!!}
                                                </div>
                                                <div class="col-md-1 col-md-offset-1">
                                                      <a href="{{ route('asingapregunta.edit',$pregunta->id, $pregunta->description)}}"class="btn btn-primary" title="Editar"> <i class="fa fa-edit"></i></a>
                                                </div>
                                          </div>
                                        </td>
                                    </tr>
                                </tbody>
                            @endforeach
                        </table>
                        {!!$preguntasResps->appends(['status' => $status])->links()!!}
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
    <!-- Se agrega el render para mostrar las tabs de paginacion -->

  </div>
@stop
