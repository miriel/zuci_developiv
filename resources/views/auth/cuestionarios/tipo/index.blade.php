@extends('layouts.adm')

@section('content')

@include('cuestionarios.tipo.modal.create')
@include('cuestionarios.tipo.modal.edit')
<div class="inner-wrapper">
  <section role="main" class="content-body pb-0">

    @include('flash::message')
    @if(Session::has('message'))
        <div class="alert alert-success" role="alert">
          {{Session::get('message')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    @endif
    @if(Session::has('flash_delete'))
        <div class="alert alert-warning " role="alert">
          {{Session::get('flash_delete')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    @endif
    <br>
    @include('flash::message')
    @if(Session::has('flash_success'))
        <div class="alert alert-success" role="alert">
          {{Session::get('flash_success')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    @endif
    <div class="col-md-12">
          <section class="card card-featured card-featured-primary mb-4">
            <header class="card-header">
              <div class="card-actions">
                    @can('create',new App\QuestionnaireType)
                  <button type="button" class="btn btn-outline btn-link" data-toggle='modal'
                    data-target="#createTipoCuest" style="color:#fff;">	<i class="fa fa-plus"> 	Agregar</i></button>
                    @endcan
              </div>

              <h2 class="card-title blanco">Tipos cuestionarios</h2>
                <input type="hidden" name="status" id="status" value="{{  $status }}" />
            </header>
            <div class="card-body">
              <!-- <code>.card-featured.card-featured-primary</code> -->
              <div class="	">

                      @can('view',new App\QuestionnaireType)
              <div class="tabs tabs-primary">
                <ul class="nav nav-tabs">
                  <li class="nav-item active">
                    <a class="nav-link active" href="#altas" data-toggle="tab"><i class="fa fa-star"></i> Altas</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#bajas" data-toggle="tab">Bajas</a>
                  </li>
                </ul>
                <div class="tab-content">
                  <div  id="altas" class="tab-pane active">
                    <table width="" class="table table-responsive-lg table-sm ">
                      <thead>
                          <tr>
                            <td><b>ID</b></td>
                            <td><b>Nombre</b></td>
                            <td><b>Estatus</b></td>
                            <td><b>Accion</b></td>
                          </tr>
                      </thead>
                        @foreach($tipoCuestAl as $tipo)
                        <tbody>
                            <tr class="odd gradeX">
                              <td>{{ $tipo->id }}</td>
                              <td>{{ $tipo->description }}</td>
                                <td>
                                  <img src="{{ url('admin/img/success.png') }}"alt="">
                                  <!-- <button type="button" class="btn btn-success btn-circle"><i class="fa fa-check"></i></button> -->
                                </td>
                                <td class="">
                                    <div class="row">
                                       @can('delete',new App\QuestionnaireType)
                                       <div class="col-md- col-md-offset-1">
                                          {!!Form::open(['route'=> ['tipocuestionario.destroy',$tipo->id, 'estatus' => $tipo->status],'method'=>'DELETE'])!!}
                                            {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-xs btn-default','title'=>'Inactivar']) }}
                                            {!!Form::close()!!}
                                        </div>
                                        @endcan
                                         @can('update',new App\QuestionnaireType)
                                         <div class="col-md-1 col-md-offset-1">
                                            <a data-toggle='modal' data-toggle='modal' id="editTipoCuest"
                                            data-target='#editTipoCuest' data-id='{{ $tipo->id }}' class="btn btn-xs btn-default ">  <i class="fa fa-edit"></i></a>
                                          </div>
                                        @endcan                                      
                                  </div>
                                </td>
                            </tr>
                          </tbody>
                        @endforeach
                    </table>
                    <div class=" pull-right">
                    {!!$tipoCuestAl->appends(['status' => 1])!!}
                    </div>
                  </div>
                  <div id="bajas" class="tab-pane fade">
                    <table width="" class="table table-responsive-md table-sm 	">
                      <thead>
                          <tr>
                            <td><b>ID</b></td>
                            <td><b>Nombre</b></td>
                            <td><b>Estatus</b></td>
                            <td><b>Accion</b></td>
                          </tr>
                      </thead>
                        @foreach($tipoCuestBa as $tipoBa)
                        <tbody>
                            <tr class="odd gradeX">
                              <td>{{ $tipoBa->id }}</td>
                              <td>{{ $tipoBa->description }}</td>
                                <td>
                                  <img src="{{ url('admin/img/danger.png') }}"alt="">
                                </td>
                                <td>
                                    <div class="row">
                                      @can('delete',new App\QuestionnaireType)
                                        <div class="col-md- col-md-offset-">
                                          {!!Form::open(['route'=> ['tipocuestionario.destroy',$tipoBa->id, 'estatus' => $tipoBa->status],'method'=>'DELETE'])!!}
                                            {{ Form::button('<i class="fa fa-check"></i>', ['type' => 'submit', 'class' => 'btn btn-outline btn-success','title'=>'Activar']) }}
                                          {!!Form::close()!!}
                                        </div>
                                        @endcan
                                        @can('update',new App\QuestionnaireType)
                                        <div class="col-md-1 col-md-offset-1">
                                          <button type="button" class="btn btn-outline btn-info" id="editTipoCuest" data-toggle='modal'
                                          data-target='#editTipoCuest' data-id='{{ $tipoBa->id }}'><i class='fa fa-external-link'></i></button>
                                  </div>
                                  @endcan
                                  </div>
                                </td>
                            </tr>
                          </tbody>
                        @endforeach
                    </table>
                    <div class=" pull-right">
                        {!!$tipoCuestBa->appends(['status' => 0])!!}
                    </div>
                </div>
              </div>
              </div>
              @endcan
            </div>
          </section>
        </div>
    <!-- end: page -->
  </section>
</div>

@stop

@section('scripts')
  {!!Html::script('js/Generics.js')!!}
  {!!Html::script('js/admin/tipocuestionario_index.js')!!}
  {!!Html::script('js/admin/tipocuestionario_create.js')!!}
@endsection
