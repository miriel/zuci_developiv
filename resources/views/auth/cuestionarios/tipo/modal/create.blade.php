<!-- Modal -->
<div class="modal fade" id="createTipoCuest" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class="letracolor">Agregar Tipo cuestionario</h4>
          <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <!-- Mensajes exitosos -->
            <div id="msj-success" class="alert alert-success" role="alert" style="display:none">
                Cuestionario agregado correctamente
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- Mensajes de error -->
            {!!Form::label('qtname','Nombre')!!}
            {!! Form::text('qtname',null, ['id'=> 'qtname','class'=>'form-control'])!!}
            <div id="msj-error" class=" alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msj"></strong>
            </div>
            <div class="modal-footer">
              <input type="button" class="btn btn-outline btn-success" id="btnCreaTipoCuest" value="Guardar" />              
            </div>
      </div>
    </div>
  </div>
</div>
