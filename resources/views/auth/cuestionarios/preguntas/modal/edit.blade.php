<!-- Modal -->
<div class="modal fade" id="editPregnts" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class=" letracolor">Editar pregunta</h4>
          <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <!-- Id de la pregunta respuesta -->
            <input type="hidden" id="idPregRespCst" />
            <input type="hidden" id="idPregCst" />

            <input type="hidden" id="qtnameRespCst" />
            <input type="hidden" id="qunameRespCst" />

            <input type="hidden" id="idSeccionRespCst" />
            <input type="hidden" id="idSubseccionRespCst" />

            <!-- Mensajes exitosos -->
            <div id="msj-success" class="alert alert-success" role="alert" style="display:none">
                Cuestionario agregado correctamente
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!!Form::label('qsname','Nombre')!!}
            {!!Form::select('qsname',$preguntas, null, ['id'=>'qsname','class'=>'form-control','placeholder'=>'Selecciona']) !!}
            <div id="msj-error" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msj"></strong>
            </div>
            {!!Form::label('annameAddRsp','Agregar Respuesta')!!}
            {!!Form::select('annameAddRsp',$respuestas, null, ['id'=>'annameAddRsp','class'=>'form-control','placeholder'=>'Selecciona']) !!}
            <div id="msj-error" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msj"></strong>
            </div>
            <div class="modal-footer">
              <input type="button" class="btn btn-outline btn-success" id="btnEditPreg" value="Actualizar" />
            </div>
      </div>
    </div>
  </div>
</div>
