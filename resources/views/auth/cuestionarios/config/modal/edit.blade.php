<!-- Modal -->
<div class="modal fade" id="editConfigCuest" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class="letracolor">Modificar configuracion cuestionario</h4>
          <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <input type="hidden" id="idConfCuest" name="idConfCuest" />
            <!-- Mensajes exitosos -->
            <div id="msj-success" class="alert alert-success" role="alert" style="display:none">
                Cuestionario agregado correctamente
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>


            {!!Form::label('typecompanyEd','Tipo compañia:')!!}
            {!!Form::select('typecompanyEd',$tipoCompania,null, ['id' => 'typecompanyEd', 'class' => 'form-control', 'placeholder' => 'Selecciona'])!!}
            <div id="msj-errorEd1" class=" alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjEd1"></strong>
            </div>
            {!!Form::label('questionnaireEd','Cuestionario:')!!}
            {!!Form::select('questionnaireEd',$cuestionario, null,['id' => 'questionnaireEd', 'class' => 'form-control', 'placeholder' => 'Selecciona'])!!}
            <div id="msj-errorEd2" class=" alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjEd2"></strong>
            </div>
            {!!Form::label('everybodyEd','Aplicar cuestionario a todas la compañias ( Tipos compañia )')!!}
            {!! Form::select('everybodyEd',[
                '0'=>'NO',
                '1'=>'SI',
                ],null,['id'=>'everybodyEd','class'=>'form-control'])
            !!}

          <div class="modal-footer">
            <input type="button" class="btn btn-outline btn-success" id="btnUpdtConfCuest" value="Actualizar" />
          </div>
      </div>
    </div>
  </div>
</div>
