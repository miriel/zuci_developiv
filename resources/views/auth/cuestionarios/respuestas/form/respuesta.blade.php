{!! Form::label('anname','Respuesta') !!}
{!! Form::text('anname',null, ['id'=>'anname','class'=>'form-control'])!!}
<div id="msj-error1" class="alert-danger" role="alert" style="display:none">
    <strong class="help-block blanco" id="msj1"></strong>
</div>
{!! Form::label('anatfk','Tipo de respuesta') !!}
{!! Form::select('anatfk',$tipoRespuesta, null, ['id'=>'anatfk','class'=>'form-control','placeholder'=>'Selecciona']) !!}
<div id="msj-error2" class="alert-danger" role="alert" style="display:none">
    <strong class="help-block blanco" id="msj2"></strong>
</div>
{!! Form::label('anvalue','Valor de ponderacion') !!}
{!! Form::select('anvalue',[
    ''=>'N/A',
    '0'=>'0',
    '1'=>'1',
    '2'=>'2',
    '3'=>'3',
    '4'=>'4',
    '5'=>'5'
    ],null,['id'=>'anvalue','class'=>'form-control'])
!!}
{!! Form::label('status','Estatus') !!}
{!! Form::select('status',[
    '0'=>'0',
    '1'=>'1',
    ],null,['id'=>'status','class'=>'form-control'])
!!}


{!!Form::hidden('value_txt',null,['id'=>'value_txt','class'=>'form-control'])!!}
