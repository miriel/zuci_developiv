<!-- Modal -->
<div class="modal fade" id="editRespsts" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">

        <h4 class="letracolor">Cambiar respuesta</h4>
          <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <input type="hidden" id="idPregRespAspRs" /> <!--Id de la respuesta pegunta tabla id autoincrementable(questionnaire_answers)-->
            <input type="hidden" id="idRespAspRs" /> <!--Id de la respuesta -->
            <input type="hidden" id="idPregAspRs" /> <!--Id de la pregunta -->
            <!-- Mensajes exitosos -->
            <div id="msj-success" class="alert alert-success" role="alert" style="display:none">
                Cuestionario agregado correctamente
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::label('annameUpdRespPre','Respuesta') !!}
            {!!Form::select('annameUpdRespPre',$respuestas, null, ['id'=>'annameUpdRespPre','class'=>'form-control','placeholder'=>'Selecciona']) !!}
            <div id="msj-error1UpRsp" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msj1UpRsp"></strong>
            </div>
            {!! Form::label('anatfkUpdRespPre','Tipo de respuesta') !!}
            {!! Form::text('anatfkUpdRespPre',null, ['id'=>'anatfkUpdRespPre','class'=>'form-control','readonly'=>'readonly'])!!}
            <div id="msj-error2" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msj2"></strong>
            </div>
            {!! Form::label('anvalueUpdRespPre','Valor de ponderacion') !!}
            {!! Form::text('anvalueUpdRespPre',null, ['id'=>'anvalueUpdRespPre','class'=>'form-control','readonly'=>'readonly'])!!}
            {!! Form::label('statusUpdRespPre','Estatus') !!}
            {!! Form::select('statusUpdRespPre',[
                '0'=>'Inactivo',
                '1'=>'Activo',
                ],null,['id'=>'statusUpdRespPre','class'=>'form-control'])
            !!}
            {!!Form::hidden('value_txt',null,['id'=>'value_txt','class'=>'form-control'])!!}

      </div>
      <div class="modal-footer">
        <input type="button" class="btn btn-outline btn-success" id="btnUpdtAsResp" value="Actualizar" />        
      </div>
    </div>

  </div>
</div>
