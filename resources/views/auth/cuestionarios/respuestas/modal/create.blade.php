<!-- Modal -->
<div class="modal fade" id="createRespsts" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class="letracolor">Crear respuesta</h4>
          <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <input type="hidden" id="id" name="id"  />
            <!-- Mensajes exitosos -->
            <div id="msj-success" class="alert alert-success" role="alert" style="display:none">
                Cuestionario agregado correctamente
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::label('anname','Respuesta') !!}
            {!! Form::text('anname',null, ['id'=>'anname','class'=>'form-control'])!!}
            <div id="msj-error1" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msj1"></strong>
            </div>
            {!! Form::label('anatfk','Tipo de respuesta') !!}
            {!! Form::select('anatfk',$tipoRespuesta, null, ['id'=>'anatfk','class'=>'form-control','placeholder'=>'Selecciona']) !!}
            <div id="msj-error2" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msj2"></strong>
            </div>
            {!! Form::label('anvalue','Valor de ponderacion') !!}
            {!! Form::select('anvalue',[
                ''=>'N/A',
                '0'=>'0',
                '1'=>'1',
                '2'=>'2',
                '3'=>'3',
                '4'=>'4',
                '5'=>'5'
                ],null,['id'=>'anvalue','class'=>'form-control'])
            !!}
            <input type="hidden" id="status" name="status" value="N" />
      </div>
      <div class="modal-footer">
        <input type="button" class="btn btn-outline btn-success" id="btnCreateResp" value="Actualizar" />                
      </div>
    </div>

  </div>
</div>
