@extends('layouts.adm')

@section('content')
<div id="page-wrapper">
    <br>
    <!-- /.row -->
    <div class="row">
          @if(Session::has('message'))
              <div class="alert alert-success" role="alert">
                {{Session::get('message')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
          @endif
          <div class="row">
              <div class="col-lg-12">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                          Alta de respuestas
                      </div>
                      <div class="panel-body">
                          <div class="row">
                              <div class="col-lg-12">
                                    {!! Form::open(['route'=>'respuesta.store','method'=>'POST'])!!}
                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" >
                                    <div class="form-group">
                                          @include('cuestionarios.respuestas.form.respuesta')
                                    </div>
                                    <div class="form-group">
                                      {!! Form::submit('Guardar',['class'=>'btn btn-default'])!!}
                                    </div>
                                {!! Form::close() !!}
                              </div>

                          </div>
                          <!-- /.row (nested) -->
                      </div>
                      <!-- /.panel-body -->
                  </div>
                  <!-- /.panel -->
              </div>
              <!-- /.col-lg-12 -->
          </div>
          <!-- /.row -->
      </div>
   </div>
@stop

@section('scripts')
    {!! Html::script('js/admin/Generales.js') !!}
    {!! Html::script('js/admin/cuestionario_create.js') !!}
@endsection
