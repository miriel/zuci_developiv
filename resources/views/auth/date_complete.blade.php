@extends('layouts.app')

@section('content')
        <div role="main" class="main">

          <section class="page-header ">
            <div class="container">
              <div class="row">
                <div class="col-lg-12 text-center">
                  <h1>INGRESO DE CONTRASEÑA</h1>
                </div>
              </div>
            </div>
          </section>
            <div class="container text-left register">
            <div class="row ">
              <div class="col-lg-3"> </div>
              <div class="col-md-6">
                <div class="featured-box featured-box-primary  mt-5 ">
                  <div class="box-content">
                    <h4 class="heading-primary text-uppercase mb-3 text-center">Ingreso de Contraseña </h4>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/complete/'.$id) }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                          <div class="col-lg-3"> </div>
                            <div class="col-md-12 text-center">
                              <label for="password" class="control-label text-left">Password </label>
                              <i class="fa fa-question margin" style="color:red;" data-toggle="tooltip" data-placement="right"
  															title="La contraseña debe contener por lo menos 1 letra mayuscula, 1 numero y un caracter especial ($#%&)"></i>
                                <input id="password" type="password" class="form-control" name="password" >
                                  <p style="color:#ff1515;" class="men">Utiliza ocho caracteres alfanumericos como mínimo </p>
                                <span class="help-block"><p style="color:#FFF;" class="alert-danger">{{$errors->first('password')}}</p></span>
                            </div>
                        </div><br>

                        <div class="form-group">
                                   <div class="col-lg-12"></div>
                            <div class="col-md-12">
                              <label for="password-confirm" class="control-label">Confirm Password</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
                            </div>
                        </div><br>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Completar Datos
                                </button>
                            </div>
                        </div>
                    </form>
                  </div>
                </div>
              </div>

          </div>
      </div>
@endsection
