@extends('layouts.app')

@section('content')

<div class="container">
  @if (session('status'))
      <div class="alert alert-success">
          {{ session('status') }}
      </div>
  @endif
  <div class="row">
    <div class="col-md-4 col-md-offset-4 mx-auto mt-4">
        <div class="login-panel panel panel-default">
      <div class="panel-heading">
          <h3 class="panel-title">Reset Password</h3>
      </div>
            <div class="panel-body">
              <div class="text-center mt-4 mb-5">
                <h4>Forgot your password?</h4>
                <p>Enter your email address and we will send you instructions on how to reset your password.</p>
              </div>
                  <form class="form" method="POST" action="{{ route('password.email') }}">
                  {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-6 control-label">E-Mail Address</label>
                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div>
                <button type="submit" class="btn btn-lg  btn-primary btn-block"> Send Password Reset Link </button>
              </form>
              <div class="text-center">
                <a class="d-block small" href="{{ route('register') }}">Register an Account</a>
                <a class="d-block small" href="{{ route('login') }}">Login?  </a>
              </div>
            </div>
     </div>
    </div>
  </div>
</div>
@endsection
