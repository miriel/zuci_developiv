<<<<<<< HEAD

=======
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
@extends('layouts.adm')
	@section('content')
			<div class="inner-wrapper">
				<section role="main" class="content-body pb-0">
					@include('flash::message')
					@if(Session::has('message'))
							<div class="alert alert-success" role="alert">
								{{Session::get('message')}}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
					@endif
					@if(Session::has('flash_delete'))
							<div class="alert alert-warning " role="alert">
								{{Session::get('flash_delete')}}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
<<<<<<< HEAD

=======
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
					@endif
					<br>
					@include('flash::message')
					@if(Session::has('flash_success'))
							<div class="alert alert-success" role="alert">
								{{Session::get('flash_success')}}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
					@endif
					<div class="col-md-12">
								<section class="card card-featured card-featured-primary mb-4">
									<header class="card-header">
										<h2 class="card-title blanco">Cuestionarios Asignados</h2>
									</header>
									<div class="card-body">
										<!-- <code>.card-featured.card-featured-primary</code> -->
										<div class="	">
											<div class="tabs tabs-primary">
												<ul class="nav nav-tabs">
													<li class="nav-item active">
														<a class="nav-link active" href="#altas" data-toggle="tab"><i class="fa fa-star"></i> Altas</a>
													</li>
												</ul>
												<div class="tab-content">
													<div  id="altas" class="tab-pane active">
														<table width="100%" class="table table-responsive-md table-sm  ">
															<thead>
																	<tr>
																		<td><b>ID</b></td>
																		<td><b>Nombre</b></td>
																		<td><b>Estatus</b></td>
																		<td><b>Acciones</b></td>
																	</tr>
															</thead>
																<tbody>
                                  @foreach($cuestionarios as $cuestionario)
                                      <tr class="odd gradeX">
                                          <td>{{ $cuestionario->id }}</td>
                                          <td>{{ $cuestionario->description }}</td>
																				<td>
																					@foreach($status as $stat)
																				 	  @if( $cuestionario->status == $stat->cmval)
																							 <button type="button" class="btn btn-outline btn-{{$stat->cmimg}}" >{{$stat->cmdesc}}</button>
																						@endif
																					@endforeach

                                       </td>
																				<td class="">
																						<div class="row">
																								@role('Auditor')
																								<div class="col-md-1 col-md-offset-1">
                                                  <a href="{{ route('micuestionarios.edit',['id' => $cuestionario->id, 'nameCuest' => $cuestionario->description ])}}"
																									class="btn btn-primary" title="Editar"> <i class="fa fa-edit"></i></a>
                                              	</div>
																								@else
																								<div class="col-md-1 col-md-offset-1">
                                                  <a href="{{ route('miscuestionarios.edit',['id' => $cuestionario->id, 'nameCuest' => $cuestionario->description ])}}"
																									class="btn btn-primary" title="Editar"> <i class="fa fa-edit"></i></a>
                                              	</div>
																								@endrole
																					</div>
																				</td>
																		</tr>
																	</tbody>
																@endforeach
														</table>
													</div>

												</div>
											</div>
											</div>

									</div>
								</section>
							</div>



					<!-- end: page -->
				</section>
			</div>

			@stop
