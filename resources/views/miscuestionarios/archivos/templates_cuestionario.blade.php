@extends('layouts.adm')

@section('scripts')
  {!!Html::script('js/Generics.js')!!}
  {!!Html::script('js/admin/upload_files.js')!!}
@endsection

@section('content')

  @include('miscuestionarios.modal.fileCuestionario')
  @include('miscuestionarios.modal.templateCuestionario')
  <!-- Permisos para visualizar cajas -->

  <?php  $disabled="disabled='disabled'";
         $disabledCollec=true;
  ?>
  @can('view',new \Spatie\Permission\Models\Role)
      <?php  $disabled="";
      $disabledCollec=false; ?>
  @endcan


      <div class="inner-wrapper">
          <section role="main" class="content-body pb-0">
                <section role="main" class="content-body pb-0">
                      <div class="col-md-12 ">
                        <section class="card card-featured card-featured-primary mb-4">
                          <header class="card-header">
                            <div class="card-actions">
                            </div>
                            <input type="hidden" name="_token" id="token"value="{{ csrf_token() }}">
                            <h2 class="card-title blanco">
                              <div class="text-center">
                                  <?php $tituloFile = 'Administración de Documentos - Procedimientos y Evidencias';
                                        $displayRechazo="none"; // Boton rechazo de documentos
                                        $displayAceptaProc="none"; // Boton acepta proceso
                                        $displayDeleteFile="block"; // boton borra archivos
                                        ?>
                                  @role('Auditor')
                                    <?php $tituloFile = 'Administración de Documentos - Para Revisión y Calificación.';
                                          $displayRechazo="block"; // Boton rechazo de documentos
                                          $displayAceptaProc="block"; // Boton acepta proceso
                                          $displayDeleteFile="none"; // boton borra archivos
                                    ?>
                                  @endrole
                                  {{ $tituloFile }}
                              </div>
                              {{$name}}
                            </h2>
                          </header>
                          <div class="card-body">
                            <table class="table table-responsive-md table-sm" border="0">
                              <thead>
                                <tr>
                                  <td>Empresa: {{Auth::user()->wkrkarea }}</td>
                                </tr>
                                  <tr>
                                    <td>Fecha: {{$fechaActual }} </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <div class="row">
                                        <div  class="col-md-2">
                                            <img src="{{url('img/file_plus_.PNG')}}" widht="40" height="45" data-toggle='modal' data-target='#subeFilesCuestionario'  data-backdrop="static"
                                            id="subeFilesM" /> <br>Agregar <br>Nuevo <br>Documento
                                        </div>
                                        <div  class="col-md-2" style="display: {{$displayDeleteFile}};">
                                            <img src="{{url('img/file_delete.PNG')}}" widht="40" height="45"
                                            id="eliminaFilesCuest" /> <br>Eliminar <br>Documento
                                        </div>
                                        <div  class="col-md-2">
                                            <img src="{{url('img/descargar_g.PNG')}}" widht="40" height="45" id="descargaAllFilsCuest" /> <br>Descargar <br>Documento
                                        </div>
                                        <div  class="col-md-2" style="display: {{$displayRechazo}};">
                                            <img src="{{url('img/previus_icon.png')}}" widht="40" height="45"
                                            id="rechazaFiles" /> <br>Rechazar <br>proceso
                                        </div>
                                        <div  class="col-md-2" style="display: {{$displayAceptaProc}};">
                                            <img src="{{url('img/next_icon.png')}}" widht="40" height="45"
                                            id="aceptaFiles" /> <br>Aprobar <br>proceso
                                        </div>
                                        <div  class="col-md-2">
                                            <img src="{{url('img/descargar_g.PNG')}}" widht="40" height="45" data-toggle='modal' data-target='#descargaTemplatesUser'
                                            id="descargaTemplatesUser" /> <br>Descargar <br>Template
                                        </div>
                                      </div>
                                    </td>
                                  </tr>
                              </thead>
                            </table>
                            <div class="panel panel-primary col-lg-12">
                                  <div class="table-responsive-sm">
                                        {!! Form::open(['route'=>'file.create','method'=>'GET'])!!}
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">No.</th>
                                                    <th scope="col">
                                                      <input type="checkbox" name="filesCheckAllCuest" id="filesCheckAllCuest" value='0'/> | Icono
                                                    </th>
                                                    <th scope="col"> Archivo</th>
                                                    <th scope="col">Cometario</th>
                                                    <th scope="col">Calificacion</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Acciones</th>
                                                </tr>
                                                </thead>
                                                <?php $count=1; ?>
                                                @foreach($answerFiles as $files)
                                                    <tbody>
                                                        <?php
                                                        $valFile = explode(".",$files->afqnamefile);
                                                        $extension = $valFile[1];
                                                        ?>
                                                        <tr>
                                                            <td>{{$count}}</td>
                                                            <td>
                                                              @if( $files->status == 8 )
                                                                <input type="checkbox" disabled="disabled" name="fileCheckCuest" id="fileCheckCuest" value="0" />
                                                              @else
                                                                <input type="checkbox" name="fileCheckCuest" id="fileCheckCuest_{{$files->idFile}}" value="{{ $files->idFile }}" />
                                                              @endif
                                                              |
                                                              @if( $extension == 'png' || $extension == 'jpg' || $extension == 'jpeg' || $extension == 'bmp'
                                                                    || $extension == 'PNG' || $extension == 'JPG' || $extension == 'JPEG' || $extension == 'BMP')
                                                                  <i class="fa fa-file-picture-o" aria-hidden="true"></i>
                                                              @elseif( $extension == 'doc' || $extension == 'docx' || $extension == 'DOC' || $extension == 'DOCX' )
                                                                  <i class="fa fa-file-word-o" aria-hidden="true"></i>
                                                              @elseif( $extension == 'pdf' )
                                                                  <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                                              @elseif( $extension == 'xls' || $extension   == 'xlsx' || $extension   == 'XLSX' || $extension   == 'XLS' )
                                                                  <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                                              @elseif( $extension == 'ppt' || $extension   == 'pptx' || $extension   == 'PPTX' || $extension   == 'PPTS' )
                                                                  <i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>
                                                              @endif
                                                            </td>
                                                            <td>{{ $files->afqnamefile }} <br><?php $conversion = $files->afqsize / '1048576'; ?> {{ round($conversion,2) }} MB </td>
                                                            <td>
                                                                <div class="form-group">
                                                                  <textarea {{$disabled}} class="form-control" cols="15" id="cometarios_{{ $files->idFile }}" rows="1" onblur="guardarComentFile({{ $files->idFile }})">{{ $files->coment }}</textarea>
                                                                  <div id="msj-errorcom_{{ $files->idFile }}" class="alert alert-danger" role="alert" style="display:none">
                                                                      <strong class="help-block blanco" id="msjcom_{{ $files->idFile }}"></strong>
                                                                  </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                {!!Form::select('calificacion',$calificacion,$files->calif,['id'=>'calificacion_'.$files->idFile.'','class'=>'form-control','placeholder'=>'N/A', 'disabled' => $disabledCollec, 'onchange' => 'guardarCalificFile('.$files->idFile.',this.value)' ]) !!}
                                                                <div id="msj-errorcal_{{ $files->idFile }}" class="alert alert-danger" role="alert" style="display:none">
                                                                    <strong class="help-block blanco" id="msjcal_{{ $files->idFile }}"></strong>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                @foreach( $catStatus as $catStat )
                                                                    @if( $files->status == $catStat->valStatus )
                                                                        {{  $catStat->nameStatus }}
                                                                    @endif
                                                                @endforeach
                                                            </td>
                                                            <td>
                                                                <!-- No se podra descargar mientrar este cancelado para cualquier perfil de usuario -->
                                                                @if( $files->status != 8 )
                                                                  <a style="color:#000;" href="{{ url('downloadCuest',['file' => $files->afqnamefile] ) }}">
                                                                    <i   class="fa fa-cloud-download" aria-hidden="true" title="Descargar"></i>
                                                                  </a>
                                                                @endif

                                                                  &nbsp;&nbsp;
                                                                <a style="color:#000;" >
                                                                  <!-- Perfil usuario -->
                                                                  @if( $disabledCollec == true)
                                                                      @if( $files->status == 2  )
                                                                        <i class="fa fa-close" aria-hidden="true" onclick="deleteFileCuest({{ $files->idFile }},{{ $files->status }})" title="Cancelar"></i>
                                                                        &nbsp;&nbsp;
                                                                        <i class="fa fa-share-square-o" aria-hidden="true" onclick="nextFilterCuest({{ $files->idFile }},{{ $files->status }})" title="Enviar"></i>
                                                                      @endif
                                                                      @if( $files->status == 8 || $files->status == 3 || $files->status == 5 )
                                                                        <i class="fa fa-lock" aria-hidden="true" ></i>
                                                                      @elseif( $files->status == 4 )
                                                                        <i class="fa fa-close" aria-hidden="true" onclick="deleteFileCuest({{ $files->idFile }},{{ $files->status }})" title="Cancelar"></i>
                                                                        &nbsp;&nbsp;
                                                                        <i class="fa fa-share-square-o" aria-hidden="true" onclick="nextFilterCuest({{ $files->idFile }},{{ $files->status }})" title="Enviar"></i>
                                                                      @endif
                                                                  <!-- Perfil auditot o administrador -->
                                                                  @else
                                                                      @if( $files->status == 3 || $files->status == 5   )
                                                                          <i class="fa fa-close" aria-hidden="true" onclick="deleteFileCuest({{ $files->idFile }},{{ $files->status }})" title="Cancelar"></i>
                                                                          &nbsp;&nbsp;
                                                                          <i class="fa fa-share-square-o" aria-hidden="true" onclick="nextFilterCuest({{ $files->idFile }},{{ $files->status }})" title="Enviar"></i>
                                                                      @endif
                                                                      @if( $files->status == 8 || $files->status == 2 )
                                                                          <i class="fa fa-lock" aria-hidden="true" ></i>
                                                                      @endif
                                                                  @endif
                                                                </a>

                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                    <?php $count++;?>
                                                @endforeach



                                            </table>
                                        {!! Form::close() !!}
                                    </div>
                              </div>
                          </div>
                        </section>
                      </div>
                </section>
          </section>
      </div>



    @stop
