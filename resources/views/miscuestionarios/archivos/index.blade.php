@extends('layouts.adm')

@section('scripts')
  {!!Html::script('js/Generics.js')!!}
  {!!Html::script('js/admin/upload_files.js')!!}
  {!!Html::script('js/preguntas.js')!!}
@endsection

@section('content')
  @include('miscuestionarios.modal.file')
  @include('miscuestionarios.modal.template')

  <!-- Permisos para visualizar cajas -->
  <?php $disabledCaliFile = true; ?>
  @can('calificarArchivo',new App\AnswerFile)
  <?php $disabledCaliFile = false; ?>
  @endcan

  <!-- Permisos para visualizar caja para agregar comentarios -->
  <?php $disabledComentAudit='disabled'; ?>
  @can('comentFileAudit',new App\AnswerFile)
  <?php $disabledComentAudit=''; ?>
  @endcan

  <?php $disabledComentUser = (auth()->user()->roles[0]->id == 3) ? 'disabled' : ''; ?>

      <div class="inner-wrapper">
          <section role="main" class="content-body pb-0">
                <section role="main" class="content-body pb-0">
                      <div class="col-md-12 ">
                        <section class="card card-featured card-featured-primary mb-4">
                          <header class="card-header">
                            <div class="card-actions">
                            </div>
                            <input type="hidden" name="_token" id="token"value="{{ csrf_token() }}">
                            <h2 class="card-title blanco">
                              <div class="text-center">
                                  <?php $tituloFile = 'Administración de Documentos - Procedimientos y Evidencias';
                                        $displayRechazo="none"; // Boton rechazo de documentos
                                        $displayAceptaProc="none"; // Boton acepta proceso
                                        $displayDeleteFile="block"; // boton borra archivos
                                        ?>
                                  @if( auth()->user()->roles[0]->id == 3 )
                                    <?php $tituloFile = 'Administración de Documentos - Para Revisión y Calificación.';
                                          $displayRechazo="block"; // Boton rechazo de documentos
                                          $displayAceptaProc="block"; // Boton acepta proceso
                                          $displayDeleteFile="none"; // boton borra archivos
                                    ?>
                                  @endif
                                  {{ $tituloFile }}
                              </div> <br>
                              {{$name}}<br>
                              <?php if(!empty($datelle)) { ?>
                                {{ $detalle[0]->nameSect }}<br>
                                {{ $detalle[0]->nameSubSect }}<br>
                                {{ $detalle[0]->namePreg }}<br>
                              <?php } ?>
                            </h2>
                          </header>
                          <div class="card-body">
                            <table class="table table-responsive-md table-sm" border="0">
                              <thead>
                                <tr>
                                  <td>Empresa: {{Auth::user()->job }}</td>
                                </tr>
                                  <tr>
                                    <td>Fecha: {{$fechaActual }} </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <div class="row">
                                        <div  class="col-md-2">
                                            <img src="{{url('img/file_plus_.PNG')}}" widht="40" height="45" data-toggle='modal' data-target='#subeFilesM'  data-backdrop="static"
                                            id="subeFilesM" /> <br>Agregar <br>Nuevo <br>Documento
                                        </div>
                                        <div  class="col-md-2" style="display: {{$displayDeleteFile}};">
                                            <img src="{{url('img/file_delete.PNG')}}" widht="40" height="45"
                                            id="eliminaFilesM" /> <br>Eliminar <br>Documento
                                        </div>
                                        <div  class="col-md-2">
                                            <img src="{{url('img/descargar_g.PNG')}}" widht="40" height="45" id="descargaAll" /> <br>Descargar <br>Documento
                                        </div>
                                        <div  class="col-md-2" style="display: {{$displayRechazo}};">
                                            <img src="{{url('img/previus_icon.png')}}" widht="40" height="45"
                                            id="rechazaFiles" /> <br>Rechazar <br>proceso
                                        </div>
                                        <div  class="col-md-2" style="display: {{$displayAceptaProc}};">
                                            <img src="{{url('img/next_icon.png')}}" widht="40" height="45"
                                            id="aceptaFiles" /> <br>Aprobar <br>proceso
                                        </div>
                                        <div  class="col-md-2">
                                            <img src="{{url('img/descargar_g.PNG')}}" widht="40" height="45" data-toggle='modal' data-target='#descargaTemplatesUser'
                                            id="descargaTemplatesUser" /> <br>Descargar <br>Template
                                        </div>
                                      </div>
                                    </td>
                                  </tr>
                              </thead>
                            </table>
                            <div class="panel panel-primary col-lg-12">
                                  <div class="table-responsive-sm">
                                        {!! Form::open(['route'=>'file.create','method'=>'GET'])!!}
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">
                                                      <input type="checkbox" name="fileCheckAll" id="fileCheckAll" value='0'/> | Icono
                                                    </th>
                                                    <th scope="col"> Archivo</th>
                                                    <th scope="col">Cometario User</th>
                                                    <th scope="col">Cometario Audit.</th>
                                                    <th scope="col">Calificacion</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Acciones</th>
                                                </tr>
                                                </thead>
                                                <?php $abc = 'a'; ?>
                                                @foreach($answerFiles as $files)
                                                    <tbody>
                                                        <?php
                                                        $valFile = explode(".",$files->afnamefile);
                                                        $extension = $valFile[1];
                                                        ?>
                                                        <tr>
                                                            <td>
                                                              @if( $files->status == 8 )
                                                                <input type="checkbox" disabled="disabled" name="fileCheck" id="fileCheck" value="0" />
                                                              @else
                                                                <input type="checkbox" name="fileCheck" id="fileCheck_{{$files->idFile}}" value="{{ $files->idFile }}" />
                                                              @endif
                                                              |
                                                              @if( $extension == 'png' || $extension == 'jpg' || $extension == 'jpeg' || $extension == 'bmp'
                                                                    || $extension == 'PNG' || $extension == 'JPG' || $extension == 'JPEG' || $extension == 'BMP')
                                                                  <i class="fa fa-file-picture-o" aria-hidden="true"></i>
                                                              @elseif( $extension == 'doc' || $extension == 'docx' || $extension == 'DOC' || $extension == 'DOCX' )
                                                                  <i class="fa fa-file-word-o" aria-hidden="true"></i>
                                                              @elseif( $extension == 'pdf' )
                                                                  <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                                              @elseif( $extension == 'xls' || $extension   == 'xlsx' || $extension   == 'XLSX' || $extension   == 'XLS' )
                                                                  <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                                              @elseif( $extension == 'ppt' || $extension   == 'pptx' || $extension   == 'PPTX' || $extension   == 'PPTS' )
                                                                  <i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>
                                                              @endif
                                                            </td>
                                                            <td>{{ $files->afnamefile }} <br>{{ $files->afnopags}}<br><?php $conversion = $files->afsize / '1048576'; ?> {{ round($conversion,2) }} MB </td>

                                                            <td>
                                                                <div class="form-group limiteUser text-right">
<<<<<<< HEAD
                                                                  <textarea  {{$disabledComentUser}} class="form-control comentarioUser" cols="15" id="cometariosUser_{{ $files->idFile }}" rel="cantidadActualUser_{{$abc}}" rows="1" maxlength="100" onblur="guardarComentFile({{ $files->idFile }})">{{ $files->coment }}</textarea><span id="cantidadActualUser_{{$abc}}" style="font-size:8pt">100</span>
=======
                                                                  <textarea  {{$disabledComentUser}} class="form-control comentarioUser" cols="15" id="cometariosUser_{{ $files->idFile }}" rel="cantidadActualUser_{{$abc}}" rows="1" maxlength="100" onblur="guardarComentFile({{ $files->idFile }})">{{ $files->coment_user }}</textarea><span id="cantidadActualUser_{{$abc}}" style="font-size:8pt">100</span>
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
                                                                  <div id="msj-errorcom_{{ $files->idFile }}" class="alert alert-danger" role="alert" style="display:none">
                                                                      <strong class="help-block blanco" id="msjcom_{{ $files->idFile }}"></strong>
                                                                  </div>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="form-group limiteAuditor text-right">
<<<<<<< HEAD
                                                                  <textarea  {{$disabledComentAudit}} class="form-control comentarioAuditor" cols="15" id="cometariosAudit_{{ $files->idFile }}" rel="cantidadActualAuditor_{{$abc}}" rows="1" maxlength="100" onblur="guardarComentFileAudit({{ $files->idFile }})">{{ $files->coment_user }}</textarea><span id="cantidadActualAuditor_{{$abc}}" style="font-size:8pt">100</span>
=======
                                                                  <textarea  {{$disabledComentAudit}} class="form-control comentarioAuditor" cols="15" id="cometarios_{{ $files->idFile }}" rel="cantidadActualAuditor_{{$abc}}" rows="1" maxlength="100" onblur="guardarComentFile({{ $files->idFile }})">{{ $files->coment }}</textarea><span id="cantidadActualAuditor_{{$abc}}" style="font-size:8pt">100</span>
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
                                                                  <div id="msj-errorcom_{{ $files->idFile }}" class="alert alert-danger" role="alert" style="display:none">
                                                                      <strong class="help-block blanco" id="msjcom_{{ $files->idFile }}"></strong>
                                                                  </div>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                {!!Form::select('calificacion',$calificacion,$files->calif,['id'=>'calificacion_'.$files->idFile.'','class'=>'form-control','placeholder'=>'N/A', 'disabled' => $disabledCaliFile, 'onchange' => 'guardarCalificFile('.$files->idFile.',this.value)' ]) !!}
                                                                <div id="msj-errorcal_{{ $files->idFile }}" class="alert alert-danger" role="alert" style="display:none">
                                                                    <strong class="help-block blanco" id="msjcal_{{ $files->idFile }}"></strong>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                @foreach( $catStatus as $catStat )
                                                                    @if( $files->status == $catStat->valStatus )
                                                                        {{  $catStat->nameStatus }}
                                                                    @endif
                                                                @endforeach
                                                            </td>
                                                            <td>
                                                                <!-- No se podra descargar mientrar este cancelado para cualquier perfil de usuario -->
                                                                @if( $files->status != 8 )
                                                                  <a style="color:#000;" href="{{ url('download',['file' => $files->afnamefile] ) }}">
                                                                    <i   class="fa fa-cloud-download" aria-hidden="true" title="Descargar"></i>
                                                                  </a>
                                                                @endif

                                                                  &nbsp;&nbsp;
                                                                <a style="color:#000;" >
                                                                  <!-- Perfil usuario -->
                                                                  @if( auth()->user()->roles[0]->id == 2)
                                                                      @if( $files->status == 2  )
                                                                        <i class="fa fa-close" aria-hidden="true" onclick="deleteFile({{ $files->idFile }},{{ $files->status }})" title="Cancelar"></i>
                                                                        &nbsp;&nbsp;
                                                                        <i class="fa fa-share-square-o" aria-hidden="true" onclick="nextFilter({{ $files->idFile }},{{ $files->status }})" title="Enviar"></i>
                                                                      @endif
                                                                      @if( $files->status == 8 || $files->status == 3 || $files->status == 5 )
                                                                        <i class="fa fa-lock" aria-hidden="true" ></i>
                                                                      @elseif( $files->status == 4 )
                                                                        <i class="fa fa-close" aria-hidden="true" onclick="deleteFile({{ $files->idFile }},{{ $files->status }})" title="Cancelar"></i>
                                                                        &nbsp;&nbsp;
                                                                        <i class="fa fa-share-square-o" aria-hidden="true" onclick="nextFilter({{ $files->idFile }},{{ $files->status }})" title="Enviar"></i>
                                                                      @endif
                                                                  <!-- Perfil auditot o administrador -->
                                                                  @else
                                                                      @if( $files->status == 3 || $files->status == 5   )
                                                                          <i class="fa fa-close" aria-hidden="true" onclick="deleteFile({{ $files->idFile }},{{ $files->status }})" title="Cancelar"></i>
                                                                          &nbsp;&nbsp;
                                                                          <i class="fa fa-share-square-o" aria-hidden="true" onclick="nextFilter({{ $files->idFile }},{{ $files->status }})" title="Enviar"></i>
                                                                      @endif
                                                                      @if( $files->status == 8 || $files->status == 2 )
                                                                          <i class="fa fa-lock" aria-hidden="true" ></i>
                                                                      @endif
                                                                  @endif
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                    <?php $abc++; ?>
                                                @endforeach

                                            </table>
                                        {!! Form::close() !!}
                                    </div>
                              </div>
                          </div>
                        </section>
                      </div>
                </section>
          </section>
      </div>
      <script>
      var rol = {{ auth()->user()->roles[0]->id }};
      if(rol === 3){
        $('.comentarioUser').prop("disabled", true);
      }else{
        $('.comentarioAuditor').prop("disabled", true);
      }
      </script>
    @stop
