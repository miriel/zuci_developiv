<div id="page-wrapper">
  <br>
    @include('flash::message')
    @if(Session::has('message'))
        <div class="alert alert-success" role="alert">
          {{Session::get('message')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    @endif
  <br>
    <div class="row">
        <!-- /.col-lg-12 -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ URL::to('miscuestionarios') }}" class="btn btn-success"> <<  Regresar</a> Calificar cuestionario
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                      <div class="contono">
                        <h4 class=" text-whrite letra"><!--Planeación de la seguridad--></h4>
                      </div>
                      <div class="form-group g-lg-100">
                        <!--<h4 class=" letra2">1.1 - Análisis de riesgos</h4>-->
                          <div class="row conte">
                            <div class="col-lg-12">
                              <div class="form-group">
                                <div class="table-responsive">
                                    <table width="100%" class="table" id="">
                                        <tbody>
                                              <input type="hidden" id="totPreguntas" name="totPreguntas" value="{{ count($cuestPreguntas) }}" />
                                              <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" content="{{ csrf_token() }}" />
                                              @foreach($cuestPreguntas as  $cuestPregunta )
                                                    <tr class="odd gradeX">
                                                          <td><b>{{  $cuestPregunta->pregunta   }}<b></td>
                                                          <td>
                                                              <input type="hidden" id="idCuestion" name="idCuestion" value="{{ $cuestPregunta->idCuestion }}" />
                                                              <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" content="{{ csrf_token() }}" />
                                                          </td>
                                                    </tr>
                                                    @foreach($cuestRespuestas as  $cuestRespuesta )
                                                      <tr>
                                                          @if($cuestPregunta->idPregunta  == $cuestRespuesta->idPregunta )
                                                               @if($cuestRespuesta->tipoCaja == '1' )
                                                                        <td>
                                                                            {!!Form::model($cuestRespuesta,['route'=>['miscuestionarios.update',$cuestRespuesta->idCA],'method'=>'PUT'])!!}
                                                                                {{ $cuestRespuesta->respuesta }}
                                                                                {!!Form::text('value',null,['id'=>'value_'.$cuestRespuesta->idCA.'','class'=>'form-control','placeholder'=>'Nombre de respuesta'])!!}
                                                                            {!! Form::close() !!}
                                                                        </td>

                                                                        <td>
                                                                            <a  class="btn btn-success" OnClick="guardaRespuesta({{ $cuestRespuesta->idCA }},{{ $cuestPregunta->idPregunta }})" >Guardar</a>
                                                                        </td>
                                                                @endif
                                                                @if($cuestRespuesta->tipoCaja == '2' )
                                                                     <td>
                                                                          {!!Form::model($cuestRespuesta,['route'=>['miscuestionarios.update',$cuestRespuesta->idCA],'method'=>'PUT'])!!}
                                                                            {{ Form::file('thefile') }}
                                                                            {{ $cuestRespuesta->respuesta }}
                                                                          {!! Form::close() !!}
                                                                     </td>
                                                                     <td>
                                                                       <input type="text" id="value_{{ $cuestRespuesta->idCA }}" name="value_{{ $cuestRespuesta->idCA }}" value="{{ $cuestRespuesta->idCA }}" />
                                                                       <a href="#" class="btn btn-success" click="guardaRespuesta({{ $cuestRespuesta->idCA }})" >Guardar</a>
                                                                     </td>
                                                                 @endif
                                                                 @if($cuestRespuesta->tipoCaja == '3' )
                                                                      <td>
                                                                        {!!Form::model($cuestRespuesta,['route'=>['miscuestionarios.update',$cuestRespuesta->idCA],'method'=>'PUT'])!!}
                                                                            {!! Form::select('value',[
                                                                                ''=>'N/A',
                                                                                '0'=>'0',
                                                                                '1'=>'1',
                                                                                '2'=>'2',
                                                                                '3'=>'3',
                                                                                '4'=>'4',
                                                                                '5'=>'5'
                                                                                ],null,['id'=>'value','class'=>'form-control'])
                                                                            !!}
                                                                            {{ $cuestRespuesta->respuesta }}
                                                                          {!! Form::close() !!}
                                                                      </td>
                                                                      <td>
                                                                          {{ Form::button('Guardar', ['type' => 'submit', 'class' => 'btn btn-success','title'=>'Inactivar']) }}
                                                                      </td>
                                                                  @endif
                                                                  @if($cuestRespuesta->tipoCaja == '4' )
                                                                       <td>
                                                                          @if($cuestRespuesta->value !=  '')
                                                                            <input type="radio" name="radio_{{ $cuestPregunta->idPregunta }}" id="value_{{ $cuestRespuesta->idCA }}"
                                                                            onClick="guardaRespuestaRadio({{ $cuestRespuesta->idCA }},{{ $cuestRespuesta->valCheck }},{{ $cuestPregunta->idPregunta }}, {{ $cuestPregunta->idCuestion }})"
                                                                            checked />
                                                                          @else
                                                                            <input type="radio" name="radio_{{ $cuestPregunta->idPregunta }}" id="value_{{ $cuestRespuesta->idCA }}"
                                                                            onClick="guardaRespuestaRadio({{ $cuestRespuesta->idCA }},{{ $cuestRespuesta->valCheck }},{{ $cuestPregunta->idPregunta }}, {{ $cuestPregunta->idCuestion }} )" />
                                                                          @endif
                                                                          {{ $cuestRespuesta->respuesta  }}
                                                                       </td>
                                                                       <td>
                                                                       </td>
                                                                   @endif
                                                                   @if($cuestRespuesta->tipoCaja == '6' )
                                                                        <td>
                                                                           @if($cuestRespuesta->value !=  '')
                                                                             <input type="checkbox" name="check_{{ $cuestPregunta->idPregunta }}" id="value_{{ $cuestRespuesta->idCA }}"
                                                                             onClick="guardaRespuestaCheck({{ $cuestRespuesta->idCA }},{{ $cuestRespuesta->valCheck }},{{ $cuestPregunta->idPregunta }},{{ $cuestPregunta->idCuestion }})"
                                                                             checked />
                                                                           @else
                                                                             <input type="checkbox" name="check_{{ $cuestPregunta->idPregunta }}" id="value_{{ $cuestRespuesta->idCA }}"
                                                                             onClick="guardaRespuestaCheck({{ $cuestRespuesta->idCA }},{{ $cuestRespuesta->valCheck }},{{ $cuestPregunta->idPregunta }},{{ $cuestPregunta->idCuestion }})" />
                                                                           @endif
                                                                           {{ $cuestRespuesta->respuesta  }}
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                  @endif
                                                          @endif
                                                      </tr>
                                                  @endforeach

                                              @endforeach
                                        </tbody>
                                    </table>
                                  </div>
                                </div>

                              </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
    <!-- Se agrega el render para mostrar las tabs de paginacion -->

  </div>
