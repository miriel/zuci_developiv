@extends('layouts.adm')
@section('content')
@section('scripts')
      {!!Html::script('js/Generics.js')!!}
      {!!Html::script('js/admin/update_miscuestionarios.js')!!}
      {!!Html::script('js/preguntas.js')!!}
@endsection

@include('miscuestionarios.modal.templateCuestionario')
<div class="inner-wrapper">
   <section role="main" class="content-body pb-0">
      <section role="main" class="content-body pb-0">
         <div class="col-md-12 ">
            <section class="card card-featured card-featured-primary mb-4">
               <header class="card-header">
                  <div class="row">
                     <div class="col-lg-5">
                        <h2 class="card-title blanco">
                           @foreach($arrayIdCuestionario as $idCuestionario)
                           {{ $nameCuestion = $arrayNameCuestionario[$idCuestionario] }}
                           @endforeach
                           <?php $totalPreguntas = count($arrayIdCuestionario); ?>
                           <input type="hidden" id="totPreguntas" name="totPreguntas" value="{{ $totalPreguntasCuestioario }}" />
                           <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" content="{{ csrf_token() }}" />
                        </h2>
                     </div>
                     <div class="col-md-2">
                        <h2 class="card-title blanco" id="porcentaje">Prom: 0</h2>
                     </div>
                     <div class="col-md-3"></div>
                     <div class="col-md-2">
                        <h2 class="card-title blanco" id="porcentajeAudit">PromAudit: 0</h2>
                     </div>

                  </div>
                  <br>

                  <div class="row">
                        <div class="col-lg-5"></div>
                        <div class="col-lg-1">
                           <a type="button" class=" btn-outline btn-link" href="{{ URL::previous() }}"style="color:#fff;">	<i class="fa fa-mail-reply-all"> 	Regresar</i></a>
                        </div>
                        <div class="col-lg-1">
                           <button type="button" class=" btn-outline btn-link" style="color:#fff;"
                              data-toggle='modal' data-target='#descargaTemplatesUser'
                              id="descargaTemplatesUser">Templates</button>
                        </div>

                        @role('Auditor')
                        <div class="col-lg-1">
                           <a type="" class="btn-outline btn-link" href="{{URL("auditFile")}}" style="color:#fff;"> <i class="fa fa-plus"> Guardar</i></a>
                        </div>
                         @endrole

                        @role('User')
                        <div class="col-lg-1">
                           <a type="" class="btn-outline btn-link" href="{{URL("miscuestionarios")}}" style="color:#fff;"> <i class="fa fa-plus"> Guardar</i></a>
                        </div>
                         @endrole
                 </div>
               </header>
               <div class="row">
                  <div class="col-lg-12">
                     <div class="card-body">
                        <div class="col-lg-12 col-xl-12">
                           <div class="" id="">
                              <?php $disabledRadio = 'disabled'?>
                              @can('cuestionarioRadio',new App\CompanyAnswer)
                              <?php $disabledRadio = ''; ?>
                              @endcan
                              <?php $disabledTextArea = 'disabled'?>
                              @can('cuestionarioTextArea',new App\CompanyAnswer)
                              <?php $disabledTextArea = ''?>
                              @endcan
                              <?php $disabledText = 'disabled'?>
                              @can('cuestionarioText',new App\CompanyAnswer)
                              <?php $disabledText = ''?>
                              @endcan
                              <?php $disabledButtom = "style='pointer-events: none'"?>
                              @can('cuestionarioButton',new App\CompanyAnswer)
                              <?php $disabledButtom = ''; ?>
                              @endcan
                              <?php $disabledSelect = 'disabled'?>
                              @can('cuestionarioSelect',new App\CompanyAnswer)
                              <?php $disabledSelect = ''; ?>
                              @endcan
                              <?php $visible=0; ?>
                              @can('dapTxtAudit',new App\CompanyAnswer)
                              <?php $visible=1; ?>
                              @endcan
                              @foreach($arrayIdSecciones as $idSeccion)
                              <?php $countSubsecc=1; ?>
                              <div class="card card-default">
                                      <div class="card-header">
                                        <h4 class="card-title text-center m-0">
                                          @foreach($nombre as $nom)
                                          {{$nom->catvalue}}
                                          @endforeach <br>
                                        </h4>
                                      </div>
                                    </div>
                              @foreach($arrayIdSubSecciones as $idSubSeccion)
                              @if(isset($arrayNameSubseccion[$idSeccion][$idSubSeccion]))
                              <h4 class="letra2">{{ $arrayNameSubseccion[$idSeccion][$idSubSeccion] }}</h4>
                              @endif
                              <?php
                                 $caja="";
                                 $totNoResp = count($arrayIdRespuesta);

                                 $table="<table border='0' width='100%' >";
                                 $countPreg=1;
                                 $abc = 'a';
                                 foreach($arrayIdPreguntas as $idPregunta){

                                   if(!isset($arrayNamePregunta[$idSeccion][$idSubSeccion][$idPregunta])){
                                   }else{
                                     $table.="<tr>";
                                     $table.="<td colspan='".$totNoResp."'><h4 class='letra3'>".$arrayNamePregunta[$idSeccion][$idSubSeccion][$idPregunta]."<h4></td>";
                                     $table.="</tr>";
                                     $table.="<tr>";
                                     $table.="<td colspan=''>&nbsp;</td>";
                                     $table.="</tr>";
                                   }

                                   $table.="<tr>";
                                   foreach($arrayIdRespuesta as $idRespuesta){

                                    if(!isset($arrayNameRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta])) {

                                    }else{

                                      if(isset($arrayIdPregRep[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta])) {
                                        $idRespPreg =  $arrayIdPregRep[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta];
                                        $value =  $arrayPregRepValue[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta];

                                        $valPonder = $arrayValPondRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta];
                                        if($valPonder == ""){ $valPonder=0; }

                                        // Obtiene el tipo de respuesta que es el tipod e caja de texto
                                       if(isset($arrayTipoRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta])){
                                         $texto = $arrayNameRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta];
                                         //if($id != 8 && $texto != ''){
                                         $cabeceras = ['Cumple','No cumple','Archivo'];
                                         if($id === '8'){
                                           if(!in_array(trim($texto),$cabeceras)){
                                              $table.="<td align='center'>".$arrayNameRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta]."</td>";
                                           }
                                         }else{
                                           $table.="<td align='center'>".$arrayNameRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta]."</td>";
                                         }
                                         // if($id != '8' && trim($texto) != 'Cumple' && trim($texto) != 'No cumple' && trim($texto) != 'Archivo'){
                                         //
                                         // }
                                         //}
                                        }
                                      }
                                    }
                                  }
                                 $table.="</tr>";

                                 ///####################3 FOREACH DEL SIGUIENTE TR DE TIPOS DE RESPUESTAS   ###################################

                                 $table.="<tr class='respuestas'>";
                                 $a = 'a';
                                 $b = 'a';
                                 foreach($arrayIdRespuesta as $idRespuesta){
                                  if(!isset($arrayNameRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta])) {

                                  }else{

                                    if(isset($arrayIdPregRep[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta])) {
                                      $idRespPreg =  $arrayIdPregRep[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta];
                                      $value =  $arrayPregRepValue[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta];

                                      $valPonder = $arrayValPondRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta];
                                      if($valPonder == ""){ $valPonder=0; }

                                      // Obtiene el tipo de respuesta que es el tipod e caja de texto
                                     if(isset($arrayTipoRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta])){

                                         //$table.="<td>".$arrayNameRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta]."</td>";

                                        // Form: text
                                        if( $arrayTipoRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta] == 1  ){

                                          $table.="<td align='center'><input type='text' id='value' name='value' class='form-control tama $b'col-md-2
                                          onblur='guardarRespuestaTextCS($idRespPreg,this.value,$idPregunta,$id,$countPreg)'
                                          value='".$value."' $disabledText /></td>";
                                          $b++;
                                        }

                                        if( $arrayTipoRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta] == 2  ){
                                          if($id != '8'){
                                            $table.="<td class='text-center '>";
                                                 $table.="<a href='".route('file.index',['id'=>$idRespPreg,'name'=>$nameCuestion, 'folio' => $folio])."' class='btn btn-primary' title='Editar' $disabledButtom> <i class='fa fa-edit'></i></a>";
                                            $table.="</td>";
                                          }
                                        }

                                         if( $arrayTipoRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta] == 3  ){
                                          // Deshabilita el select si es tiene acceso a la vista: view_calif_dap


                                          $table.="<td align='center'>";
                                          $class = ($id == '8') ? 'ocho' : '';
                                          // Codigo en duro cuando el perfil de usuario sea el auditor y la respuesta sea Nivel de cumplimiento del auditor REspuesta con el ID:=(120)

                                          if(  $idRespuesta==120) {
                                              $table.="<select name='value' id='value' class='form-control col-lg-6 nivelCumplimientoAudit $class'  onChange='guardarRespuestaSelectCSAudit($idRespPreg,this.value,$idPregunta,$id,$countPreg,$countSubsecc)'>";
                                              if($value==1){
                                                $table.="<option selected='selected' value='1'>1</option>";
                                              }else{
                                                $table.="
                                                <option value='0'>Seleccionar</option>
                                                <option value='1'>1</option>";
                                              }

                                              if($value==2){
                                                $table.="<option selected='selected' value='2'>2</option>";
                                              }else{
                                                $table.="<option value='2'>2</option>";
                                              }

                                              if($value==3){
                                                $table.="<option selected='selected' value='3'>3</option>";
                                              }else{
                                                $table.="<option value='3'>3</option>";
                                              }

                                              if($value==4){
                                                $table.="<option selected='selected' value='4'>4</option>";
                                              }else{
                                                $table.="<option value='4'>4</option>";
                                              }

                                              if($value==5){
                                                $table.="<option selected='selected' value='5'>5</option>";
                                              }else{
                                                $table.="<option value='5'>5</option>";
                                              }

                                              $table.="</select>";




                                          }else{

                                            //$table.="<td align='center'>
                                              $table.="<select name='value' id='value' class='form-control col-lg-6 nivelCumplimiento $class' $disabledSelect   onChange='guardarRespuestaSelectCS($idRespPreg,this.value,$idPregunta,$id,$countPreg,$countSubsecc)'>";
                                              if($value==1){
                                                $table.="<option selected='selected' value='1'>1</option>";
                                              }else{
                                                $table.="
                                                <option value='0'>Seleccionar</option>
                                                <option value='1'>1</option>";
                                              }

                                              if($value==2){
                                                $table.="<option selected='selected' value='2'>2</option>";
                                              }else{
                                                $table.="<option value='2'>2</option>";
                                              }

                                              if($value==3){
                                                $table.="<option selected='selected' value='3'>3</option>";
                                              }else{
                                                $table.="<option value='3'>3</option>";
                                              }

                                              if($value==4){
                                                $table.="<option selected='selected' value='4'>4</option>";
                                              }else{
                                                $table.="<option value='4'>4</option>";
                                              }

                                              if($value==5){
                                                $table.="<option selected='selected' value='5'>5</option>";
                                              }else{
                                                $table.="<option value='5'>5</option>";
                                              }

                                              $table.="</select>";

                                          }



                                          $table.="</td>";
                                        }

                                        if( $arrayTipoRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta] == 4  ){
                                          if($id != 8){
                                            if($value !=  ''){
                                              $table.="<td align='center'>
                                              <input type='radio' class='radio' name='radio_$idPregunta$idSubSeccion' id='value_$idRespPreg'
                                              onClick='guardaRespuestaRadioCS($idRespPreg,$valPonder,$idPregunta,$id, $idSeccion,$countPreg,$idSubSeccion)'
                                              value='$idRespPreg'
                                              checked $disabledRadio/>
                                              </td>";
                                            }else{
                                              $table.="<td align='center'>".
                                              "<input type='radio' name='radio_$idPregunta$idSubSeccion' id='value_$idRespPreg'
                                              onClick='guardaRespuestaRadioCS($idRespPreg,$valPonder,$idPregunta,$id, $idSeccion,$countPreg,$idSubSeccion)'
                                              value='$idRespPreg' $disabledRadio/>
                                              </td>";
                                            }
                                          }
                                        }

                                        if( $arrayTipoRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta] == 5  ){
                                          $table.="<td>".$idRespuesta."--".$arrayNameRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta]."</td>";
                                        }

                                        if( $arrayTipoRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta] == 6  ){
                                          $table.="<td>".$arrayNameRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta]."</td>";
                                        }

                                        if( $arrayTipoRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta] == 7 ){
                                          $table.="<td class='limite' align='right'>";
                                          $table.="<textarea id='value' name='value' maxlength='100' rel='cantidadActualUser_$abc' style='width:90% !important; height:40px;' autofocus onblur='guardarRespuestaTextareaCS($idRespPreg,this.value,$idPregunta,$id,$countPreg,$countSubsecc)' cols='15' class='form-control $a $class' $disabledTextArea >".$value."</textarea> <span id='cantidadActualUser_$abc' style='font-size:8pt'>100</span>";
                                          $table.="</td>";
                                          $a++;
                                          $abc++;
                                        }


                                      }
                                    }
                                  }
                                 }
                                  $table.="</tr >";
                                  $countPreg++;
                                 }

                                 $table.="</table>";
                                 echo $table;
                                 ?>
                              <?php $countSubsecc++; ?>
                              @endforeach
                              <?php $abc++; ?>
                              @endforeach
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </section>
   </section>
</div>
<script>
$(document).ready(function() {
  // Deshabilita cajas de formulario de acuerdo al perfil de usuario (rol de usuario) mediante javascript
  var rol = {{ auth()->user()->roles[0]->id }};
  var idCuestionario = {{ $id }};
  habilitarCampos(rol, idCuestionario);
  $('.ocho').prop("disabled", true);

});
</script>
@stop
