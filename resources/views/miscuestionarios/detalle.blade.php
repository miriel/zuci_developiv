@extends('layouts.adm')
	@section('content')

			<div class="inner-wrapper">
				<section role="main" class="content-body pb-0">

          @include('flash::message')
            @if(Session::has('flash_success'))
            <div class="alert alert-success" role="alert">
              {{Session::get('flash_success')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif

					<div class="col-md-12">
								<section class="card card-featured card-featured-primary mb-4">

									<div class="card-body">
										<!-- <code>.card-featured.card-featured-primary</code> -->
										<div class="row	">
                      <div class="col-md-12">
  							<section class="card card-primary mb-4">
  								<header class="card-header">
  									<h2 class="card-title"> EVALUACION GENERAL DE RIESGOS</h2>
  								</header>
  								<div class="card-body">
                    <div class="panel-body">
                      <table  width="100%" class="table table-responsive-md table-sm  ">
                        <thead>
                          <tr>
                            <td>OPERACION </td>
                            <td>ACTIVIDAD</td>
                            <td>DESCRIPCION DEL PELIGRO</td>
                            <td>CONSECUENCIAS</td>
                            <td>EXPOSICION</td>
                            <td>PROBABILIDAD</td>
                            <td>GRADO DE RIESGO</td>
                            <td>REQICSITOS LEGALES Y OTROS </td>
                          </tr>
                        </thead>
                        <tr>
                          <td>Seguridad física</td>
                          <td>Instalaciones</td>
                          <td>Desgaste en paredes, techos, plafones, losetas</td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td>Avería en drenaje</td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td>Ventana rota o descompuesta</td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td>Destrucción Total por desastre natural</td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td>Fallas en suministro de agua</td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td>Fallas en instalación eléctrica </td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td>Caída del servicio eléctrico</td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td>mal estado del mobiliario</td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td>Accesos en puertas y casetas</td>
                          <td>Violación de normas de seguridad en la entrada a la agencia</td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td>Intrusión de una persona ajena a la agencia</td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td>Sistemas de acceso se averíen</td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td>Descuido de los vigilantes de seguridad de accesos</td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td>Desgaste del registro de entrada a las instalaciones</td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td>Mala atención de personal encargado del acceso</td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td>Cohecho con el personal de acceso para incurrir en algun acto ilegal</td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td>Algún enfrentamiento armado en las afueras de las instalaciones</td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td>Conflicto con el personal de vigilancia en acceso de la agencia</td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td>Accesos se encuentren violados a la apertura</td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td>Acceso de personal no autorizado fuera de horas laborables</td>
                          <td> <select>
                              <option value="volvo">selecciona</option>
                                <option value="volvo">N</option>
                                <option value="saab">I</option>
                                <option value="vw">S</option>
                                <option value="audi">M</option>
                              </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">MB</option>
                                    <option value="vw">B</option>
                                    <option value="audi">M</option>
                                    <option value="audi">A</option>
                                    <option value="audi">MA</option>
                                  </select>
                            </td>
                            <td>
                              <select>
                                  <option value="volvo">selecciona</option>
                                    <option value="volvo">I</option>
                                    <option value="saab">CPI</option>
                                    <option value="vw">PU</option>
                                    <option value="audi">RPP</option>
                                    <option value="audi">PP</option>
                                    <option value="audi">DE</option>
                                  </select>
                            </td>
                            <td>
                            <select>
                                <option value="volvo">selecciona</option>
                                  <option value="volvo">B</option>
                                  <option value="saab">M</option>
                                  <option value="vw">A</option>
                                  <option value="audi">MA</option>
                                  <option value="audi">E</option>
                                </select>
                          </td>
                            <td><input type="text" name="" value=""></td>
                        </tr>




                      </table>

                    </div>
  								</div>
  							</section>
  						        </div>

						        </div>
									</div>
								</section>
							</div>



					<!-- end: page -->
				</section>
			</div>



			@stop
