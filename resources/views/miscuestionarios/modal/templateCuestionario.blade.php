<!-- Modal -->
<div class="modal fade" id="descargaTemplatesUser" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true" >
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content bgColorWhite">
         <div class="modal-header modalcolor">
            <div class="row" style="width:100%;">
               <div class="col-md-6">
                  <h4 class="letracolor">Descarga de templates</h4>
               </div>
               <div class="col-md-6 text-right" style="margin-top:15px; margin-left:-5px;">
                  <a style="color:#fff; cursor:pointer;" class="downloadButtom">Descargar <i class="fa fa-cloud-download downloadButtom" aria-hidden="true" title="Descargar"></i></a>
               </div>
            </div>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body" style="overflow-y:auto; height: 400px;">
            <?php $count=1;?>
            <table class="table table-responsive-md table-sm" border="0" style="line-height:2 !important">
               <thead>
                  <tr>
                     <td>Id</td>
                     <td>Nombre</td>
                     <td class="text-center" style="width:125px;">
                        <a id="selectAll" class="btn-outline btn-link" style="color:#000; cursor:pointer">Seleccionar todo</a>
                     </td>
                  </tr>
               </thead>
               <tbody>
                 <?php if(!empty($filesTemplates)){ ?>
                  @foreach($filesTemplates as $templateFls)
                  <tr>
                     <td>{{$count}}</td>
                     <td>{{trim($templateFls->nameTemplate)}}</td>
                     <td align="center">
                        <i class="fa fa-cloud-download" aria-hidden="true" title="Descargar"></i> <input class="donwnload" type="checkbox" rel="{{ $templateFls->nameTemplate }}">
                     </td>
                  </tr>
                  <?php $count++; ?>
                  @endforeach
                <?php } ?>
               </tbody>
            </table>
         </div>
      </div>
      <div class="modal-footer">
      </div>
   </div>
</div>
