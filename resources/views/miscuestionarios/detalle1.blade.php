@extends('layouts.adm')

@section('content')

  <div class="inner-wrapper">
      <section role="main" class="content-body pb-0">
          	<section role="main" class="content-body pb-0">
                	<div class="col-md-12 ">
                    <section class="card card-featured card-featured-primary mb-4">
                      <header class="card-header">
                        <div class="card-actions">
                            <a class="" href="{{route("home")}}">
                              <button type="button" class="btn btn-outline btn-success"  style="color:#fff;">		Guardar</button>
                            </a>
                        </div>

                        <h2 class="card-title blanco">
                          EVALUACION GENERAL DE RIESGOS
                        </h2>
                      </header>
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="card-body">
                            <div class="col-lg-12 col-xl-12">
                              <div class="accordion accordion-primary " id="accordion2Primary">

                                <div class="card card-default">
                                  <div class="card-header">
                                    <h4 class="card-title m-0">
                                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#hola">
                                    Instalaciones
                                      </a>

                                    </h4>
                                  </div>
                                  <div id="hola" class="collapse">
                                    <div class="card-body">
                                      <table>
                                        <thead  style="font-size:10px;">
                                          <tr>
                                            <td>DESCRIPCION DEL PELIGRO</td>
                                            <td>CONSECUENCIAS</td>
                                            <td class="col-lg-1">Total</td>
                                            <td>EXPOSICION</td>
                                            <td class="col-lg-1">Total</td>
                                            <td>PROBABILIDAD</td>
                                            <td class="col-lg-1">Total</td>
                                            <td class="col-lg-1">Total</td>
                                            <td>GRADO DE RIESGO</td>
                                            <td>REQICSITOS LEGALES Y OTROS </td>
                                          </tr>
                                        </thead>
                                        <tr>
                                          <td>Desgaste en paredes, techos, plafones, losetas</td>
                                          <td> <select >
                                              <option value="volvo"></option>
                                                <option value="volvo">N</option>
                                                <option value="saab">I</option>
                                                <option value="vw">S</option>
                                                <option value="audi">M</option>
                                              </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">MB</option>
                                                    <option value="vw">B</option>
                                                    <option value="audi">M</option>
                                                    <option value="audi">A</option>
                                                    <option value="audi">MA</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">CPI</option>
                                                    <option value="vw">PU</option>
                                                    <option value="audi">RPP</option>
                                                    <option value="audi">PP</option>
                                                    <option value="audi">DE</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>

                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                            <select>
                                                <option value="volvo"></option>
                                                  <option value="volvo">B</option>
                                                  <option value="saab">M</option>
                                                  <option value="vw">A</option>
                                                  <option value="audi">MA</option>
                                                  <option value="audi">E</option>
                                                </select>
                                          </td>
                                            <td><input type="text" class=""name="" value=""></td>
                                        </tr>
                                        <tr>
                                          <td>Avería en drenaje</td>
                                          <td> <select >
                                              <option value="volvo"></option>
                                                <option value="volvo">N</option>
                                                <option value="saab">I</option>
                                                <option value="vw">S</option>
                                                <option value="audi">M</option>
                                              </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">MB</option>
                                                    <option value="vw">B</option>
                                                    <option value="audi">M</option>
                                                    <option value="audi">A</option>
                                                    <option value="audi">MA</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">CPI</option>
                                                    <option value="vw">PU</option>
                                                    <option value="audi">RPP</option>
                                                    <option value="audi">PP</option>
                                                    <option value="audi">DE</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>

                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                            <select>
                                                <option value="volvo"></option>
                                                  <option value="volvo">B</option>
                                                  <option value="saab">M</option>
                                                  <option value="vw">A</option>
                                                  <option value="audi">MA</option>
                                                  <option value="audi">E</option>
                                                </select>
                                          </td>
                                            <td><input type="text" class=""name="" value=""></td>
                                        </tr>
                                        <tr>
                                          <td>Ventana rota o descompuesta</td>
                                          <td> <select >
                                              <option value="volvo"></option>
                                                <option value="volvo">N</option>
                                                <option value="saab">I</option>
                                                <option value="vw">S</option>
                                                <option value="audi">M</option>
                                              </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">MB</option>
                                                    <option value="vw">B</option>
                                                    <option value="audi">M</option>
                                                    <option value="audi">A</option>
                                                    <option value="audi">MA</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">CPI</option>
                                                    <option value="vw">PU</option>
                                                    <option value="audi">RPP</option>
                                                    <option value="audi">PP</option>
                                                    <option value="audi">DE</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>

                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                            <select>
                                                <option value="volvo"></option>
                                                  <option value="volvo">B</option>
                                                  <option value="saab">M</option>
                                                  <option value="vw">A</option>
                                                  <option value="audi">MA</option>
                                                  <option value="audi">E</option>
                                                </select>
                                          </td>
                                            <td><input type="text" class=""name="" value=""></td>
                                        </tr>
                                        <tr>
                                          <td>Destrucción Total por desastre natural</td>
                                          <td> <select >
                                              <option value="volvo"></option>
                                                <option value="volvo">N</option>
                                                <option value="saab">I</option>
                                                <option value="vw">S</option>
                                                <option value="audi">M</option>
                                              </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">MB</option>
                                                    <option value="vw">B</option>
                                                    <option value="audi">M</option>
                                                    <option value="audi">A</option>
                                                    <option value="audi">MA</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">CPI</option>
                                                    <option value="vw">PU</option>
                                                    <option value="audi">RPP</option>
                                                    <option value="audi">PP</option>
                                                    <option value="audi">DE</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>

                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                            <select>
                                                <option value="volvo"></option>
                                                  <option value="volvo">B</option>
                                                  <option value="saab">M</option>
                                                  <option value="vw">A</option>
                                                  <option value="audi">MA</option>
                                                  <option value="audi">E</option>
                                                </select>
                                          </td>
                                            <td><input type="text" class=""name="" value=""></td>
                                        </tr>
                                        <tr>
                                          <td>Fallas en suministro de agua</td>
                                          <td> <select >
                                              <option value="volvo"></option>
                                                <option value="volvo">N</option>
                                                <option value="saab">I</option>
                                                <option value="vw">S</option>
                                                <option value="audi">M</option>
                                              </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">MB</option>
                                                    <option value="vw">B</option>
                                                    <option value="audi">M</option>
                                                    <option value="audi">A</option>
                                                    <option value="audi">MA</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">CPI</option>
                                                    <option value="vw">PU</option>
                                                    <option value="audi">RPP</option>
                                                    <option value="audi">PP</option>
                                                    <option value="audi">DE</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>

                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                            <select>
                                                <option value="volvo"></option>
                                                  <option value="volvo">B</option>
                                                  <option value="saab">M</option>
                                                  <option value="vw">A</option>
                                                  <option value="audi">MA</option>
                                                  <option value="audi">E</option>
                                                </select>
                                          </td>
                                            <td><input type="text" class=""name="" value=""></td>
                                        </tr>
                                        <tr>
                                          <td>Fallas en instalación eléctrica</td>
                                          <td> <select >
                                              <option value="volvo"></option>
                                                <option value="volvo">N</option>
                                                <option value="saab">I</option>
                                                <option value="vw">S</option>
                                                <option value="audi">M</option>
                                              </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">MB</option>
                                                    <option value="vw">B</option>
                                                    <option value="audi">M</option>
                                                    <option value="audi">A</option>
                                                    <option value="audi">MA</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">CPI</option>
                                                    <option value="vw">PU</option>
                                                    <option value="audi">RPP</option>
                                                    <option value="audi">PP</option>
                                                    <option value="audi">DE</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>

                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                            <select>
                                                <option value="volvo"></option>
                                                  <option value="volvo">B</option>
                                                  <option value="saab">M</option>
                                                  <option value="vw">A</option>
                                                  <option value="audi">MA</option>
                                                  <option value="audi">E</option>
                                                </select>
                                          </td>
                                            <td><input type="text" class=""name="" value=""></td>
                                        </tr>
                                        <tr>
                                          <td>Caída del servicio eléctrico</td>
                                          <td> <select >
                                              <option value="volvo"></option>
                                                <option value="volvo">N</option>
                                                <option value="saab">I</option>
                                                <option value="vw">S</option>
                                                <option value="audi">M</option>
                                              </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">MB</option>
                                                    <option value="vw">B</option>
                                                    <option value="audi">M</option>
                                                    <option value="audi">A</option>
                                                    <option value="audi">MA</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">CPI</option>
                                                    <option value="vw">PU</option>
                                                    <option value="audi">RPP</option>
                                                    <option value="audi">PP</option>
                                                    <option value="audi">DE</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>

                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                            <select>
                                                <option value="volvo"></option>
                                                  <option value="volvo">B</option>
                                                  <option value="saab">M</option>
                                                  <option value="vw">A</option>
                                                  <option value="audi">MA</option>
                                                  <option value="audi">E</option>
                                                </select>
                                          </td>
                                            <td><input type="text" class=""name="" value=""></td>
                                        </tr>
                                        <tr>
                                          <td>mal estado del mobiliario</td>
                                          <td> <select >
                                              <option value="volvo"></option>
                                                <option value="volvo">N</option>
                                                <option value="saab">I</option>
                                                <option value="vw">S</option>
                                                <option value="audi">M</option>
                                              </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">MB</option>
                                                    <option value="vw">B</option>
                                                    <option value="audi">M</option>
                                                    <option value="audi">A</option>
                                                    <option value="audi">MA</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">CPI</option>
                                                    <option value="vw">PU</option>
                                                    <option value="audi">RPP</option>
                                                    <option value="audi">PP</option>
                                                    <option value="audi">DE</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>

                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                            <select>
                                                <option value="volvo"></option>
                                                  <option value="volvo">B</option>
                                                  <option value="saab">M</option>
                                                  <option value="vw">A</option>
                                                  <option value="audi">MA</option>
                                                  <option value="audi">E</option>
                                                </select>
                                          </td>
                                            <td><input type="text" class=""name="" value=""></td>
                                        </tr>
                                      </table>

                                    </div>
                                  </div>
                                </div>
                                <div class="card card-default">
                                  <div class="card-header">
                                    <h4 class="card-title m-0">
                                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2Primary" href="#holaa">
                                    Accesos en puertas y casetas
                                      </a>

                                    </h4>
                                  </div>
                                  <div id="holaa" class="collapse">
                                    <div class="card-body">
                                      <table width="100%" class="table table-responsive-md table-sm  ">
                                        <thead  style="font-size:10px;">
                                          <tr>
                                            <td>DESCRIPCION DEL PELIGRO</td>
                                            <td>CONSECUENCIAS</td>
                                            <td class="col-">Total</td>
                                            <td>EXPOSICION</td>
                                            <td class="col-l">Total</td>
                                            <td>PROBABILIDAD</td>
                                            <td class="col-lg">Total</td>
                                            <td class="col-lg">Total</td>
                                            <td>GRADO DE RIESGO</td>
                                            <td>REQICSITOS LEGALES Y OTROS </td>
                                          </tr>
                                        </thead>
                                        <tr>
                                          <td>Violación de normas de seguridad en la entrada a la agencia</td>
                                          <td> <select >
                                              <option value="volvo"></option>
                                                <option value="volvo">N</option>
                                                <option value="saab">I</option>
                                                <option value="vw">S</option>
                                                <option value="audi">M</option>
                                              </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">MB</option>
                                                    <option value="vw">B</option>
                                                    <option value="audi">M</option>
                                                    <option value="audi">A</option>
                                                    <option value="audi">MA</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">CPI</option>
                                                    <option value="vw">PU</option>
                                                    <option value="audi">RPP</option>
                                                    <option value="audi">PP</option>
                                                    <option value="audi">DE</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>

                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                            <select>
                                                <option value="volvo"></option>
                                                  <option value="volvo">B</option>
                                                  <option value="saab">M</option>
                                                  <option value="vw">A</option>
                                                  <option value="audi">MA</option>
                                                  <option value="audi">E</option>
                                                </select>
                                          </td>
                                            <td><input type="text" class=""name="" value=""></td>
                                        </tr>
                                        <tr>
                                          <td>Intrusión de una persona ajena a la agencia</td>
                                          <td> <select >
                                              <option value="volvo"></option>
                                                <option value="volvo">N</option>
                                                <option value="saab">I</option>
                                                <option value="vw">S</option>
                                                <option value="audi">M</option>
                                              </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">MB</option>
                                                    <option value="vw">B</option>
                                                    <option value="audi">M</option>
                                                    <option value="audi">A</option>
                                                    <option value="audi">MA</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">CPI</option>
                                                    <option value="vw">PU</option>
                                                    <option value="audi">RPP</option>
                                                    <option value="audi">PP</option>
                                                    <option value="audi">DE</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>

                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                            <select>
                                                <option value="volvo"></option>
                                                  <option value="volvo">B</option>
                                                  <option value="saab">M</option>
                                                  <option value="vw">A</option>
                                                  <option value="audi">MA</option>
                                                  <option value="audi">E</option>
                                                </select>
                                          </td>
                                            <td><input type="text" class=""name="" value=""></td>
                                        </tr>
                                        <tr>
                                          <td>Sistemas de acceso se averíen</td>
                                          <td> <select >
                                              <option value="volvo"></option>
                                                <option value="volvo">N</option>
                                                <option value="saab">I</option>
                                                <option value="vw">S</option>
                                                <option value="audi">M</option>
                                              </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">MB</option>
                                                    <option value="vw">B</option>
                                                    <option value="audi">M</option>
                                                    <option value="audi">A</option>
                                                    <option value="audi">MA</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">CPI</option>
                                                    <option value="vw">PU</option>
                                                    <option value="audi">RPP</option>
                                                    <option value="audi">PP</option>
                                                    <option value="audi">DE</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>

                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                            <select>
                                                <option value="volvo"></option>
                                                  <option value="volvo">B</option>
                                                  <option value="saab">M</option>
                                                  <option value="vw">A</option>
                                                  <option value="audi">MA</option>
                                                  <option value="audi">E</option>
                                                </select>
                                          </td>
                                            <td><input type="text" class=""name="" value=""></td>
                                        </tr>
                                        <tr>
                                          <td>Descuido de los vigilantes de seguridad de accesos</td>
                                          <td> <select >
                                              <option value="volvo"></option>
                                                <option value="volvo">N</option>
                                                <option value="saab">I</option>
                                                <option value="vw">S</option>
                                                <option value="audi">M</option>
                                              </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">MB</option>
                                                    <option value="vw">B</option>
                                                    <option value="audi">M</option>
                                                    <option value="audi">A</option>
                                                    <option value="audi">MA</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">CPI</option>
                                                    <option value="vw">PU</option>
                                                    <option value="audi">RPP</option>
                                                    <option value="audi">PP</option>
                                                    <option value="audi">DE</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>

                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                            <select>
                                                <option value="volvo"></option>
                                                  <option value="volvo">B</option>
                                                  <option value="saab">M</option>
                                                  <option value="vw">A</option>
                                                  <option value="audi">MA</option>
                                                  <option value="audi">E</option>
                                                </select>
                                          </td>
                                            <td><input type="text" class=""name="" value=""></td>
                                        </tr>
                                        <tr>
                                          <td>Desgaste del registro de entrada a las instalaciones</td>
                                          <td> <select >
                                              <option value="volvo"></option>
                                                <option value="volvo">N</option>
                                                <option value="saab">I</option>
                                                <option value="vw">S</option>
                                                <option value="audi">M</option>
                                              </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">MB</option>
                                                    <option value="vw">B</option>
                                                    <option value="audi">M</option>
                                                    <option value="audi">A</option>
                                                    <option value="audi">MA</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">CPI</option>
                                                    <option value="vw">PU</option>
                                                    <option value="audi">RPP</option>
                                                    <option value="audi">PP</option>
                                                    <option value="audi">DE</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>

                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                            <select>
                                                <option value="volvo"></option>
                                                  <option value="volvo">B</option>
                                                  <option value="saab">M</option>
                                                  <option value="vw">A</option>
                                                  <option value="audi">MA</option>
                                                  <option value="audi">E</option>
                                                </select>
                                          </td>
                                            <td><input type="text" class=""name="" value=""></td>
                                        </tr>
                                        <tr>
                                          <td>Mala atención de personal encargado del acceso</td>
                                          <td> <select >
                                              <option value="volvo"></option>
                                                <option value="volvo">N</option>
                                                <option value="saab">I</option>
                                                <option value="vw">S</option>
                                                <option value="audi">M</option>
                                              </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">MB</option>
                                                    <option value="vw">B</option>
                                                    <option value="audi">M</option>
                                                    <option value="audi">A</option>
                                                    <option value="audi">MA</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">CPI</option>
                                                    <option value="vw">PU</option>
                                                    <option value="audi">RPP</option>
                                                    <option value="audi">PP</option>
                                                    <option value="audi">DE</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>

                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                            <select>
                                                <option value="volvo"></option>
                                                  <option value="volvo">B</option>
                                                  <option value="saab">M</option>
                                                  <option value="vw">A</option>
                                                  <option value="audi">MA</option>
                                                  <option value="audi">E</option>
                                                </select>
                                          </td>
                                            <td><input type="text" class=""name="" value=""></td>
                                        </tr>
                                        <tr>
                                          <td>Cohecho con el personal de acceso para incurrir en algun acto ilegal</td>
                                          <td> <select >
                                              <option value="volvo"></option>
                                                <option value="volvo">N</option>
                                                <option value="saab">I</option>
                                                <option value="vw">S</option>
                                                <option value="audi">M</option>
                                              </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">MB</option>
                                                    <option value="vw">B</option>
                                                    <option value="audi">M</option>
                                                    <option value="audi">A</option>
                                                    <option value="audi">MA</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">CPI</option>
                                                    <option value="vw">PU</option>
                                                    <option value="audi">RPP</option>
                                                    <option value="audi">PP</option>
                                                    <option value="audi">DE</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>

                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                            <select>
                                                <option value="volvo"></option>
                                                  <option value="volvo">B</option>
                                                  <option value="saab">M</option>
                                                  <option value="vw">A</option>
                                                  <option value="audi">MA</option>
                                                  <option value="audi">E</option>
                                                </select>
                                          </td>
                                            <td><input type="text" class=""name="" value=""></td>
                                        </tr>
                                        <tr>
                                          <td>Algún enfrentamiento armado en las afueras de las instalaciones</td>
                                          <td> <select >
                                              <option value="volvo"></option>
                                                <option value="volvo">N</option>
                                                <option value="saab">I</option>
                                                <option value="vw">S</option>
                                                <option value="audi">M</option>
                                              </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">MB</option>
                                                    <option value="vw">B</option>
                                                    <option value="audi">M</option>
                                                    <option value="audi">A</option>
                                                    <option value="audi">MA</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                              <select>
                                                  <option value="volvo"></option>
                                                    <option value="volvo">I</option>
                                                    <option value="saab">CPI</option>
                                                    <option value="vw">PU</option>
                                                    <option value="audi">RPP</option>
                                                    <option value="audi">PP</option>
                                                    <option value="audi">DE</option>
                                                  </select>
                                            </td>
                                            <td><input type="text" class="col-lg-1"name="" value=""></td>

                                            <td><input type="text" class="col-lg-1"name="" value=""></td>
                                            <td>
                                            <select>
                                                <option value="volvo"></option>
                                                  <option value="volvo">B</option>
                                                  <option value="saab">M</option>
                                                  <option value="vw">A</option>
                                                  <option value="audi">MA</option>
                                                  <option value="audi">E</option>
                                                </select>
                                          </td>
                                            <td><input type="text" class=""name="" value=""></td>
                                        </tr>
                                      </table>

                                    </div>
                                  </div>
                                </div>

                              </div>
                            </div>
                          </div>

                        </div>

                      </div>
                    </section>
                  </div>
            </section>
      </section>
  </div>
  <script>
    // Muestra el id de la pregunta respuesta en la ventana modal para cargar las imagenes para posteriormente guardar la imagen con el ID de la respuesta y
    // guardar el valor de la imagen en la respuesta que corresponde (tabla BD)
    $(document).ready(function(e){
        $('#subeFilesM').on('show.bs.modal', function(e){
            var id = $(e.relatedTarget).data().id;
            $(e.currentTarget).find('#id').val(id);
        });
    });
  </script>

@stop
