@extends('layouts.adm')

@section('content')

@section('scripts')
    {!!Html::script('js/Generics.js')!!}
    {!!Html::script('js/admin/update_miscuestionarios.js')!!}
    {!!Html::script('js/preguntas.js')!!}
@endsection

    @include('miscuestionarios.form.subseccion')


@stop
