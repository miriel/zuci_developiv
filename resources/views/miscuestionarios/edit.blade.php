@extends('layouts.adm')

@section('content')

    @if( count($secciones) > 1)
      @include('miscuestionarios.form.consecciones')
    @else
      @include('miscuestionarios.form.sinsecciones')
    @endif


@endsection
