@extends('layouts.adm')
@section('content')
@include('miscuestionarios.modal.templateCuestionario')

  <div class="inner-wrapper">
      <section role="main" class="content-body pb-0">
          	<section role="main" class="content-body pb-0">
                	<div class="col-md-12 ">
                    <section class="card card-featured card-featured-primary mb-4">
                      <header class="card-header">
                        <div class="row">
                          <div class="col-lg-7">
                            <h2 class="card-title blanco">
                              @foreach($nombre as $nom)
                              {{$nameCuestionario[0]->quname}}
                              <br/>
                              {{$nom->catvalue}}
                              @endforeach
                            </h2>
                          </div>
                          <div class="col-lg-2">
                          </div>
                          <div class="col-lg-1">
                            <a type="button" class=" btn-outline btn-link" href="{{ URL::previous() }}"style="color:#fff;">	<i class="fa fa-mail-reply-all"> 	Regresar</i></a>
                          </div>
                          <div class="col-lg-1">
                            <button type="button" class=" btn-outline btn-link" style="color:#fff;"
                                data-toggle='modal' data-target='#descargaTemplatesUser'
                                id="descargaTemplatesUser">Templates</button>
                          </div>
                          <!-- <div class="col-lg-1">
                            <a type="" class="btn-outline btn-link" href="{{URL("miscuestionarios")}}" style="color:#fff;"> <i class="fa fa-plus"> Guardar</i></a>
                          </div> -->
                        </div>
                      </header>
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="card-body">
                            <div class="col-lg-12 col-xl-12">
                              <div class="accordion accordion-primary " id="accordion2Primary">

                                @foreach($subsecciones as $subseccion)
                                  <div class="card card-default">
                                    <div class="card-header">
                                      <h4 class="card-title m-0">
                                        <a href="{{route('preguntas.inicio',['idSeccion'=>$id,'subseccion'=>$subseccion->id,'cuestionario'=>$cuestionario,'folioAudita'=>$folio])}}">
                                          {{ $subseccion->valor }}
                                        </a>
                                      </h4>
                                    </div>
                                  </div>
                                @endforeach

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
            </section>
      </section>
  </div>
@stop
