@extends('layouts.adm')
@section('content')
<style media="screen">
.wrapper {
  display: grid;
  grid-template-columns: 100px repeat(3, 3fr) 100px;
}

.nueva {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
    border-color: red;
}
.tama{
  width: 90%;
}
</style>

@section('scripts')
    {!!Html::script('js/Generics.js')!!}
    {!!Html::script('js/admin/update_miscuestionarios.js')!!}
@endsection

<!-- Modal para cargar diferentes archivos -->
  <div class="inner-wrapper">
      <section role="main" class="content-body pb-0">
<<<<<<< HEAD
            <section role="main" class="content-body pb-0">
=======
          	<section role="main" class="content-body pb-0">
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
                    <section class="card card-featured card-featured-primary mb-4">
                      <header class="card-header">
                        <div class="row">
                          <div class="col-lg-7">
                            <h2 class="card-title blanco">
                                {{ $idCuestionario[0]->quname }}
                            </h2>
                          </div>
                          <div class="col-lg-2">
                          </div>
                          @can('Crear Cotizacion', new App\quotations)
                          <div class="col-lg-1">
                              <a type="" class="btn-outline btn-link"  style="color:#fff;" href="{{ route('cotizador.index',['id' => $folio,'id2'=>$id2,'empresa'=>$empresa,'idcues'=>$idcues])}}"> <i class="fa fa-plus"> Cotizar</i></a>
                          </div>
                          @endcan
                          <div class="col-lg-1">
<<<<<<< HEAD
                            <a type="" class=" btn-outline btn-link" href="{{ URL::previous() }}"style="color:#fff;">   <i class="fa fa-mail-reply-all">    Regresar</i></a>
=======
                            <a type="" class=" btn-outline btn-link" href="{{ URL::previous() }}"style="color:#fff;">	<i class="fa fa-mail-reply-all"> 	Regresar</i></a>
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
                          </div>
                          <div class="col-lg-1">
                            <a type="" class="btn-outline btn-link" href="{{URL("miscuestionarios")}}" style="color:#fff;"> <i class="fa fa-plus"> Guardar</i></a>
                          </div>
                        </div>
                      </header>
                      <div class="card-body">
                            <div class="contono">
                              <h4 class=" text-whrite letra"><!--Planeación de la seguridad--></h4>
                            </div>
                            <div class="form-group ">
                              <!--<h4 class=" letra2">1.1 - Análisis de riesgos</h4>-->
                                <div class=" conte">
                                  <input type="hidden" id="totPreguntas" name="totPreguntas" value="{{ count($cuestPreguntas) }}" />
                                  <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" content="{{ csrf_token() }}" />
                                <div class="row">
                                    <?php $countPreg=1; ?>
                                    @foreach($cuestPreguntas as  $cuestPregunta )
                                    <div class="col-lg-12">
                                                <table>
                                                  <thead>
                                                    <tr>
                                                      <th ><b>{{ $cuestPregunta->pregunta   }}<b></th>
                                                    </tr>
                                                    <tr>
                                                      <th>&nbsp;</th>
                                                    </tr><br>
                                                  </thead>
                                                  <td>

                                                      @foreach($cuestRespuestas as  $cuestRespuesta )
                                                          @if($cuestPregunta->idPregunta  == $cuestRespuesta->idPregunta )
                                                          @if($cuestRespuesta->tipoCaja == '1' )
                                                               {!!Form::textarea('value',$cuestRespuesta->value,['id'=>'value_'.$cuestRespuesta->idCA.'',
                                                               'class'=>'form-control tama col-lg-12', 'rows' =>'1','cols'=>'100','placeholder'=>'Nombre de respuesta',
                                                               'onblur'=>'guardaRespuesta('.$cuestRespuesta->idCA.','.$cuestPregunta->idPregunta.','.$countPreg.')'])!!}
                                                               {!!Form::hidden('value',$cuestPregunta->idCuestion,['id'=>'idCuestion','class'=>'col-lg-12'])!!}
                                                               {!! Form::close() !!}
                                                           @endif
                                                           @if($cuestRespuesta->tipoCaja == '2' )
                                                             <a href='{{ route('file.index',['id'=>$cuestRespuesta->idCA,'name'=>$idCuestionario[0]->quname, 'folio' => $folio, 'secciones' => '0']) }}' class='btn btn-primary' title='Editar' $disabledButtom> <i class='fa fa-edit'></i></a>
                                                             {{ $cuestRespuesta->respuesta }}
                                                           @endif

                                                               @if($cuestRespuesta->tipoCaja == '3' )
                                                                      {!!Form::model($cuestRespuesta,['route'=>['miscuestionarios.update',$cuestRespuesta->idCA],'method'=>'PUT'])!!}
                                                                          {!! Form::select('value',[
                                                                              ''=>'N/A',
                                                                              '0'=>'0',
                                                                              '1'=>'1',
                                                                              '2'=>'2',
                                                                              '3'=>'3',
                                                                              '4'=>'4',
                                                                              '5'=>'5'
                                                                              ],null,['id'=>'value','class'=>'form-control'])
                                                                          !!}
                                                                          {{ $cuestRespuesta->respuesta }}
                                                                        {!! Form::close() !!}
                                                                        {{ Form::button('Guardar', ['type' => 'submit', 'class' => 'btn btn-success','title'=>'Inactivar']) }}
                                                                  @endif
                                                            @if($cuestRespuesta->tipoCaja == '4' )
                                                                     @if($cuestRespuesta->value !=  '')
                                                                       <input type="radio" name="radio_{{ $cuestPregunta->idPregunta }}" id="value_{{ $cuestRespuesta->idCA }}"
                                                                       onClick="guardaRespuestaRadio({{ $cuestRespuesta->idCA }},{{ $cuestRespuesta->valCheck }},{{ $cuestPregunta->idPregunta }}, {{ $cuestPregunta->idCuestion }},{{$countPreg}})"
                                                                       checked />
                                                                     @else
                                                                       <input type="radio" name="radio_{{ $cuestPregunta->idPregunta }}" id="value_{{ $cuestRespuesta->idCA }}"
                                                                       onClick="guardaRespuestaRadio({{ $cuestRespuesta->idCA }},{{ $cuestRespuesta->valCheck }},{{ $cuestPregunta->idPregunta }}, {{ $cuestPregunta->idCuestion }},{{$countPreg}} )" />
                                                                     @endif
                                                                     {{ $cuestRespuesta->respuesta  }}
                                                            @endif
                                                            @if($cuestRespuesta->tipoCaja == '6' )
                                                            <div  class="pre-scrollable">
                                                              <table class="table nueva table-bordered table-hover header-fixed"  id="sometable">
                                                                  <thead>
                                                                    <tr>
                                                                      <td >
                                                                        @if($cuestRespuesta->value !=  '')
                                                                            <input class=" " type="checkbox" name="check_{{ $cuestPregunta->idPregunta }}" id="value_{{ $cuestRespuesta->idCA }}"
                                                                            onClick="guardaRespuestaCheck({{ $cuestRespuesta->idCA }},{{ $cuestRespuesta->valCheck }},{{ $cuestPregunta->idPregunta }},{{ $cuestPregunta->idCuestion }},{{$countPreg}})"
                                                                            checked />
                                                                        @else
                                                                          <input class=" " type="checkbox" name="check_{{ $cuestPregunta->idPregunta }}" id="value_{{ $cuestRespuesta->idCA }}"
                                                                          onClick="guardaRespuestaCheck({{ $cuestRespuesta->idCA }},{{ $cuestRespuesta->valCheck }},{{ $cuestPregunta->idPregunta }},{{ $cuestPregunta->idCuestion }},{{$countPreg}})" />
                                                                      @endif
                                                                        {{ $cuestRespuesta->respuesta  }}

                                                                      </td>
                                                                    </tr>
                                                                   </thead>
                                                                   <tbody>
                                                                   </tbody>

                                                                 </div>
                                                             @endif
                                                       @endif
                                                      @endforeach
                                                    </td>
                                                </table>
<<<<<<< HEAD
                                      </div>
=======
	                                   </div>
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
                                     <?php $countPreg++; ?>
                                     @endforeach
                                  </div>

                                </div>
                                </div>
                          </div>
                    </section>
                  </div>
            </section>
      </section>
@stop
