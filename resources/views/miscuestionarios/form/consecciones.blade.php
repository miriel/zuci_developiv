@extends('layouts.adm')
@section('content')
@include('miscuestionarios.modal.templateCuestionario')

  <div class="inner-wrapper">
      <section role="main" class="content-body pb-0">
          	<section role="main" class="content-body pb-0">
                	<div class="col-md-12 ">
                    <section class="card card-featured card-featured-primary mb-4">
                      <header class="card-header">
                        <div class="row">
                          <div class="col-lg-7">
                            <h2 class="card-title blanco">
                              {{ $idCuestionario[0]->quname }}
                            </h2>
                          </div>
                          <div class="col-lg-2">
                          </div>
                          <div class="col-lg-1">
                            <a type="button" class=" btn-outline btn-link" href="{{ URL::previous() }}"style="color:#fff;">	<i class="fa fa-mail-reply-all"> 	Regresar</i></a>
                          </div>
                          <div class="col-lg-1">
                            @can('boton',new App\CompanyAnswer)
                            <button type="button" class=" btn-outline btn-link" style="color:#fff;"
                                data-toggle='modal' data-target='#descargaTemplatesUser'
                                id="descargaTemplatesUser">Templates</button>
                                @endcan
                          </div>
                          <!-- <div class="col-lg-1">
                            <a type="" class="btn-outline btn-link" href="{{URL("miscuestionarios")}}" style="color:#fff;"> <i class="fa fa-plus"> Guardar</i></a>
                          </div> -->
                        </div>
                      </header>
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="card-body">
                            <div class="col-lg-12 col-xl-12">
                              <div class="accordion accordion-primary " id="accordion2Primary">
                                @foreach($secciones as $seccion)
                                  <div class="card card-default">
                                    <div class="card-header">
                                      <h4 class="card-title m-0">
                                        <a href="{{route('subsecciones.inicio',['folio'=>$folio,'id'=>$seccion->catfk,'cuestionario'=>$id])}}">
                                          {{ $seccion->seccion }}
                                        </a>
                                      </h4>
                                    </div>
                                  </div>
                                @endforeach
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
            </section>
      </section>
  </div>
  <script>
    // Muestra el id de la pregunta respuesta en la ventana modal para cargar las imagenes para posteriormente guardar la imagen con el ID de la respuesta y
    // guardar el valor de la imagen en la respuesta que corresponde (tabla BD)
    $(document).ready(function(e){
        $('#subeFilesM').on('show.bs.modal', function(e){
            var id = $(e.relatedTarget).data().id;
            $(e.currentTarget).find('#id').val(id);
        });
    });
  </script>

@stop
