@extends('layouts.adm')
@section('content')
@include('miscuestionarios.modal.templateCuestionario')

  <div class="inner-wrapper">
      <section role="main" class="content-body pb-0">
          	<section role="main" class="content-body pb-0">
                	<div class="col-md-12 ">
                    <section class="card card-featured card-featured-primary mb-4">
                      <header class="card-header">
                        <div class="row">
                          <div class="col-lg-7">
                            <h2 class="card-title blanco">
                              <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" content="{{ csrf_token() }}" />
                            </h2>
                          </div>
                          <div class="col-lg-2">
                          </div>
                          <div class="col-lg-1">
                            <input type="hidden" id="totPreguntas" name="totPreguntas" value="{{ count($arrPreguntasRespuestas) }}" />
                            <a type="button" class=" btn-outline btn-link" href="{{ URL::previous() }}"style="color:#fff;">	<i class="fa fa-mail-reply-all"> 	Regresar</i></a>
                          </div>
                          <div class="col-lg-1">
                            <button type="button" class=" btn-outline btn-link" style="color:#fff;"
                                data-toggle='modal' data-target='#descargaTemplatesUser'
                                id="descargaTemplatesUser">Templates</button>
                          </div>
                          <div class="col-lg-1">
                            <a type="" class="btn-outline btn-link" href="{{URL("miscuestionarios")}}" style="color:#fff;"> <i class="fa fa-plus"> Guardar</i></a>
                          </div>
                        </div>
                      </header>
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="card-body">
                            <div class="col-lg-12 col-xl-12">
                              <div class="accordion accordion-primary " id="accordion2Primary">

                                <div class="card card-default">
                                  <div class="card-header">
                                    <h4 class="card-title m-0">
                                      <div class="row">
                                        <div class="col-md-12 col-lg-12 col-xl-12">
                                          <div class="row">
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                              <a href="#2" aria-expanded="true">
                                                {{$nombreSeccion[0]->nombreSeccion}}
                                              </a>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                              <a id="porcentaje" aria-expanded="true">
                                                Porcentaje: 0%
                                              </a>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </h4>
                                  </div>

                                  <div id="2">
                                    <div class="card-body">
                                      <h4 class=" letra2">{{$nombreSubseccion[0]->nombreSubseccion}}</h4>
                                      <table border="0" width="100%">
                                        <tbody>
                                          <?php $totalPreguntas = count($arrPreguntasRespuestas); ?>
                                          @foreach($arrPreguntasRespuestas as $pregunta => $valores)
                                            <tr>
                                               <td colspan="6">
                                                  <h4 class="letra3">{{$pregunta}}</h4>
                                                  <h4></h4>
                                               </td>
                                            </tr>
                                            <tr>
                                               <td colspan="">&nbsp;</td>
                                            </tr>
                                            <tr>
                                               @foreach($valores as $valor)
                                                <td align="center">{{$valor['texto']}}</td>
                                               @endforeach
                                            </tr>
                                            <tr>
                                               @foreach($valores as $valor)
                                                <td align="center" class="datos">
                                                  <?php
                                                    if($valor['tipoResp'] == "4"){
                                                      echo '<input type="radio" name="radio_12530" id="value_'.$valor["id"].'" onclick="guardaRespuestaRadioCS('.$valor["id"].','.$valor['valor'].','.$valor['idPregunta'].','.$valor['idCuestion'].', '.$valor['idSeccion'].','.($valor['noPregunta']+1).','.$valor['idSubseccion'].')" value="26303">';
                                                    }else if($valor['tipoResp'] == "3"){
                                                      echo '<select name="nivelCumplimiento" id="value" class="form-control col-lg-6" onChange="guardarRespuestaSelectCS('.$valor['id'].',this.value,'.$valor['idPregunta'].','.$valor['idCuestion'].','.$valor['idSeccion'].','.($valor['noPregunta']+1).','.$valor['idSubseccion'].')">
                                                            <option value="0">Seleccionar</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                          </select>';
                                                    }else if($valor['tipoResp'] == "7"){
                                                      echo '<textarea id="value" name="value" style="height:40px;" autofocus="" onblur="guardarRespuestaTextareaCS('.$valor["id"].',this.value,'.$valor['idPregunta'].','.$valor['idCuestion'].', '.$valor['idSeccion'].','.($valor['noPregunta']+1).','.$valor['idSubseccion'].')" cols="15" class="form-control"></textarea>';
                                                    }else if($valor['tipoResp'] == "1"){
                                                      echo '<input type="text" id="value" name="value" class="form-control tama " col-md-2="" onblur="guardarRespuestaTextCS('.$valor["id"].',this.value,'.$valor['idPregunta'].','.$valor['idCuestion'].', '.$valor['idSeccion'].','.($valor['noPregunta']+1).','.$valor['idSubseccion'].')" value="">';
                                                    }else if($valor['tipoResp'] == "2"){
                                                      echo '<a href="'.route('file.index',['id'=>$valor['id'],'name'=>$valor['idCuestion'],'folio'=>$folio]).'" class="btn btn-primary" title="Editar"><i class="fa fa-edit"></i></a>';
                                                    }
                                                  ?>
                                                </td>
                                               @endforeach
                                            </tr>
                                          @endforeach
                                        </tbody>
                                      </table>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
            </section>
      </section>
  </div>
  <script>
  $(document).ready(function(e){

    var totalPreguntas = "<?php echo $totalPreguntas; ?>" ;

    $('.nivelCumplimiento').change(function(){
      var totalNivelCumplimiento = 0;
      var datoNivelCumplimiento = 0;
      $("select").each(function(){
        datoNivelCumplimiento = $(this).val();
        if($.isNumeric(datoNivelCumplimiento)){
            totalNivelCumplimiento += parseInt(datoNivelCumplimiento);
        }
      });
      var porcentaje = (totalNivelCumplimiento / totalPreguntas)
      $('#porcentaje').text('Porcentaje: '+porcentaje.toFixed(2)+'%');
    });

    $('#subeFilesM').on('show.bs.modal', function(e){
        var id = $(e.relatedTarget).data().id;
        $(e.currentTarget).find('#id').val(id);
    });

  });
  </script>
@stop
