{!! Form::open(['route'=> 'file.store', 'method' => 'POST', 'files'=>'true', 'id' => 'my-dropzone' , 'class' => 'dropzone']) !!}
  {{ csrf_field() }}
  <input type="hidden" id="id" name="id" />
  <div class="dz-message" >
      Carga tus archivos
  </div>
  <div class="dropzone-previews"></div>
  <button type="submit" class="btn btn-success" id="submit">Save</button>
{!! Form::close() !!}
