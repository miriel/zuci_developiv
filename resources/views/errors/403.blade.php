@extends('layouts.adm')
	@section('content')

			<div class="loader"></div>
			<div class="inner-wrapper">
				<section role="main" class="content-body pb-0">



					<div class="col-md-12">
								<section class="card card-featured card-featured-primary mb-4">

									<div class="card-body">
										<!-- <code>.card-featured.card-featured-primary</code> -->
										<div class="row	">
                      <div class="col-lg-3">

                      </div>
                      <div class="col-md-6">
  							<section class="card card-primary mb-4">
  								<header class="card-header">
  									<!-- <div class="card-actions">
  										<a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
  										<a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
  									</div> -->

  									<h2 class="card-title text-center">   ERROR</h2>
  								</header>
  								<div class="card-body">
                    <div class="panel-body">
                      <div class="panel-body">

                    <div class="row text-center">
                      <div class="col-lg-3"></div>

                      <div class="col-lg-6">
                        <!-- <h1>pagina no encontrada eror 403 </h1> -->
                        <h1> Pagina No Autorizada</h1><br>
                        <h4 style="color:red;" class="text-center"> {{ $exception->getMessage() }}</h4><br>
													<img src="{{ url('admin/img/triste.png') }}"alt=""><br><br>
                        <strong style="color:#000;"><a class="btn btn-primary" href="{{ url()->previous() }}">Regresar</a></strong>

                      </div><br>

                    </div>

                      </div>
  								</div>
  							</section>
  						        </div>

						        </div>
									</div>
								</section>
							</div>



					<!-- end: page -->
				</section>
			</div>



			<script type="text/javascript">
			$(window).load(function() {
			    $(".loader").fadeOut("slow");
			});
			</script>

			@stop
