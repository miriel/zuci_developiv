<!-- Modal -->
<div class="modal fade" id="createAsignPregnt" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class="letracolor">Agregar pregunta - respuesta</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <input type="hidden" id="id" />
            <!-- Mensajes exitosos -->
            <div id="msj-successAdPrRsp" class="alert alert-success" role="alert" style="display:none">
                Cuestionario agregado correctamente
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                          {!!Form::label('qtname','Tipo Cuestionario*')!!}
                          <input type="hidden" id="qtname" name="qtname" />
                          <input type="text" id="txtqtname" name="txtqtname" class="form-control" readonly=”readonly” />
                          <!-- Mensajes de error -->
                          <div id="msj-error1" class="alert alert-danger" role="alert" style="display:none">
                              <strong class="help-block blanco" id="msj1"></strong>
                          </div>

                      </div>
                      <div class="form-group">
                          <button type="button" class="btn btn-primary btn-xs" id="addPreg">+</button> {!!Form::label('qsnameAdPrRsp','Pregunta*')!!}
                          {!!Form::select('qsnameAdPrRsp',$preguntas, null, ['id'=>'qsnameAdPrRsp','class'=>'form-control','placeholder'=>'Selecciona']) !!}

                          <!-- Mensajes de error -->
                          <div id="msj-errorPr2" class="alert alert-danger" role="alert" style="display:none">
                              <strong class="help-block blanco" id="msjPr2"></strong>
                          </div>

                      </div>
                      <!-- Opcion: Secciones -->
                      <div class="form-group">
                          {!!Form::label('seccion','Seccion')!!}
                          {!!Form::select('seccion',$secciones, null, ['id'=>'seccion','class'=>'form-control','placeholder'=>'Selecciona']) !!}
                      </div>
                   </div>
                   <div class="col-lg-6">
                        <div class="form-group">
                           {!!Form::label('quname','Cuestionario*')!!}
                           <input type="hidden" id="quname" name="quname" />
                           <input type="text" id="txtquname" name="txtquname" class="form-control" readonly=”readonly” />
                           <!-- Mensajes de error -->
                           <div id="msj-error3" class="alert alert-danger" role="alert" style="display:none">
                               <strong class="help-block blanco" id="msj3"></strong>
                           </div>

                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-primary btn-xs" id="addResp">+</button> {!!Form::label('annameAdPrRsp','Respuesta*')!!}
                            {!!Form::select('annameAdPrRsp',$respuestas, null, ['id'=>'annameAdPrRsp','class'=>'form-control','placeholder'=>'Selecciona']) !!}

                            <!-- Mensajes de error -->
                            <div id="msj-error4" class="alert alert-danger" role="alert" style="display:none">
                                <strong class="help-block blanco" id="msj4"></strong>
                            </div>
                        </div>
                        <!-- Opcion: Subsecciones -->
                        <div class="form-group">
                            {!!Form::label('subseccion','SubSeccion')!!}
                            {!!Form::select('subseccion',$subsecciones, null, ['id'=>'subseccion','class'=>'form-control','placeholder'=>'Selecciona']) !!}
                            </div>
                        </div>
                     </div>
                </div>
                <br>
                <table class="table" id="cuestionMezcla">
                      <thead>
                          <tr>
                              <th>
                               PREGUNTAS Y RESPUESTAS ASIGNADAS
                               <input type="hidden" size="1" name="contPreg" id="contPreg" value=""  />
                               <input type="hidden" size="1" name="contResp" id="contResp" value=""  />
                               <!-- Mensajes de errror para las preguntas agragadas por javascript -->
                               <div id="msj-error5" class="alert alert-danger" role="alert" style="display:none">
                                   <strong class="help-block blanco" id="msj5"></strong>
                               </div>

                               <!-- Mensajes de errror para las respuestas agragadas por javascript -->
                               <div id="msj-error6" class="alert alert-danger" role="alert" style="display:none">
                                   <strong class="help-block blanco" id="msj6"></strong>
                               </div>
                               <th>
                           </tr>
                      </thead>

                </table>
      </div>
      <div class="modal-footer">
        {!!link_to('#', $title='Guardar',$attributes = ['id'=>'btnCrePregCuest','class'=>'btn btn-primary'], $secure =  null)!!}
      </div>
    </div>

  </div>
</div>
