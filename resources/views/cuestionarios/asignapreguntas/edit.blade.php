  @extends('layouts.adm')

@section('content')
<div id="page-wrapper">
  <br>
    @include('flash::message')
    @if(Session::has('message'))
        <div class="alert alert-success" role="alert">
          {{Session::get('message')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    @endif
  <br>
    <div class="row">
        <!-- /.col-lg-12 -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Editar cuestionario
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table table-bordered table-hover" id="">
                            <thead>
                                <tr>
                                  <td>ID</td>
                                  <td><b>Cuestionario: </b> @foreach($respuestas as $respuesta) @if($loop->first)  {{  $respuesta->nombreCuest  }}  @endif    @endforeach  </td>
                                  <td align="center">Accion</td>
                                </tr>
                            </thead>
                            <tbody>
                                  @foreach($preguntas as  $pregunta )
                                        <tr class="odd gradeX">
                                              <td>{{  $pregunta->id  }}</td>
                                              <td><b>{{  $pregunta->pregunta  }}<b></td>
                                              <td>
                                                  <div class="row">
                                                      <div class="col-md-1 col-md-offset-1">
                                                          <a href="{{ route('pregunta.edit',$pregunta->id)}}" class="btn btn-primary" title="Editar"> <i class="fa fa-edit"></i></a>
                                                      </div>
                                                      <div class="col-md-1 col-md-offset-1">

                                                      </div>
                                                  </div>
                                              </td>
                                        </tr>
                                        @foreach( $respuestas as  $respuesta )
                                            <tr>
                                                @if($pregunta->id  == $respuesta->id )

                                                    <td>{{ $respuesta->idResp }}</td>
                                                    <td>{{ $respuesta->respuesta }}</td>
                                                    <td>
                                                      <div class="row">
                                                          <div class="col-md-1 col-md-offset-1">
                                                              <a href="{{ route('respuesta.edit',$respuesta->idResp)}}" class="btn btn-primary" title="Editar"> <i class="fa fa-edit"></i></a>
                                                          </div>
                                                          @if( $respuesta->statusPregResp == 1  )
                                                              <div class="col-md-1 col-md-offset-1">
                                                                  {!!Form::open(['route'=> ['asingapregunta.destroy',$respuesta->idRelPregResp, 'nameResp' => $respuesta->respuesta, 'status' => $respuesta->statusPregResp],'method'=>'DELETE'])!!}
                                                                          {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger','title'=>'Deshabilitar']) }}
                                                                  {!!Form::close()!!}
                                                              </div>
                                                          @else
                                                              <div class="col-md-1 col-md-offset-1">
                                                                  {!!Form::open(['route'=> ['asingapregunta.destroy',$respuesta->idRelPregResp, 'nameResp' => $respuesta->respuesta, 'status' => $respuesta->statusPregResp],'method'=>'DELETE'])!!}
                                                                          {{ Form::button('<i class="fa fa-check"></i>', ['type' => 'submit', 'class' => 'btn btn-success','title'=>'Deshabilitar']) }}
                                                                  {!!Form::close()!!}
                                                              </div>
                                                          @endif
                                                      </div>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                  @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
    <!-- Se agrega el render para mostrar las tabs de paginacion -->

  </div>
@stop
