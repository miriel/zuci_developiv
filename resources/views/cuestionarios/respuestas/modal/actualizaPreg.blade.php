<!-- Modal -->
<div class="modal fade" id="editRespst" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class="letracolor">Edita respuesta</h4>
          <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <input type="hidden" id="idPreg" />
            <!-- Mensajes exitosos -->
            <div id="msj-successEd" class="alert alert-success" role="alert" style="display:none">
                Cuestionario agregado correctamente
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::label('annameEd','Respuesta') !!}
            {!! Form::text('annameEd',null, ['id'=>'annameEd','class'=>'form-control'])!!}
            <div id="msjPr-error1" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjPr1"></strong>
            </div>
            {!! Form::label('anatfkEd','Tipo de respuesta') !!}
            {!! Form::select('anatfkEd',$tipoRespuesta, null, ['id'=>'anatfkEd','class'=>'form-control','placeholder'=>'Selecciona']) !!}
            <div id="msjPr-error2" class="alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjPr2"></strong>
            </div>
            {!! Form::label('anvalueEd','Valor de ponderacion') !!}
            {!! Form::select('anvalueEd',[
                ''=>'N/A',
                '0'=>'0',
                '1'=>'1',
                '2'=>'2',
                '3'=>'3',
                '4'=>'4',
                '5'=>'5'
                ],null,['id'=>'anvalueEd','class'=>'form-control'])
            !!}
      </div>
      <div class="modal-footer">        
        <input type="button" class="btn btn-outline btn-success" id="btnUpdResp" value="Actualizar" />
      </div>
    </div>

  </div>
</div>
