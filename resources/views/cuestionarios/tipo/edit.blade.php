@extends('layouts.adm')

@section('content')
    <div id="page-wrapper">
        <br>
        @if(Session::has('message'))
            <div class="alert alert-success" role="alert">
              {{Session::get('message')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
        <!-- /.row -->
        <div class="row">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Editar/Ver tipo cuestionario
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                      {!!Form::model($tipoCuestionario,['route'=> ['tipocuestionario.update',$tipoCuestionario->qtfk],'method'=>'PUT'])!!}
                                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" >
                                            <div class="form-group">
                                                  @include('cuestionarios.tipo.form.tipocuestionario')
                                            </div>
                                            <div class="form-group">
                                              {!! Form::submit('Actualizar',['class'=>'btn btn-default'])!!}
                                            </div>
                                      {!! Form::close() !!}
                                </div>

                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
     </div>
@stop
