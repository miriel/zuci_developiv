<!-- Modal -->
<div class="modal fade" id="editCatalog" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class="letracolor">Editar estándar y Sub estándar</h4>
          <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <input type="hidden" id="idEdCat" name="idEdCat" />
            <!-- Mensajes exitosos -->
            <div id="msj-successEd" class="alert alert-success" role="alert" style="display:none">
                Cuestionario agregado correctamente
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            {!! Form::label('catvalueEd','Nombre') !!}
            {!! Form::text('catvalueEd',null, ['id'=>'catvalueEd','class'=>'form-control'])!!}
            <!-- Mensajes de error -->
            <div id="msj-errorEd" class=" alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjEd"></strong>
            </div>

            {!! Form::label('catnamtablEd','Tipo de estándar') !!}
            {!! Form::select('catnamtablEd',[
                'questionnaries_section'=>'Estándar',
                'questionnaries_subsection'=>'Sub estándar',
                ],null,['id'=>'catnamtablEd','class'=>'form-control'])
            !!}

            <div class="modal-footer">
              <input type="button" class="btn btn-outline btn-success" id="btnEditCatlg" value="Guardar" />
            </div>
      </div>
    </div>
  </div>
</div>
