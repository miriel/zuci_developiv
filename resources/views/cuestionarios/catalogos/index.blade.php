@extends('layouts.adm')

@section('content')

<!-- Ventanas modales -->
@include('cuestionarios.catalogos.modal.create')
@include('cuestionarios.catalogos.modal.edit')

    <div class="inner-wrapper">
      <section role="main" class="content-body pb-0">

        @include('flash::message')
        @if(Session::has('message'))
            <div class="alert alert-success" role="alert">
              {{Session::get('message')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
        @if(Session::has('flash_delete'))
            <div class="alert alert-warning " role="alert">
              {{Session::get('flash_delete')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
        <br>
        @include('flash::message')
        @if(Session::has('flash_success'))
            <div class="alert alert-success" role="alert">
              {{Session::get('flash_success')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
        <div class="col-md-12">
              <section class="card card-featured card-featured-primary mb-4">
                <header class="card-header">
                  <div class="card-actions">
                      @can('create',new App\Catalogs)
                      <button type="button" class="btn btn-outline btn-link" data-toggle='modal'
                        data-target="#createCatalogo" style="color:#fff;">	<i class="fa fa-plus"> 	Agregar</i></button>
                        @endcan
                  </div>

                  <h2 class="card-title blanco">Tipos de estándar y Sub estándar</h2>
                    <input type="hidden" name="status" id="status" value="{{  $status }}" />
                </header>
                <div class="card-body">
                  <!-- <code>.card-featured.card-featured-primary</code> -->
                  <div class="	">
            <div class="tabs tabs-primary">
              <ul class="nav nav-tabs">
                <li class="nav-item active">
                  <a class="nav-link active" href="#altas" data-toggle="tab"><i class="fa fa-star"></i> Altas</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#bajas" data-toggle="tab">Bajas</a>
                </li>
              </ul>
              <div class="tab-content">
                <div  id="altas" class="tab-pane active">
                  <table width="" class="table table-responsive-lg table-sm ">
                    <thead>
                        <tr>
                          <td><b>ID</b></td>
                          <td><b>Nombre</b></td>
                          <td><b>Estatus</b></td>
                          <td><b>Acción</b></td>
                        </tr>
                    </thead>
                      @foreach($seccionesAl as $seccionAl)
                      <tbody>
                          <tr class="odd gradeX">
                            <td>{{ $seccionAl->id }}</td>
                            <td>{{ $seccionAl->description }}</td>
                              <td>
                                <img src="{{ url('admin/img/success.png') }}"alt="">
                                <!-- <button type="button" class="btn btn-success btn-circle"><i class="fa fa-check"></i></button> -->
                              </td>
                              <td class="">
                                  <div class="row">
                                    @can('delete',new App\Catalogs)
                                      <div class="col-md- col-md-offset-1">
                                        {!!Form::open(['route'=> ['catalogo.destroy',$seccionAl->id, 'estatus' => $seccionAl->status, 'description' => $seccionAl->description  ],'method'=>'DELETE'])!!}
                                           {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-outline btn-danger','title'=>'Inactivar']) }}
                                        {!!Form::close()!!}
                                      </div>
                                      @endcan
                                      @can('update',new App\Catalogs)
                                      <div class="col-md-1 col-md-offset-1">
                                        <button type="button" class="btn btn-outline btn-info" data-toggle='modal' id="editCatalog"
                                        data-target='#editCatalog' data-id='{{ $seccionAl->id }}'><i class='fa fa-external-link'></i></button>
                                      </div>
                                      @endcan
                                </div>
                              </td>
                          </tr>
                        </tbody>
                      @endforeach
                  </table>
                  <div class=" pull-right">
                  {!!$seccionesAl->appends(['status' => 1])!!}
                  </div>
                </div>
                <div id="bajas" class="tab-pane fade">
                  <table width="" class="table table-responsive-md table-sm 	">
                    <thead>
                        <tr>
                          <td><b>ID</b></td>
                          <td><b>Nombre</b></td>
                          <td><b>Estatus</b></td>
                          <td><b>Acción</b></td>
                        </tr>
                    </thead>
                      @foreach($seccionesBa as $seccionBa)
                      <tbody>
                          <tr class="odd gradeX">
                            <td>{{ $seccionBa->id }}</td>
                            <td>{{ $seccionBa->description }}</td>
                              <td>
                                <img src="{{ url('admin/img/danger.png') }}"alt="">
                              </td>
                              <td>
                                  <div class="row">
                                    @can('delete',new App\Catalogs)
                                      <div class="col-md- col-md-offset-">
                                        {!!Form::open(['route'=> ['catalogo.destroy',$seccionBa->id, 'estatus' => $seccionBa->status, 'description' => $seccionBa->description],'method'=>'DELETE'])!!}
                                          {{ Form::button('<i class="fa fa-check"></i>', ['type' => 'submit', 'class' => 'btn btn-outline btn-success','title'=>'Activar']) }}
                                        {!!Form::close()!!}
                                      </div>
                                      @endcan
                                      @can('update',new App\Catalogs)
                                      <div class="col-md-1 col-md-offset-1">
                                        <button type="button" class="btn btn-outline btn-info" id="editCatalog" data-toggle='modal'
                                        data-target='#editCatalog' data-id='{{ $seccionBa->id }}'><i class='fa fa-external-link'></i></button>
                                      </div>
                                      @endcan
                                </div>
                              </td>
                          </tr>
                        </tbody>
                      @endforeach
                  </table>
                  <div class=" pull-right">
                      {!!$seccionesBa->appends(['status' => 0])!!}
                  </div>
              </div>
            </div>
          </div>
                </div>
              </section>
            </div>
        <!-- end: page -->
      </section>
    </div>

@stop
@section('scripts')
  {!!Html::script('js/Generics.js')!!}
  {!!Html::script('js/admin/cuestionario_catalogos.js')!!}
@endsection
