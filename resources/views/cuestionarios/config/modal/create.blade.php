<!-- Modal -->
<div class="modal fade" id="createConfigCuest" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class=" letracolor">Agregar configuración cuestionario</h4>
          <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div> 
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <!-- Mensajes exitosos -->
            <div id="msj-successCr" class="alert alert-success" role="alert" style="display:none">
                Cuestionario agregado correctamente
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>


            {!!Form::label('typecompanyCr','Tipo compañía:')!!}
            {!!Form::select('typecompanyCr',$tipoCompania,null, ['id' => 'typecompanyCr', 'class' => 'form-control', 'placeholder' => 'Selecciona'])!!}
            <div id="msj-errorCr1" class=" alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjCr1"></strong>
            </div>
            {!!Form::label('questionnaireCr','Cuestionario:')!!}
            {!!Form::select('questionnaireCr',$cuestionario, null,['id' => 'questionnaireCr', 'class' => 'form-control', 'placeholder' => 'Selecciona'])!!}
            <div id="msj-errorCr2" class=" alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjCr2"></strong>
            </div>
            {!!Form::label('everybodyCr','Aplicar cuestionario a todas la compañías ( Tipos compañía )')!!}
            {!! Form::select('everybodyCr',[
                '0'=>'NO',
                '1'=>'SI',
                ],null,['id'=>'everybodyCr','class'=>'form-control'])
            !!}
          <div class="modal-footer">
            <input type="button" class="btn btn-outline btn-success" id="btnCreaConfigCuest" value="Guardar" />
          </div>
      </div>
    </div>
  </div>
</div>
