{!!Form::label('typecompany','Tipo compañia:')!!}
{!!Form::select('typecompany',$tipoCompania,null, ['id' => 'typecompany', 'class' => 'form-control', 'placeholder' => 'Selecciona'])!!}
{!!Form::label('questionnaire','Cuestionario:')!!}
{!!Form::select('questionnaire',$cuestionario, null,['id' => 'questionnaire', 'class' => 'form-control', 'placeholder' => 'Selecciona'])!!}
@if ($errors->has('questionnaire'))
    <div class="panel-body">
        <div class="alert-danger">
            {{ $errors->first('questionnaire') }}
        </div>
    </div>
@endif
{!!Form::label('everybody','Aplicar cuestionario a todas la compañias ( Tipos compañia )')!!}
{!! Form::select('everybody',[
    '0'=>'NO',
    '1'=>'SI',
    ],null,['id'=>'everybody','class'=>'form-control'])
!!}
