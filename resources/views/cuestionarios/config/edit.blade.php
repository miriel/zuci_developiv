@extends('layouts.adm')

@section('content')
<div id="page-wrapper">
    <br>
    <!-- /.row -->
    <div class="row">
          <div class="row">
              <div class="col-lg-12">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                          Editar configuracion de cuestionario
                      </div>
                      <div class="panel-body">
                          <div class="row">
                              <div class="col-lg-12">
                                  {!!Form::model($config,['route'=> ['configcuestionario.update',$config->id],'method'=>'PUT'])!!}
                                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" >
                                        <div class="form-group">
                                              @include('cuestionarios.config.form.config')
                                        </div>
                                        <div class="form-group">
                                          {!! Form::submit('Actualizar',['class'=>'btn btn-default'])!!}
                                        </div>
                                  {!! Form::close() !!}
                              </div>

                          </div>
                          <!-- /.row (nested) -->
                      </div>
                      <!-- /.panel-body -->
                  </div>
                  <!-- /.panel -->
              </div>
              <!-- /.col-lg-12 -->
          </div>
          <!-- /.row -->
      </div>
   </div>

@stop
