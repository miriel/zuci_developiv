@extends('layouts.adm')

@section('content')

@include('cuestionarios.config.modal.create')
@include('cuestionarios.config.modal.edit')

<div class="inner-wrapper">
  <section role="main" class="content-body pb-0">
    <!-- <header class="page-header">
      <h2>Pais</h2>

      <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
          <li>
            <a href="{{url("home")}}">
              <i class="fa fa-home"></i>
            </a>
          </li>
          <li><span class="blanco"></span></li>
        </ol>

      </div>
    </header> -->
    @include('flash::message')
    @if(Session::has('message'))
        <div class="alert alert-success" role="alert">
          {{Session::get('message')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    @endif
    @if(Session::has('flash_delete'))
        <div class="alert alert-warning " role="alert">
          {{Session::get('flash_delete')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    @endif
    <br>
    @include('flash::message')
    @if(Session::has('flash_success'))
        <div class="alert alert-success" role="alert">
          {{Session::get('flash_success')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    @endif
    <div class="col-md-12">
          <section class="card card-featured card-featured-primary mb-4">
            <header class="card-header">
              <div class="card-actions">
                 @can('create',new App\QuestionnaireConfiguration)
                  <button type="button" class="btn btn-outline btn-link" data-toggle='modal'
                  data-target='#createConfigCuest' style="color:#fff;">	<i class="fa fa-plus"> 	Agregar</i></button>
                  @endcan
              </div>

              <h2 class="card-title blanco">Catálogo reglas de configuración de cuestionarios para tipos de compañía</h2>
                <input type="hidden" name="status" id="status" value="{{  $status }}" />
            </header>
            <div class="card-body">
              <!-- <code>.card-featured.card-featured-primary</code> -->
              <div class="	">
                @can('view',new App\QuestionnaireConfiguration)
                <div class="tabs tabs-primary">
                  <ul class="nav nav-tabs">
                    <li class="nav-item active">
                      <a class="nav-link active" href="#altas" data-toggle="tab"><i class="fa fa-star"></i> Altas</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#bajas" data-toggle="tab">Bajas</a>
                    </li>
                  </ul>
                  <div class="tab-content">
                    <div  id="altas" class="tab-pane active">
                      <table width="" class="table table-responsive-lg table-sm ">
                        <thead>
                            <tr>
                              <td><b>ID</b></td>
                              <td><b>Cuestionario</b></td>
                              <td><b>Tipo de Compañía</b></td>
                              <td><b>Acción</b></td>
                            </tr>
                        </thead>
                              @foreach($configuracionesAl as $ConfigAl)
                          <tbody>
                              <tr class="odd gradeX">
                                <td>{{ $ConfigAl->id }}</td>
                                <td>{{ $ConfigAl->nameCuest }}</td>
                                <td>{{ $ConfigAl->typeCompany }}</td>
                                  <td class="">
                                      <div class="row">
                                        @can('delete',new App\QuestionnaireConfiguration)
                                          <div class="col-md- col-md-offset-1">
                                            {!!Form::open(['route'=> ['configcuestionario.destroy',$ConfigAl->id, 'status' => $ConfigAl->status],'method'=>'DELETE'])!!}
                                              {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-outline btn-danger','title'=>'Inactivar']) }}
                                            {!!Form::close()!!}
                                          </div>
                                          @endcan
                                          @can('update',new App\QuestionnaireConfiguration)
                                          <div class="col-md-1 col-md-offset-1">
                                            <button type="button" class="btn btn-outline btn-info" data-toggle='modal' id="editConfigCuest"
                                            data-target='#editConfigCuest' data-id='{{ $ConfigAl->id }}'><i class='fa fa-external-link'></i></button>
                                          </div>
                                          @endcan
                                    </div>
                                  </td>
                              </tr>
                            </tbody>
                          @endforeach
                      </table>
                      <div class=" pull-right">
                          {!!$configuracionesAl->appends(['status' => 1])!!}
                      </div>
                    </div>
                    <div id="bajas" class="tab-pane fade">
                      <table width="" class="table table-responsive-md table-sm 	">
                        <thead>
                            <tr>
                              <td><b>ID</b></td>
                              <td><b>Cuestionario</b></td>
                              <td><b>TipoC Compañía</b></td>
                              <td><b>Acción</b></td>
                            </tr>
                        </thead>
                              @foreach($configuracionesBa as $ConfigBa)
                          <tbody>
                              <tr class="odd gradeX">
                                <td>{{ $ConfigBa->id }}</td>
                                <td>{{ $ConfigBa->nameCuest }}</td>
                                <td>{{ $ConfigBa->typeCompany }}</td>
                                  <td>
                                      <div class="row">
                                        @can('delete',new App\QuestionnaireConfiguration)
                                          <div class="col-md- col-md-offset-">
                                            {!!Form::open(['route'=> ['configcuestionario.destroy',$ConfigBa->id, 'status' => $ConfigBa->status],'method'=>'DELETE'])!!}
                                              {{ Form::button('<i class="fa fa-check"></i>', ['type' => 'submit', 'class' => 'btn btn-outline btn-success','title'=>'Activar']) }}
                                            {!!Form::close()!!}
                                          </div>
                                          @endcan
                                          @can('update',new App\QuestionnaireConfiguration)
                                          <div class="col-md-1 col-md-offset-1">
                                            <button type="button" class="btn btn-outline  btn-info" id="editConfigCuest" data-toggle='modal'
                                            data-target='#editConfigCuest' data-id='{{ $ConfigBa->id }}'><i class='fa fa-external-link'></i></button>
                                          </div>
                                          @endcan
                                    </div>
                                  </td>
                              </tr>
                            </tbody>
                          @endforeach
                      </table>
                      <div class=" pull-right">
                            {!!$configuracionesBa->appends(['status' => 0])!!}
                      </div>
                  </div>
                </div>
                </div>
                @endcan
            </div>
          </section>
        </div>



    <!-- end: page -->
  </section>
</div>
@stop
@section('scripts')
  {!!Html::script('js/Generics.js')!!}
  {!!Html::script('js/admin/Generales.js')!!}
  {!!Html::script('js/admin/cuestionarios_config.js')!!}
@endsection
