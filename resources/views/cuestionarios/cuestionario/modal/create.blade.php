<!-- Modal -->
<div class="modal fade" id="createCuest" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class=" letracolor">Agregar Cuestionario</h4>
          <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <input type="hidden" id="id" />
            <div class="form-group">

              <!-- Mensajes exitosos -->
              <div id="msj-success" class="alert alert-success" role="alert" style="display:none">
                  Cuestionario agregado correctamente
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              {!!Form::label('quname','Nombre cuestionario')!!}
              {!! Form::text('quname',null, ['id'=>'quname','class'=>'form-control'])!!}
              <!-- Mensajes de error -->
              <div id="msj-error" class="alert alert-danger" role="alert" style="display:none">
                  <strong class="help-block blanco" id="msj"></strong>
              </div>

              {!!Form::label('atqqtfk','Tipo Cuestionario')!!}
              {!!Form::select('atqqtfk',$tipoCuestionario,null, ['id'=>'atqqtfk','class'=>'form-control'])!!}

            </div>
      </div>
      <div class="modal-footer">
        <input type="button" class="btn btn-outline btn-success" id="btnCreCuest" value="Guardar" />
      </div>
    </div>

  </div>
</div>
