@section('styles')
  {!!Html::style('dropzone/dist/dropzone.css')!!}
  {!!Html::style('dropzone/dist/custom.css')!!}
@endsection

<!-- Modal -->
<div class="modal fade" id="subeFilesAdm" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class="letracolor">Carga de archivos</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="container">
            <div class="row">
              <div class="panel panel-primary col-lg-12">
                {!! Form::open(['route' => 'tempcuest.store', 'method' => 'POST', 'files'=>'true', 'id' => 'my-dropzone' , 'class' => 'dropzone']) !!}
                        <div class="panel-body">
                            <input type="hidden" id="idCuestionario" name="idCuestionario" value="{{ $idCuestion }}" />

                              <b>Tipo de documento: &nbsp;&nbsp;</b>
                              {!! Form::select('tipoFile',$tipoDocumentos, null, ['id'=>'tipoFile','class'=>'form-control']) !!}
                              <br>
                              <!--<b>No. Paginas: &nbsp;&nbsp;</b>-->
                              {!!Form::hidden('noPaginas',null,['id'=>'noPaginas','class'=>'form-control'])!!}
                              <br>
                              <div id="msj-errorFile" class="alert-danger" role="alert" style="display:none">
                                  <strong class="help-block blanco" id="msjFile"></strong>
                              </div>
                              <br>
                                  <div id="preview" class="dropzone-previews"></div>

                            </div>
                    {!!Form::close()!!}
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">

        <button type="submit" class="btn btn-primary" id="submit">Subir</button>
      </div>
    </div>

  </div>
</div>

@section('scripts')
  {!!Html::script('dropzone/dist/dropzone.js') !!}
@endsection

<script>
      //Dropzone.autoDiscover = true;
      Dropzone.options.myDropzone = {
          autoProcessQueue: false,
          uploadMultiple:false,
          maxFilezise: 1, // Tamaño en MB que acepta para cargar una archivo
          maxFiles: 1, // Numero maximo de archvos para cargar
          thumbnailWidth:100,
          thumbnailHeight:100,
          addRemoveLinks: true,  // Habilita el link para eliminar el archivo temporalmente cargado antes de subirlo
          parallelUploads: 4, // Numero de cargas en paralelo
          acceptedFiles: ".jpg, .jpeg, .bmp, .png, .doc, .xls, .pdf, .xlsx, .docx, .pptx, .ppt",
          dictRemoveFile: "Remover",

          init: function() {
              var submitBtn = document.querySelector("#submit");
              myDropzone = this;

              var filesAdded = 0;

               this.on("removed file", function (file) {
                 filesAdded -= 1;
               });

              submitBtn.addEventListener("click", function(e){
                  e.preventDefault();
                  e.stopPropagation();
                  myDropzone.processQueue();


              });
              this.on("addedfile", function(file) {
                  filesAdded++;
              });

              this.on("complete", function(file, responseText) {
                  myDropzone.removeFile(file);
                  /*if (filesAdded == 1) {
                    alert("funcion complete: archivos cargados");
                  }*/
              });

              this.on("success", myDropzone.processQueue.bind(myDropzone)
              );

              this.on("error", function(file, errorMessage, xhr) {
                  $("#msjFile").html("El tipo de Archivo es incorrecto, por favor verificarlo");
                  $("#msj-errorFile").fadeIn();
                  filesAdded=0;
              });

              // Funcion  que se ejecuta al terminar de cargar todos los archivos
               this.on("queuecomplete", function (file, response) {
                 $("#noPaginas").val("");
                 if(filesAdded == 1){
                   carga();
                 }
              });

              // Funcion cuando se termina de agregar un archivo
              this.on("addedfile", function (file) {
                // Elimina todos los archivos cagados
                //this.removeAllFiles(true);

              });
          }
      };

      // Acciones que se ejecuta al terminar de cargar el DOM
      $(document).ready(function(){
          // Al hacer click en la ventana de cerrar
          $("#subeFilesAdm").on('hidden.bs.modal', function () {
            $("#noPaginas").val("");
            //mydropzone.removeAllFiles();
            // Cancel current uploads
            //mydropzone.removeAllFiles(true);
            Dropzone.forElement("#my-dropzone").removeAllFiles(true);
          });
      });
  </script>
