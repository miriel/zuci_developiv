@extends('layouts.adm')

@section('scripts')
  {!!Html::script('js/Generics.js')!!}
  {!!Html::script('js/admin/upload_files.js')!!}
@endsection


@section('content')
@include('cuestionarios.cuestionario.modal.file')
<div class="inner-wrapper">
    <input type="hidden" name="_token" id="token"value="{{ csrf_token() }}">
    <section role="main" class="content-body pb-0">
          <section role="main" class="content-body pb-0">
                <div class="col-md-12 ">
                  <section class="card card-featured card-featured-primary mb-4">
                    <header class="card-header">
                      <h2 class="card-title blanco">
                        <div class="text-center blanco"> Administración de Documentos – Templetes</div><br>
                        Administración de Documentos – Templetes<br>
                        {{ $detalle[0]->nameCuestionario }}<br>
                        {{ $detalle[0]->nameSeccion }}<br>
                        {{ $detalle[0]->nameSubSeccion }}<br>
                        {{ $detalle[0]->qsname }}<br>
                      </h2>
                    </header>
                    <div class="row">
                      <div class="col-lg-12">
                        <div class="card-body">
                          <table class="table table-responsive-md table-sm" border="0">
                            <thead>
                                <tr>
                                  <td><b>Fecha</b></td>
                                  <td>{{ $fechaActual }}</td>
                                </tr>
                                <tr>
                                  <td>
                                      <div class="row">
                                        <div  class="col-md-2">
                                            <img src="{{url('img/file_plus_.PNG')}}" widht="40" height="45" data-toggle='modal' data-target='#subeFilesAdm' id="subeFilesM" /> <br>Agregar <br>Template
                                        </div>
                                        <div  class="col-md-2">
                                            <img src="{{url('img/descargar_g.PNG')}}" widht="40" height="45" id="descargaAllTemplates" /> <br>Decargar <br>Template
                                        </div>
                                        <div  class="col-md-2">
                                          <img src="{{url('img/file_delete.PNG')}}" widht="40" height="45"
                                          id="inactivaFlsTempCuestXPregAll" /> <br>Activa/Inactiva<br>Templates
                                        </div>
                                      </div>
                                      <!--<button type="submit" class="btn btn-success" data-toggle='modal' data-target='#subeFilesAdm' id="subeFilesM">Agregar archivo</button>-->
                                  </td>
                                  <td align="left">

                                  </td>
                                </tr>
                            </thead>
                          </table>
                          <div class="panel panel-primary col-lg-12">
                                <div class="table-responsive-sm">
                                  {!! Form::open(['route'=>'fileTemplate.create','method'=>'GET'])!!}
                                      <table class="table">
                                          <thead>
                                            <tr>
                                                <th scope="col">No. </th>
                                                <th scope="col">
                                                    <input type="checkbox" name="fileCheckAllTempXResp" id="fileCheckAllTempXResp" value='0'/> | Icono
                                                </th>
                                                <th scope="col">Nombre </th>
                                                <th scope="col">Estatus</th>
                                                <th scope="col">Tipo Archivo</th>
                                                <th scope="col">Acciones</th>
                                            </tr>
                                          </thead>
                                          <?php $count=1; ?>
                                          @foreach($filesQuestionnaries as $files)
                                              <tbody>
                                                  <?php
                                                  $valFile = explode(".",$files->qfnamefile);
                                                  $extension = $valFile[1];
                                                  ?>
                                                  <tr>
                                                      <td>{{$count}}</td>
                                                      <td>
                                                        <input type="checkbox" name="fileCheckTemplates" id="fileCheckTemplates_{{ $files->idFile }}" value="{{ $files->idFile }}" /> |
                                                        @if( $extension == 'png' || $extension == 'jpg' || $extension == 'jpeg' || $extension == 'bmp'
                                                              || $extension == 'PNG' || $extension == 'JPG' || $extension == 'JPEG' || $extension == 'BMP')
                                                            <i class="fa fa-file-picture-o" aria-hidden="true"></i>
                                                        @elseif( $extension == 'doc' || $extension == 'docx' || $extension == 'DOC' || $extension == 'DOCX' )
                                                            <i class="fa fa-file-word-o" aria-hidden="true"></i>
                                                        @elseif( $extension == 'pdf' )
                                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                                        @elseif( $extension == 'xls' || $extension   == 'xlsx' || $extension   == 'XLSX' || $extension   == 'XLS' )
                                                            <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                                        @elseif( $extension == 'ppt' || $extension   == 'pptx' || $extension   == 'PPTX' || $extension   == 'PPT' )
                                                            <i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>
                                                        @endif
                                                      </td>
                                                      <td>{{ $files->qfnamefile }}<br>{{ $files->qfnopags }} Pàginas<br>{{ $files->qfsize }} Bytes</td>
                                                      <td>
                                                          @foreach($catStatus as $catStst)
                                                            @if($files->status ==  $catStst->valStatus )
                                                                {{ $catStst->nameStatus }}
                                                            @endif
                                                          @endforeach
                                                      </td>
                                                      <td>{{ $files->tipoFile }}</td>
                                                      <td>
                                                          @if($files->status == 1)
                                                              <i class="fa fa-close" aria-hidden="true" onclick="deleteFileTempXPreg({{ $files->idFile }},{{ $files->status }})"></i>
                                                          @else
                                                              <i class="fa fa-check" aria-hidden="true" onclick="deleteFileTempXPreg({{ $files->idFile }},{{ $files->status }})"></i>
                                                          @endif
                                                      </td>
                                                  </tr>
                                              </tbody>
                                              <?php $count++; ?>
                                          @endforeach

                                      </table>
                                  {!! Form::close() !!}







                                  </div>
                            </div>
                        </div>

                      </div>

                    </div>
                  </section>
                </div>
          </section>
    </section>
</div>
@stop
