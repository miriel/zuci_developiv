@extends('layouts.adm')
@section('content')
@include('cuestionarios.cuestionario.modal.create')

        <div class="inner-wrapper">
          <section role="main" class="content-body pb-0">
                @include('flash::message')
                @if(Session::has('message'))
                    <div class="alert alert-success" role="alert">
                      {{Session::get('message')}}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                @endif
                @if(Session::has('flash_delete'))
                    <div class="alert alert-warning " role="alert">
                      {{Session::get('flash_delete')}}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                @endif
                <br>
                @include('flash::message')
                @if(Session::has('flash_success'))
                    <div class="alert alert-success" role="alert">
                      {{Session::get('flash_success')}}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                @endif
                <div class="col-md-12">
                  <section class="card card-featured card-featured-primary mb-4">
                    <header class="card-header">
                      <div class="card-actions">
                          @can('create',new App\Questionnaire)
                          <button type="button" class="btn btn-outline btn-link" data-toggle='modal'
                          data-target='#createCuest' style="color:#fff;">	<i class="fa fa-plus"> 	Agregar</i></button>
                          @endcan
                      </div>
                      <h2 class="card-title blanco">Cuestionarios
                        <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" content="{{ csrf_token() }}" />
                        <input type="hidden" id="status" name="status" value="{{ $status }}" />
                      </h2>

                    </header>
                    <div class="card-body">
                        <div class="">
                            @can('view',new App\Questionnaire)
                        <div class="tabs tabs-primary">

                              <ul class="nav nav-tabs">
                                <li class="nav-item active">
                                  <a class="nav-link active" href="#altas" data-toggle="tab"><i class="fa fa-star"></i> Altas</a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" href="#bajas" data-toggle="tab">Bajas</a>
                                </li>
                              </ul>

                              <div class="tab-content">
                                  <div id="altas" class="tab-pane active">
                                    <table width="100%" class="table table-responsive-md table-sm">
                                      <thead>
                                          <tr>
                                            <td><b>ID</b></td>
                                            <td><b>Nombre</b></td>
                                            <td><b>Estatus</b></td>
                                            <td><b>Acción</b></td>
                                          </tr>
                                      </thead>
                                      @foreach($cuestionariosAl as $cuestionario)
                                        <tbody>
                                            <tr class="odd gradeX">
                                                <td>{{ $cuestionario->id }}</td>
                                                <td>{{ $cuestionario->description }}</td>
                                                <td><button type="button" class="btn btn-success btn-circle"><i class="fa fa-check"></i></button></td>
                                                <td class="">
                                                    <div class="row">
                                                        @can('delete',new App\Questionnaire)
                                                        <div class="col-md- col-md-offset-">
                                                          {!!Form::open(['route'=> ['cuestionario.destroy',$cuestionario->id, 'status' => $cuestionario->status],'method'=>'DELETE'])!!}
                                                             {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-outline btn-danger','title'=>'Inactivar']) }}
                                                          {!!Form::close()!!}
                                                        </div>
                                                        @endcan
                                                          @can('update',new App\Questionnaire)
                                                        <div class="col-md-1 col-md-offset-1">
                                                          <a href="{{ route('cuestionario.edit',$cuestionario->id)}}"class="btn btn-primary" title="Editar"> <i class="fa fa-edit"></i></a>
                                                        </div>
                                                        @endcan
                                                  </div>
                                                </td>
                                            </tr>
                                          </tbody>
                                        @endforeach
                                    </table>
                                    <div class=" pull-right">
                                        {!!$cuestionariosAl->appends(['status' => 1])!!}
                                    </div>
                                  </div>
                                  <div id="bajas" class="tab-pane fade">
                                    <table width="100%" class="table" id="">
                                      <thead>
                                          <tr>
                                            <td><b>ID</b></td>
                                            <td><b>Respuesta</b></td>
                                            <td><b>Estatus</b></td>
                                            <td><b>Acción</b></td>
                                          </tr>
                                      </thead>
                                      @foreach($cuestionariosBa as $cuestionarioBa)
                                        <tbody>
                                            <tr class="odd gradeX">
                                                <td>{{ $cuestionarioBa->id }}</td>
                                                <td>{{ $cuestionarioBa->description }}</td>
                                                <td><button type="button" class="btn btn-danger btn-circle"><i class="fa fa-times"></i></button></td>
                                                <td>
                                                    <div class="row">
                                                        @can('delete',new App\Questionnaire)
                                                        <div class="col-md- col-md-offset-">
                                                          {!!Form::open(['route'=> ['cuestionario.destroy',$cuestionarioBa->id, 'status' => $cuestionarioBa->status],'method'=>'DELETE'])!!}
                                                            {{ Form::button('<i class="fa fa-check"></i>', ['type' => 'submit', 'class' => 'btn btn-outline btn-success','title'=>'Activar']) }}
                                                          {!!Form::close()!!}
                                                        </div>
                                                        @endcan
                                                          @can('update',new App\Questionnaire)
                                                        <div class="col-md-1 col-md-offset-1">
                                                                <a href="{{ route('cuestionario.edit',$cuestionarioBa->id)}}"class="btn btn-primary" title="Editar"> <i class="fa fa-edit"></i></a>
                                                        </div>
                                                        @endcan
                                                  </div>
                                                </td>
                                            </tr>
                                          </tbody>
                                        @endforeach
                                    </table>
                                    <div class=" pull-right">
                                        {!!$cuestionariosBa->appends(['status' => 0])!!}
                                    </div>
                                  </div>
                              </div>
                        </div>
                            @else
                            <div class="row text-center">
                              <div class="col-lg-3"></div>
                              <div class="col-lg-6">
                                <!-- <h1>pagina no encontrada eror 403 </h1> -->
                                <h1 style="color:red;"> Pagina No Autorizada</h1><br>
                                <strong style="color:#000;"><a class="btn btn-primary" href="{{ url()->previous() }}">Regresar</a></strong>
                              </div><br>
                            </div>
                            @endcan
                          </div>
                      </div>
                    </section>
                  </div>
            </section>
        </div>


@stop

@section('scripts')
    {!!Html::script('js/Generics.js')!!}
    {!!Html::script('js/admin/asigna_preguntas.js')!!}
    {!!Html::script('js/admin/cuestionario_create.js')!!}
    {!!Html::script('js/admin/cuestionario_index.js')!!}
@endsection
