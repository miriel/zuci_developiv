<!-- Modal -->
<div class="modal fade" id="pregnsts" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class=" letracolor">Agregar Pregunta</h4>
          <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
            <input type="hidden" id="id" />
            <!-- Mensajes exitosos -->
            <div id="msj-success" class="alert alert-success" role="alert" style="display:none">
                Pregunta agregada correctamente
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!!Form::label('qsname','Pregunta')!!}
            {!!Form::text('qsname',null,['id'=>'qsnameCr','class'=>'form-control'])!!}

            <div id="msj-errorCr" class=" alert-danger" role="alert" style="display:none">
                <strong class="help-block blanco" id="msjCr"></strong>
            </div>

      </div>
      <div class="modal-footer">
        <input type="button" class="btn btn-outline btn-success" id="btnCrePreg" value="Guardar" />
      </div>
    </div>

  </div>
</div>
