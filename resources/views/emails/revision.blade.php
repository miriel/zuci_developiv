<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
       <title>A Simple Responsive HTML Email</title>

          <style type="text/css">
                 * {
                     margin: 0;
                     padding: 0;
                     font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
                     font-size: 100%;
                     line-height: 1.6;
                 }
                 img {
                     max-width: 100%;
                 }
                 body {
                     -webkit-font-smoothing: antialiased;
                     -webkit-text-size-adjust: none;
                     width: 100%!important;
                     height: 100%;
                 }
                 a {
                     color: #348eda;
                 }
                 .btn-primary {
                     text-decoration: none;
                     color: #FFF;
                     background-color: #348eda;
                     border: solid #348eda;
                     border-width: 10px 20px;
                     line-height: 2;
                     font-weight: bold;
                     margin-right: 10px;
                     text-align: center;
                     cursor: pointer;
                     display: inline-block;
                     border-radius: 25px;
                 }
                 .btn-secondary {
                     text-decoration: none;
                     color: #FFF;
                     background-color: #aaa;
                     border: solid #aaa;
                     border-width: 10px 20px;
                     line-height: 2;
                     font-weight: bold;
                     margin-right: 10px;
                     text-align: center;
                     cursor: pointer;
                     display: inline-block;
                     border-radius: 25px;
                 }
                 .last {
                     margin-bottom: 0;
                 }
                 .first {
                     margin-top: 0;
                 }
                 .padding {
                     padding: 10px 0;
                 }
                 table.body-wrap {
                     width: 100%;
                     padding: 20px;
                 }
                 table.body-wrap .container {
                     border: 1px solid #f0f0f0;
                 }
                 table.footer-wrap {
                     width: 100%;
                     clear: both!important;
                 }
                 .footer-wrap .container p {
                     font-size: 12px;
                     color: #666;

                 }
                 table.footer-wrap a {
                     color: #999;
                 }
                 h1, h2, h3 {
                     font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
                     line-height: 1.1;
                     margin-bottom: 15px;
                     color: #000;
                     margin: 40px 0 10px;
                     line-height: 1.2;
                     font-weight: 200;
                 }
                 h1 {
                     font-size: 36px;
                 }
                 h2 {
                     font-size: 28px;
                 }
                 h3 {
                     font-size: 22px;
                 }
                 p, ul, ol {
                     margin-bottom: 10px;
                     font-weight: normal;
                     font-size: 14px;
                 }
                 ul li, ol li {
                     margin-left: 5px;
                     list-style-position: inside;
                 }
                 .container {
                     display: block!important;
                     max-width: 600px!important;
                     margin: 0 auto!important; /* makes it centered */
                     clear: both!important;
                 }
                 .body-wrap .container {
                     padding: 20px;
                 }
                 .content {
                     max-width: 600px;
                     margin: 0 auto;
                     display: block;
                 }
                 .content table {
                     width: 100%;
                 }
                 .zuci{
                   background-color: #fe8b00;
                   }
                   .minusculas{
                     text-transform:lowercase;
                   }
                   .mayusculas{
                     text-transform:uppercase;
                   }
             </style>
         </head>
<<<<<<< HEAD
=======

>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
         <body bgcolor="">
         <!-- Main Body -->
         <table class="body-wrap">
             <tr>
                 <td></td>
                 <td class="container" bgcolor="#FFFFFF">
                     <div class="content">
                         <table>
                             <tr>
                                 <td align="center">
                                     <img src="http://94.76.200.229/pedro.meza/cis.v1/public/img/logo_correo.png" alt="Company Logo"/>
                                     <h1 style="color: #fe8b00;">CIS</h1><br>
                                     <p>  Tu cuestionario ha sido revisado </p><br>
                                      <p>  Con el folio: {{$folio}} </p><br>
                                     <p> <strong>Se le notificara por correo cuando su cotización este lista</strong> </p>

                                 </td>
                             </tr>

                             <!-- Email content goes here .. -->
                             @yield('content')
                         </table>
                     </div>
                 </td>
                 <td></td>
             </tr>
         </table>
         <!-- /Main Body -->
         <!-- Footer -->
         <table class="footer-wrap">
             <tr>
                 <td></td>
                 <td class="container">
                     <div class="content">
                         <table>
                             <tr>
                                 <td align="center">
                                   <div class="footer-bottom row text-center">
                                     <p style="color: #fe8b00;">Contacto</p>
                                     <p class="number">
                                       012-6532-568-9746 <br>
                                       012-6532-569-9748
                                     </p>
<<<<<<< HEAD
=======

>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
                                     <p class="footer-text m-0 col-lg-6 col-md-12" style="color: #fe8b00;">
                                       <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                 Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved |<i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="#" target="_blank">ZUCI</a>
                                 <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0.
                                     </p>
                                     <div class="footer-social col-lg-6 col-md-12">
                                       <a href="#"><i class="fa fa-facebook"></i></a>
                                       <a href="#"><i class="fa fa-twitter"></i></a>
                                       <a href="#"><i class="fa fa-dribbble"></i></a>
                                       <a href="#"><i class="fa fa-behance"></i></a>
                                     </div>-->
                                   </div>
                                 </td>
                             </tr>
                         </table>
                     </div>
                 </td>
                 <td></td>
             </tr>
         </table>
         <!-- /Footer -->
         </body>
         </html>
