<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
       <title>A Simple Responsive HTML Email</title>
    {!!Html::style('admin/css/theme.css')!!}
    {!!Html::style('admin/vendor/bootstrap/css/bootstrap.css')!!}
    	{!!Html::style('admin/css/estilos.css')!!}
    <style media="screen">
      .texto{
        text-align: right;
      }
    </style>
         </head>

         <body bgcolor="">
           <div class="inner-wrapper">
               <section role="main" class="content-body pb-0">
                    <section role="main" class="content-body pb-0">
                          <div class="col-md-12 ">
                             <section class="card card-featured card-featured-primary mb-4">
                               <header class="card-header">
                                 <div class="card-actions">
                                   <!-- <a class="" href="{{route("home")}}">
                                     <button type="button" class="btn btn-outline btn-success"  style="color:#fff;">		Guardar</button>
                                   </a> -->
                                 </div>
                                 <h2 class="card-title blanco">Cotizaciòn </h2>
                               </header>
                               <form action="{{ url('enviar') }}" method="POST">
                                 {{ csrf_field()}}
                               <div class="row">
                                 <div class="col-lg-12">
                                   <div class="card-body">
                                     <div class="row">
                                       <div class="col-lg-1">
                                       </div>
                                       <div class="col-lg-5">
                                           <img src="{{ url('img/logo.png') }}" class="text-center" width="105" height="105" alt="Cis" />
                                       </div>
                                       <div class="col-lg-4" >
                                         <h3 class="text-right">Cotizacion No. {{$id}}</h3>
                                       </div>
                                     </div><br>
                                      <div class="container">
                                        @foreach($user as $use)
                                        <div class="row">
                                          <div class="col-lg-6">
                                            <p> <strong> Nombre :</strong> {{$use->name}} {{$use->apaternal}} {{$use->amaternal}}</p>
                                            <p> <strong>Correo :</strong> {{$use->email}}</p>
                                            @foreach($compania as $compani)
                                            <p> <strong>Empresa :</strong> {{$compani->coname}}</p>
                                              @endforeach
                                           </div>
                                          <div class="col-lg-6 text-right">
                                           <p> <strong>Fecha :</strong> {{$use->qtdate}} </p>
                                           <p> <strong>Telefono :</strong> {{$use->phone_fixed}}</p>
                                          </div>
                                        </div>
                                          @endforeach
                                       <div class="row">
                                           @foreach($divisas as $div)
                                           @if($div->cmfk == $use->qtcmfk)
                                           <div class="col-lg-6">
                                             <p> <strong>La cotizacio es :</strong> {{$div->cmdesc}}  </p>
                                           </div>
                                           <div class="col-lg-6">
                                             <p class="text-right"><strong>Tipo de cambio :</strong> {{$div->cmmax}}</p>
                                           </div>
                                           @endif
                                           @endforeach

                                       </div>

                                      </div>
                                     <div class="row">
                                       <div class="col-lg-12 table-responsive">
                                         <table class="table" style="font-size:14px;" id="tableUserdefaull" >
                                           <h3></h3>
                                             <thead>
                                               <tr>
                                                 <td>Cantidad </td>
                                                 <td>Descripcion del servicio </td>
                                                 <td>PRECIO UNIT EN PESOS MX.</td>
                                                 <td>PRECIO DE TIPO DE MONEDA. </td>
                                                 <td>Precio Total </td>
                                               </tr>
                                             </thead>
                                             @foreach($roles as $rol)
                                             <tr>
                                               <td> <p>{{$rol->qupquantity}}</p> </td>
                                               <td> <p>{{$rol->name}}</p> </td>
                                               <td> <p>{{number_format($rol->qupunitprice,2,'.',',')}}</p> </td>
                                               <td> <p>{{number_format($rol->qupconversion,2,'.',',')}}</p> </td>
                                               <td> <p>{{number_format($rol->quptotal,2,'.',',')}}</p> </td>
                                             </tr>
                                             @endforeach
                                             @foreach($cuestionarios as $cues)
                                             <tr>
                                               <td> <p>{{$cues->qpqquantity}}</p> </td>
                                               <td> <p>{{$cues->quname}}</p> </td>
                                               <td> <p>{{number_format($cues->qpqunitprice,2,'.',',')}}</p> </td>
                                               <td> <p>{{number_format($cues->qpqconversion,2,'.',',')}}</p> </td>
                                               <td> <p>{{number_format($cues->qpqtotal,2,'.',',')}}</p> </td>
                                               </tr>
                                             @endforeach
                                             @foreach($analisis as $ana)
                                             <tr>
                                               <td> <p>{{$ana->qsqquantity}}</p> </td>
                                               <td> <p>{{$ana->quname}}</p> </td>
                                               <td> <p>{{number_format($ana->qsqunitprice,2,'.',',')}}</p> </td>
                                               <td> <p>{{number_format($ana->qsqconversion,2,'.',',')}}</p> </td>
                                               <td> <p>{{number_format($ana->qsqtotal,2,'.',',')}}</p> </td>
                                             </tr>
                                             @endforeach
                                              @foreach($user as $use)
                                             <tr>
                                               <td></td>
                                               <td></td>
                                               <td></td>
                                               <td> <strong>Subtotal :</strong></td>
                                               <td><p>  {{number_format($use->qtsubtotal,2,'.',',')}} </p></td>
                                             </tr>
                                             <tr>
                                               <td></td>
                                               <td></td>
                                               <td></td>
                                               <td> <strong>Iva :</strong></td>
                                               <td><p>  {{number_format($use->qtiva,2,'.',',')}} </p></td>
                                             </tr>
                                             <tr>
                                               <td></td>
                                               <td></td>
                                               <td></td>
                                               <td> <strong>Total :</strong></td>
                                               <td><p> {{number_format($use->qttotal,2,'.',',')}} </p></td>
                                             </tr>
                                             <tr>
                                               <td></td>
                                               <td></td>
                                               <td></td>
                                               <td> <strong>Descuento :</strong></td>
                                               <td><p> {{ $use->qtporcentaje}} %</p></td>
                                             </tr>
                                             <tr>
                                               <td></td>
                                               <td></td>
                                               <td></td>
                                               <td> <strong>Total A Pagar :</strong></td>
                                               <td><p> {{number_format($use->qtdiscount,2,'.',',')}} %</p></td>
                                             </tr>

                                              @endforeach
                                         </table>
                                           @foreach($user as $use)
                                         <div class="row">
                                           <div class="col-lg-10">
                                             <p> <strong style="color:red;"> Validez de tu cotizacion :</strong> {{$use->qtdateend}}</p>
                                           </div>
                                         </div>
                                           @endforeach
                                       </div>

                                     </div>
                                   </div>
                                 </div>
                               </div>
                               </form>
                             </section>
                           </div>
                     </section>
               </section>
           </div>

         </body>
         </html>
