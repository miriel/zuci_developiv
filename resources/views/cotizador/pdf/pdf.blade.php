<!DOCTYPE html>
<html>
<head>
	<title>Cotizacion</title>
	<!-- <link rel="stylesheet" href="sass/main.css" media="screen" charset="utf-8"/> -->
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
	<meta http-equiv="content-type" content="text-html; charset=utf-8">
  {!!Html::style('admin/vendor/bootstrap/css/bootstrap.css')!!}
	<style media="screen">


	.header,
	.footer {
    width: 100%;
    text-align: center;
    position: fixed;
}
.header {
    top: 0px;
}
.footer {
    bottom: 0px;
		text-align: right;
}
.pagenum:before {
    content: counter(page);

}

	</style>
</head>

<body>
			<div class="header">

		</div>
	<?php
  $table = "";
    if(count($user)== '0'){
      $table.="<h4>No tiene Registros</h4>";
    }
    else{
      foreach ($user as $use ) {
            $table.= "<h3 class='text-right '> <strong> Cotizacion </strong></u>     No. ".$id." </h3>";
            $table.="<div class='row'>";
            $table.="<div class='col-lg-6'>";
            $table.="<p> <strong> Nombre :</strong> ".$use->name."\n".$use->apaternal."\n".$use->amaternal."</p>";
            $table.="<p> <strong> Folio : </strong>".$folio."</p>";
            $table.="<p> <strong> Email : </strong>".$use->email."</p>";
            foreach($compania as $compani){
              $table.="<p> <strong> Empresa : </strong>".$compani->coname."</p>";
            }
            foreach($divisas as $div){
              if($div->cmfk == $use->qtcmfk){
            $table.="<p> <strong> EL tipo de moneda de la cotizacio es </strong>".$div->cmdesc."</p>";
              $table.="<p class='text-right'> <strong> Tipo de cambio: </strong>".$div->cmmax."</p>";
            }
            }
            $table.=" </div>";

            $table.="<div class='col-lg-6 text-righ'>";
            $table.="<p class='text-right'> <strong> Fecha : </strong>".$use->qtdate."</p>";
            $table.="<p class='text-right'> <strong> Telefono : </strong>".$use->phone_fixed."</p>";
            $table.=" </div>";
            $table.=" </div>";
            $table.="<table border='0' width='100%'  >";
                $table.="<thead>";
                $table.="<tr class=''>";
                $table.="<td> <p class='color1'><strong>Cantidad  </strong></p></td>";
                $table.="<td> <p class='color1'><strong>Descripcion del servicio </strong></p></td>";
                $table.="<td> <p class='color1'><strong>Prec. UNIT EN PESOS MX.  </strong></p></td>";
                $table.="<td> <p class='color1'><strong>Prec. DE TIPO DE MONEDA. </strong></p></td>";
                $table.="<td> <p class='color1'><strong>Prec. Total </strong></p></td>";
                $table.="</tr>";
                $table.="</thead>";
                foreach($roles as $rol){
                  $table.="<tr class=''>";
                    $table.="<td><p>".$rol->qupquantity."</p></td>";
                    $table.="<td><p>".$rol->name."</p></td>";
                    $table.="<td><p>".number_format($rol->qupunitprice,2,'.',',')."</p></td>";
                    $table.="<td><p>".number_format($rol->qupconversion,2,'.',',')."</p></td>";
                    $table.="<td><p>".number_format($rol->quptotal,2,'.',',')."</p></td>";
                  $table.="</tr>";
                }
                foreach($cuestionarios as $cues){
                  $table.="<tr class=''>";
                    $table.="<td><p>".$cues->qpqquantity."</p></td>";
                    $table.="<td><p>".$cues->quname."</p></td>";
                    $table.="<td><p>".number_format($cues->qpqunitprice,2,'.',',')."</p></td>";
                    $table.="<td><p>".number_format($cues->qpqconversion,2,'.',',')."</p></td>";
                    $table.="<td><p>".number_format($cues->qpqtotal,2,'.',',')."</p></td>";
                  $table.="</tr>";
                }
                foreach($analisis as $ana){
                  $table.="<tr class=''>";
                    $table.="<td><p>".$ana->qsqquantity."</p></td>";
                    $table.="<td><p>".$ana->cmdesc."</p></td>";
                    $table.="<td><p>".number_format($ana->qsqunitprice,2,'.',',')."</p></td>";
                    $table.="<td><p>".number_format($ana->qsqconversion,2,'.',',')."</p></td>";
                    $table.="<td><p>".number_format($ana->qsqtotal,2,'.',',')."</p></td>";
                  $table.="</tr>";
                }


            $table.="</table><br>";
              $table.="<div class='row'>";
              $table.="<div class='col-lg-4'>";
              $table.=" </div>";
              $table.="<div class='col-lg-3 text-right'>";
						  $table.="<p class='text-right'> <strong> Subtotal : </strong>".number_format($use->qtsubtotal,2,'.',',')."</p>";
							$table.="<p class='text-right'> <strong> Iva : </strong>".number_format($use->qtiva,2,'.',',')."</p>";
							$table.="<p class='text-right'> <strong> Total : </strong>".number_format($use->qttotal,2,'.',',')."</p>";
							$table.="<p class='text-right'> <strong> Descueto : </strong>".$use->qtporcentaje."% </p>";
							$table.="<p class='text-right'> <strong> Total A Pagar : </strong>".number_format($use->qtdiscount,2,'.',',')."</p>";
              $table.=" </div>";
              $table.=" </div>";
							$table.="<div class='row'>";
							$table.="<div class='col-lg-7'>";
							$table.="<p class=''> <strong style='color:red;'> Validez de tu cotizacion  : </strong>".$use->qtdateend."</p>";
							$table.=" </div>";
							$table.=" </div>";


      }
}

    echo $table;
	 ?>
	 <div class="footer text-left">
	     Page <span class="pagenum"></span>
	 </div>

</body>

</html>
