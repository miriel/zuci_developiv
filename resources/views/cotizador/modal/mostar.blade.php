<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="users">
        <div class="table-responsive">
        	<table class="table">
        		<thead>
        			<tr>
        				<th>Id</th>
        				<th>Nombre</th>
        				<th><span class="pull-right">Cant.</span></th>
        				<th><span class="pull-right">Precio</span></th>
        				<th style="width: 36px;"></th>
        			</tr>
        			@foreach($roles as $rol)
        			<tr>
        				<th>{{$rol->id}}</th>
        				<th>{{$rol->name}}</th>
        				<td class='col-xs-1'>
        				<div class="pull-right">
        				<input type="text" class="form-control" style="text-align:right" id="cantidad_{{$rol->id}}"  value="1" >
        				</div></td>
        				<td class='col-xs-2'><div class="pull-right">
        				<input type="text" class="form-control" style="text-align:right" id="precio_venta_{{$rol->id}}" value="{{$rol->cost}}" disabled>
        				</div></td>
        				<td ><span class="pull-right"><a href="#" onclick="agregar('{{$rol->id}}')"><i class="fa fa-plus"></i></a></span></td>

        			</tr>
        			@endforeach
        		</thead>
        	</table>
        	<div class=" pull-right">
        		{{ $roles->render()}}
        	</div>

        </div>

      </div>
    </div>
  </div>
</div>
