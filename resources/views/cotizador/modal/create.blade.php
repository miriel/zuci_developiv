<!-- Modal -->
<div class="modal fade" id="servicios" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" >
    <!-- Modal content-->
    <div class="modal-content bgColorWhite">
      <div class="modal-header modalcolor">
        <h4 class=" letracolor">Agregar Servicios</h4>
          <button type="button" class="close blanco" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
    			<div class="col-lg-12">
    				<div class="tabs">
    					<ul class="nav nav-tabs nav-justified">
    						<li class="nav-item disabled active" id="disabled1" >
    							<a class="nav-link " href="#popular10" data-toggle="tab">Usuarios</a>
    						</li>
    						<!--	<li class="nav-item disabled" id="disabled2">
    							<a class="nav-link" href="#recent10"  id="disabled2" data-toggle="tab" >Servicios Primarios</a>
    						</li> -->
                <li class="nav-item disabled" id="disabled3">
    							<a class="nav-link" href="#analisis"  data-toggle="tab">Servicios Secundarios</a>
    						</li>
    					</ul>
    					<div class="tab-content">
    						<div id="popular10" class="tab-pane active">
    							<!-- <p> <strong>Usuarios</strong> </p> -->
    							<div class="table-responsive">
    								  <input type="hidden" id="token" name="token" value="{{ csrf_token() }}" />
    								<table class="table tabla">
    									<thead>
    										<tr>
    											<th>Nombre</th>
    											<th><span class="pull-right">Cant.</span></th>
    											<th><span class="pull-right">Precio</span></th>
    											<th style="width: 36px;"></th>
    										</tr>
                        @foreach($roles as $rol)
    										<tr>
                					<th> <input type="hidden" name="" id="name_{{$rol->id}}"value="<?php echo $rol->name; ?>"> <?php echo $rol->name; ?></th>
    											<td class='col-xs-1'>
    											<div class="pull-right">
    											<input type="number"  placeholder="Min: 1, max: 1000" min="1" max="1000"class="form-control cantidad"  style="text-align:right" id="cantidad_{{$rol->id}}" required value="1">
    											</div></td>
    											<td class='col-xs-2'><div class="pull-right">
    											<input type="text" class="form-control" name="precio_venta" style="text-align:right" id="precio_venta" value="{{$rol->cost}}" disabled>
    											</div></td>
    											<td ><span class="pull-right"><button href="#" class="mb-1 mt-1 mr-1 btn btn-info addPreg" onclick="addServicios({{$rol->id}},{{$rol->cost}})" id="addPreg_{{$rol->id}}"><i class="fa fa-plus"></i></button></span></td>

    										</tr>
    									 @endforeach
    									</thead>
    								</table>

    							</div>
    							</div>
    						<div id="recent10" class="tab-pane">

    							<div class="table-responsive">
    								<table class="table tabla">
    									<thead>
    										<tr>
    											<th>Nombre</th>
    											<th><span class="pull-right">Cant.</span></th>
    											<th><span class="pull-right">Precio</span></th>
    											<th style="width: 36px;"></th>
    										</tr>
                        @foreach($cuestionarios as $cues)
                        @if($cues->qustatus == '1')
                        <tr style="display:none;">
                          <th><input type="hidden" name="" id="idcuestionario_{{$cues->qufk}}" value="{{$cues->qufk}}">
                            <input type="hidden" name="" id="ncuestion_{{$cues->qufk}}"value="<?php echo $cues->quname; ?>"><?php echo $cues->quname;?></th>
                          <td class='col-xs-1'>
                          <div class="pull-right">
                          <input type="text" class="form-control can" style="text-align:right" id="ccuestion_{{$cues->qufk}}"  value="1" >
                          </div></td>
                          <td class='col-xs-2'><div class="pull-right">
                          <input type="text" class="form-control" style="text-align:right" id="pcuestion_{{$cues->qufk}}" value="{{$cues->qucost}}" disabled>
                          </div></td>
                          <td ><span class="pull-right"><button href="#" class="mb-1 mt-1 mr-1 btn btn-info add" onclick="addCuestionarios({{$cues->qufk}},{{$cues->qucost}})" id="addCuest_{{$cues->qufk}}"><i class="fa fa-plus"></i></button></span></td>
                        </tr>

                        @endif
    									@endforeach
    									</thead>
    								</table>

    							</div>

    						</div>
                <div id="analisis" class="tab-pane">

                  <div class="table-responsive">
                    <table class="table tabla">
                      <thead>
                        <tr>
                          <th>Nombre</th>
                          <th><span class="pull-right">Cant.</span></th>
                          <th><span class="pull-right">Precio</span></th>
                          <th style="width: 36px;"></th>
                        </tr>
                          @foreach($cuestionarios as $ana)
                            @if($ana->qustatus == 2)
                            <tr>
                              <th><input type="hidden" name="" id="nanalisis_{{$ana->qufk}}"value="<?php echo $ana->quname; ?>"><?php echo $ana->quname;?></th>
                              <td class='col-xs-1'>
                              <div class="pull-right">
                              <input type="text" class="form-control can" style="text-align:right" id="canalisis_{{$ana->qufk}}"  value="1" >
                              </div></td>
                              <td class='col-xs-2'><div class="pull-right">
                              <input type="text" class="form-control" style="text-align:right" id="panalisis_{{$ana->qufk}}" value="{{$ana->qucost}}" disabled>
                              </div></td>
                              <td ><span class="pull-right"><button href="#" class="mb-1 mt-1 mr-1 btn btn-info add" onclick="addanalisis({{$ana->qufk}},{{$ana->qucost}})" id="addAna_{{$ana->qufk}}"><i class="fa fa-plus"></i></button></span></td>
                            </tr>
                            @endif
                          @endforeach
                      </thead>
                    </table>

                  </div>

                </div>
    					</div>
    				</div>
    			</div>
    		</div>
    </div>

  </div>
</div>
</div>

<!-- Large modal -->
