@extends('layouts.adm')
@section('content')
		@include('cotizador.modal.create')

  <section role="main" class="content-body pb-0">
		@include('flash::message')
		@if(Session::has('message'))
				<div class="alert alert-success" role="alert">
					{{Session::get('message')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
		@endif
  <div class="inner-wrapper">
    <section role="main" class="content-body pb-0">
      <div class="col-md-12">
            <section class="card card-featured card-featured-primary mb-4">
              <header class="card-header">
                <div class="card-actions">

                </div>
                <!-- <h2 class="card-title blanco"><i class="fa fa-edit"></i> Nueva Cotizacion</h2> -->
              </header>
              <div class="card-body">
								<!-- Mensajes exitosos -->
								<div id="msj-success" class="alert alert-success" role="alert" style="display:none">
										cotizacion agregado correctamente
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
										</button>
								</div>
								<!-- Mensajes de error -->
                <!-- <code>.card-featured.card-featured-primary</code> -->
                <div class="	">
                  <div class="container">
		                 <div class="row-fluid">
			                  <div class="col-md-12">
			                     <h2><span class="glyphicon glyphicon-edit"></span> Nueva Cotización</h2>
														<input type="hidden" class="form-control"  id="idcues" value="{{$idcues}}" name="" disabled>
			                       <hr>
														 @foreach($user as $us)
                           <div class="row">
                             <div class="col-lg-6">
															 <input type="hidden" class="form-control"  id="session" value="{{$us->id}}" name="" disabled>
                               <label for="">Empresa</label>
                               @foreach($empresa as $empre)
                               @if($empre->ctfk == $em)
                               <input type="text" class="form-control"  id="name" value="{{$empre->ctname}}" name="" disabled>
                               @endif
                               @endforeach
                             </div>
                             <div class="col-lg-3">
                               <label for="">Fecha</label>
                               <input type="text" class="form-control" id="fecha" name="" value="{{$fechaActual}}" disabled>
                             </div>
                             <div class="col-lg-3">
                               <label for="">Contacto</label>
                               <input type="text" class="form-control" id="telefono" value="{{$us->lada_cel}}{{$us->phone_fixed}}" disabled>
                             </div>
                           </div>
                           <div class="row">
                             <div class="col-lg-2">
                               <label for="">Folio</label>
                               <input type="text" class="form-control" id="folio" value="{{$us->folio}}"name="" disabled>
                             </div>
                             <div class="col-lg-5">
                               <label for="">Email</label>
                               <input type="text" class="form-control" id="correo" value="{{$us->email}}" name="" disabled>
                             </div>
														 <div id="msj-error" class="alert-danger" role="alert" style="display:none">
								                 <strong class="help-block blanco" id="msj"></strong>
								             </div>
                             <div class="col-lg-3">
                               <strong><label  for="">Tipo de Moneda</label></strong>
                                 <div class="form-group" id="ocultar">
                                      <select class="form-control"  onchange="mostar(this.value)" id="divisa">
																				<option value="0">Selecciona</option>
                                            @foreach($divisas as $div)
                                          <option value="{{$div->cmfk}}" >{{$div->cmdesc}}</option>
                                          @endforeach
                                      </select>
                                  </div>
                             </div>
														 <div class="col-lg-1">
															 <input type="hidden" id="cmmoneda" name="cmmoneda" value="0" disabled>
														 </div>
                           </div><br>
													 @endforeach
                         </div>
                       </div>

                            <div class="col-md-12">
                               <div class="pull-right">
                                 <button type="button" class="btn btn-info " data-toggle='modal'
         												data-target='#servicios' onchange="ocultar()" id="modal" style="color:#fff;">	<i class="fa fa-plus"> 	Agregar productos</i></button>

																	 <button type="button" id="borrar" onclick="eliminartodo()" class="btn btn-danger">
																			  Eliminar Cotizacion
																	 </button>
                                </div>
                             </div>
                          </form>
                        <br><br>

													<div class="tabla1">
														<table class="table" style="font-size:14px;" id="tablaServicios" >
																<h3><strong> Servicios Usuarios</strong></h3>
														</table>
													</div>
													<div class="userdefaull">
														<table class="table" style="font-size:14px;" id="tableUserdefaull" >
																<thead>
																	<tr>
																		<td>Descripcion del servicio </td>
																		<td>Cantidad </td>
																		<td>PRECIO UNIT.</td>
																		<td>PRECIO. </td>
																		<td>Precio Total </td>
																	</tr>
																</thead>
																@foreach($roldefaull as $rol)
																<td id="filadescription_{{$rol->id}}">
																	<input type="hidden" id="{{$rol->id}}" value="{{$rol->id}}" class="form-control name"><p>{{$rol->name}}</p></td>
																<td id="filadescription_{{$rol->id}}">
																	<input type='number'  min='1' max='1000' id="can_{{$rol->id}}" onchange="editServicios({{$rol->id}},{{$rol->cost}})" value="1" class="form-control id userc" ></td>
																<td id="filadescription_{{$rol->id}}">
																	 <input type="text" name="" id="pre_{{$rol->id}}" class="form-control id userp" value="{{$rol->cost}}" disabled> </td>
																<td id="filadescription_{{$rol->id}}">
																	<input type="text" name="" id="prec_{{$rol->id}}" class="form-control id usercon borrar" value="" disabled> </td></td>
																<td id="filadescription_{{$rol->id}}">
																	<input type="text" name="" id="total_{{$rol->id}}" class="form-control id borrar usertotal" value="" disabled> </td></td>
																@endforeach
														</table>
													</div>
													<div class="tabla2">
														<table class="table" style="font-size:14px;" id="tablaCuestion" >
																<h3><strong> Servicios Primarios</strong></h3>
														</table>
													</div>
													<div class="cuestiondefaull">
														<table class="table" style="font-size:14px;" id="tablaCuestiondefaull" >
																<thead>
																	<tr>
																		<td>Descripcion del servicio </td>
																		<td>Cantidad </td>
																		<td>PRECIO UNIT.</td>
																		<td>PRECIO. </td>
																		<td>Precio Total </td>
																	</tr>
																</thead>
																@foreach($cuesdefaull as $cues)
																<tr>
																	<td><input type="hidden" name="" id="c_{{$cues->qufk}}" value="{{$cues->qufk}}" class="form-control id2 primariosid"> <p>{{$cues->quname}} </p></td>
																	<td><input type='number'  min='1' max='1000' id="cancuestion_{{$cues->qufk}}" value="1" class='form-control id primariosc' disabled></td>
																	<td> <input type="text" name="" id="preccues_{{$cues->qufk}}" class="form-control primariosp" value="{{$cues->qucost}}" disabled> </td>
																	<td><input type="text" name="" id="precuestion_{{$cues->qufk}}" class="form-control borrar primarioscon" value="" disabled> </td></td>
																	<td><input type="text" name="" id="totalcuestion_{{$cues->qufk}}" class="form-control borrar primariostotal" value="" disabled> </td></td>
																</tr>
																@endforeach
														</table>
													</div>
													<div class="tabla3">
														<table class="table" style="font-size:14px;" id="tablaAnalisis" >
																<h3><strong> Servicios Secundarios</strong></h3>
														</table>
													</div>
													<br>
														<div class="row">
															<div class="col-lg-9"></div>
															<div class="col-lg-3">
																<label for="">Sub Total</label>
															<input type="text" class="text-right form-control" id="subtotal" name="" value="0" disabled>
															<label for="">Iva 16%</label>
															<input type="text" class="text-right form-control" id="iva" name="" value="0" disabled>
															<label for="">Total</label>
														<input type="text"  class="text-right form-control" id="total" name="" value="0" disabled>
															</div>
														</div>

														<div class="row">
															<div class="col-lg-9"></div>
															<div class="col-lg-3">
																<label for="">Descuento auditor</label>
																<div class="input-group mb-3">
																	<span class="input-group-addon">%</span>
																	<input type="text" class="form-control text-right" onchange="des()" id="descuento" value="0">
																</div>
																<label for="">Total:</label>
																	<input type="text" class="form-control text-right" id="audittotal" value="0" disabled>
																</div>
															</div>

														</div><br>
														<div class="row">
															<div class="col-lg-9">
															</div>
															<div class="col-lg-3">
																<!-- {!!link_to('#', $title='Registrar',$attributes=['id'=>'registro','class'=>'btn btn-primary'],$secure = null)!!} -->
															<button type="button" class="btn btn-success" id="guardar" onclick="guardar()" name="button"><i class="fa fa-envelope-o" aria-hidden="true"></i> Guardar y enviar correo</button>
															</div>

														</div>
													</div>
		 									</div>
										</div>
          		</div>
    			</section>
  			</div>
		  </section>
		</div>
	</section>
  <!-- Large modal -->

@stop
@section('scripts')
    {!!Html::script('js/Generics.js')!!}
		{!!Html::script('js/admin/cotizador/inicio.js')!!}
    {!!Html::script('js/admin/cotizador/addservicios.js')!!}
		{!!Html::script('js/admin/cotizador/borrarservicios.js')!!}
		{!!Html::script('js/admin/cotizador/editservicio.js')!!}
		{!!Html::script('js/admin/cotizador/guardarcotizacion.js')!!}
@endsection
