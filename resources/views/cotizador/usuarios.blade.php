@extends('layouts.adm')
	@section('content')


			<div class="inner-wrapper">
				<section role="main" class="content-body pb-0">

          @include('flash::message')
            @if(Session::has('flash_success'))
            <div class="alert alert-success" role="alert">
              {{Session::get('flash_success')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            @include('flash::message')
            @if(Session::has('flash_delete'))
            <div class="alert alert-warning " role="alert">
              {{Session::get('flash_delete')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
					<div class="col-md-12">
								<section class="card card-featured card-featured-primary mb-4">
									<header class="card-header">
										<div class="card-actions">

										</div>
										<h2 class="card-title blanco">Cotizaciones </h2>

									</header>
									<div class="card-body">
										<!-- <code>.card-featured.card-featured-primary</code> -->
										<div class="	">
							<div class="tabs tabs-primary">
								<ul class="nav nav-tabs">
									<li class="nav-item active">
										<a class="nav-link active" href="#altas" data-toggle="tab"><i class="fa fa-star"></i> usuarios</a>
									</li>
									<!-- <li class="nav-item">
										<a class="nav-link" href="#bajas" data-toggle="tab">Bajas</a>
									</li> -->
								</ul>
								<div class="tab-content">
									<div  id="altas" class="tab-pane active">
										<table width="100%" class="table table-responsive-md table-sm  ">
											<thead>
													<tr>
                            <td>No.</td>
                            <td>Folio</td>
                            <td>Nombre</td>
														<td>Fecha de Registro</td>
                            <td>Estatus</td>
														<td>Fecha de Termino</td>
                            <td>Acciones</td>
													</tr>
											</thead>
                      @foreach($cotizaciones as $user)
                      <tr class="odd gradeX">
                        <td>{{ $user->qtfk }}</td>
                        <td>{{ $user->qtfolio }}</td>
												@foreach($users as $usuario)
												@if($usuario->id == $user->qtuserfk)
                        <td>{{ $usuario->name }} {{$usuario->apaternal}} {{$usuario->amaternal}}</td>
												@endif
												@endforeach
												<td>{{$user->qtdate}}</td>
											  @if($user->qtstatus == 1)
												<td><img src="{{ url('admin/img/success.png') }}"alt=""></td>
												@endif
												<td>{{$user->qtdateend}}</td>
                        <td>
                          <a href="{{ route('cotizaciones.show',['user'=> $user,'folio'=>$user->qtfolio])}}" class="btn btn-xs btn-default"> <i class="fa fa-eye"></i> </a>

                        </td>
                      </tr>
                  </tbody>
												@endforeach
										</table>
										<div class=" pull-right">
												{{ $cotizaciones->appends(['status' => 1])}}
										</div>
									</div>

								</div>
							</div>
						</div>
									</div>
								</section>
							</div>



					<!-- end: page -->
				</section>
			</div>
		@stop
