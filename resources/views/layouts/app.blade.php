<!DOCTYPE html>
<html  lang="" class="no-js">
	<head>
			<!-- Mobile Specific Meta -->
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

			<!-- Author Meta -->
			<meta name="author" content="codepixer">
			<!-- Meta Description -->
			<meta name="description" content="">
			<!-- Meta Keyword -->
			<meta name="keywords" content="">
			<!-- meta character set -->
			<meta charset="UTF-8">
			<!-- Site Title -->
			<meta name="csrf-token" content="{{ csrf_token() }}">
			<!-- Web Fonts  -->

			<title>{{ config('app.name', 'ZUCI') }}</title>
  <!-- CSS ============================================= -->
		<!-- Fuentes -->
		<!-- Estilos-->
		<!-- Vendor CSS -->
		<!-- <link rel="stylesheet" href="inicio/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="inicio/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="inicio/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="inicio/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="inicio/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="inicio/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="inicio/vendor/magnific-popup/magnific-popup.min.css"> -->

		<!-- Theme CSS -->
		<!-- <link rel="stylesheet" href="inicio/css/theme.css">
		<link rel="stylesheet" href="inicio/css/theme-elements.css">
		<link rel="stylesheet" href="inicio/css/theme-blog.css">
		<link rel="stylesheet" href="inicio/css/theme-shop.css"> -->

		<!-- Current Page CSS -->
		<!-- <link rel="stylesheet" href="inicio/vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="inicio/vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="inicio/vendor/rs-plugin/css/navigation.css">
		<link rel="stylesheet" href="inicio/vendor/circle-flip-slideshow/css/component.css"> -->

		<!-- Demo CSS -->
		<!-- Skin CSS -->
		<!-- <link rel="stylesheet" href="inicio/css/skins/default.css"> -->

		<!-- Theme Custom CSS -->
		<!-- <link rel="stylesheet" href="inicio/css/custom.css"> -->

		<!-- Head Libs -->

		{!!Html::style('inicio/css/estilos.css')!!}
		<!-- Vendor CSS -->
		{!!Html::style('inicio/vendor/bootstrap/css/bootstrap.min.css')!!}
		{!!Html::style('inicio/vendor/font-awesome/css/font-awesome.min.css')!!}
		{!!Html::style('inicio/vendor/animate/animate.min.css')!!}
		{!!Html::style('inicio/vendor/simple-line-icons/css/simple-line-icons.min.css')!!}
		{!!Html::style('inicio/vendor/owl.carousel/assets/owl.carousel.min.css')!!}
		{!!Html::style('inicio/vendor/owl.carousel/assets/owl.theme.default.min.css')!!}
		{!!Html::style('inicio/vendor/magnific-popup/magnific-popup.min.css')!!}

		<!-- Theme CSS -->
		{!!Html::style('inicio/css/theme.css')!!}
		{!!Html::style('inicio/css/theme-elements.css')!!}
		{!!Html::style('inicio/css/theme-blog.css')!!}
		{!!Html::style('inicio/css/theme-shop.css')!!}

		<!-- Current Page CSS -->
		{!!Html::style('inicio/vendor/rs-plugin/css/settings.css')!!}
		{!!Html::style('inicio/vendor/rs-plugin/css/layers.css')!!}
		{!!Html::style('inicio/vendor/rs-plugin/css/navigation.css')!!}
		{!!Html::style('inicio/vendor/circle-flip-slideshow/css/component.css')!!}
		<!-- Demo CSS -->
		<!-- Skin CSS -->
		{!!Html::style('inicio/css/skins/default.css')!!}
		<!-- Theme Custom CSS -->
		{!!Html::style('inicio/css/custom.css')!!}
		<!-- Head Libs -->
		{!!Html::script('inicio/vendor/modernizr/modernizr.min.js')!!}
			<!-- <script src="inicio/vendor/modernizr/modernizr.min.js"></script> -->


		<link rel="shortcut icon" href="img/favicon.ico">

	</head>
	<body  background="{{ url('inicio/img/cis/fondo_form.png') }}" oncopy="return false" onpaste="return false">
<!-- funcion para bloquear copiar y pegar-->
<header id="header" class="header-narrow header-semi-transparent" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '1'}">

</header>


										@yield('content')

										<!-- End footer Area -->
										<!-- Vendor -->
										{!!Html::script('inicio/vendor/jquery/jquery.min.js')!!}
										{!!Html::script('inicio/vendor/jquery.appear/jquery.appear.min.js')!!}
										{!!Html::script('inicio/vendor/jquery.easing/jquery.easing.min.js')!!}
										{!!Html::script('inicio/vendor/jquery-cookie/jquery-cookie.min.js')!!}
										{!!Html::script('inicio/vendor/popper/umd/popper.min.js')!!}
										{!!Html::script('inicio/vendor/bootstrap/js/bootstrap.min.js')!!}
										{!!Html::script('inicio/vendor/common/common.min.js')!!}
										{!!Html::script('inicio/vendor/jquery.validation/jquery.validation.min.js')!!}
										{!!Html::script('inicio/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js')!!}
										{!!Html::script('inicio/vendor/jquery.gmap/jquery.gmap.min.js')!!}
										{!!Html::script('inicio/vendor/jquery.lazyload/jquery.lazyload.min.js')!!}
										{!!Html::script('inicio/vendor/isotope/jquery.isotope.min.js')!!}
										{!!Html::script('inicio/vendor/owl.carousel/owl.carousel.min.js')!!}
										{!!Html::script('inicio/vendor/magnific-popup/jquery.magnific-popup.min.js')!!}
										{!!Html::script('inicio/vendor/vide/vide.min.js')!!}

										<!-- Theme Base, Components and Settings -->
										{!!Html::script('inicio/js/theme.js')!!}

										<!-- Current Page Vendor and Views -->
										{!!Html::script('inicio/vendor/rs-plugin/js/jquery.themepunch.tools.min.js')!!}
										{!!Html::script('inicio/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')!!}
										{!!Html::script('inicio/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js')!!}
										{!!Html::script('inicio/js/views/view.home.js')!!}

										<!-- Theme Custom -->
										{!!Html::script('inicio/js/custom.js')!!}

										<!-- Theme Initialization Files -->
										{!!Html::script('inicio/js/theme.init.js')!!}
										<!-- Vendor -->
										<!-- <script src="inicio/vendor/jquery/jquery.min.js"></script>
										<script src="inicio/vendor/jquery.appear/jquery.appear.min.js"></script>
										<script src="inicio/vendor/jquery.easing/jquery.easing.min.js"></script>
										<script src="inicio/vendor/jquery-cookie/jquery-cookie.min.js"></script>
										<script src="inicio/vendor/popper/umd/popper.min.js"></script>
										<script src="inicio/vendor/bootstrap/js/bootstrap.min.js"></script>
										<script src="inicio/vendor/common/common.min.js"></script>
										<script src="inicio/vendor/jquery.validation/jquery.validation.min.js"></script>
										<script src="inicio/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
										<script src="inicio/vendor/jquery.gmap/jquery.gmap.min.js"></script>
										<script src="inicio/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
										<script src="inicio/vendor/isotope/jquery.isotope.min.js"></script>
										<script src="inicio/vendor/owl.carousel/owl.carousel.min.js"></script>
										<script src="inicio/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
										<script src="inicio/vendor/vide/vide.min.js"></script> -->

										<!-- Theme Base, Components and Settings -->
										<!-- <script src="inicio/js/theme.js"></script> -->

										<!-- Current Page Vendor and Views -->
										<!-- <script src="inicio/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
										<script src="inicio/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
										<script src="inicio/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js"></script>
										<script src="inicio/js/views/view.home.js"></script> -->

										<!-- Theme Custom -->
										<!-- <script src="inicio/js/custom.js"></script> -->

										<!-- Theme Initialization Files -->
										<!-- <script src="inicio/js/theme.init.js"></script> -->

										@yield('scripts')



	</body>
</html>
