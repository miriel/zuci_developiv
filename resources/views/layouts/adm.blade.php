<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Cis</title>
  <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon.ico">
  	<title>{{ config('app.name', 'CIS') }}</title>
	   <!-- Estilos-->
		{!!Html::style('admin/css/estilos.css')!!}
     <!-- {!!Html::style('vendor/bootstrap/css/bootstrap.css')!!} -->
    <!-- Vendor CSS -->
    <!-- <link rel="stylesheet" href="admin/vendor/bootstrap/css/bootstrap.css" /> -->
    {!!Html::style('admin/vendor/bootstrap/css/bootstrap.css')!!}
    <!-- <link rel="stylesheet" href="admin/vendor/animate/animate.css"> -->
    <!-- {!!Html::style('admin/vendor/animate/animate.css')!!} -->


    <!-- <link rel="stylesheet" href="admin/vendor/font-awesome/css/font-awesome.css" /> -->
    {!!Html::style('admin/vendor/font-awesome/css/font-awesome.css')!!}
    <!-- <link rel="stylesheet" href="admin/vendor/magnific-popup/magnific-popup.css" /> -->
    <!-- {!!Html::style('admin/vendor/magnific-popup/magnific-popup.css')!!} -->
    <!-- <link rel="stylesheet" href="admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" /> -->
    <!-- {!!Html::style('admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css')!!} -->

    <!-- Specific Page Vendor CSS -->
    <!-- <link rel="stylesheet" href="admin/vendor/jquery-ui/jquery-ui.css" /> -->
    {!!Html::style('admin/vendor/jquery-ui/jquery-ui.css')!!}
    <!-- <link rel="stylesheet" href="admin/vendor/jquery-ui/jquery-ui.theme.css" /> -->
    {!!Html::style('admin/vendor/jquery-ui/jquery-ui.theme.css')!!}
    <!-- <link rel="stylesheet" href="admin/vendor/bootstrap-multiselect/bootstrap-multiselect.css" /> -->
    {!!Html::style('admin/vendor/bootstrap-multiselect/bootstrap-multiselect.css')!!}
    <!-- <link rel="stylesheet" href="admin/vendor/morris/morris.css" /> -->
    {!!Html::style('admin/vendor/morris/morris.css')!!}
    <!-- Theme CSS -->
    <!-- <link rel="stylesheet" href="admin/css/theme.css" /> -->
    {!!Html::style('admin/css/theme.css')!!}
    <!-- Skin CSS -->
    <!-- <link rel="stylesheet" href="admin/css/skins/default.css" /> -->
    {!!Html::style('admin/css/skins/default.css')!!}
    <!-- Theme Custom CSS -->
    <!-- <link rel="stylesheet" href="admin/css/custom.css"> -->
    {!!Html::style('admin/css/custom.css')!!}
    <!-- Head Libs -->
    <!-- <script src="admin/vendor/modernizr/modernizr.js"></script> -->
    {!!Html::script('admin/vendor/modernizr/modernizr.js')!!}
    <!-- <link rel="stylesheet" href="vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" /> -->
    <!-- {!!Html::script('admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css')!!} -->
    {!!Html::style('dropzone/dropzone.css')!!}
    {!!Html::style('dropzone/basic.css')!!}
    <!-- {!!Html::style('vendor/pnotify/pnotify.custom.css')!!} -->

    {!! Html::script('dropzone/dropzone.js')!!}
    <!-- {!! Html::script('dropzone/dropzone-amd-module.min.js')!!} -->
    {!!Html::style('admin/easyWizard.css')!!}

    @yield('styles')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</head>

<body>

  <!-- start: header -->
  <header class="header header-nav-menu">
    <div class="logo-container">
      <a href="{{url("home")}}"class="logo">
        <img src="{{ url('admin/img/lgoo.png') }}" width="75" height="35" alt="Cis" />
      </a>
      <button class="btn header-btn-collapse-nav d-lg-none" data-toggle="collapse" data-target=".header-nav">
        <i class="fa fa-bars"></i>
      </button>

      <!-- start: header nav menu -->
      <div class="header-nav collapse">
        <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
          <nav>
            <ul class="nav nav-pills" id="mainNav">
              <li class="">
                  <a class="nav-link" href="{{url("home")}}"> Inicio </a>
              </li>
              @can('miscuestionarios', new App\Questionnaire)
              <li class=" ">
                  <a class="nav-link" href="{{ url("miscuestionarios")}}">Mis cuestionarios </a>
              </li>
              @endcan
              <li class=" ">
                  <a class="nav-link" href="{{ url("cotizaciones")}}">Cotizaciones </a>
              </li>
              @can('view', new App\User)
              <li class="dropdown ">
              <a class="nav-link" href="{{ url("auditFile")}}">Auditoría</a>
              </li>
              @endcan
             <li class="dropdown ">

                  <a class="nav-link dropdown-toggle" href="#">  Configuración </a>
                  <ul class="dropdown-menu">
                    <li class="dropdown-submenu">
                      <a class="nav-link">Usuarios </a>
                        <ul class="dropdown-menu">
                          <li><a class="nav-link" href="{{ url("user")}}">Usuarios</a></li>


                           @can('view',new \Spatie\Permission\Models\Role)
                          <li><a class="nav-link" href="{{ url("roles")}}">Roles</a></li>
                          @endcan
                             @can('view',new \Spatie\Permission\Models\Permission)
                          <li><a class="nav-link" href="{{ url("permissions")}}">Permisos</a></li>
                          @endcan
                        </ul>
                    </li>
                      <li class="dropdown-submenu">
                        @can('catalogo',new App\User)
                        <a class="nav-link">Catalogos </a>
                        @endcan
                          <ul class="dropdown-menu">
                            @can('view',new App\Company_Turn)
                            <li><a class="nav-link" href="{{ url("giro")}}">Giro</a></li>
                            @endcan
                            @can('view',new App\Company_Type)
                            <li> <a class="nav-link" href="{{url("empresa")}}">Empresa </a> </li>
                            @endcan
                            <!-- <li> <a class="nav-link" href="{{route("planes.index")}}">Planes</a></li>
                            <li> <a class="nav-link" href="{{ url("planes/show")}}">Planes actuales</a></li> -->
                            @can('view',new App\Company_Turn)
                            <li> <a class="nav-link" href="{{route("tipoArchivo.index")}}">Tipos de archivos</a> </li>
                            @endcan
                            @can('view',new App\Company_Turn)
                            <li> <a class="nav-link" href="{{route("calificacionArchivo.index")}}">Calificaciones de archivos</a> </li>
                            @endcan
                            @can('view',new App\Country)
                            <li> <a class="nav-link" href="{{route("pais.index")}}">Países</a></li>
                            @endcan
                            @can('view',new App\City)
                            <li> <a class="nav-link" href="{{route("ciudad.index")}}">Ciudades</a></li>
                            @endcan
                              @can('create',new App\CountrycitiesModel)
                            <li> <a class="nav-link" href="{{ route("agrega.create")}}"> Asignar una Ciudad a un País</a></li>
                            @endcan
                              @can('view',new App\catalog_master)
                            <li> <a class="nav-link" href="{{ route("catalogos.index")}}"> Análisis de Riesgo</a></li>
                            @endcan
                              <li> <a class="nav-link" href="{{ route("divisas.index")}}">Divisas</a></li>
                              <li> <a class="nav-link" href="{{ route("admincotizador.index")}}">Prec. Cotizador</a></li>
                          </ul>
                      </li>

                      <li class="dropdown-submenu">
                          @can('evaluaciones',new App\QuestionnaireType)
                        <a class="nav-link">Evaluaciones </a>
                          @endcan
                          <ul class="dropdown-menu">
                            @can('view',new App\QuestionnaireType)
                            <li><a class="nav-link" href="{{ route('tipocuestionario.index',['status' => '1'] )}}">Tipos</a></li>
                            @endcan
                            @can('view',new App\Questionnaire)
                            <li> <a class="nav-link" href="{{ route('cuestionario.index')}}">Cuestionarios </a> </li>
                            @endcan
                            @can('view',new App\Question)
                            <li> <a class="nav-link" href="{{ route('pregunta.index',['status' => '1'] )}}">Preguntas</a></li>
                            @endcan
                            @can('view',new App\Answer)
                            <li> <a class="nav-link" href="{{ route('respuesta.index',['status' => '1'] )}}">Respuestas</a></li>
                            @endcan
                            @can('view',new App\QuestionnaireConfiguration)
                            <li> <a class="nav-link" href="{{ route('configcuestionario.index',['status' => '1'] )}}">Vinculación</a></li>
                            @endcan
                            @can('secciones',new App\Catalogs)
                            <li> <a class="nav-link" href="{{ route('catalogo.index',['status' => '1'] )}}">Secciones</a></li>
                            @endcan
                          </ul>
                      </li>
                      <!-- <li class="dropdown-submenu">
                        <a class="nav-link">Auditoria </a>
                          <ul class="dropdown-menu">
                            <li><a class="nav-link" href="{{ url("auditFile")}}">Archivos</a></li>
                          </ul>
                      </li> -->

                  </ul>
              </li>


            </ul>

          </nav>
        </div>
      </div>
      <!-- end: header nav menu -->
    </div>

    <!-- start: search & user box -->
    <div class="header-right">
      <span class="separator"></span>
      <ul class="notifications">
        <li>
          <!--<a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
            <i class="fa fa-tasks"></i>
            <span class="badge">3</span>
          </a>
        -->
          <div class="dropdown-menu notification-menu large">
            <div class="notification-title">
              <span class="float-right badge badge-default">3</span>
              Tasks
            </div>

            <div class="content">
              <ul>
                <li>
                  <p class="clearfix mb-1">
                    <span class="message float-left">Generating Sales Report</span>
                    <span class="message float-right text-dark">60%</span>
                  </p>
                  <div class="progress progress-xs light">
                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                  </div>
                </li>

                <li>
                  <p class="clearfix mb-1">
                    <span class="message float-left">Importing Contacts</span>
                    <span class="message float-right text-dark">98%</span>
                  </p>
                  <div class="progress progress-xs light">
                    <div class="progress-bar" role="progressbar" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100" style="width: 98%;"></div>
                  </div>
                </li>

                <li>
                  <p class="clearfix mb-1">
                    <span class="message float-left">Uploading something big</span>
                    <span class="message float-right text-dark">33%</span>
                  </p>
                  <div class="progress progress-xs light mb-1">
                    <div class="progress-bar" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100" style="width: 33%;"></div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </li>
        <li>
          <!--<a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
            <i class="fa fa-envelope"></i>
            <span class="badge">4</span>
          </a>-->

          <div class="dropdown-menu notification-menu">
            <div class="notification-title">
              <span class="float-right badge badge-default">230</span>
              Messages
            </div>

            <div class="content">
              <ul>
                <li>
                  <a href="#" class="clearfix">
                    <figure class="image">
                      <img src="admin/img/!sample-user.jpg" alt="Joseph Doe Junior" class="rounded-circle" />
                    </figure>
                    <span class="title">Joseph Doe</span>
                    <span class="message">Lorem ipsum dolor sit.</span>
                  </a>
                </li>
                <li>
                  <a href="#" class="clearfix">
                    <figure class="image">
                      <img src="admin/img/!sample-user.jpg" alt="Joseph Junior" class="rounded-circle" />
                    </figure>
                    <span class="title">Joseph Junior</span>
                    <span class="message truncate">Truncated message. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet lacinia orci. Proin vestibulum eget risus non luctus. Nunc cursus lacinia lacinia. Nulla molestie malesuada est ac tincidunt. Quisque eget convallis diam, nec venenatis risus. Vestibulum blandit faucibus est et malesuada. Sed interdum cursus dui nec venenatis. Pellentesque non nisi lobortis, rutrum eros ut, convallis nisi. Sed tellus turpis, dignissim sit amet tristique quis, pretium id est. Sed aliquam diam diam, sit amet faucibus tellus ultricies eu. Aliquam lacinia nibh a metus bibendum, eu commodo eros commodo. Sed commodo molestie elit, a molestie lacus porttitor id. Donec facilisis varius sapien, ac fringilla velit porttitor et. Nam tincidunt gravida dui, sed pharetra odio pharetra nec. Duis consectetur venenatis pharetra. Vestibulum egestas nisi quis elementum elementum.</span>
                  </a>
                </li>
                <li>
                  <a href="#" class="clearfix">
                    <figure class="image">
                      <img src="admin/img/!sample-user.jpg" alt="Joe Junior" class="rounded-circle" />
                    </figure>
                    <span class="title">Joe Junior</span>
                    <span class="message">Lorem ipsum dolor sit.</span>
                  </a>
                </li>
                <li>
                  <a href="#" class="clearfix">
                    <figure class="image">
                      <img src="admin/img/!sample-user.jpg" alt="Joseph Junior" class="rounded-circle" />
                    </figure>
                    <span class="title">Joseph Junior</span>
                    <span class="message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet lacinia orci. Proin vestibulum eget risus non luctus. Nunc cursus lacinia lacinia. Nulla molestie malesuada est ac tincidunt. Quisque eget convallis diam.</span>
                  </a>
                </li>
              </ul>

              <hr />

              <div class="text-right">
                <a href="#" class="view-more">View All</a>
              </div>
            </div>
          </div>
        </li>
        <li>
          <!--<a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
            <i class="fa fa-bell"></i>
            <span class="badge">3</span>
          </a>-->

          <div class="dropdown-menu notification-menu">
            <div class="notification-title">
              <span class="float-right badge badge-default">3</span>
              Alerts
            </div>

            <div class="content">
              <ul>
                <li>
                  <a href="#" class="clearfix">
                    <div class="image">
                      <i class="fa fa-thumbs-down bg-danger"></i>
                    </div>
                    <span class="title">Server is Down!</span>
                    <span class="message">Just now</span>
                  </a>
                </li>
                <li>
                  <a href="#" class="clearfix">
                    <div class="image">
                      <i class="fa fa-lock bg-warning"></i>
                    </div>
                    <span class="title">User Locked</span>
                    <span class="message">15 minutes ago</span>
                  </a>
                </li>
                <li>
                  <a href="#" class="clearfix">
                    <div class="image">
                      <i class="fa fa-signal bg-success"></i>
                    </div>
                    <span class="title">Connection Restaured</span>
                    <span class="message">10/10/2017</span>
                  </a>
                </li>
              </ul>

              <hr />

              <div class="text-right">
                <a href="#" class="view-more">View All</a>
              </div>
            </div>
          </div>
        </li>
      </ul>

      <span class="separator"></span>

      <div id="userbox" class="userbox">
        <a href="#" data-toggle="dropdown">
          <figure class="profile-picture">
            <img src="{{ url('admin/img/user.png') }}" alt="Joseph Doe" class="rounded-circle" data-lock-picture="{{ url('admin/img/!logged-user.jpg')}}" />
          </figure>
          <div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
            <h5>{{ Auth::user()->name }} {{ Auth::user()->apaternal }} </h5>
            <!-- <span class="role">administrator</span> -->
          </div>

          <i class="fa custom-caret"></i>
        </a>

        <div class="dropdown-menu">
          <ul class="list-unstyled">
            <li class="divider"></li>
            <li>
              <a role="menuitem" tabindex="-1" href="#"><i class="fa fa-user"></i> My Profile</a>
            </li>
            <!-- <li>
              <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a>
            </li> -->
            <li>
              <a href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
              <i class="fa fa-power-off"></i> Logout
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>

            </li>
          </ul>
        </div>
      </div>
    </div>
    <!-- end: search & user box -->
  </header>
  <!-- end: header -->
  <!-- /#wrapper -->
<div class="loader"></div>
        @yield('content')

        			<script type="text/javascript">
        			$(window).load(function() {
        			    $(".loader").fadeOut("slow");
        			});
        			</script>
        <!-- <footer class="sticky-footer">
          <div class="container">
            <div class="text-center">
              <small>Copyright © Your Website 2018</small>
            </div>
          </div>
        </footer> -->


         <!-- Vendor -->
         <!-- <script src="admin/vendor/jquery/jquery.js"></script> -->
         {!! Html::script('admin/vendor/jquery/jquery-2.2.4.min.js')!!}
         {!! Html::script('admin/vendor/jquery/jquery.js')!!}
         <!-- <script src="admin/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script> -->
         {!! Html::script('admin/vendor/jquery-browser-mobile/jquery.browser.mobile.js')!!}
         <!-- <script src="admin/vendor/popper/umd/popper.min.js"></script> -->
         {!! Html::script('admin/vendor/popper/umd/popper.min.js')!!}
         <!-- <script src="admin/vendor/bootstrap/js/bootstrap.js"></script> -->
         {!! Html::script('admin/vendor/bootstrap/js/bootstrap.js')!!}
         <!-- <script src="admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> -->
         {!! Html::script('admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')!!}
         <!-- <script src="admin/vendor/common/common.js"></script> -->
         {!! Html::script('admin/vendor/common/common.js')!!}
         <!-- <script src="admin/vendor/nanoscroller/nanoscroller.js"></script> -->
         {!! Html::script('admin/vendor/nanoscroller/nanoscroller.js')!!}
         <!-- <script src="admin/vendor/magnific-popup/jquery.magnific-popup.js"></script> -->
         {!! Html::script('admin/vendor/magnific-popup/jquery.magnific-popup.js')!!}
         <!-- <script src="admin/vendor/jquery-placeholder/jquery-placeholder.js"></script> -->
         {!! Html::script('admin/vendor/jquery-placeholder/jquery-placeholder.js')!!}

         <!-- Specific Page Vendor -->
         <!-- <script src="admin/vendor/jquery-ui/jquery-ui.js"></script> -->
         {!! Html::script('admin/vendor/jquery-ui/jquery-ui.js')!!}
         <!-- <script src="admin/vendor/jqueryui-touch-punch/jqueryui-touch-punch.js"></script> -->
         {!! Html::script('admin/vendor/jqueryui-touch-punch/jqueryui-touch-punch.js')!!}
         <!-- <script src="admin/vendor/jquery-appear/jquery-appear.js"></script> -->
         {!! Html::script('admin/vendor/jquery-appear/jquery-appear.js')!!}
         <!-- <script src="admin/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script> -->
         {!! Html::script('admin/vendor/bootstrap-multiselect/bootstrap-multiselect.js')!!}
         <!-- <script src="admin/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js"></script> -->
         {!! Html::script('admin/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js')!!}
         <!-- <script src="admin/vendor/flot/jquery.flot.js"></script> -->
         {!! Html::script('admin/vendor/flot/jquery.flot.js')!!}
         <!-- <script src="admin/vendor/flot.tooltip/flot.tooltip.js"></script> -->
         {!! Html::script('admin/vendor/flot.tooltip/flot.tooltip.js')!!}
         <!-- <script src="admin/vendor/flot/jquery.flot.pie.js"></script> -->
         {!! Html::script('admin/vendor/flot/jquery.flot.pie.js')!!}
         <!-- <script src="admin/vendor/flot/jquery.flot.categories.js"></script> -->
         {!! Html::script('admin/vendor/flot/jquery.flot.categories.js')!!}
         <!-- <script src="admin/vendor/flot/jquery.flot.resize.js"></script> -->
         {!! Html::script('admin/vendor/flot/jquery.flot.resize.js')!!}
         <!-- <script src="admin/vendor/jquery-sparkline/jquery-sparkline.js"></script> -->
         {!! Html::script('admin/vendor/jquery-sparkline/jquery-sparkline.js')!!}
         <!-- <script src="admin/vendor/raphael/raphael.js"></script> -->
         {!! Html::script('admin/vendor/raphael/raphael.js')!!}
         <!-- <script src="admin/vendor/morris/morris.js"></script> -->
         {!! Html::script('admin/vendor/morris/morris.js')!!}
         <!-- <script src="admin/vendor/gauge/gauge.js"></script> -->
         {!! Html::script('admin/vendor/gauge/gauge.js')!!}
         <!-- <script src="admin/vendor/snap.svg/snap.svg.js"></script> -->
         {!! Html::script('admin/vendor/snap.svg/snap.svg.js')!!}
         <!-- <script src="admin/vendor/liquid-meter/liquid.meter.js"></script> -->
         {!! Html::script('admin/vendor/liquid-meter/liquid.meter.js')!!}
         <!-- <script src="admin/vendor/jqvmap/jquery.vmap.js"></script> -->
         {!! Html::script('admin/vendor/jqvmap/jquery.vmap.js')!!}
         <!-- <script src="admin/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script> -->
         {!! Html::script('admin/vendor/jqvmap/data/jquery.vmap.sampledata.js')!!}
         <!-- <script src="admin/vendor/jqvmap/maps/jquery.vmap.world.js"></script> -->
         {!! Html::script('admin/vendor/jqvmap/maps/jquery.vmap.world.js')!!}
         <!-- <script src="admin/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script> -->
         {!! Html::script('admin/vendor/jqvmap/maps/continents/jquery.vmap.africa.js')!!}
         <!-- <script src="admin/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script> -->
         {!! Html::script('admin/vendor/jqvmap/maps/continents/jquery.vmap.asia.js')!!}
         <!-- <script src="admin/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script> -->
         {!! Html::script('admin/vendor/jqvmap/maps/continents/jquery.vmap.australia.js')!!}
         <!-- <script src="admin/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script> -->
         {!! Html::script('admin/vendor/jqvmap/maps/continents/jquery.vmap.europe.js')!!}
         <!-- <script src="admin/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script> -->
         {!! Html::script('admin/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js')!!}
         <!-- <script src="admin/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script> -->
         {!! Html::script('admin/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js')!!}

         <!-- Theme Base, Components and Settings -->
         <!-- <script src="admin/js/theme.js"></script> -->
          {!! Html::script('admin/js/theme.js')!!}
         <!-- Theme Custom -->
         <!-- <script src="admin/js/custom.js"></script> -->
         {!! Html::script('admin/js/custom.js')!!}
         <!-- Theme Initialization Files -->
         <!-- <script src="admin/js/theme.init.js"></script> -->
          {!! Html::script('admin/js/theme.init.js')!!}
          {!! Html::script('admin/easyWizard.js')!!}
          <!-- Examples -->
          <!-- <script src="js/examples/examples.wizard.js"></script> -->
          <!-- {!! Html::script('admin/js/examples/examples.wizard.js')!!} -->

          <!-- {!! Html::script('admin/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js')!!} -->


         <!-- {!! Html::script('admin/vendor/js/modernizr.js')!!} -->
         <!-- {!! Html::script('admin/vendor/js/modernizr.min.js')!!} -->


         <!-- Examples -->
         <!-- <script src="admin/js/examples/examples.header.menu.js"></script> -->
         {!! Html::script('admin/js/examples/examples.header.menu.js')!!}
         <!-- <script src="admin/js/examples/examples.dashboard.js"></script> -->
          {!! Html::script('admin/js/examples/examples.dashboard.js')!!}


          <!-- <script src="vendor/select2/js/select2.js"></script> -->
            {!! Html::script('admin/vendor/select2/js/select2.js')!!}

          <!-- <script src="vendor/pnotify/pnotify.custom.js"></script> -->
            <!-- {!! Html::script('admin/vendor/pnotify/pnotify.custom.js')!!} -->



            {!! Html::script('admin/js/examples/examples.modals.js')!!}


            {!! Html::script('admin/js/examples/examples.modals.js')!!}


          <!-- <script>toastr.options.progressBar = true;</script> -->

          @yield('scripts')


</body>

</html>
