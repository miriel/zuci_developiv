<!doctype html>
<html class="fixed"  lang="{{ app()->getLocale() }}">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>{{ config('app.name', 'CIS') }}</title>
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Estilos-->
    {!!Html::style('admin/css/estilos.css')!!}

		<!-- Vendor CSS -->
    {!!Html::style('admin/vendor/bootstrap/css/bootstrap.css')!!}
    {!!Html::style('admin/vendor/animate/animate.css')!!}

    {!!Html::style('admin/vendor/font-awesome/css/font-awesome.css')!!}
    {!!Html::style('admin/vendor/magnific-popup/magnific-popup.css')!!}
    {!!Html::style('admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css')!!}

		<!-- Specific Page Vendor CSS -->
    {!!Html::style('admin/vendor/pnotify/pnotify.custom.css')!!}

		<!-- Theme CSS -->
    {!!Html::style('admin/css/theme.css')!!}

		<!-- Skin CSS -->
    {!!Html::style('admin/css/skins/default.css')!!}

		<!-- Theme Custom CSS -->
    {!!Html::style('admin/css/custom.css')!!}
  @yield('styles')
		<!-- Head Libs -->
    {!!Html::script('admin/vendor/modernizr/modernizr.js')!!}

	</head>
	<body>
		<section class="body">


        <!-- start: header -->
        <header class="header header-nav-menu">
          <div class="logo-container">
            <a href="{{url("home")}}"class="logo">
              <img src="{{ url('admin/img/lgoo.png') }}" width="75" height="35" alt="Cis" />
            </a>
            <button class="btn header-btn-collapse-nav d-lg-none" data-toggle="collapse" data-target=".header-nav">
              <i class="fa fa-bars"></i>
            </button>

            <!-- start: header nav menu -->
            <div class="header-nav collapse">
              <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
                <nav>
                  <ul class="nav nav-pills" id="mainNav">
                    <li class="">
                        <a class="nav-link" href="{{url("home")}}"> Inicio </a>
                    </li>
                    @can('miscuestionarios', new App\Questionnaire)
                    <li class=" ">
                        <a class="nav-link" href="{{ url("miscuestionarios")}}">Mis cuestionarios </a>
                    </li>
                    @endcan
                    <li class=" ">
                        <a class="nav-link" href="{{ url("cotizaciones")}}">Cotizaciones </a>
                    </li>
                    @can('view', new App\User)
                    <li class="dropdown ">
                    <a class="nav-link" href="{{ url("auditFile")}}">Auditoría</a>
                    </li>
                    @endcan
                   <li class="dropdown ">

                        <a class="nav-link dropdown-toggle" href="#">  Configuración </a>
                        <ul class="dropdown-menu">
                          <li class="dropdown-submenu">
                            <a class="nav-link">Usuarios </a>
                              <ul class="dropdown-menu">
                                <li><a class="nav-link" href="{{ url("user")}}">Usuarios</a></li>


                                 @can('view',new \Spatie\Permission\Models\Role)
                                <li><a class="nav-link" href="{{ url("roles")}}">Roles</a></li>
                                @endcan
                                   @can('view',new \Spatie\Permission\Models\Permission)
                                <li><a class="nav-link" href="{{ url("permissions")}}">Permisos</a></li>
                                @endcan
                              </ul>
                          </li>
                            <li class="dropdown-submenu">
                              @can('catalogo',new App\User)
                              <a class="nav-link">Catalogos </a>
                              @endcan
                                <ul class="dropdown-menu">
                                  @can('view',new App\Company_Turn)
                                  <li><a class="nav-link" href="{{ url("giro")}}">Giro</a></li>
                                  @endcan
                                  @can('view',new App\Company_Type)
                                  <li> <a class="nav-link" href="{{url("empresa")}}">Empresa </a> </li>
                                  @endcan
                                  <!-- <li> <a class="nav-link" href="{{route("planes.index")}}">Planes</a></li>
                                  <li> <a class="nav-link" href="{{ url("planes/show")}}">Planes actuales</a></li> -->
                                  @can('view',new App\Company_Turn)
                                  <li> <a class="nav-link" href="{{route("tipoArchivo.index")}}">Tipos de archivos</a> </li>
                                  @endcan
                                  @can('view',new App\Company_Turn)
                                  <li> <a class="nav-link" href="{{route("calificacionArchivo.index")}}">Calificaciones de archivos</a> </li>
                                  @endcan
                                  @can('view',new App\Country)
                                  <li> <a class="nav-link" href="{{route("pais.index")}}">Países</a></li>
                                  @endcan
                                  @can('view',new App\City)
                                  <li> <a class="nav-link" href="{{route("ciudad.index")}}">Ciudades</a></li>
                                  @endcan
                                    @can('create',new App\CountrycitiesModel)
                                  <li> <a class="nav-link" href="{{ route("agrega.create")}}"> Asignar una Ciudad a un País</a></li>
                                  @endcan
                                    @can('view',new App\catalog_master)
                                  <li> <a class="nav-link" href="{{ route("catalogos.index")}}"> Análisis de Riesgo</a></li>
                                  @endcan
                                    <li> <a class="nav-link" href="{{ route("divisas.index")}}">Divisas</a></li>
                                    <li> <a class="nav-link" href="{{ route("admincotizador.index")}}">Prec. Cotizador</a></li>
                                </ul>
                            </li>

                            <li class="dropdown-submenu">
                                @can('evaluaciones',new App\QuestionnaireType)
                              <a class="nav-link">Evaluaciones </a>
                                @endcan
                                <ul class="dropdown-menu">
                                  @can('view',new App\QuestionnaireType)
                                  <li><a class="nav-link" href="{{ route('tipocuestionario.index',['status' => '1'] )}}">Tipos</a></li>
                                  @endcan
                                  @can('view',new App\Questionnaire)
                                  <li> <a class="nav-link" href="{{ route('cuestionario.index')}}">Cuestionarios </a> </li>
                                  @endcan
                                  @can('view',new App\Question)
                                  <li> <a class="nav-link" href="{{ route('pregunta.index',['status' => '1'] )}}">Preguntas</a></li>
                                  @endcan
                                  @can('view',new App\Answer)
                                  <li> <a class="nav-link" href="{{ route('respuesta.index',['status' => '1'] )}}">Respuestas</a></li>
                                  @endcan
                                  @can('view',new App\QuestionnaireConfiguration)
                                  <li> <a class="nav-link" href="{{ route('configcuestionario.index',['status' => '1'] )}}">Vinculación</a></li>
                                  @endcan
                                  @can('secciones',new App\Catalogs)
                                  <li> <a class="nav-link" href="{{ route('catalogo.index',['status' => '1'] )}}">Secciones</a></li>
                                  @endcan
                                </ul>
                            </li>
                            <!-- <li class="dropdown-submenu">
                              <a class="nav-link">Auditoria </a>
                                <ul class="dropdown-menu">
                                  <li><a class="nav-link" href="{{ url("auditFile")}}">Archivos</a></li>
                                </ul>
                            </li> -->

                        </ul>
                    </li>


                  </ul>

                </nav>
              </div>
            </div>
            <!-- end: header nav menu -->
          </div>

          <!-- start: search & user box -->
          <div class="header-right">
            <span class="separator"></span>
            <ul class="notifications">
              <li>
                <!--<a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
                  <i class="fa fa-tasks"></i>
                  <span class="badge">3</span>
                </a>
              -->
                <div class="dropdown-menu notification-menu large">
                  <div class="notification-title">
                    <span class="float-right badge badge-default">3</span>
                    Tasks
                  </div>

                  <div class="content">
                    <ul>
                      <li>
                        <p class="clearfix mb-1">
                          <span class="message float-left">Generating Sales Report</span>
                          <span class="message float-right text-dark">60%</span>
                        </p>
                        <div class="progress progress-xs light">
                          <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                        </div>
                      </li>

                      <li>
                        <p class="clearfix mb-1">
                          <span class="message float-left">Importing Contacts</span>
                          <span class="message float-right text-dark">98%</span>
                        </p>
                        <div class="progress progress-xs light">
                          <div class="progress-bar" role="progressbar" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100" style="width: 98%;"></div>
                        </div>
                      </li>

                      <li>
                        <p class="clearfix mb-1">
                          <span class="message float-left">Uploading something big</span>
                          <span class="message float-right text-dark">33%</span>
                        </p>
                        <div class="progress progress-xs light mb-1">
                          <div class="progress-bar" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100" style="width: 33%;"></div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              <li>
                <!--<a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
                  <i class="fa fa-envelope"></i>
                  <span class="badge">4</span>
                </a>-->

                <div class="dropdown-menu notification-menu">
                  <div class="notification-title">
                    <span class="float-right badge badge-default">230</span>
                    Messages
                  </div>

                  <div class="content">
                    <ul>
                      <li>
                        <a href="#" class="clearfix">
                          <figure class="image">
                            <img src="admin/img/!sample-user.jpg" alt="Joseph Doe Junior" class="rounded-circle" />
                          </figure>
                          <span class="title">Joseph Doe</span>
                          <span class="message">Lorem ipsum dolor sit.</span>
                        </a>
                      </li>
                      <li>
                        <a href="#" class="clearfix">
                          <figure class="image">
                            <img src="admin/img/!sample-user.jpg" alt="Joseph Junior" class="rounded-circle" />
                          </figure>
                          <span class="title">Joseph Junior</span>
                          <span class="message truncate">Truncated message. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet lacinia orci. Proin vestibulum eget risus non luctus. Nunc cursus lacinia lacinia. Nulla molestie malesuada est ac tincidunt. Quisque eget convallis diam, nec venenatis risus. Vestibulum blandit faucibus est et malesuada. Sed interdum cursus dui nec venenatis. Pellentesque non nisi lobortis, rutrum eros ut, convallis nisi. Sed tellus turpis, dignissim sit amet tristique quis, pretium id est. Sed aliquam diam diam, sit amet faucibus tellus ultricies eu. Aliquam lacinia nibh a metus bibendum, eu commodo eros commodo. Sed commodo molestie elit, a molestie lacus porttitor id. Donec facilisis varius sapien, ac fringilla velit porttitor et. Nam tincidunt gravida dui, sed pharetra odio pharetra nec. Duis consectetur venenatis pharetra. Vestibulum egestas nisi quis elementum elementum.</span>
                        </a>
                      </li>
                      <li>
                        <a href="#" class="clearfix">
                          <figure class="image">
                            <img src="admin/img/!sample-user.jpg" alt="Joe Junior" class="rounded-circle" />
                          </figure>
                          <span class="title">Joe Junior</span>
                          <span class="message">Lorem ipsum dolor sit.</span>
                        </a>
                      </li>
                      <li>
                        <a href="#" class="clearfix">
                          <figure class="image">
                            <img src="admin/img/!sample-user.jpg" alt="Joseph Junior" class="rounded-circle" />
                          </figure>
                          <span class="title">Joseph Junior</span>
                          <span class="message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet lacinia orci. Proin vestibulum eget risus non luctus. Nunc cursus lacinia lacinia. Nulla molestie malesuada est ac tincidunt. Quisque eget convallis diam.</span>
                        </a>
                      </li>
                    </ul>

                    <hr />

                    <div class="text-right">
                      <a href="#" class="view-more">View All</a>
                    </div>
                  </div>
                </div>
              </li>
              <li>
                <!--<a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
                  <i class="fa fa-bell"></i>
                  <span class="badge">3</span>
                </a>-->

                <div class="dropdown-menu notification-menu">
                  <div class="notification-title">
                    <span class="float-right badge badge-default">3</span>
                    Alerts
                  </div>

                  <div class="content">
                    <ul>
                      <li>
                        <a href="#" class="clearfix">
                          <div class="image">
                            <i class="fa fa-thumbs-down bg-danger"></i>
                          </div>
                          <span class="title">Server is Down!</span>
                          <span class="message">Just now</span>
                        </a>
                      </li>
                      <li>
                        <a href="#" class="clearfix">
                          <div class="image">
                            <i class="fa fa-lock bg-warning"></i>
                          </div>
                          <span class="title">User Locked</span>
                          <span class="message">15 minutes ago</span>
                        </a>
                      </li>
                      <li>
                        <a href="#" class="clearfix">
                          <div class="image">
                            <i class="fa fa-signal bg-success"></i>
                          </div>
                          <span class="title">Connection Restaured</span>
                          <span class="message">10/10/2017</span>
                        </a>
                      </li>
                    </ul>

                    <hr />

                    <div class="text-right">
                      <a href="#" class="view-more">View All</a>
                    </div>
                  </div>
                </div>
              </li>
            </ul>

            <span class="separator"></span>

            <div id="userbox" class="userbox">
              <a href="#" data-toggle="dropdown">
                <figure class="profile-picture">
                  <img src="{{ url('admin/img/user.png') }}" alt="Joseph Doe" class="rounded-circle" data-lock-picture="{{ url('admin/img/!logged-user.jpg')}}" />
                </figure>
                <div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
                  <h5>{{ Auth::user()->name }} {{ Auth::user()->apaternal }} </h5>
                  <!-- <span class="role">administrator</span> -->
                </div>

                <i class="fa custom-caret"></i>
              </a>

              <div class="dropdown-menu">
                <ul class="list-unstyled">
                  <li class="divider"></li>
                  <li>
                    <a role="menuitem" tabindex="-1" href="#"><i class="fa fa-user"></i> My Profile</a>
                  </li>
                  <!-- <li>
                    <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a>
                  </li> -->
                  <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    <i class="fa fa-power-off"></i> Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>

                  </li>
                </ul>
              </div>
            </div>
          </div>
          <!-- end: search & user box -->
        </header>
        <!-- end: header -->

			  @yield('content')


		</section>

		<!-- Vendor -->
     {!! Html::script('admin/vendor/jquery/jquery.js')!!}
     {!! Html::script('admin/vendor/jquery-browser-mobile/jquery.browser.mobile.js')!!}
     {!! Html::script('admin/vendor/popper/umd/popper.min.js')!!}
     {!! Html::script('admin/vendor/bootstrap/js/bootstrap.js')!!}
     {!! Html::script('admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')!!}
     {!! Html::script('admin/vendor/common/common.js')!!}
     {!! Html::script('admin/vendor/nanoscroller/nanoscroller.js')!!}
     {!! Html::script('admin/vendor/magnific-popup/jquery.magnific-popup.js')!!}
     {!! Html::script('admin/vendor/jquery-placeholder/jquery-placeholder.js')!!}

		<!-- Specific Page Vendor -->
     {!! Html::script('admin/vendor/jquery-validation/jquery.validate.js')!!}
     {!! Html::script('admin/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js')!!}
     {!! Html::script('admin/vendor/pnotify/pnotify.custom.js')!!}

		<!-- Theme Base, Components and Settings -->
     {!! Html::script('admin/js/theme.js')!!}

		<!-- Theme Custom -->
     {!! Html::script('admin/js/custom.js')!!}

		<!-- Theme Initialization Files -->
     {!! Html::script('admin/js/theme.init.js')!!}

		<!-- Examples -->
     {!! Html::script('admin/js/examples/examples.wizard.js')!!}

		 <!-- Specific Page Vendor -->
		{!! Html::script('admin/vendor/jquery-appear/jquery-appear.js')!!}

       @yield('scripts')
	</body>
</html>
