<!DOCTYPE html>
<html  lang="" class="no-js">
	<head>
			<!-- Mobile Specific Meta -->
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

			<!-- Author Meta -->
			<meta name="author" content="codepixer">
			<!-- Meta Description -->
			<meta name="description" content="">
			<!-- Meta Keyword -->
			<meta name="keywords" content="">
			<!-- meta character set -->
			<meta charset="UTF-8">
			<!-- Site Title -->
			<meta name="csrf-token" content="{{ csrf_token() }}">
			<!-- Web Fonts  -->

			<title>{{ config('app.name', 'ZUCI') }}</title>
  <!-- CSS ============================================= -->
		<!-- Fuentes -->
		<!-- Estilos-->
		<!-- Vendor CSS -->
		<!-- <link rel="stylesheet" href="inicio/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="inicio/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="inicio/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="inicio/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="inicio/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="inicio/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="inicio/vendor/magnific-popup/magnific-popup.min.css"> -->

		<!-- Theme CSS -->
		<!-- <link rel="stylesheet" href="inicio/css/theme.css">
		<link rel="stylesheet" href="inicio/css/theme-elements.css">
		<link rel="stylesheet" href="inicio/css/theme-blog.css">
		<link rel="stylesheet" href="inicio/css/theme-shop.css"> -->

		<!-- Current Page CSS -->
		<!-- <link rel="stylesheet" href="inicio/vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="inicio/vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="inicio/vendor/rs-plugin/css/navigation.css">
		<link rel="stylesheet" href="inicio/vendor/circle-flip-slideshow/css/component.css"> -->

		<!-- Demo CSS -->
		<!-- Skin CSS -->
		<!-- <link rel="stylesheet" href="inicio/css/skins/default.css"> -->

		<!-- Theme Custom CSS -->
		<!-- <link rel="stylesheet" href="inicio/css/custom.css"> -->

		<!-- Head Libs -->

		{!!Html::style('inicio/css/estilos.css')!!}
		<!-- Vendor CSS -->
		{!!Html::style('inicio/vendor/bootstrap/css/bootstrap.min.css')!!}
		{!!Html::style('inicio/vendor/font-awesome/css/font-awesome.min.css')!!}
		{!!Html::style('inicio/vendor/animate/animate.min.css')!!}
		{!!Html::style('inicio/vendor/simple-line-icons/css/simple-line-icons.min.css')!!}
		{!!Html::style('inicio/vendor/owl.carousel/assets/owl.carousel.min.css')!!}
		{!!Html::style('inicio/vendor/owl.carousel/assets/owl.theme.default.min.css')!!}
		{!!Html::style('inicio/vendor/magnific-popup/magnific-popup.min.css')!!}

		<!-- Theme CSS -->
		{!!Html::style('inicio/css/theme.css')!!}
		{!!Html::style('inicio/css/theme-elements.css')!!}
		{!!Html::style('inicio/css/theme-blog.css')!!}
		{!!Html::style('inicio/css/theme-shop.css')!!}

		<!-- Current Page CSS -->
		{!!Html::style('inicio/vendor/rs-plugin/css/settings.css')!!}
		{!!Html::style('inicio/vendor/rs-plugin/css/layers.css')!!}
		{!!Html::style('inicio/vendor/rs-plugin/css/navigation.css')!!}
		{!!Html::style('inicio/vendor/circle-flip-slideshow/css/component.css')!!}
		<!-- Demo CSS -->
		<!-- Skin CSS -->
		{!!Html::style('inicio/css/skins/default.css')!!}
		<!-- Theme Custom CSS -->
		{!!Html::style('inicio/css/custom.css')!!}
		<!-- Head Libs -->
		{!!Html::script('inicio/vendor/modernizr/modernizr.min.js')!!}
			<!-- <script src="inicio/vendor/modernizr/modernizr.min.js"></script> -->


		<link rel="shortcut icon" href="img/favicon.ico">

	</head>
	<body  oncopy="return false" onpaste="return false">
<!-- funcion para bloquear copiar y pegar-->
<header id="header" class="header-narrow header-semi-transparent" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '1'}">
	<div class="header-body">
		<div class="header-container container">
			<div class="header-row">
				<div class="header-column">
					<div class="header-row">
						<div class="header-logo">
							<a href="#">
								<!-- <img alt="Porto" width="82" height="40" src="img/lgoo.png"> -->
							</a>
						</div>
					</div>
				</div>
				<div class="header-column justify-content-end">
					<div class="header-row">
						<div class="header-nav header-nav-stripe">
							<div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
								<nav class="collapse">
									<ul class="nav nav-pills" id="mainNav">
										<li class="dropdown">
											<a class="dropdown-item " href="{{url("/")}}">
												Home
											</a>
										</li>

										<li class="dropdown">
											<a class="dropdown-item dropdown-toggle" href="#">
												Idiomas
											</a>
											<ul class="dropdown-menu">
												<li><a class="dropdown-item" href="{{ url('lang', ['en']) }}"> <img class=" img-fluid img-responsive justify-content-center"  src="img/us.png"alt="" > Ingles</a></li>
												<li><a class="dropdown-item"  href="{{ url('lang', ['es']) }}"><img class=" img-fluid img-responsive justify-content-center"  src="img/es.png"alt="" > Español</a></li>
												<!-- <li><a class="" href="{{ url('lang', ['fr']) }}" ><img class=" img-fluid img-responsive justify-content-center"  src="img/fr.png"alt="" > Franc&eacutes </a></li>
							 					<li><a class=""  href="{{ url('lang', ['pt']) }}"><img class=" img-fluid img-responsive justify-content-center"  src="img/pt.png"alt="" > Portugu&eacutes </a></li> -->

											</ul>
										</li>
									</ul>
								</nav>
							</div>
							<!-- <ul class="header-social-icons social-icons d-none d-sm-block">
								<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
								<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
							</ul> -->
							<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
								<i class="fa fa-bars"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>


										@yield('content')
										<footer id="footer" >
											<div class="container">
												<div class="row">
													<!-- <div class="footer-ribbon">
														<span>Get in Touch</span>
													</div> -->
													<div class="col-lg-3">
														<div class="newsletter">
															<a href="#"><img class="img-fluid img-responsive justify-content-center  tamaño" src="img/logo_general.png" href="#" alt=""></a>
															<!-- <h4>Newsletter</h4>
															<p>Keep up on our always evolving product features and technology. Enter your e-mail and subscribe to our newsletter.</p>

															<div class="alert alert-success d-none" id="newsletterSuccess">
																<strong>Success!</strong> You've been added to our email list.
															</div>

															<div class="alert alert-danger d-none" id="newsletterError"></div>

															<form id="newsletterForm" action="php/newsletter-subscribe.php" method="POST">
																<div class="input-group">
																	<input class="form-control form-control-sm" placeholder="Email Address" name="newsletterEmail" id="newsletterEmail" type="text">
																	<span class="input-group-btn">
																		<button class="btn btn-light" type="submit">Go!</button>
																	</span>
																</div>
															</form> -->
														</div>
													</div>
													<div class="col-lg-3">
														<h4 style="color:#fff;">Latest Tweets</h4>
														<div id="tweet" class="twitter" data-plugin-tweets data-plugin-options="{'username': '', 'count': 2}">
															<p style="color:#fff;">Please wait...</p>
														</div>
													</div>
													<div class="col-lg-4">
														<div class="contact-details">
															<h4>Contact Us</h4>
															<ul class="contact">
																<li><p style="color:#fff;"><i class="fa fa-map-marker"></i> <strong>Address:</strong> 1234 Street Name, City Name, United States</p></li>
																<li><p style="color:#fff;"><i class="fa fa-phone"></i> <strong>Phone:</strong> (123) 456-789</p></li>
																<li><p style="color:#fff;"><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:mail@example.com">mail@example.com</a></p></li>
															</ul>
														</div>
													</div>
													<div class="col-lg-2">
														<h4>Follow Us</h4>
														<ul class="social-icons">
															<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
															<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
															<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="footer-copyright">
												<div class="container">
													<div class="row">
														<div class="col-lg-1">
															<a href="index.html" class="logo">
																<!-- <img alt="Porto Website Template" class="img-fluid" src="img/logo-footer.png"> -->
															</a>
														</div>
														<div class="col-lg-7">
															<p style="color:#fff;">© Copyright 2018. All Rights Reserved.</p>
														</div>
														<div class="col-lg-4">
															<nav id="sub-menu">
																<ul>
																	<li><a href="page-faq.html">FAQ's</a></li>
																	<li><a href="sitemap.html">Sitemap</a></li>
																	<li><a href="contact-us.html">Contact</a></li>
																</ul>
															</nav>
														</div>
													</div>
												</div>
											</div>
										</footer>
										<!-- End footer Area -->
										<!-- Vendor -->
										{!!Html::script('inicio/vendor/jquery/jquery.min.js')!!}
										{!!Html::script('inicio/vendor/jquery.appear/jquery.appear.min.js')!!}
										{!!Html::script('inicio/vendor/jquery.easing/jquery.easing.min.js')!!}
										{!!Html::script('inicio/vendor/jquery-cookie/jquery-cookie.min.js')!!}
										{!!Html::script('inicio/vendor/popper/umd/popper.min.js')!!}
										{!!Html::script('inicio/vendor/bootstrap/js/bootstrap.min.js')!!}
										{!!Html::script('inicio/vendor/common/common.min.js')!!}
										{!!Html::script('inicio/vendor/jquery.validation/jquery.validation.min.js')!!}
										{!!Html::script('inicio/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js')!!}
										{!!Html::script('inicio/vendor/jquery.gmap/jquery.gmap.min.js')!!}
										{!!Html::script('inicio/vendor/jquery.lazyload/jquery.lazyload.min.js')!!}
										{!!Html::script('inicio/vendor/isotope/jquery.isotope.min.js')!!}
										{!!Html::script('inicio/vendor/owl.carousel/owl.carousel.min.js')!!}
										{!!Html::script('inicio/vendor/magnific-popup/jquery.magnific-popup.min.js')!!}
										{!!Html::script('inicio/vendor/vide/vide.min.js')!!}

										<!-- Theme Base, Components and Settings -->
										{!!Html::script('inicio/js/theme.js')!!}

										<!-- Current Page Vendor and Views -->
										{!!Html::script('inicio/vendor/rs-plugin/js/jquery.themepunch.tools.min.js')!!}
										{!!Html::script('inicio/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')!!}
										{!!Html::script('inicio/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js')!!}
										{!!Html::script('inicio/js/views/view.home.js')!!}

										<!-- Theme Custom -->
										{!!Html::script('inicio/js/custom.js')!!}

										<!-- Theme Initialization Files -->
										{!!Html::script('inicio/js/theme.init.js')!!}
										<!-- Vendor -->
										<!-- <script src="inicio/vendor/jquery/jquery.min.js"></script>
										<script src="inicio/vendor/jquery.appear/jquery.appear.min.js"></script>
										<script src="inicio/vendor/jquery.easing/jquery.easing.min.js"></script>
										<script src="inicio/vendor/jquery-cookie/jquery-cookie.min.js"></script>
										<script src="inicio/vendor/popper/umd/popper.min.js"></script>
										<script src="inicio/vendor/bootstrap/js/bootstrap.min.js"></script>
										<script src="inicio/vendor/common/common.min.js"></script>
										<script src="inicio/vendor/jquery.validation/jquery.validation.min.js"></script>
										<script src="inicio/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
										<script src="inicio/vendor/jquery.gmap/jquery.gmap.min.js"></script>
										<script src="inicio/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
										<script src="inicio/vendor/isotope/jquery.isotope.min.js"></script>
										<script src="inicio/vendor/owl.carousel/owl.carousel.min.js"></script>
										<script src="inicio/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
										<script src="inicio/vendor/vide/vide.min.js"></script> -->

										<!-- Theme Base, Components and Settings -->
										<!-- <script src="inicio/js/theme.js"></script> -->

										<!-- Current Page Vendor and Views -->
										<!-- <script src="inicio/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
										<script src="inicio/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
										<script src="inicio/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js"></script>
										<script src="inicio/js/views/view.home.js"></script> -->

										<!-- Theme Custom -->
										<!-- <script src="inicio/js/custom.js"></script> -->

										<!-- Theme Initialization Files -->
										<!-- <script src="inicio/js/theme.init.js"></script> -->

										@yield('scripts')



	</body>
</html>
