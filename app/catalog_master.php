<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class catalog_master extends Model
{

  public $timestamps = false;
  protected $primaryKey="cmfk";
  protected $table="catalog_master";
  protected $fillable = ['cmtpcat','cmdesc','cmabbr','cmstatus','cmtpquest','cmval','cmtpquest','cmuserfk','cmmin','cmmax','cmmoneda'];

  public function owenr()
  {
    return $this->belongsTo(User::class, 'cmuserfk');
  }
  public function scopeAllowed($query)
  {
    // if( auth()->user()->hasRole('Admin'))
    if( auth()->user()->can('view',$this))
    {
     return $query;
    }else
    {
      return  $query->where('cmuserfk', auth()->id());
    }
  }


}
