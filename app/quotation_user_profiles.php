<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class quotation_user_profiles extends Model
{
  protected $table = 'quotation_user_profiles';
  protected $primaryKey = 'qupfk';
  protected $fillable = ['qupid','qupqtfk','qupdate','qupfolio','qupquantity','quptotal','qupstatus','qupuser','qupunitprice','qupconversion'];
  public $timestamps = false;
  // const CREATED_AT = 'cotinsertdt';
  // const UPDATED_AT = 'cotupddt';
}
