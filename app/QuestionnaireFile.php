<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionnaireFile extends Model
{
    const CREATED_AT = "qfinsertdt";
    const UPDATED_AT = "qfupddt";
    protected $primaryKey = "qffk";
    protected $table = "questionnaire_files";
    protected $fillable = ['qfqafk','qfnamefile','qfroute','qfnopags','qfsize','qfstatus','qfip','qfuserfk','qfinsertdt','qfupddt','qftypfil'];    

}
