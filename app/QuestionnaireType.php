<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionnaireType extends Model
{
    const CREATED_AT = 'qtinsertdt';
    const UPDATED_AT = 'qtupddt';
    protected $primaryKey="qtfk";
    protected $table = "questionnaire_types";
    protected $fillable=['qtname','qtstatus','qtip','qtuserfk','qtinsertdt','qtupddt'];


    public function owenr()
    {
      return $this->belongsTo(User::class, 'qtuserfk');
    }


    public function scopeAllowed($query)
    {
      // if( auth()->user()->hasRole('Admin'))
      if( auth()->user()->can('view',$this))
      {
       return $query;
      }else
      {
        return  $query->where('qtuserfk', auth()->id());
      }

    }

}
