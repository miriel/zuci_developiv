<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packages extends Model
{
  protected $table = "packages";
  protected $primaryKey = 'pgfk';
  protected $fillable = ['pgname','pgdesc','pgprice','pgperiod','pghowprice','pgquantity','pgstatus','pgip','pguserpk','pginsertdt','pgupddt'];
  const CREATED_AT = 'pginsertdt';
  const UPDATED_AT = 'pgupddt';

  }
