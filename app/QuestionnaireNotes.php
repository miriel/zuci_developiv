<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionnaireNotes extends Model
{
    protected $table = "questionnarie_notes";
    protected $primaryKey = 'qnfk';
    protected $fillable = ['qndesc','qnidprofile','qnstatus','qnip','qnuserfk','qninsertdt','qnupddt'];
    const CREATED_AT = 'qninsertdt';
    const UPDATED_AT = 'qnupddt';
}
