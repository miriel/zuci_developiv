<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanyAnswer;
use DB;
use Carbon\Carbon;
use App\AnswerFile;
use Auth;
use Chumper\Zipper\Zipper; // Libreria para poder comprimir archivos
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use App\catalog_master;

class FilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $resquest)
    {

      $folio = $resquest->folio;
      $carbon = new Carbon();
      $date = Carbon::now();
      $fechaActual = $date->format('d-m-Y');

      $name = $resquest->name;  // Nombre del cuestionario
      $idCompanyAnsw = $resquest->id; // Id de la tabla company answers (respuestas por compañia)

      // no contiene subsecciones
<<<<<<< HEAD
      if($resquest->secciones == '0'){
=======
      if(!empty($resquest->secciones)){
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
        $detalle = DB::table('company_answers')
          ->select('questions.qsfk AS idPreg',
                   'questions.qsname AS namePreg',
                   'answers.anfk AS idResp',
                   'answers.anname AS nameResp'
                   )
          ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
          ->join('questions','questionnaire_answers.qaqsfk','=','questions.qsfk')
          ->join('answers','questionnaire_answers.qaanfk','answers.anfk')
          ->where('company_answers.cafk','=',$idCompanyAnsw)
          ->get();

          $answerFiles = DB::table('answer_files')
          ->select('affk as idFile',
                   'afnamefile',
                   'aftypfil',
                   'afstatus as status',
                   'afnopags',
                   'afsize',
                   'afcomtry as coment',
                   'afcomtryusr as coment_user',
                   'afqualtion as calif'
                   )
          ->where('afqafk','=',$idCompanyAnsw)
          ->where('affoliofk','=',$folio)
          //->where('answer_files.afstatus',$fileFils)
          ->orderBy('answer_files.affk','DESC')
          ->get();

          $filesTemplates = DB::table('company_answers')
            ->select('questionnaire_answers.qafk','questionnaire_files.qfnamefile AS nameTemplate','questionnaire_files.qfnopags AS noPagns','questionnaire_files.qfroute AS ruta')
            ->join('questionnaire_answers','company_answers.caqafk','questionnaire_answers.qafk')
            ->join('questionnaire_files','questionnaire_answers.qafk','questionnaire_files.qfqafk')
            ->where('company_answers.cafk','=',$idCompanyAnsw)
            ->get();

      }else{
        $detalle = DB::table('company_answers')
          ->select('questions.qsfk AS idPreg',
                   'questions.qsname AS namePreg',
                   'answers.anfk AS idResp',
                   'answers.anname AS nameResp',
                   'catalogs.catfk AS idSeccion',
                   'catalogs.catvalue AS nameSect',
                   'catSub.catfk AS idSubsecc',
                   'catSub.catvalue AS nameSubSect'
                   )
          ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
          ->join('questions','questionnaire_answers.qaqsfk','=','questions.qsfk')
          ->join('answers','questionnaire_answers.qaanfk','answers.anfk')
          ->join('catalogs','questionnaire_answers.qascafk','=','catalogs.catfk')
          ->join('catalogs as catSub','questionnaire_answers.qasbcatfk','=','catSub.catfk')
          ->where('company_answers.cafk','=',$idCompanyAnsw)
          ->get();

          $answerFiles = DB::table('answer_files')
          ->select('affk as idFile',
                   'answer_files.afnamefile',
                   'answer_files.aftypfil',
                   'answer_files.afstatus as status',
                   'catalogs.catvalue AS tipoFile',
                   'answer_files.afnopags',
                   'answer_files.afsize',
                   'answer_files.afcomtry as coment',
                   'answer_files.afcomtryusr as coment_user',
                   'answer_files.afqualtion as calif'
                   )
          ->join('catalogs','answer_files.aftypfil','=','catalogs.catfk')
          ->where('afqafk','=',$idCompanyAnsw)
          ->where('affoliofk','=',$folio)
          //->where('answer_files.afstatus',$fileFils)
          ->orderBy('answer_files.affk','DESC')
          ->get();

          $filesTemplates = DB::table('company_answers')
            ->select('questionnaire_answers.qafk','questionnaire_files.qfnamefile AS nameTemplate','questionnaire_files.qfnopags AS noPagns','questionnaire_files.qfroute AS ruta')
            ->join('questionnaire_answers','company_answers.caqafk','questionnaire_answers.qafk')
            ->join('questionnaire_files','questionnaire_answers.qafk','questionnaire_files.qfqafk')
            ->where('company_answers.cafk','=',$idCompanyAnsw)
            ->get();

      }

      $tipoDocumentos = DB::table('catalogs')
        ->where('catnamtabl','=','document_type')
        ->where('cattype','=','file')
        ->pluck('catvalue','catfk');

      // Obtiene el catalogo de status de registros
      $catStatus = DB::table('catalog_master')
        ->select('catalog_master.cmdesc as nameStatus','catalog_master.cmval as valStatus')
        ->where('catalog_master.cmtpcat','=',58)
        ->where('catalog_master.cmstatus','=','1')
        ->get();

      $calificacion = catalog_master::where('cmtpcat','=','59')->where('cmstatus','=','1')->pluck('cmdesc','cmfk');

      return view('miscuestionarios/archivos/index',compact('name','detalle','fechaActual','tipoDocumentos','idCompanyAnsw','answerFiles','idCompanyAnsw','folio','filesTemplates',
                  'catStatus','calificacion') );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request )
    {
      $carbon = new Carbon();
      $date = Carbon::now();
      $fechaActual = $date->format('dmYHis');

      //Valida que existan archivos seleccionados
      if($request->fileCheck){

          $fileSep = explode(",",$request->fileCheck);
          for($i=0; $i<count($fileSep); $i++){
            // Se acumula el resultado de las opciones seleccionadas en un Arreglo
            $arrarFilsD[] = $fileSep[$i];
          }

          // Se ejecuta query que obtiene las opciones seleccionadas mediante el arreglo acumulado
          $nameFile=AnswerFile::select('afnamefile')->whereIn('affk', $arrarFilsD)->get();

          foreach($nameFile as $filesAddZip){
              // Agrega los archivos seleccionador al archivo temporal
              $files = glob(public_path('archivos/cuestionarios/'.$filesAddZip->afnamefile));
              // Crear el archivo zip temporal
              \Zipper::make('archivos/tmp/filestmp_'.$fechaActual.'.zip')->add($files)->close();
          }

          // Decarga el archivo temporal en la PC del usuario
          return response()->download(public_path('/archivos/tmp/filestmp_'.$fechaActual.'.zip'));

      }else{
        // Si no existen archivos seleccionados regresa a la pantalla inicial
        return back();
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          //$folio = Auth::user()->folio;
          $folio = $request->folioAudita;
          $nameEmpresa = Auth::user()->wkrkarea;
          $carbon = new Carbon();
          $date = Carbon::now();
          $fechaActual = $date->format('Ymd');
          $fechaActual2 = $date->format('Y-m-d H:i:s');


          $rutaLocal='/archivos/cuestionarios/';
          $path = public_path().$rutaLocal;
          $files = $request->file('file');
          $idSeccion = $request->idSeccion;
          $idSubSeccion = $request->idSubSeccion;
          $tipoFile = $request->tipoFile;
          $idPreg = $request->idPregunta;
          $idResp = $request->idRespuesta;
          $tamanoArchivoByte = filesize($files); // Obtiene el tamaño del archivo

          $siglaFile = DB::table('catalogs')
              ->select('catacronym')
              ->where('catfk','=',$tipoFile)
              ->get();

          // Foreach para multimples archivos se modifica la variable por file
          //foreach($files as $file){ $file;
                $fileName = $files->getClientOriginalName(); // Obtiene el nombre original del archivo
                $extension = $files->getClientOriginalExtension(); // Obtiene la extencion del archivo
                $request->idCompanyAnsw;
                $fileCreate = AnswerFile::create([
                    'afqafk' => $request->idCompanyAnsw,
                    'aftypfil' => $tipoFile,
                    'afip' => $request->ip(),
                    'afuserfk' => $request->user()->id,
                    'afinsertdt' => $fechaActual2,
                    'afupddt' => $fechaActual2,
                    'affoliofk' => $folio,
                    'afroute' => $rutaLocal,
                    'afsize' => $tamanoArchivoByte,
                    'afnopags' => $request->noPaginas,
                    'afstatus' => 2,
                ]);

                // ID del archivo
                $idFile = $fileCreate->affk;
                $nameFile = $siglaFile[0]->catacronym."_".$nameEmpresa."_".$idSeccion."_".$idSubSeccion."_".$idPreg."_".$idResp."_".$fechaActual."_".$idFile.".".$extension;

                $updtFile = AnswerFile::find($idFile);
                $updtFile->afnamefile = $nameFile;
                $updtFile->save();

                if($files->move($path, $nameFile)){
                  $updtFile->save();
                }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       return "show";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $partyQuery="";
      $Files = AnswerFile::find($id);

      // Guarda comentario
      if($request->tipoUpdate == 'comentario'){
        $Files->afcomtry = $request->comentario;
      }

      if($request->tipoUpdate == 'comentarioUser'){
        $Files->afcomtryusr = $request->comentarioUser;
      }

      // Guarda calificacion
      if($request->tipoUpdate == 'calificacion'){
        $Files->afqualtion = $request->calif;
      }

      $partyQuery;
      $Files->save();

      return response()->json([
        "mensaje" => "Mensaje creado"
      ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id, Request $request)
    {
      if($request->deleteAllFiles == 1){
        // Rechazo en caso de que sea el auditor
        if( $request->status == 3 || $request->status == 5 ){
            $statsUpd = 4;
        // Rechazo en caso de que que sea el usuario
        }else{
            $statsUpd = 8;
        }

        if($request->datos > 0){
          foreach($request->datos as $idFilesAll){
            $fileUpdate = AnswerFile::find($idFilesAll);
            $fileUpdate->afstatus = 8;
            $fileUpdate->save();
          }
        }

      }else{
          // Rechazo en caso de que sea el auditor
          if( $request->status == 3 || $request->status == 5 ){

            $fileUpdateCal = AnswerFile::find($id);
            $fileUpdateCal->afcomtry = $request->comentario;
            $fileUpdateCal->afqualtion = $request->calificacion;
            $fileUpdateCal->save();

            $valDel = 4;

          }else{
              // Cancela en caso de que sea el usuario
              if( $request->status == 2){
                $valDel = 8;
              }
          }

          $fileUpdate = AnswerFile::find($id);
          $fileUpdate->afstatus = $valDel;
          $fileUpdate->save();

      }

      return response()->json([
        "mensaje" => "creado"
      ]);

    }


    /*
      Descarga archivos pasandole el parametro como nombre del mismo archivo
    */

    protected function downloadFile($src){
<<<<<<< HEAD

=======
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
        if(is_file($src)){
          $finfo = finfo_open(FILEINFO_MIME_TYPE);
          $content_type = finfo_file($finfo, $src);
          finfo_close($finfo);
          $file_name=basename($src).PHP_EOL;
          $size = filesize($src);
          header("Content-Type: $content_type");
          header("Content-Disposition: attachment; filename=$file_name");
          header("Content-Transfer-Encoding: binary");
          header("Content-Length: $size");
          readfile($src);
          return true;
        }else{
          return false;
        }
    }

    protected function download($file){
      if(!$this->downloadFile(public_path()."/archivos/cuestionarios/".$file)){
        return redirect()->back();
      }
    }

    // Envia al siguiente nivel de revision
    public function signiv1(Request $request, $id){
      $valSigNiv="";
      // En caso de que el usuario envie el archivo al auditor
      if($request->status == 2){
          $valSigNiv = 3;
      }else
      // En caso de que el auditor envie al siguiente nivel
      if( $request->status == 3 ||  $request->status == 5 ){
          $valSigNiv = 6;
      }else
      // En caso de que el auditor halla rechazado el arhivo y se regresa al usuario
      // Y el usuario reenvia el archivo para su revision del auditor
      if( $request->status == 4) {
        $valSigNiv = 5;
      }

      $sigNivelFile = AnswerFile::find($id);
      $sigNivelFile->afstatus = $valSigNiv;
      $sigNivelFile->save();

      return response()->json([
        "mensaje" => "creado"
      ]);
    }

}
