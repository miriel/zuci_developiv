<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;
use App\catalog_master;
use DB;
use App\quotation_user_profiles;
use App\quotations;
use Response;
use App\quotation_primary_questionnaires;
use App\quotation_secondary_questionnaires;
use App\Company;
use Illuminate\Support\Facades\Mail;
use Session;
use Barryvdh\DomPDF\Facade as PDF;

class CotizadorUser extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $cotizaciones = quotations::allowed()->orderBy('qtfk','DEC')->where('qtstatus','=','1')->paginate(20,['*'],'cotizador');
      $users = User::orderBy('id','ASC')->where('status','=','1')->get();
      return view('cotizador.usuarios',compact('users','cotizaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user,Request $request)
    {
      $folio = $request->folio;
      $id = $user;

          $roles = quotations::select('perfiles.qupid','roles.name','perfiles.qupquantity','perfiles.qupunitprice','perfiles.qupconversion','perfiles.quptotal')
        ->join('quotation_user_profiles AS perfiles','perfiles.qupqtfk','=','quotations.qtfk')
        ->join('roles','roles.id','=','perfiles.qupid')
        ->where('quotations.qtfolio',$folio)
        ->where('quotations.qtfk',$id)
         ->get();

         $cuestionarios = quotations::select('questionnaires.qufk','questionnaires.quname','cuestionarios.qpqquantity','cuestionarios.qpqunitprice','cuestionarios.qpqconversion','cuestionarios.qpqtotal')
         ->join('quotation_primary_questionnaires as cuestionarios','cuestionarios.qpqqtfk','=','quotations.qtfk')
         ->join('questionnaires','questionnaires.qufk','=','cuestionarios.qpqqufk')
         ->where('quotations.qtfolio',$folio)
         ->where('quotations.qtfk',$id)
          ->get();

          $analisis = quotations::select('questionnaires.qufk','questionnaires.quname','analisis.qsqquantity','analisis.qsqunitprice','analisis.qsqconversion','analisis.qsqtotal')
          ->join('quotation_secondary_questionnaires as analisis','analisis.qsqqtfkc','=','quotations.qtfk')
          ->join('questionnaires','questionnaires.qufk','=','analisis.qsqqtfk')
          ->where('quotations.qtfolio',$folio)
          ->where('quotations.qtfk',$id)
           ->get();

           $user = User::select('name','apaternal','amaternal','email','folio','wkrkarea','job','phone_fixed','quotations.qtdate','quotations.qtsubtotal','quotations.qtiva','quotations.qttotal','quotations.qtdateend','quotations.qtcmfk','quotations.qtdiscount','quotations.qtporcentaje')
           ->join('quotations','quotations.qtfolio','=','users.folio')
           ->where('quotations.qtfolio',$folio)
           ->where('quotations.qtfk',$id)
            ->get();
           $compania = Company::select('coname','cosocrzn')
           ->join('users','users.id','=','companies.couserfk')
           ->where('users.folio',$folio)
            ->get();
            $divisas = catalog_master::
              select('cmfk','cmtpcat','cmdesc','cmstatus','cmmin','cmmax','cmmoneda')
              ->where('cmtpcat','=','60')
              ->where('cmstatus','=','1')
              ->orderby('cmfk','ASC')
              ->get();

          return view('cotizador.view',compact('id','folio','roles','cuestionarios','analisis','user','compania','divisas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pdf(Request $request)
    {
      $folio = $request->folio;
      $id = $request->id;

      $roles = quotations::select('perfiles.qupid','roles.name','perfiles.qupquantity','perfiles.qupunitprice','perfiles.qupconversion','perfiles.quptotal')
    ->join('quotation_user_profiles AS perfiles','perfiles.qupqtfk','=','quotations.qtfk')
    ->join('roles','roles.id','=','perfiles.qupid')
    ->where('quotations.qtfolio',$folio)
    ->where('quotations.qtfk',$id)
     ->get();

     $cuestionarios = quotations::select('questionnaires.qufk','questionnaires.quname','cuestionarios.qpqquantity','cuestionarios.qpqunitprice','cuestionarios.qpqconversion','cuestionarios.qpqtotal')
     ->join('quotation_primary_questionnaires as cuestionarios','cuestionarios.qpqqtfk','=','quotations.qtfk')
     ->join('questionnaires','questionnaires.qufk','=','cuestionarios.qpqqufk')
     ->where('quotations.qtfolio',$folio)
     ->where('quotations.qtfk',$id)
      ->get();

      $analisis = quotations::select('catalog_master.cmfk','catalog_master.cmdesc','analisis.qsqquantity','analisis.qsqunitprice','analisis.qsqconversion','analisis.qsqtotal')
      ->join('quotation_secondary_questionnaires as analisis','analisis.qsqqtfkc','=','quotations.qtfk')
      ->join('catalog_master','catalog_master.cmfk','=','analisis.qsqqtfk')
      ->where('quotations.qtfolio',$folio)
      ->where('quotations.qtfk',$id)
       ->get();

       $user = User::select('name','apaternal','amaternal','email','folio','wkrkarea','job','phone_fixed','quotations.qtdate','quotations.qtsubtotal','quotations.qtiva','quotations.qttotal','quotations.qtdateend','quotations.qtcmfk','quotations.qtdiscount','quotations.qtporcentaje')
       ->join('quotations','quotations.qtfolio','=','users.folio')
       ->where('quotations.qtfolio',$folio)
       ->where('quotations.qtfk',$id)
        ->get();
       $compania = Company::select('coname','cosocrzn')
       ->join('users','users.id','=','companies.couserfk')
       ->where('users.folio',$folio)
        ->get();
        $divisas = catalog_master::
          select('cmfk','cmtpcat','cmdesc','cmstatus','cmmin','cmmax','cmmoneda')
          ->where('cmtpcat','=','60')
          ->where('cmstatus','=','1')
          ->orderby('cmfk','ASC')
          ->get();



        $pdf = PDF::loadView('cotizador.pdf.pdf', compact('id','folio','roles','cuestionarios','analisis','user','compania','divisas'));
            $pdf->setPaper('A4', 'landscape');
          //    $pdf->setPaper('A4', 'portrait');
          return $pdf->stream();

        // return $pdf->download('listado.pdf');
    }
}
