<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use Laracasts\Flash\flash;
use Session;
use App\Country;

class CiudadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $status = $request->status;
      if($request->status == ""){ $status = 1; }

      $ciudad = City::allowed()->where('cistatus','=','1')->orderBy('cifk','ASC')->Paginate(10,['*'],'ciudad1');
      $ciudadbaja = City::allowed()->where('cistatus','=','0')->orderBy('cifk','ASC')->Paginate(10,['*'],'ciudad2');
      return view('catalogos.ciudad.index',compact('status','ciudad','ciudadbaja'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $this->authorize('create', new City);
        return view('catalogos.ciudad.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', new City);
        $ciudad = $this->validate($request,[
          'ciname' => 'required|unique:cities',
          'cilada' => 'required|numeric|unique:cities',
        ]);
        $ciudad = new City($request->all());
        $ciudad->ciip = $request->ip();
        $ciudad->ciusuerpk = $request->user()->id;
        $ciudad->save();

        return response()->json([
          "mensaje" => "creado"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cifk)
    {
      $city = City::find($cifk);
      $this->authorize('update', $city);

        $ciudad = City::find($cifk);
          return response()->json($ciudad);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $ciudad = $this->validate($request,[
          'ciname' => 'required',
          'cilada' => 'required|numeric',
        ]);
        // $city = City::find($id);
        // $this->authorize('update', $city);

        $ciudad = City::find($id);
        $this->authorize('update', $ciudad);
        $ciudad->fill($request->all());
        $ciudad->save();
        return response()->json([
          "mensaje" => "creado"
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {

      if($request->status == 1){
          $status=0;
          $mnsgStatus="baja";
      }else{
          $status=1;
          $mnsgStatus="alta";
      }
      // funcion de eliminar
      $ciudad = City::find($id);
        $this->authorize('delete', $ciudad);
      $ciudad->cistatus=$status;
      $ciudad->save();
      Session::flash('flash_success','EL giro '.$ciudad->ciname.' ha sido dada de baja con exito!');
      // return redirect()->route('giro.index');
      return back();
    }

}
