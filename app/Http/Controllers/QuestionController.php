<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Question;
use App\QuestionnaireAnswer;
use Session;
use App\Http\Requests\QuestionRequest;
use Illuminate\Validation\Rule; // Libreria para la validacaion de RUles

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = $request->status;
        if($request->status == ""){ $status = 1; }
        $preguntasAl = DB::table('questions')
            ->select('qsfk AS id','qsname AS description','qsstatus AS status')
            ->where('qsstatus','=',1)
            ->orderBy('qsfk')
            ->paginate(6,['*'],'preg1');
        $preguntasBa = DB::table('questions')
            ->select('qsfk AS id','qsname AS description','qsstatus AS status')
            ->where('qsstatus','=',0)
            ->orderBy('qsfk')
            ->paginate(6,['*'],'preg2');
        $preguntas = Question::where('qsstatus','=','1')->pluck('qsname','qsfk');
        return view("cuestionarios.preguntas.index",compact('preguntasAl','preguntasBa','status','preguntas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', new Question);
        return view('cuestionarios.preguntas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionRequest $request)
    {
      $this->authorize('create', new Question);
        $preguntas = Question::create($request->all());
        $preguntas->qsip = $request->ip();
        $preguntas->qsuserfk = $request->user()->id;
        $preguntas->save();
        return response()->json([
          "mensaje" => "creado"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $preguntasFind = Question::find($id);
          $this->authorize('update', $preguntasFind);
        return response()->json($preguntasFind);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      // ACtualiza y valida a preguntas del modulo de cuestionarios
      if($request->modulo  == "cuestionarios"){

          $this->idTipoCuestionario = $request->idTipoCuestionario;
          $this->annameAddRsp = $request->annameAddRsp; // Id de respuesta seleccionada
          $this->qsname = $request->qsname; // id de la pregunta seleccionada

          $this->qsname =
          $this->validate($request, [
                // Valida si que la pregunta sea requerida ademas de que la pregunta no sea repetida pero solamente en el mismo cuestionario
                'qaqsfk' => [
                      'required',
                      Rule::unique('questionnaire_answers')->where(function ($query) {
                          $query->where('qaatqfk', $this->idTipoCuestionario)
                                ->where('qaanfk', $this->annameAddRsp);
                                //->where('qaqsfk', $this->qsname);
                      }),
                ],
           ]);

           // Insert una nueva pregunta
            if($request->annameAddRsp > 0){

                $ansTypeQuest = DB::table('answer_type_questionnaires')
                  ->select('atqbk')
                  ->where('atqqtfk','=',$request->qtnameRespCst)
                  ->where('atqqufk','=',$request->qunameRespCst)
                  ->get();

                  $respuestas = QuestionnaireAnswer::create([
                    'qaqsfk' => $request->qaqsfk,
                    'qaanfk' => $request->annameAddRsp,
                    'qaatqfk' => $ansTypeQuest[0]->atqbk,
                    'qascafk' => $request->idSeccionRespCst,
                    'qasbcatfk' => $request->idSubseccionRespCst,
                    'qauserfk' => $request->user()->id,
                    'qaip' => $request->ip()
                  ]);

            }

            $preguntas = QuestionnaireAnswer::find($id);
            $preguntas->qaqsfk = $request->qaqsfk;
            $preguntas->qaip = $request->ip();
            $preguntas->qauserfk = $request->user()->id;
            $preguntas->save();

      }

      // ACtualiza y valida a preguntas del modulo preguntas
      if($request->modulo  == "preguntas"){
        /*$this->validate($request, [
              'qsname' => 'required|unique:questions'
         ]);*/
          $preguntas = Question::find($id);
          $this->authorize('update', $preguntas);
          $preguntas = Question::find($id);
          $preguntas->qsname = $request->qsname;
          $preguntas->qsip = $request->ip();
          $preguntas->qsuserfk = $request->user()->id;
          $preguntas->save();

      }

      return response()->json([
          "mensaje" => "creado---".$request->qaqsfk,
      ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        if($request->estatus == 1){
            $estaus=0;
            $mnsStatus = "baja";
        }else{
            $estaus=1;
            $mnsStatus = "alta";
        }
        $pretunta = Question::find($id);
          $this->authorize('delete', $pretunta);
        $pretunta->qsstatus = $estaus;
        $pretunta->save();
        Session::flash('message','La pregunta: '.$request->qsname." fue dada de ".$mnsStatus);
        return back();

    }

    public function getQuestion(Request $request){
          if($request->ajax()){
              $preguntas = Question::questions();
              return response()->json($preguntas);
          }

    }
}
