<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Answer;
use App\QuestionnaireAnswer;
use App\QuestionnaireType;
use App\Questionnaire;
use Session;
use App\Http\Requests\AnswerQuestionnaireRequest;
use DB;
use Response;

class QuestionnaireAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status=$request->status;
        $preguntasResps = DB::table('questionnaires')
            ->join('answer_type_questionnaires','questionnaires.qufk','=','answer_type_questionnaires.atqqufk')
            ->select('qufk AS id','quname AS description','qustatus AS status')
            ->where('qustatus','=',$status)
            ->paginate(10);
        return view('cuestionarios.asignapreguntas.index',compact('preguntasResps','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          $preguntas = Question::where('qsstatus','=','1')
              ->orderBy('qsname')
              ->pluck('qsname','qsfk');
          $respuestas = Answer::where('anstatus','=','1')
              ->orderBy('anname')
              ->pluck('anname','anfk');
          $cuestionarios =  Questionnaire::where('qustatus','=','1')
              ->orderBy('quname')
              ->pluck('quname','qufk');
          $tipoCuestionario =  QuestionnaireType::where('qtstatus','=','1')
              ->orderBy('qtname')
              ->pluck('qtname','qtfk');
          return view('cuestionarios.asignapreguntas.create',compact('preguntas','respuestas','tipoCuestion','cuestionarios','tipoCuestionario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnswerQuestionnaireRequest $request)
    {

        $ansTypeQuest = DB::table('answer_type_questionnaires')
          ->select('atqbk')
          ->where('atqqtfk','=',$request->qtname)
          ->where('atqqufk','=',$request->quname)
          ->get();

        foreach($request->respuestaHidden as $resp){
            $respuestas = QuestionnaireAnswer::create([
              'qaqsfk' => $request->qaqsfk,
              'qaanfk' => $resp,
              'qaatqfk' => $ansTypeQuest[0]->atqbk,
              'qascafk' => $request->seccion,
              'qasbcatfk' => $request->subseccion,
              'qauserfk' => $request->user()->id,
              'qaip' => $request->ip()
            ]);
        }

        return response()->json([
          "mensaje" => "creado"
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipoCuestionario = DB::table('answer_type_questionnaires')
            ->select('questionnaire_types.qtfk AS idTipoCuest','questionnaire_types.qtname AS nameTipoCuest','questionnaires.qufk AS idCuest',
                'questionnaires.quname AS nameCuest')
            ->join('questionnaires','answer_type_questionnaires.atqqufk','=','questionnaires.qufk')
            ->join('questionnaire_types','answer_type_questionnaires.atqqtfk','=','questionnaire_types.qtfk')
            ->where('answer_type_questionnaires.atqbk','=',$id)
            ->get();

        return response()->json($tipoCuestionario);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
      if($request->status == 1){
          $estatus=0;
          $mnsgStatus="baja";
      }else{
          $estatus=1;
          $mnsgStatus="alta";
      }
      $respuesta = QuestionnaireAnswer::find($id);
      $respuesta->status = $estatus;
      $respuesta->save();
      Session::flash('message','La respuesta: '.$request->nameResp."  fue dada de ".$mnsgStatus);
      return back();
    }

    public function getCuestionarios(Request $request, $id){
        if($request->ajax()){
          $cuestions = Questionnaire::cuestionario($id);
          return response()->json($cuestions);
        }
    }
}
