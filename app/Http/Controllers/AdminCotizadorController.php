<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;
use App\Questionnaire;

class AdminCotizadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::orderBy('id','ASC')->where('statuscot','=','1')->get();
        $primarios = Questionnaire::orderBy('qufk','ASC')->where('qustatus','=','1')->paginate(20,['*'],'primarios');
        $secundarios = Questionnaire::orderBy('qufk','ASC')->where('qustatus','=','2')->paginate(20,['*'],'secundarios');

        return view('catalogos.cotizador.index',compact('roles','primarios','secundarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
      $nombre = explode(",", $id);
      if($nombre[1] == 'R'){
        $role = Role::find($nombre[0]);
        return response()->json($role);
      }
      if($nombre[1] == 'P'){
        $cuest = Questionnaire::find($nombre[0]);
        return response()->json($cuest);
      }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $id = $request->id;
      $roles= $this->validate($request, [
            'cost' => 'required|numeric',
       ]);
       // funcion para guardar los roles de usuario diferenciados con la variable de R
        if($request->letra == 'R'){
          // si la variable es R guarda en la tabla de roles
          $rol = Role::find($id);
          $rol->cost = $request->cost;
          $rol->save();
          return response()->json([
            "mensaje" => "creado"
          ]);
        }
        // funcion para guardar cuestionarios con la variable de P para diferenciar una de otra
        if($request->letra == 'P'){
          // si la variable es  P guarda en la tabla de cuestionarios
          $cues = Questionnaire::find($id);
          $cues->qucost = $request->cost;
          $cues->save();
          return response()->json([
            "mensaje" => "creado"
          ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
