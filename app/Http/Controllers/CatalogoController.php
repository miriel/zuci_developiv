<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Catalogs;
use App\Http\Requests\CatalogoRequest;
use Session;

class CatalogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $status = $request->status;
      if($request->status == ""){ $status = 1; }

      $seccionesAl = Catalogs::select('catfk AS id','catvalue AS description','catstatus AS status')
          ->allowed()
          ->where('cattype','=','secciones')
          ->where('catstatus','=',1)
          ->orderBy('catfk')
          ->paginate(6,['*'],'secc1');

      $seccionesBa = Catalogs::select('catfk AS id','catvalue AS description','catstatus AS status')
          ->allowed()
          ->where('cattype','=','secciones')
          ->where('catstatus','=',0)
          ->orderBy('catfk')
          ->paginate(6,['*'],'secc2');
      return view("cuestionarios.catalogos.index",compact('seccionesAl','seccionesBa','status'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CatalogoRequest $request)
    {
        $this->authorize('create', new Catalogs);
        $catalogo = Catalogs::create([
           'catnamtabl' => $request->catnamtabl,
           'catvalue' => $request->catvalue,
           'catip' => $request->ip(),
           'catuserfk'  => $request->user()->id,
           'cattype' => 'secciones',
        ]);

        return response()->json([
          "mensaje" => "creado"
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $catalogo = Catalogs::find($id);
        $this->authorize('update', $catalogo);
      return response()->json($catalogo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $empre= $this->validate($request, [
            'catvalue' => 'required',
       ]);

       $catalogo = Catalogs::find($id);
       $this->authorize('update', $catalogo);
       $catalogo->catnamtabl = $request->catnamtabl;
       $catalogo->catvalue = $request->catvalue;
       $catalogo->save();

       return response()->json([
         "mensaje" => "creado"
       ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
      if($request->estatus == 1){
          $estatus=0;
          $mnsgStatus="baja";
      }else{
          $estatus=1;
          $mnsgStatus="alta";
      }
      $catalogs = Catalogs::find($id);
      $this->authorize('Delete', $catalogs);
      $catalogs->catstatus = $estatus;
      $catalogs->save();
      Session::flash('message','El tipo de cuestionario: '.$request->description."  fue dada de ".$mnsgStatus);
       return back();
    }
}
