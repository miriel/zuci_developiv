<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use Laracasts\Flash\flash;
use Session;


class PaisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $status = $request->status;
      if($request->status == ""){ $status = 1; }

      $pais = Country::allowed()->where('cnstatus','=','1')->orderBy('cnfk','ASC')->paginate(10,['*'],'pais1');
      $paisbajas = Country::allowed()->where('cnstatus','=','0')->orderBy('cnfk','ASC')->paginate(10,['*'],'pais2');
      return view('catalogos.pais.index',compact('status','pais','paisbajas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('catalogos.pais.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $pais=  $this->validate($request, [
           'cnname' => 'required|unique:countries',
       ]);
        $pais = new Country($request->all());
        $pais->cnip=$request->ip();
        $pais->cnusuerpk=$request->user()->id;
        // dd($giro);
        $pais->save();

        return response()->json([
          "mensaje" => "creado"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pais = Country::find($id);
          return response()->json($pais);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $empre= $this->validate($request, [
            'cnname' => 'required',
       ]);
      $pais = Country::find($id);
      $pais->fill($request->all());
      $pais->save();
      return response()->json([
        "mensaje" => "creado"
      ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
      if($request->status == 1){
          $status=0;
          $mnsgStatus="baja";
      }else{
          $status=1;
          $mnsgStatus="alta";
      }
      // funcion de eliminar
      $pais = Country::find($id);
      $pais->cnstatus=$status;
      $pais->save();
      Session::flash('flash_success','EL giro '.$pais->cnname.' ha sido dada de baja con exito!');
      // return redirect()->route('giro.index');
      return back();
    }
    public function baja()
    {
      // $giro = Company_Turn::orderBy('id','DESC')->paginate();
      $pais = Country::where('cnstatus','=','0')->orderBy('cnfk','ASC')->Paginate(10);
          return view('catalogos.pais.index')->with('pais',$pais);
    }
}
