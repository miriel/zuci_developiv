<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GiroRequest;
use App\Company_Turn;
use Laracasts\Flash\flash;
use Session;
use Auth;
use Illuminate\Routing\Redirector;
use App\Http\Controllers\user;
use Alertify;
use odannyc\Alertify\AlertifyNotifier;
use DB;
use Barryvdh\DomPDF\Facade as PDF;
use App\Exports\UsersExport;
use Excel;
use Carbon\Carbon;

class GiroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
          $status = $request->status;
          if($request->status == ""){ $status = 1; }

          $giro = Company_Turn::allowed()->where('cotstatus','=','1')->orderBy('cotfk','ASC')->paginate(10,['*'],'giro1');
          $girobajas = Company_Turn::allowed()->where('cotstatus','=','0')->orderBy('cotfk','ASC')->paginate(10,['*'],'giro2');

              // $giro = Company_Turn::allowed()->get();

          return view('catalogos.giro.index', compact('status','giro','girobajas'));
    }
    public function excel()
  {
      /**
       * toma en cuenta que para ver los mismos
       * datos debemos hacer la misma consulta
      **/
      $users = Company_Turn::select('cotfk', 'cotname')->get();
          Excel::create('users', function($excel) use($users) {
              $excel->sheet('Sheet 1', function($sheet) use($users) {
                  $sheet->fromArray($users);
              });
          })->export('xls');
      /*Excel::load('Laravel Excel', function($excel) {
             $excel->sheet('Excel sheet', function($sheet) {
                 //otra opción -> $products = Product::select('name')->get();
                 $products = Company_Turn::all();
                 $sheet->fromArray($products);
                 $sheet->setOrientation('landscape');
             });
         })->export('xls');
        /* $data = Company_Turn::all();
          Excel::create('usuarios', function ($excel) use ($data) {
              $excel->sheet('Hoja Uno', function ($sheet) use ($data) {

                  $sheet->with($data, null, 'A1', false, false);
              });
          })->download('xlsx');*/
  }


    public function pdf()
    {
        /**
         * toma en cuenta que para ver los mismos
         * datos debemos hacer la misma consulta
        **/
        $carbon = new Carbon();
        $date = Carbon::now();
        $fechaActual = $date->toFormattedDateString('Y-m-d ');
         $nombre = Auth::user()->name;
         $apellido =  Auth::user()->apaternal;
         $area = Auth::user()->wkrkarea;

         $giro = Company_Turn::allowed()->where('cotstatus','=','1')->orderBy('cotfk','ASC')->get();
        //
        $pdf = PDF::loadView('catalogos.giro.pdf.giro', compact('giro','nombre','apellido','area','fechaActual'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream();

        // return $pdf->download('listado.pdf');



    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', new Company_Turn);
        return view('catalogos.giro.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GiroRequest $request)
    {
        $this->authorize('create', new Company_Turn);
        $giro = new Company_Turn($request->all());
        $giro->cotip=$request->ip();
        $giro->cotuserfk=$request->user()->id;
        // dd($giro);
        $giro->save();

      return response()->json([
        "mensaje" => "creado"
      ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cotfk)
    {
      $Company_Turn = Company_Turn::find($cotfk);
      $this->authorize('update', $Company_Turn);

      $giro = Company_Turn::find($cotfk);
      return response()->json($giro);

  }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cotfk)
    {
      $empre= $this->validate($request, [
            'cotname' => 'required',
       ]);
      $Company_Turn = Company_Turn::find($cotfk);
      // autorizacion para poder editar
      $this->authorize('update', $Company_Turn);

        $giro = Company_Turn::find($cotfk);
        $giro->cotname = $request->cotname;
        $giro->save();
        return response()->json([
          "mensaje" => "creado"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cotfk, Request $request)
    {
      // verificar si puede elimiar
    $Company_Turn = Company_Turn::find($cotfk);
    // autorizacion para poder editar
    $this->authorize('delete', $Company_Turn);

      if($request->estatus == 1){
          $estatus=0;
          $mnsgStatus="baja";
      }else{
          $estatus=1;
          $mnsgStatus="alta";
      }
      // funcion de eliminar
      $giro = Company_Turn::find($cotfk);
      $giro->cotstatus = $estatus;
      $giro->save();
      Session::flash('flash_success','EL giro '.$giro->cotname.' ha sido dada de baja con exito!');
      // return redirect()->route('giro.index');
      return back();
  }
  public function baja()
  {
    // $giro = Company_Turn::orderBy('id','DESC')->paginate();
    $giro = Company_Turn::where('cotstatus','=','0')->orderBy('cotfk','ASC')->paginate(10);
      return view('catalogos.giro.index')->with('giro',$giro);
  }

public function importFile(Request $request){

    if($request->hasFile('sample_file')){
        $path = $request->file('sample_file')->getRealPath();
        $data = \Excel::load($path)->get();
        if($data->count()){
            foreach ($data as $key => $value) {
                $arr[] = [
                  'cotfk' => $value->cotfk,
                  'cotname' => $value->cotname,
                  'cotstatus' => $value->cotstatus,
                  'cotip' => $value->cotip,
                  'cotuserfk' => $value->cotuserfk,
                  'cotinsertdt' => $value->cotinsertdt,
                  'cotupddt' => $value->cotupddt
                ];
            }
            if(!empty($arr)){
                DB::table('company_turns')->insert($arr);
                dd('Insert Recorded successfully.');
            }
        }
    }
    dd('Request data does not have any files to import.');
}

public function exportFile($type){

    $products = Company_Turn::get()->toArray();
    return \Excel::create('company_turns', function($excel) use ($products) {
        $excel->sheet('sheet name', function($sheet) use ($products)
        {
            $sheet->fromArray($products);
        });
    })->download($type);
}


}
