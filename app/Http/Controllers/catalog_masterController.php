<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\catalog_master;
use Illuminate\Support\Facades\Auth;
use DB;
use App\relationship_catalogs;
use View;
use Session;
use Illuminate\Routing\Redirector;
use App\Http\Controllers\user;
use App\Http\Requests\QuestionnaireTypeRequest;

class catalog_masterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $status = $request->status;
      if($request->status == ""){ $status = 1; }

      $tipoCuestionario = $request->id;
      $operacion = catalog_master::
      select('cmfk','cmtpcat','cmdesc','cmstatus','cmtpquest')
      ->where('cmstatus','=','1')
      ->where('cmtpcat','=','2')
      ->where('cmtpquest',$tipoCuestionario)
      ->orderBy('cmfk','ASC')
      ->paginate(10,['*'],'operacion1');

      $operacionbaja = catalog_master::
      select('cmfk','cmtpcat','cmdesc','cmstatus','cmtpquest')
      ->where('cmstatus','=','0')
      ->where('cmtpcat','=','2')
      ->where('cmtpquest',$tipoCuestionario)
      ->orderBy('cmfk','ASC')
      ->paginate(10,['*'],'operacion2');

      $cuestionarios = DB::table('company_types')
        ->select('ctfk','ctname','ctstatus')
        ->where('ctfk',$tipoCuestionario)
        ->get();

        return view('analisis.operacion.index',compact('status','operacion','operacionbaja','cuestionarios','tipoCuestionario'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $status = $request->status;
      if($request->status == ""){ $status = 1; }
      // ID de sesion del usuario
        $id = Auth::user()->id;

        // Validacion de campo requerido
        $operacion=  $this->validate($request, [
             'cmdesc' => 'required|max:80|min:5',
         ]);

         $operacion = catalog_master::create([
           'cmtpcat' => '2',
           'cmdesc' => $request->cmdesc,
           'cmtpquest' => $request->qtfk,
           'cmuserfk' => $id
         ]);

          $resultado = $operacion->cmfk;

            $operacion = relationship_catalogs::create([
              'rctpcat' => $resultado,
              'rccmfk' => $resultado,
              // 'rcctfk' => $request->qtfk,
            ]);


          return response()->json([
            "mensaje" => "creado"
          ]);




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cmfk)
    {
      $operacion = catalog_master::find($cmfk);
      return response()->json($operacion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $operacion= $this->validate($request, [
            'cmdesc' => 'required',
       ]);

       $operacion = catalog_master::find($id);
       $operacion->cmdesc = $request->cmdesc;
       $operacion->save();

       return response()->json([
         "mensaje" => "creado"
       ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cmfk, Request $request)
    {
      if($request->estatus == 1){
          $estaus=0;
          $mnsStatus = "baja";
      }else{
          $estaus=1;
          $mnsStatus = "alta";
      }

      $operacion= catalog_master::find($cmfk);
      /*$this->authorize('delete', $pretunta);*/
      $operacion->cmstatus = $estaus;
      $operacion->save();
      Session::flash('message','El registro: '.$request->desc." fue dada de ".$mnsStatus);
      return back();

    }
}
