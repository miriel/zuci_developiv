<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use App\catalog_master;

class AuditoriaArchivosController extends Controller
{
<<<<<<< HEAD

=======
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
    public function index()
    {

       $users = User::join('company_answers','users.folio','company_answers.cafoliofk')
        ->groupBy('users.folio','users.id')
        ->select('users.id as id', DB::raw("CONCAT(users.name,' ',users.apaternal,' ',users.amaternal)AS name"), 'users.email','users.folio','users.ctfk')
        ->where('status','=',1)
        ->get();

        return view('auditoria.archivos.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
<<<<<<< HEAD

    }
=======
        //
    }

>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // Parametro ID (Folio de cada usuario que tiene un cuestionario asignado)
    public function show($id, Request $request)
    {
<<<<<<< HEAD

=======
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
      $id2 = $request->id2;
      $empresa = $request->ctfk;
      $cuestionarios = DB::table('company_answers')
        ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
        ->join('answer_type_questionnaires','questionnaire_answers.qaatqfk','=','answer_type_questionnaires.atqbk')
        ->join('questionnaires','answer_type_questionnaires.atqqufk','=','questionnaires.qufk')
        ->join('questionnaries_status','company_answers.casqfk','=','questionnaries_status.sqfk')
        ->select('answer_type_questionnaires.atqqufk AS id','questionnaires.quname as description' ,'questionnaries_status.sqstatus AS status',
            'company_answers.cafoliofk as folioAudit')
        ->where('company_answers.cafoliofk','=',$id)
        ->groupBy('answer_type_questionnaires.atqqufk' ,'questionnaires.quname' ,'questionnaries_status.sqstatus','company_answers.cafoliofk')
        ->paginate(10);

        $status = catalog_master::select('cmdesc','cmval','cmimg')
        ->where('cmstatus','=','2')
        ->where('cmtpcat','=','60')
        ->get();

        return view('auditoria.archivos.cuestionarios',compact('cuestionarios','id','empresa','id2','status'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
