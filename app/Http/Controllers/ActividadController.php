<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\catalog_master;
use App\relationship_catalogs;
use Illuminate\Support\Facades\Auth;
use DB;
use View;
use Session;
use Illuminate\Routing\Redirector;
use App\Http\Controllers\user;

class ActividadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $idoperacion = $request->id;

      $cuestion = $request->id2;
      $status = $request->status;
      if($request->status == ""){ $status = 1; }

      $actividad = DB::table('catalog_master AS ope')
      ->select(
        'ope.cmfk AS idperacion',
        'act.cmfk AS idOperacion',
        'act.cmdesc AS desOperacion',
        'act.cmstatus AS cmstatus',
        'act.cmtpcat As catalogo'

      )
      ->leftjoin('relationship_catalogs AS op_act','op_act.rccmfk','=','ope.cmfk')
      ->leftjoin('catalog_master AS act','act.cmtpcat','=','op_act.rctpcat')
      ->where('ope.cmfk',$idoperacion)
      ->Where('act.cmstatus','=','1')
      ->where('ope.cmtpquest',$cuestion)
      ->orderBy('act.cmfk','ASC')
      ->paginate(10,['*'],'actividad1');

      $actividadbaja = DB::table('catalog_master AS ope')
      ->select(
        'ope.cmfk AS idOperacion',
        'act.cmfk AS idOperacion',
        'act.cmdesc AS desOperacion',
        'act.cmstatus AS cmstatus',
        'act.cmtpcat As catalogo'
      )
      ->leftjoin('relationship_catalogs AS op_act','op_act.rccmfk','=','ope.cmfk')
      ->leftjoin('catalog_master AS act','act.cmtpcat','=','op_act.rctpcat')
      ->where('ope.cmfk',$idoperacion)
      ->Where('act.cmstatus','=','0')
      ->where('ope.cmtpquest',$cuestion)
      ->orderBy('act.cmfk','ASC')
      ->paginate(10,['*'],'actividad2');

      $cuestionarios = DB::table('company_types')
        ->select('ctfk','ctname','ctstatus')
        ->where('ctfk',$cuestion)
        ->get();


         return view('analisis.actividad.index',compact('status','actividad','actividadbaja','cuestionarios','idoperacion','cuestion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $status = $request->status;
      if($request->status == ""){ $status = 1; }
      // ID de sesion del usuario
        $id = Auth::user()->id;

        // Validacion de campo requerido
        $actividad=  $this->validate($request, [
             'cmdesc' => 'required'
         ]);

       $actividad = catalog_master::create([
         'cmtpcat' => $request->qtfk,
         'cmdesc' => $request->cmdesc,
         'cmtpquest' => $request->cuest,
         'cmuserfk' => $id,
       ]);
       $resultado = $actividad->cmfk;

         $operacion = relationship_catalogs::create([
           'rctpcat' => $resultado,
           'rccmfk' => $resultado,
         ]);


      return response()->json([
        "mensaje" => "creado"
      ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cmfk)
    {
      $actividad = catalog_master::find($cmfk);
      return response()->json($actividad);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $actividad= $this->validate($request, [
            'cmdesc' => 'required',
       ]);

       $actividad = catalog_master::find($id);
       $actividad->cmdesc = $request->cmdesc;
       $actividad->save();

       return response()->json([
         "mensaje" => "creado"
       ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cmfk, Request $request)
    {
      if($request->estatus == 1){
          $estaus=0;
          $mnsStatus = "baja";
      }else{
          $estaus=1;
          $mnsStatus = "alta";
      }

      $actividad= catalog_master::find($cmfk);
      /*$this->authorize('delete', $pretunta);*/
      $actividad->cmstatus = $estaus;
      $actividad->save();
      Session::flash('message','El registro: '.$request->mcdesc." fue dada de ".$mnsStatus);
      return back();
    }
}
