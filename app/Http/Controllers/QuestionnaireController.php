<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;
use Session;
use App\QuestionnaireType;
use DB;
use App\Http\Requests\QuestionnaireRequest;
use bb;
use App\AnswerTypeQuestionnaire;
use App\AnswerType;
use App\Question;
use App\Answer;
use App\Catalogs;

class QuestionnaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $status = $request->status;
      if($request->status == ""){ $status = 1; }

      $cuestionariosAl = DB::table('questionnaires')
        ->select('qufk AS id','quname AS description','qustatus AS status')
        ->where('qustatus','=',1)
        ->orderBY('qufk')
        ->paginate(6,['*'],'cuest1');

      $cuestionariosBa = DB::table('questionnaires')
        ->select('qufk AS id','quname AS description','qustatus AS status')
        ->where('qustatus','=',0)
        ->orderBY('qufk')
        ->paginate(6,['*'],'cuest2');

      $tipoCuestionario = QuestionnaireType::where('qtstatus','=','1')
            ->pluck('qtname','qtfk');

      return view("cuestionarios.cuestionario.index", compact('status','cuestionariosAl','cuestionariosBa','tipoCuestionario'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          /*$tipoCuestionario = QuestionnaireType::where('qtstatus','=','1')
            ->pluck('qtname','qtfk');
          return view('cuestionarios.cuestionario.create',compact('tipoCuestionario','cuestionario'));
          */
          $this->authorize('create', new Questionnaire);
          $tipoCuestionario = QuestionnaireType::where('qtstatus','=','1')
            ->pluck('qtname','qtfk');
          return view('cuestionarios.cuestionario.create',compact('tipoCuestionario','cuestionario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionnaireRequest $request)
    {
      if($request->ajax()){
          $cuestionario = Questionnaire::create($request->all());
          $cuestionario->quip = $request->ip();
          $cuestionario->quuserfk = $request->user()->id;
          $cuestionario->save();

           $idCuetionario = $cuestionario->qufk;
           $tipoCuestionario = AnswerTypeQuestionnaire::create([
              'atqqtfk'  => $request->atqqtfk,
              'atqqufk'  => $idCuetionario,
           ]);

           return response()->json([
             "mensaje" => "creado"
           ]);
      }

      /*


       Session::flash('message','El cuestionario: '.$request->quname.'. Se ha guardado');
       return redirect('cuestionario/create');*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

      public function edit($id)
      {
        // Tipo de respuesta
        $tipoRespuesta = AnswerType::where('atstatus','=','1')
              ->pluck('atname','atfk');

        $tipoCuestionario = QuestionnaireType::where('qtstatus','=','1')
          ->pluck('qtname','qtfk');

        $cuestionario = DB::table('answer_type_questionnaires')
            ->join('questionnaires','answer_type_questionnaires.atqqufk','=','questionnaires.qufk')
            ->select('questionnaires.quname','answer_type_questionnaires.atqqtfk','questionnaires.qufk','answer_type_questionnaires.atqbk')
            ->where('questionnaires.qufk','=',$id)
            ->get();
        $preguntas = Question::where('qsstatus','=','1')
            ->orderBy('qsname')
            ->pluck('qsname','qsfk');
        $respuestas = Answer::where('anstatus','=','1')
            ->orderBy('anname')
            ->pluck('anname','anfk');

        $secciones = Catalogs::where('catstatus','=','1')
            ->where('catnamtabl','=','questionnaries_section')
            ->where('cattype','=','secciones')
            ->where('catstatus','=',1)
            ->orderBy('catvalue')
            ->pluck('catvalue','catfk');

        $subsecciones = Catalogs::where('catstatus','=','1')
            ->where('catnamtabl','=','questionnaries_subsection')
            ->where('cattype','=','secciones')
            ->where('catstatus','=',1)
            ->orderBy('catvalue')
            ->pluck('catvalue','catfk');

        $pregRespAs = DB::table('questionnaires')
            ->select(
              'catalogs.catfk as idSeccion', 'catalogs.catvalue as seccion',
              'catsub.catfk as idSubSeccion', 'catsub.catvalue as subseccion',
              'questions.qsfk AS idPregunta','questions.qsname AS namPreg','answers.anfk AS idRespst',
                     'answers.anname AS nameRespst', 'answer_types.atfk AS idTipoResp','answer_types.atname AS nameResp',
                     'answer_type_questionnaires.atqbk AS idTipoCuest', 'questionnaire_answers.qastatus AS status',
                     'questionnaire_answers.qafk AS idPregResp'
                     )
            ->join('answer_type_questionnaires','questionnaires.qufk','=','answer_type_questionnaires.atqqufk')
            ->leftjoin('questionnaire_answers','answer_type_questionnaires.atqbk','=','questionnaire_answers.qaatqfk')
            ->leftjoin('questions','questionnaire_answers.qaqsfk','=','questions.qsfk')
            ->leftjoin('answers','questionnaire_answers.qaanfk','=','answers.anfk')
            ->leftjoin('answer_types','answers.anatfk','=','answer_types.atfk')
            ->leftjoin('catalogs','questionnaire_answers.qascafk','=','catalogs.catfk')
            ->leftjoin ('catalogs as catsub','questionnaire_answers.qasbcatfk','=','catsub.catfk')
            ->where('questionnaires.qufk','=',$id)
             // Obtiene los registros con estatus 1 de alta
            ->orderBy('questions.qsfk')
            ->get();

        $count=0;
        while( $count <  count($pregRespAs)){
          $arrayIdSecciones[$pregRespAs[$count]->idSeccion] = $pregRespAs[$count]->idSeccion;
          $arrayIdSubSecciones[$pregRespAs[$count]->idSubSeccion] = $pregRespAs[$count]->idSubSeccion;
          $arrayIdTipoCuestion[$pregRespAs[$count]->idTipoCuest] = $pregRespAs[$count]->idTipoCuest;
          $arrayIdPregunta[$pregRespAs[$count]->idPregunta] = $pregRespAs[$count]->idPregunta;
          $arrayIdRespuesta[$pregRespAs[$count]->idRespst] = $pregRespAs[$count]->idRespst;
          $arrayIdTipoResp[$pregRespAs[$count]->idTipoResp] = $pregRespAs[$count]->idTipoResp;

          $arrayNameSeccion[$pregRespAs[$count]->idSeccion] = $pregRespAs[$count]->seccion;
          $arrayNameSubseccion[$pregRespAs[$count]->idSeccion][$pregRespAs[$count]->idSubSeccion] = $pregRespAs[$count]->subseccion;

          /// Arreglo de preguntas
          $arrayNamePregunta[$pregRespAs[$count]->idSeccion][$pregRespAs[$count]->idSubSeccion][$pregRespAs[$count]->idPregunta] =
              $pregRespAs[$count]->namPreg;

          $arrayNameRespuesta[$pregRespAs[$count]->idSeccion][$pregRespAs[$count]->idSubSeccion][$pregRespAs[$count]->idPregunta][$pregRespAs[$count]->idRespst] =
              $pregRespAs[$count]->nameRespst;
          $arrayIdTipoRespuesta[$pregRespAs[$count]->idSeccion][$pregRespAs[$count]->idSubSeccion][$pregRespAs[$count]->idPregunta][$pregRespAs[$count]->idRespst] =
              $pregRespAs[$count]->idTipoResp;
          $arrayStatusRespst[$pregRespAs[$count]->idSeccion][$pregRespAs[$count]->idSubSeccion][$pregRespAs[$count]->idPregunta][$pregRespAs[$count]->idRespst] = $pregRespAs[$count]->status;
          $arrayPregResp[$pregRespAs[$count]->idSeccion][$pregRespAs[$count]->idSubSeccion][$pregRespAs[$count]->idPregunta][$pregRespAs[$count]->idRespst] =
              $pregRespAs[$count]->idPregResp;
          $count++;
        }
        /*   // Contruccion de foreachs
        foreach($arrayIdPregunta as $idPregunta){
          echo $arrayNamePregunta[$idPregunta]."<br>";

          foreach($arrayIdRespuesta as $idRespuesta){
            if(!isset($arrayNameRespuesta[$idPregunta][$idRespuesta])){

            }else{
              echo $arrayNameRespuesta[$idPregunta][$idRespuesta]."<br>";
            }

            foreach($arrayIdTipoResp as $idTipoResp){
                if( !isset($arrayNameTipoRespuesta[$idPregunta][$idRespuesta][$idTipoResp]) ){

                }else{
                  echo $arrayNameTipoRespuesta[$idPregunta][$idRespuesta][$idTipoResp];
                }

            }

        }  */
        return view('cuestionarios.cuestionario.edit',compact('cuestionario','tipoCuestionario','pregRespAs','arrayIdPregunta','arrayIdRespuesta',
                    'arrayIdTipoRespuesta','arrayNamePregunta','arrayNameRespuesta','arrayNameTipoRespuesta','tipoRespuesta','preguntas','respuestas',
                    'secciones','subsecciones','arrayIdTipoCuestion','arrayStatusRespst','arrayPregResp',
                    'arrayIdSecciones','arrayIdSubSecciones',
                    'arrayNameSeccion','arrayNameSubseccion'));

}
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $cuestionario = Questionnaire::find($id);
        $cuestionario->fill($request->all());
        $cuestionario->quip = $request->ip();
        $cuestionario->quuserfk	 = $request->user()->id;
        $cuestionario->save();

        $cuestionario = AnswerTypeQuestionnaire::find($request->atqbk);
        $cuestionario->atqqtfk = $request->atqqtfk;
        $cuestionario->save();

        Session::flash("message","El cuestionario: ".$request->description.". Ha sido actualizado");
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
       if($request->status == 1){
          $statsUp = 0;
          $msgUp = "deshabilitado";
       }else{
          $statsUp = 1;
          $msgUp = "activado";
       }
       $cuestionario = Questionnaire::find($id);
       $cuestionario->qustatus = $statsUp;
       $cuestionario->save();
       Session::flash('message','El cuestionario: '.$request->nomCuesti.". Fue  ".$msgUp);
       return back();

    }

}
