<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Packages;

class AdminController extends Controller
{
  public function index()
  {
    $planes = Packages::where('pgstatus','=','1')->orderBy('pgfk','ASC')->Paginate(8);
    return view('index',compact('planes'));
  }

}
