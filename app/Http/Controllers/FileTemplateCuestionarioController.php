<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\TemplateQuestionnaire;
use Chumper\Zipper\Zipper; // Libreria para poder comprimir archivos

class FileTemplateCuestionarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $idCuestion = $request->id; // ID cuestionario
        $nameCuestion = $request->name; // ID cuestionario

        $carbon = new Carbon();
        $date = Carbon::now();
        $fechaActual = $date->format('d-m-Y');

        $catStatus = DB::table('catalog_master')
          ->select('catalog_master.cmdesc as nameStatus','catalog_master.cmval as valStatus')
          ->where('catalog_master.cmtpcat','=',58)
          ->where('catalog_master.cmstatus','=','1')
          ->get();

        $tipoDocumentos = DB::table('catalogs')
          ->where('catnamtabl','=','document_type')
          ->where('cattype','=','file')
          ->pluck('catvalue','catfk');


        $templatsCuestion = DB::table('template_questionnaires')
          ->select('template_questionnaires.qtfk AS idFile','template_questionnaires.qtnamefile','template_questionnaires.qtsize','catalogs.catvalue AS tipoFile',
                   'template_questionnaires.qtstatus as status')
          ->join('catalogs','template_questionnaires.qttypfil','=','catalogs.catfk')
          ->where('template_questionnaires.qtqufk','=',$idCuestion)
          ->orderBy('template_questionnaires.qtfk','DESC')
          ->get();

        return view('cuestionarios.cuestionario.form.fileTemplateCuestionario',compact('templatsCuestion','nameCuestion','fechaActual','tipoDocumentos','idCuestion',
                    'catStatus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $carbon = new Carbon();
      $date = Carbon::now();
      $fechaActual = $date->format('dmYHis');

      //Valida que existan archivos seleccionados
      if($request->fileCheck){

          $fileSep = explode(",",$request->fileCheck);
          for($i=0; $i<count($fileSep); $i++){
            // Se acumula el resultado de las opciones seleccionadas en un Arreglo
            $arrarFilsD[] = $fileSep[$i];
          }


          // Se ejecuta query que obtiene las opciones seleccionadas mediante el arreglo acumulado
          $nameFile=TemplateQuestionnaire::select('qtnamefile')->whereIn('qtfk', $arrarFilsD)->get();

          foreach($nameFile as $filesAddZip){

              // Agrega los archivos seleccionador al archivo temporal
              $files = glob(public_path('archivos/cuestionarios/templates/'.$filesAddZip->qtnamefile));
              // Crear el archivo zip temporal
             \Zipper::make('archivos/tmp/filestmp_'.$fechaActual.'.zip')->add($files)->close();

          }

          // Decarga el archivo temporal en la PC del usuario
          return response()->download(public_path('/archivos/tmp/filestmp_'.$fechaActual.'.zip'));

      }else{
        // Si no existen archivos seleccionados regresa a la pantalla inicial
        return back();
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $carbon = new Carbon();
      $date = Carbon::now();
      $fechaActual = $date->format('Ymd');
      $fechaActual2 = $date->format('Y-m-d H:i:s');
      $tipoFile = $request->tipoFile;

      $rutaLocal = '/archivos/cuestionarios/templates/';
      $path = public_path().$rutaLocal;
      $files = $request->file('file');
      $filename="";
      // Foreach para multimples archivos se modifica la variable por file
      //foreach($files as $file){ $file;

            $fileOriginName = $files->getClientOriginalName(); // Obtiene el nombre original del archivo
            $extension = $files->getClientOriginalExtension(); // Obtiene la extencion del archivo
            $tamañoArchivoByte = filesize($files); // Obtiene el tamaño del archivo
            $filename = pathinfo($fileOriginName, PATHINFO_FILENAME);

            // Valida si existe el nombre del archivo cargado
            $buscaTemplExist = db::table('template_questionnaires')
              ->selectRaw('COUNT(qtfk) as total')
              ->where('qtnamefile','=',$filename.".".strtolower($extension))
              ->where('qtqufk','=',$request->idCuestionario)
              ->get();

            if( $buscaTemplExist[0]->total > 0 ){

              $files->move($path, $filename.".".strtolower($extension));

            }else{

              $fileCreate = TemplateQuestionnaire::create([
                  'qtqufk' => $request->idCuestionario,
                  'qtnamefile' => $filename.".".strtolower($extension),
                  'qtroute' => $rutaLocal,
                  'qtnopags' => $request->noPaginas,
                  'qtsize' => $tamañoArchivoByte,
                  'qttypfil' => $tipoFile,
                  'qtip' => $request->ip(),
                  'qtuserfk' => $request->user()->id,
                  'qtinsertdt' => $fechaActual2,
                  'qtupddt' => $fechaActual2,
              ]);

              if( $files->move( $path, $filename.".".strtolower($extension) ) ){
                $fileCreate->save();
              }


            }




            return response()->json([
              "mensaje" => "creado"
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $sttusUP="";

        if( $request->deleteAllFiles == 1){
            if($request->datos != ""){
              foreach($request->datos as $datos){
                if($datos > 0){
                      //Busca los templates que seleccionaron
                      $qryVal = TemplateQuestionnaire::find($datos);

                      // Valida si estan en estatus 1 actualiza a cero
                      if( $qryVal->qtstatus == 1 ){ $statsOk=0;
                      // Valida si estan en estatus 0 actualiza a activo
                      }else{ $statsOk=1; }

                      $qryValUP = TemplateQuestionnaire::find($datos);
                      $qryValUP->qtstatus = $statsOk;
                      $qryValUP->save();

                }
              }
            }

        }else{
          if($request->status == 1){
            $sttusUP=0;
          }else{
            $sttusUP=1;
          }

          $updtFilTmplCuest = TemplateQuestionnaire::find($id);
          $updtFilTmplCuest->qtstatus	= $sttusUP;
          $updtFilTmplCuest->save();

        }



        return response()->json([
          "mensaje" => "creado"
        ]);
    }
}
