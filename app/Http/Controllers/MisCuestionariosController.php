<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use App\CompanyAnswer;
use App\AnswerResponse;
use App\StatusQuestionnarie;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\catalog_master;
use App\quotations;

class MisCuestionariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $folio = Auth::user()->folio;

          $cuestionarios = DB::table('company_answers')
            ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
            ->join('answer_type_questionnaires','questionnaire_answers.qaatqfk','=','answer_type_questionnaires.atqbk')
            ->join('questionnaires','answer_type_questionnaires.atqqufk','=','questionnaires.qufk')
            ->join('questionnaries_status','company_answers.casqfk','=','questionnaries_status.sqfk')
            ->select('answer_type_questionnaires.atqqufk AS id','questionnaires.quname as description' ,'questionnaries_status.sqstatus AS status')
            ->where('company_answers.cafoliofk','=',$folio)
            ->groupBy('answer_type_questionnaires.atqqufk' ,'questionnaires.quname' ,'questionnaries_status.sqstatus')
            ->paginate(10);
              $status = catalog_master::select('cmdesc','cmval','cmimg')
              ->where('cmstatus','=','2')
              ->where('cmtpcat','=','60')
              ->get();

          return view('miscuestionarios.index',compact('cuestionarios','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::get();
          return view('miscuestionarios.users',compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $folio="";
        // Folio del usuario que se loqueo
        if( $request->folioAudita == ""){
            $folio = Auth::user()->folio;

        }else{
            // Folio del usuario que se seleccion para ver sus respuestas en el perfil de usuario Auditor
            $folio = $request->folioAudita;

        }
        /*if( Auth::user()->hasRole('Auditor') ){
           $camposAud = array(0);
        }else{
           $camposAud = array(105,104);
        }*/
        $empresa = $request->empresa;
         $id2 = $request->id2;
         $idcues = $id;
         // $users = User::select('folio','email')
         // ->where('folio','=',$folio)
         // ->get();
         //
         //  foreach($users as $user){
         //      $email = $user->email;
         //    return $user->email;
         //  }


        $cuestPreguntas = DB::table('company_answers')
          ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
          ->join('questions','questionnaire_answers.qaqsfk','=','questions.qsfk')
          ->join('answer_type_questionnaires','questionnaire_answers.qaatqfk','=','answer_type_questionnaires.atqbk')
          ->select('questions.qsfk AS idPregunta','questions.qsname AS pregunta','answer_type_questionnaires.atqqufk AS idCuestion')
          ->where('answer_type_questionnaires.atqqufk','=',$id)
          ->where('company_answers.cafoliofk','=',$folio)
          ->where('questionnaire_answers.qastatus','=','1')
          ->groupBy('questions.qsfk','questions.qsname','answer_type_questionnaires.atqqufk')
          ->orderBy('questions.qsfk')
          ->get();

          $cuestRespuestas = DB::table('company_answers')
            ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
            ->join('questions','questionnaire_answers.qaqsfk','=','questions.qsfk')
            ->join('answer_type_questionnaires','questionnaire_answers.qaatqfk','=','answer_type_questionnaires.atqbk')
            ->join('answers','questionnaire_answers.qaanfk','=','answers.anfk')
            ->select('questions.qsfk AS idPregunta','questions.qsname AS pregunta','answers.anfk AS idRespuesta',
                'answers.anname AS respuesta','answers.anatfk AS tipoCaja','company_answers.cafk AS idCA',
                'company_answers.cavalue AS value','answers.anvalue AS valCheck')
            ->where('answer_type_questionnaires.atqqufk','=',$id)
            ->where('company_answers.cafoliofk','=',$folio)
            ->where('questionnaire_answers.qastatus','=','1')
            ->orderBy('questions.qsfk')
            ->get();

              $secciones = DB::table('company_answers')
              ->join('questionnaire_answers', 'company_answers.caqafk','=','questionnaire_answers.qafk')
              ->join('catalogs', 'questionnaire_answers.qascafk','=','catalogs.catfk')
              ->select('catalogs.catfk', 'catalogs.catvalue AS seccion')
              ->where('questionnaire_answers.qastatus', '=', 1)
              ->where('catalogs.catnamtabl', '=', 'questionnaries_section')
              ->where('company_answers.cafoliofk', '=', $folio)
              ->where('questionnaire_answers.qaatqfk','=',$id)
              ->groupBy('catalogs.catfk')
              ->orderby('catalogs.catfk', 'asc')
              ->get();

              $idCuestionario = DB::table('questionnaires')
              ->select('quname')
              ->where('qufk','=',$id)
              ->get();

              $cuestionarios = DB::table('company_answers')
                ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
                ->join('answer_type_questionnaires','questionnaire_answers.qaatqfk','=','answer_type_questionnaires.atqbk')
                ->join('questionnaires','answer_type_questionnaires.atqqufk','=','questionnaires.qufk')
                ->join('questionnaries_status','company_answers.casqfk','=','questionnaries_status.sqfk')
                ->select('answer_type_questionnaires.atqqufk AS id','questionnaires.quname as description' ,'questionnaries_status.sqstatus AS status')
                ->where('company_answers.cafoliofk','=',$folio)
                ->where('questionnaire_answers.qastatus','=','1') // Obtiene los registros con estatus 1 de alta
                ->groupBy('answer_type_questionnaires.atqqufk' ,'questionnaires.quname' ,'questionnaries_status.sqstatus')
                ->get();

              $filesTemplates = DB::table('template_questionnaires')
                ->select('template_questionnaires.qtnamefile as nameTemplate','template_questionnaires.qtsize','catalogs.catvalue as tipoArchivo')
                ->join('catalogs','template_questionnaires.qttypfil','=','catalogs.catfk')
                ->where('template_questionnaires.qtqufk','=',$id)
                ->where('template_questionnaires.qtstatus','=', 1)
                ->get();

                $status = quotations::select('qtstatus')
                ->where('qtstatus','=','1')
                ->where('qtfolio',$folio)
                ->get();
                // return $status;

      return view("miscuestionarios.edit",
            compact(
                    'email','id','idcues','cuestPreguntas','cuestRespuestas', 'cuestionarios','folio','filesTemplates','secciones','idCuestionario','id2','empresa','status'
            )
          );


    }

  public function getSubsection($id, Request $request){
     $folio="";
     $cuestionario =  $request->cuestionario;

      // Folio del usuario que se loqueo
      if( $request->folio == ""){
          $folio = Auth::user()->folio;
      }else{
          // Folio del usuario que se seleccion para ver sus respuestas en el perfil de usuario Auditor
          $folio = $request->folio;
      }

      $subsecciones = DB::table('company_answers')
      ->join('questionnaire_answers', 'company_answers.caqafk','=','questionnaire_answers.qafk')
      ->join('catalogs', 'questionnaire_answers.qasbcatfk','=','catalogs.catfk')
      ->select('catalogs.catfk AS id', 'catalogs.catvalue AS valor')
      ->where('company_answers.cafoliofk','=',$folio)
      ->where('catalogs.catnamtabl','=','questionnaries_subsection')
      ->where('questionnaire_answers.qascafk','=',$id)
      ->groupBy('catalogs.catfk')
      ->orderby('catalogs.catfk', 'asc')
      ->get();

      $filesTemplates = DB::table('template_questionnaires')
        ->select('template_questionnaires.qtnamefile as nameTemplate','template_questionnaires.qtsize','catalogs.catvalue as tipoArchivo')
        ->join('catalogs','template_questionnaires.qttypfil','=','catalogs.catfk')
        ->where('template_questionnaires.qtqufk','=',$cuestionario)
        ->where('template_questionnaires.qtstatus','=', 1)
        ->get();

        $nombre = DB::table('catalogs')
        ->select('catfk','catvalue')
        ->where('catfk',$id)
        ->get();

        $nameCuestionario = DB::table('questionnaires')
        ->select('quname')
        ->where('qufk','=',$cuestionario)
        ->get();

      return view("miscuestionarios.secciones" ,compact('nameCuestionario','nombre','folio','id','subsecciones','cuestionario','filesTemplates'));
    }

    public function getQuestions($id, Request $request){
      $seccion = $id;
      $subseccion = $request->subseccion;
      $cuestionario =  $request->cuestionario;
      $cuestionarioOriginal = $cuestionario;

      if( auth()->user()->roles[0]->id == 3 ){
         $camposAud = array(0);
      }else{
         $camposAud = array(105,104,120);
      }

      if($cuestionario == 8){
        $camposAud = array(0);
      }

      if( $request->folioAudita == ""){
          $folio = Auth::user()->folio;
      }else{
          $folio = $request->folioAudita;
      }
      $cuestionario = ($cuestionario == 8) ? "7" : $cuestionario;

      $secciones = DB::table('company_answers')
      ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
      ->leftjoin('questions','questionnaire_answers.qaqsfk','=','questions.qsfk')
      ->leftjoin('answers','questionnaire_answers.qaanfk','=','answers.anfk')
      ->leftjoin('answer_type_questionnaires','questionnaire_answers.qaatqfk','=','answer_type_questionnaires.atqbk')
      ->leftjoin('questionnaires','answer_type_questionnaires.atqqufk','=','questionnaires.qufk')
      ->leftjoin('catalogs','questionnaire_answers.qascafk','=','catalogs.catfk')
      ->leftjoin ('catalogs as catsub','questionnaire_answers.qasbcatfk','=','catsub.catfk')
      ->select(
        'questionnaires.qufk AS idCuestionario',
        'questionnaires.quname AS NameCuestionario',
        'catalogs.catfk as idSeccion', 'catalogs.catvalue as seccion',
        'catsub.catfk as idSubSeccion', 'catsub.catvalue as subseccion',
        'questions.qsfk AS idPregunta', 'questions.qsname AS pregunta',
        'answers.anfk AS idRespuesta','answers.anname AS respuesta',
        'answers.anvalue AS valPonder',
        'answers.anatfk AS tiporesp',
        'company_answers.cafk as idRespPreg','company_answers.cavalue AS value'
       )
      ->where('answer_type_questionnaires.atqqufk','=', $cuestionario)
      ->where('company_answers.cafoliofk','=',$folio)
      ->whereNotIn('answers.anfk',$camposAud)
      ->where('questionnaire_answers.qastatus','=','1')
      ->where('catalogs.catfk','=',$seccion)
      ->where('catsub.catfk','=',$subseccion)
      ->orderby('answers.anfk')
      ->get();

      $filesTemplates = DB::table('template_questionnaires')
      ->select('template_questionnaires.qtnamefile as nameTemplate','template_questionnaires.qtsize','catalogs.catvalue as tipoArchivo')
      ->join('catalogs','template_questionnaires.qttypfil','=','catalogs.catfk')
      ->where('template_questionnaires.qtqufk','=',$cuestionario)
      ->where('template_questionnaires.qtstatus','=', 1)
      ->get();

      $totalPreguntasCuestioario = DB::table('company_answers')
      ->select("catalogs.catfk")
      ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
      ->leftjoin('questions','questionnaire_answers.qaqsfk', '=', 'questions.qsfk')
      ->leftjoin('answers', 'questionnaire_answers.qaanfk', '=', 'answers.anfk')
      ->leftjoin('answer_type_questionnaires', 'questionnaire_answers.qaatqfk', '=', 'answer_type_questionnaires.atqbk')
      ->leftjoin('questionnaires', 'answer_type_questionnaires.atqqufk', '=', 'questionnaires.qufk')
      ->leftjoin('catalogs', 'questionnaire_answers.qascafk', '=', 'catalogs.catfk')
      ->leftjoin('catalogs as catsub', 'questionnaire_answers.qasbcatfk', '=', 'catsub.catfk')
      ->where('answer_type_questionnaires.atqqufk','=',$cuestionario)
      ->where('company_answers.cafoliofk', '=', $folio)
      ->whereNotIn('answers.anfk',$camposAud)
      ->where('questionnaire_answers.qastatus','=','1')
      ->distinct()
      ->count('questions.qsname');

      $contador = 0;
      while ($contador < count($secciones)) { //Extraemos los datos del array de uno en uno mientras haya datos
        $arrayIdCuestionario[$secciones[$contador]->idCuestionario] = $secciones[$contador]->idCuestionario;
        $arrayIdSecciones[$secciones[$contador]->idSeccion] = $secciones[$contador]->idSeccion;
        $arrayIdSubSecciones[$secciones[$contador]->idSubSeccion] = $secciones[$contador]->idSubSeccion;
        $arrayIdPreguntas[$secciones[$contador]->idPregunta] = $secciones[$contador]->idPregunta;
        $arrayIdRespuesta[$secciones[$contador]->idRespuesta] = $secciones[$contador]->idRespuesta;
        //$arrayIdPregResp[$secciones[$contador]->idRespPreg] = $secciones[$contador]->idRespPreg;

        $arrayNameCuestionario[$secciones[$contador]->idCuestionario] = $secciones[$contador]->NameCuestionario;
        $arrayNameSeccion[$secciones[$contador]->idSeccion] = $secciones[$contador]->seccion;
        $arrayNameSubseccion[$secciones[$contador]->idSeccion][$secciones[$contador]->idSubSeccion] = $secciones[$contador]->subseccion;
        $arrayNamePregunta[$secciones[$contador]->idSeccion][$secciones[$contador]->idSubSeccion][$secciones[$contador]->idPregunta] = $secciones[$contador]->pregunta;
        $arrayNameRespuesta[$secciones[$contador]->idSeccion][$secciones[$contador]->idSubSeccion][$secciones[$contador]->idPregunta][$secciones[$contador]->idRespuesta] = $secciones[$contador]->respuesta;
        $arrayTipoRespuesta[$secciones[$contador]->idSeccion][$secciones[$contador]->idSubSeccion][$secciones[$contador]->idPregunta][$secciones[$contador]->idRespuesta] = $secciones[$contador]->tiporesp;
        // Valor de ponderacion de laa respuesta asignada
        $arrayValPondRespuesta[$secciones[$contador]->idSeccion][$secciones[$contador]->idSubSeccion][$secciones[$contador]->idPregunta][$secciones[$contador]->idRespuesta] = $secciones[$contador]->valPonder;
        $arrayIdPregRep[$secciones[$contador]->idSeccion][$secciones[$contador]->idSubSeccion][$secciones[$contador]->idPregunta][$secciones[$contador]->idRespuesta]
                        = $secciones[$contador]->idRespPreg;
        $arrayPregRepValue[$secciones[$contador]->idSeccion][$secciones[$contador]->idSubSeccion][$secciones[$contador]->idPregunta][$secciones[$contador]->idRespuesta]
                        = $secciones[$contador]->value;
        $contador++;
      }

      $cuestionario = $cuestionarioOriginal;
      $id = $cuestionario;
      $nombre = DB::table('catalogs')
      ->select('catfk','catvalue')
      ->where('catfk',$seccion)
      ->get();

      if($cuestionario == 8){
         $nameQuestion = DB::table('questionnaires')
         ->select('quname')
         ->where('qufk','=','8')
         ->get();

         unset($arrayNameCuestionario);
         $arrayNameCuestionario[7] = $nameQuestion[0]->quname;
      }

      return view("miscuestionarios.preguntas" ,
            compact(
                    'arrayIdSecciones','arrayNameSeccion','arrayIdSubSecciones','arrayNameSubseccion','arrayIdPreguntas','arrayNamePregunta',
                    'arrayIdRespuesta','arrayNameRespuesta','arrayTipoRespuesta','arrayIdPregRep','arrayPregRepValue','arrayValPondRespuesta',
                    'id','cuestPreguntas','cuestRespuestas','contador', 'cuestionarios','arrayIdCuestionario','arrayNameCuestionario','folio',
                    'filesTemplates','id','totalPreguntas','totalPreguntasCuestioario','nombre'
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $folio = Auth::user()->folio;

      $this->validaPresguntaCuest($request->idCuestion, $request->idPregunta, $folio, $request->totPreguntas, $request->ip(), $request->user()->id,
          $request->noPregunta, $request->idSubseccion);

          // Recibe las respuestas de tipo radiobutton (varible del .js)
         if( $request->tipoRespuesta == 4  ){
           // Define si seleccionaron un radio button con cuestionarios con secciones
           if($request->secciones == 1){
                 $actResp = DB::table('company_answers')
                   ->select('company_answers.cafk AS id')
                   ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
                   ->join('answers','questionnaire_answers.qaanfk','=','answers.anfk')
                   ->where('company_answers.cafoliofk','=', $folio )
                   ->where('answers.anatfk','=', $request->tipoRespuesta )
                   ->where('questionnaire_answers.qaqsfk','=', $request->idPregunta )
                   ->where('questionnaire_answers.qascafk','=', $request->idSeccion)
                   ->where('questionnaire_answers.qasbcatfk','=', $request->idSubseccion)
                   ->get();

                foreach($actResp as $actR){
                    $seteo1 = CompanyAnswer::find($actR->id);
                    $seteo1->cavalue = '';
                    $seteo1->save();

                    $seteo2 = CompanyAnswer::find($id);
                    $seteo2->fill($request->all());
                    $seteo2->cavalue = $request->value;
                    $seteo2->caip = $request->ip();
                    $seteo2->causerfk = $request->user()->id;
                    $seteo2->save();
                }
           // Define si seleccionaron un radio button con cuestionarios sin secciones
         }else{
             $actResp = DB::table('company_answers')
               ->select('company_answers.cafk AS id')
               ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
               ->where('company_answers.cafoliofk','=', $folio )
               ->where('questionnaire_answers.qaqsfk','=', $request->idPregunta )
               ->get();

               foreach($actResp as $resp){
                 $seteo1 = CompanyAnswer::find($resp->id);
                 $seteo1->cavalue = '';
                 $seteo1->save();

                 $seteo2 = CompanyAnswer::find($id);
                 $seteo2->fill($request->all());
                 $seteo2->cavalue = $request->value;
                 $seteo2->caip = $request->ip();
                 $seteo2->causerfk = $request->user()->id;
                 $seteo2->save();
              }
           }
        }else{
          // Recibe las respuestas difetentes al radio button
          $respuesta = CompanyAnswer::find($id);
          $respuesta->fill($request->all());
          $respuesta->cavalue = $request->value;
          $respuesta->caip = $request->ip();
          $respuesta->causerfk = $request->user()->id;
          $respuesta->save();

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function validaPresguntaCuest($idCuest, $idPregunta, $folio, $totPreg, $ip, $user, $noPregunta, $countSubsecc ){
      $carbon = new Carbon();
      $date = Carbon::now();
      $fechaActual = $date->format('Y-m-d H:i:s');

        // Asigna el valor de la variable del numero de pregunta por el contador del numero de secciones en caso de no existir numero de pregunta
        if( $countSubsecc != "" ){
             $noPregunta = $countSubsecc;
        }

        // Cuenta los numero
        $actResp = DB::table('answered_response')
          ->select('arcount','arqsfk','arfk')
          ->where('arqufk','=', $idCuest )
          ->where('arfoliofk','=', $folio )
          ->get();

          if( count($actResp) == 0 ){

              AnswerResponse::create([
                  'arqufk' => $idCuest,
                  'arqsfk' => $noPregunta.",",
                  'arfoliofk'  => $folio,
                  'arip' => $ip,
                  'arusuerfk' => $user,
                  'arcount' => 1
              ]);

           }else{

               // Obtiene en un array las preguntas
               $cade = explode(",",$actResp[0]->arqsfk);

               // Busca una Pregunta en el array
               //$res = in_array($noPregunta, $cade);
               $res = in_array($idPregunta,$cade);

               // Validan si existen los datos en el arreglo
               if( $res == 1 ){
                 //echo "exist";
               }else{
                 // echo "no existe";
                 // $preguntasConcat = $actResp[0]->arqsfk.$noPregunta.",";
                 $preguntasConcat = $actResp[0]->arqsfk.$idPregunta.",";

                 $updtResp = AnswerResponse::where('arfoliofk', $folio)
                                           ->where('arqufk', $idCuest)->first();
                 $updtResp->arcount = $actResp[0]->arcount + 1;
                 $updtResp->arqsfk = $preguntasConcat;
                 $updtResp->save();
               }

               $actRespI = DB::table('answered_response')
                 ->select('arcount')
                 ->where('arqufk','=', $idCuest )
                 ->where('arfoliofk','=', $folio )
                 ->get();

               // Valida el cambio de estatus al momento de actualizar las respuestas
               if( $actRespI[0]->arcount ==  $totPreg ){
                 echo "cambio de estatus";
                 DB::table('questionnaries_status')
                 ->where('sqfoliofk', $folio )
                 ->where('sqqufk', $idCuest )
                 ->update(['sqstatus' => 2 ,'sqcuestermino' => $fechaActual]);

                 $nombre = Auth::user()->name;
                 $appaterno = Auth::user()->apaternal;
                 $amaternal = Auth::user()->amaternal;
                 $folio = Auth::user()->folio;
                 $correo = Auth::user()->email;

                 $dates = array('name'=> $nombre ,'appaterno' => $appaterno, 'amaternal' => $amaternal ,'folio'=>$folio,'correo' => $correo,'fechaActual' =>$fechaActual);
                 $email = "zumaserver@zuma-ti.com.mx";
                 $this->Email($dates,$email);
               }
          }
    }

    // envio de correo
    function Email($dates,$email){
    Mail::send('emails.cuestionario',$dates, function($message) use ($email){
      $message->subject('Bienvenido a la plataforma');
      $message->to($email);
      $message->from('no-repply@geeklife.com.ve','CIS');
    });
  }
}
