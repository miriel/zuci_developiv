<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Session;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Validation\Rule;

class UsersPermissionsController extends Controller
{
  public function update(Request $request, User $user)
  {
    $user->permissions()->detach();

    if($request->filled('permissions'))
    {
        $user->givePermissionTo($request->permissions);
    }

    Session::flash('flash_success','Los Permisos an sido actualizados');
    return back();
  }
}
