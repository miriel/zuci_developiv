<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\flash;
use App\Http\Requests\TypeDocumentRequest;
use Session;
use DB;
use App\Catalogs;
use Illuminate\Routing\Redirector;

class TipoArchivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = $request->status;
        $status = ($status == '') ? 1 : $status;

        $tiposArchivo = DB::table('catalogs')->select('catfk','catnamtabl','catvalue','catstatus','cattype','catacronym')->where('cattype','=','file')->where('catstatus','=',1)->paginate(10,['*'],'tipo1');
        $tiposArchivoBajas = DB::table('catalogs')->select('catfk','catnamtabl','catvalue','catstatus','cattype','catacronym')->where('cattype','=','file')->where('catstatus','=',0)->paginate(10,['*'],'tipo2');

        return view('catalogos.tiposArchivo.index', compact('tiposArchivo','status','tiposArchivoBajas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(TypeDocumentRequest $request)
    {
        if($_GET){

          $status = ($_GET['status'] == 'A') ? 1 : 0;

          $this->authorize('create', new Catalogs);
          $archivo = new Catalogs;
          $archivo->catnamtabl = 'document_type';
          $archivo->catvalue = $_GET['catValueAlta'];
          $archivo->catstatus = $status;
          $archivo->catip = $_SERVER['REMOTE_ADDR'];
          $archivo->catuserfk = auth()->user()->id;
          $archivo->catinsertdt = now();
          $archivo->catupddt = now();
          $archivo->cattype = 'file';
          $archivo->catacronym = $_GET['catCronymAlta'];
          $result = $archivo->save();

          if($result){
            Session::flash('flash_success','El registro se creo satisfactoriamente!');
            return response()->json([
              "mensaje" => "creado"
            ]);
          }else{
            Session::flash('flash_delete','Hubo un problema al intentar crear el registro, intente de nuevo por favor!');
            return response()->json([
              "mensaje" => "fallo"
            ]);
          }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $texto = Catalogs::find($id);
        return response()->json($texto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request, [
            'catvalue' => 'required',
            'catacronym' => 'required',
       ]
       // ,
       // [
       //   'catvalue.required' => 'El campo es obligatorio',
       //   'catacronym.required' => 'El campo es obligatorio',
       // ]
     );

      $status = ($request->status == 'A') ? 1 : 0;

      $archivo = Catalogs::find($request->idTipo);
      $archivo->catvalue = $request->catvalue;
      $archivo->catacronym = $request->catacronym;
      $archivo->catstatus = $status;
      $archivo->catupddt = now();
      $archivo->save();

      Session::flash('flash_success','El registro se actualizo satisfactoriamente!');
      return response()->json([
        "mensaje" => "creado"
      ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $status = ($request->status == 1) ? 0 : 1;

        $empresa = Catalogs::find($id);
        $empresa->catstatus=$status;
        $empresa->save();

        if($status == 1){
          Session::flash('flash_success','El registro se activo satisfactoriamente!!');
        }else{
          Session::flash('flash_delete','El tipo de archivo fue eliminado con exito!!');
        }
        return back();
    }
}
