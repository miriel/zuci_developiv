<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\UserController;
use App\User;
use App\quotation_user_profiles;
use Redirect;
use Session;
use Illuminate\Support\Facades\Mail;
use DB;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use App\Events\UserWasCreated;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(request $request)
     {
       // $giro = Company_Turn::where('id_user',auth()->id())->orderBy('id','DESC')->paginate();
          // $users = User::allowed()->get();
          // $users = User::where('id', auth()->id())->get();
          // $users = auth()->user()->user;
          // $users = User::get();
          // $this->authorize('view',new User );
          // $users = User::allowed()->get();
          // datos de session
          // $data = $request->session()->all();
          // $us = auth()->user()->id;
          // $user = DB::table('roles')
          // ->select('roles','model_has_roles.model_id as pivot_model_id','model_has_roles.role_id as pivot_role_id')
          // ->leftjoin('model_has_roles','model_has_roles.role_id','=','roles.id')
          //  ->where('model_has_roles.model_id',$us )
          //  ->where('model_has_roles.model_type','=','App\User')
          //  ->get();
           // return $user;
          $users = User::orderBy('id','ASC')->where('status','=','1')->get();
          return view('catalogos.user.index')->with('users',$users);
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
       // $this->autorize('create', new $user);
       $lista = User::orderBy('id','ASC')->where('status','=','1')->get();
       $user = new User;
       $this->authorize('create',new User );
       $campos = array(1,5);
       // $roles = Role::with('permissions')->whereNotIn('id',$campos)->get();
       $folio = auth()->user()->folio;
       $roles = quotation_user_profiles::select('qupfk','qupquantity','roles.id','roles.name')
       ->join('roles','roles.id','=','quotation_user_profiles.qupid')
       ->where('quotation_user_profiles.qupfolio',$folio)
       ->where('quotation_user_profiles.qupstatus','=','2')
       ->get();

       $permissions = Permission::select('name','id','tipo_catalogo')->orderBy('id','ASC')->get();

       return view('catalogos.user.create',compact('user','roles','permissions','lista'));

     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */

      //Funcion que genera el codigo //
           function generarCodigo($longitud) {
            $key = '';
            $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
            $max = strlen($pattern)-1;
            for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
            return $key;
       }
     public function store(Request $request)
     {
       $this->authorize('create', new User);
       //validar formulario
      $data =  $request->validate([
         'name' => 'required|max:40',
         'email' => 'required|email|max:255|unique:users',
         'apaternal' => 'required|string|regex:/^[\pL\s\-\.]+$/u|min:3|max:40',
         'amaternal' => 'required|string|regex:/^[\pL\s\-\.]+$/u|min:3|max:40',
         'wkrkarea' => 'required|min:3|max:40',
         'job' => 'required|min:3|max:40',
         'lada' => 'required',
         'phone1' => 'required|numeric|max:9999999999',
         'codigoGestor' => 'required|string|min:3|max:8',
       ]);
       //generar una contraseña
        $data['password'] = str_random(8);
       // //crear Usuario
        $id = Auth::user()->id;
        $tipo = Auth::user()->ctfk;
        $folio = Auth::user()->folio;
        $date2 = new Carbon('tomorrow');
        $fechalilmite = $date2->format('Y-m-d H:i:s');
        $code = $this->generarCodigo(8);
       $user = new User($data);
       $user->name = $data['name'];
       $user->apaternal = $data['apaternal'];
       $user->amaternal = $data['amaternal'];
       $user->folio = $folio;
       $user->code = $code;
       $user->expiredcode = $fechalilmite;
       $user->wkrkarea = $data['wkrkarea'];
       $user->job = $data['job'];
       $user->phone_fixed = $data['phone1'];
       $user->lada_cel = $data['lada'];
       $user->ctfk = $tipo;
       $user->password = bcrypt($data['password']);
       $user->supervisor = $request['supervisor'];
       $user->tipouser = $request['tipouser'];
       $user->codigoGestor = $data['codigoGestor'];
       $user->save();

       //asignamos los Roles
       // filleds significa si selecciono algun campo de roles
       if($request->filled('roles'))
       {
         $user->assignRole($request->roles);
       }
       //asignamos Permisos
       if($request->filled('permissions'))
       {
       $user->givePermissionTo($request->permissions);
       }
       //enviamos el email
       UserWasCreated::dispatch($user,$data['password']);

       // Evento => UsuarioCreado
       //Listener => Enviar Correo COn Credenciales
       //listener => enviar Correo al Administrador
       //regresamos una respuesta al usuario
       Session::flash('flash_success','El Usuario ha sido creado');
       return redirect()->route('user.index');
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show(User $user)
     {
       $this->authorize('view', $user);
         return view('catalogos.user.show',compact('user'));
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit(User $user)
     {
        $this->authorize('update', $user);

        $roles = Role::with('permissions')->get();
       $permissions = Permission::get();

         return view('catalogos.user.edit',compact('user','roles','permissions'));
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(UpdateUserRequest $request, User $user)
     {
       $this->authorize('update', $user);

       $user->update($request->validated());
       $user->password=bcrypt($request->password);
       // $user->user_id=$request->user()->id;
       // dd($giro);
       $user->save();

        // $user->Update($request->validate($rules) );


        Session::flash('flash_success','Usuario actualizado');
        return redirect()->route('user.edit', $user);


     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy(User $user)
     {
         $this->authorize('delete', $user);
         $user->delete();
         Session::flash('flash_success','Usuario actualizado');
         return redirect()->route('user.index');

     }

    public function activate($code)
    {
      $carbon = new Carbon();
      $date = Carbon::now();
      $fechaActual = $date->format('Y-m-d H:i:s');

      $users = DB::table('users')
      ->select('expiredcode','code')
      ->where('expiredcode', '>=', $fechaActual)
      ->Where('code',$code)
      ->get();

      if ($users->isEmpty())
       {
         Session::flash('flash_expired','Disculpe las molesticas su Tiempo para activar su cuenta ha caducado. Ingrese su correo para reenviarlo');
          return redirect::to('reactivacion');
      }else{
        $users = User::where('code',$code);
        $exist = $users->count();
        $user = $users->first();

          if($exist == 1 and $user->active == 0)
          {
            $id = $user->id;
            return view('auth.date_complete',compact('id'));
          }else{
            return redirect::to('/');
          }
      }
    }
    public function complete(UserRequest $request,$id)
    {
      $user = User::find($id);
      $user->password = bcrypt($request->password);
      // $user->password = $request->password;
      $user->active = 1;
      $user->email;
      $user->folio;
      $user->save();
      $email =$user->email;
      $dates = array('email'=>$user->email,'folio'=>$user->folio);
       $this->Email($dates,$email);
      Session::flash('flash_activacion','Tu cuenta ha sido verificada ya puedes iniciar session ');
    return redirect('/login')->with('message');
    }

    function Email($dates,$email){
    Mail::send('emails.reenvio',$dates, function($message) use ($email){
      $message->subject('Bienvenido a la plataforma');
      $message->to($email);
      $message->from('no-repply@geeklife.com.ve','CIS');
    });
  }


        public function validar(Request $request)
    {
    //validacion del campo email
       $this->validate($request, [
           'email' => 'required|email',
       ]);

       // Retorna una colección vacía en caso de no encontrar un registro.
       $correo = User::where('email', $request->email)->get();
        // $correo = DB::table('companies')->where('buemail', $request->email )->pluck('buname','buapaternal','buamaternal','bufolio');
       // Continúan las instrucciones porque el usuario sí existe.
       if ($correo->isEmpty()) {
         Session::flash('flash_errror', 'Correo es invalido');
         return redirect('reactivacion');
          // Devuelve un mensaje indicando que no existe un usuario con el id recibido.
          }else {
            $dates = DB::table('users')
            ->select('id','code','email','name','expiredcode')
            ->where('email','=',$request->email)
            ->get();
            $id = $dates[0]->id;

            $date2 = new Carbon('tomorrow');
           $fechalilmite = $date2->format('Y-m-d H:i:s');

            $fecha = User::find($id);
            $fecha->expiredcode = $fechalilmite;
            $fecha->save();


          $this->Email2($dates, $request->email);
          Session::flash('flash_envio', 'Se ha Enviado un link a tu correo');
          return redirect('login');
      //    Devuelve un mensaje indicando que si existe el usuario recibido y se manda ala funcion Email para envio de correo.
          }
    }

    public function Email2($dates, $email){
      Mail::send('emails.reenviolink',compact('dates'), function($message) use ($email){
        $message->subject('Reenvio de activacion');
        $message->to($email);
        $message->from('pedro.meza@zuma-ti.mx','CIS');
      });
    }

}
