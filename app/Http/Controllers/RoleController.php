<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Session;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view', new Role);

       return view('catalogos.roles.index',[
         'roles'=> Role::orderBy('id','ASC')->get(),
     ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create',$role = new Role);
        return view('catalogos.roles.create',[
          'permissions' => Permission::pluck('name','id'),
          'role' => $role
      ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $this->authorize('create', new Role);
        $data= $request->validate([
          'name'=>'required|unique:roles',
          'display_name'=>'required',
        ],[
          // 'name.required'=> trans('welcome.home'),llmar desde el lenguaje el mensaje
          'name.required'=>'El campo identificador es obligatorio',
          'name.unique'=>'El campo identificador ya esta registrado',
          'display_name.required'=>'El campo nombre es obligatorio',
        ]);
        $role =Role::create($data);

        if($request->has('permissions'))
        {
            $role->givePermissionTo($request->permissions);
        }
        Session::flash('flash_success','Rol agregado');
        return redirect()->route('roles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
      $this->authorize('update', $role);
        return view('catalogos.roles.edit', [
          'role' => $role,
          'permissions' => Permission::pluck('name','id'),

      ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
      $this->authorize('update', $role);
      $data= $request->validate([
        'display_name' => 'required'],
        ['display_name.required'=>'El campo nombre es obligatorio.']);

      $role->update($data);
      $role->permissions()->detach();
      if($request->has('permissions'))
      {
          $role->givePermissionTo($request->permissions);
      }
      Session::flash('flash_success','El rol fue actualizado correctamente');
      return redirect()->route('roles.edit', $role);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {

      $this->authorize('delete', $role);

        $role->delete();
        Session::flash('flash_success','El rol fue eliminado correctamente');
        return redirect()->route('roles.index', $role);
    }
}
