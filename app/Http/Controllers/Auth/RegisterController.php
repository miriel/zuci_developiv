<?php

namespace App\Http\Controllers\Auth;


use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\Company;
use App\Company_Type;
use App\Company_Turn;
use App\Country;
use App\Cities;
use App\answered_analysis;
use DB;
use Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use Session;
use App\ConfigCompany;
use App\CompanyAnswer;
use App\QuestionnaireNotes;
use App\StatusQuestionnarie;
use App\Role;
use App\Model_has_roles;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */




        public function showRegistrationForm( Request $request)
      {
        // Envia el valor de descripcion y el id para los selects con estatus 1 = activos
        $tipo = Company_Type::where('ctstatus','=','1')->pluck('ctname','ctfk');
        $giro = Company_Turn::where('cotstatus','=','1')->pluck('cotname','cotfk');
        $countries = Country::where('cnstatus','=','1')->pluck('cnname','cnfk');

        $arryVals = array( 'giro' => $giro, 'tipo' => $tipo, 'countries' => $countries);
            return view('auth.register', compact('arryVals'));
      }

      public function getCities(Request $request, $id){

        $city = DB::table('cities')
          ->join('country_cities','country_cities.cccifk','=','cities.cifk')
          ->select('cities.ciname','country_cities.cccifk','cities.cifk','cities.cilada','cities.cistatus')
          ->where('country_cities.cccnfk','=',$id)
          ->where('cities.cistatus','=','1')
          ->get();

          return response::json($city);
       }

             public function getlada(Request $request, $id){
               $lada = DB::table('cities')
                 ->join('country_cities','cities.cifk','=','country_cities.cccifk')
                 ->select('cities.ciname','country_cities.ccfk','cities.cilada','cities.cifk')
                 ->where('country_cities.cccifk','=',$id)
                 ->get();

                 $city = DB::table('cities')
                   ->join('country_cities','cities.cifk','=','country_cities.cccifk')
                   ->select('cities.ciname','country_cities.cccifk','cities.cifk','cities.cilada')
                   ->where('country_cities.cccnfk','=',$id)
                   ->where('cities.cistatus','=','1')
                   ->get();

                 return response::json($lada);
              }

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    //Funcion que genera el codigo //
         function generarCodigo($longitud) {
          $key = '';
          $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
          $max = strlen($pattern)-1;
          for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
          return $key;
     }
     //Fin de la funcion/

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'razon' => 'required|string|min:3|max:80',
            'nombreempresa' => 'required|string|min:3|max:80',
            'name' => 'required|string|regex:/^[\pL\s\-]+$/u|min:3|max:30',
            'email' => 'required|string|email|max:80|unique:users',
            'ApellidoPaterno' => 'required|string|regex:/^[\pL\s\-\.]+$/u|min:3|max:40',
            'apellidomaterno' => 'required|string|regex:/^[\pL\s\-\.]+$/u|min:3|max:40',
            'puesto' => 'required|min:3|max:40',
            'area' => 'required|min:3|max:40',
            'phone1' => 'required|numeric|max:9999999999',
            // 'phone2' => 'required|numeric|max:9999999999',
            'burfc' => 'required|min:10|max:13',
            'giro' => 'required|numeric|min:1',
            'tipo' => 'required|numeric|min:1',
            'countries' => 'required|numeric|min:1',
            'city' => 'required|numeric|min:1',
            // 'lada_cel' => 'required|numeric|max:9999',
            ]
            ,[
    			       'phone1.max' => ' El campo telefono no debe tener mas de 10 numeros .',
                 'phone1.required' => ' El campo telefono es requerido .',
    		    ]);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

      return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }
    protected function registered(Request $request, $user)
    {
        //
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    protected function create(array $data)
    {

      $carbon = new Carbon();
      $date = Carbon::now();
      $fechaActual = $date->format('Y-m-d H:i:s');
      $date2 = new Carbon('tomorrow');
      $fechalilmite = $date2->format('Y-m-d H:i:s');
      $folio = $date->format('YmdHis');
      $code = $this->generarCodigo(6);
      $email = $data['email'];
      $dates = array('name'=> $data['name'],'code' => $code,'folio'=>$folio);
      $this->Email($dates,$email);
      $compania=Company::create([
        'coname' => $data['nombreempresa'],
        'coemail'=>$data['email'],
        'cocotfk' => $data ['giro'],
        'coccfk' => $data['id_city'],
        'cosocrzn' => $data['razon'],
        'cowkrkarea' => $data['area'],
        'cofoliofk' => $folio,
        'corfc' => $data['burfc'],
        'coctfk' => $data['tipo'],
        'coip' => \Request::ip(),
         ]);

        $id_company =  $compania->cofk;
          $user = User::create([
            'name' => $data['name'],
            'apaternal' => $data['ApellidoPaterno'],
            'amaternal' => $data['apellidomaterno'],
            'email' => $data['email'],
            'job' => $data['puesto'],
            'folio' =>$id_company.$folio,
            'wkrkarea' => $data['area'],
            'code'=>$code,
            'phone_cel' => $data['phone2'],
            'phone_fixed' => $data['phone1'],
            'lada_cel'=>"+".$data['lada_cel'],
            'expiredcode'=> $fechalilmite,
            'ctfk'=> $data['tipo'],
            // 'password' => bcrypt($data['password']),
         ]);

          $id_user =  $user->id;
          $role = '2';
          $modelrole = Model_has_roles::create([
            'role_id' => $role,
            'model_id' =>$id_user,
            'model_type' => 'App\User',
         ]);
         $compania = Company::find($id_company);
         $compania->cofoliofk = $id_company.$folio;
         $compania->couserfk = $id_user;
         $compania->save();
      //
      // ASIGNACION DE CUESTIONARIOS DE ACUERDO AL TIPO DE EMPRESA
      //
        // Creacion de registro para los cuestionarios
        $config = DB::table('configuration_questionnaire')
            ->select('configuration_questionnaire.cqqufk AS idCuestionario')
            ->join('questionnaires','configuration_questionnaire.cqqufk','=','questionnaires.qufk')
            ->where('configuration_questionnaire.cqctfk','=', $data['tipo'] )
            ->where('configuration_questionnaire.cqstatus','=', '1' )
            ->orWhere('configuration_questionnaire.cqeverybody','=','1')
            ->get();

         foreach($config as $conf ){
               // Inserta las notas para clientes, compaÃ±ias etc.
               $insertConfComp = QuestionnaireNotes::create([ ]);
               $estatusCuest = StatusQuestionnarie::create([
                   'sqfoliofk' =>  $id_company.$folio,
                   'sqqufk' => $conf->idCuestionario,
               ]);

      //          // ID que arroja al insertar los estatus de cuestionarios
               $estatusCuestID =  $estatusCuest->sqfk;

              $pregResp = DB::table('answer_type_questionnaires')
                  ->select('questionnaire_answers.qafk AS idRespPregunta')
                  ->join('questionnaire_answers','answer_type_questionnaires.atqbk','=','questionnaire_answers.qaatqfk')
                  ->where('answer_type_questionnaires.atqqufk','=', $conf->idCuestionario  )
                  ->where('questionnaire_answers.qastatus','=','1')
                  ->get();
      //
              foreach($pregResp as $pregRsp){
                CompanyAnswer::create([
                    'caqafk' => $pregRsp->idRespPregunta,
                    'cafoliofk' => $id_company.$folio,
                    // 'caqnfk' => $insertConfComp->qnfk,
                    'casqfk' => $estatusCuestID,
                      'caip' =>  \Request::ip(),
                        'causerfk' => $id_user,

                 ]);
              }
         //End foreach
         }

        /*$config = DB::table('configuration_questionnaire')
            ->select('configuration_questionnaire.questionnaire AS idCuestionario')
            ->join('questionnaires','configuration_questionnaire.questionnaire','=','questionnaires.id')
            ->where('configuration_questionnaire.typecompany','=', $data['tipo'] )
            ->where('configuration_questionnaire.status','=', '1' )
            ->orWhere('everybody','=','true')
            ->groupBy('configuration_questionnaire.questionnaire')
            ->get();

        $concat=array();
        $concatCuestConfig=array();
        foreach($config as $con){
            // Inserta las notas para clientes, compaÃ±ias etc.
            $insertConfComp = QuestionnaireNotes::create([ ]);
            $estatusCuest = StatusQuestionnarie::create([
                'folio' => $id_company.$folio,
                'questionaire' => $con->idCuestionario,
            ]);

            // ID que arroja al insertar los estatus de cuestionarios
            $estatusCuestID =  $estatusCuest->id;

            $idRespPreguntas = DB::table('questionnaire_answers')
                   ->select('id','questionnarie')
                   ->where('questionnarie','=',$con->idCuestionario)
                   ->where('status','=','1')
                   ->get();

            // Inserta las preguntas y respuestas de acuerdo a la configuracion de los cuestionarios
            foreach($idRespPreguntas as $idRespPre){
                  CompanyAnswer::create([
                      'idpregresp' => $idRespPre->id,
                      'folio' => $id_company.$folio,
                      'idnotes' => $insertConfComp->id,
                      'statusquestionnaire' => $estatusCuestID,
                   ]);
            }
        }*/
        // $relacion =

        Session::flash('flash_registro','Se le a enviado un correo electronico para verificar su cuenta si no llega revise su SPAM Tienes 1 dia para activar tu cuenta');
        return redirect('/')->with('message');
    }


    function Email($dates,$email){
    Mail::send('emails.plantilla',$dates, function($message) use ($email){
      $message->subject('Bienvenido a la plataforma');
      $message->to($email);
      $message->from('no-repply@geeklife.com.ve','CIS');
    });
  }
  public function redirectPath()
  {
      if (method_exists($this, 'redirectTo')) {
          return $this->redirectTo();
      }

      return property_exists($this, 'redirectTo') ? $this->redirectTo : '/';
  }
}
