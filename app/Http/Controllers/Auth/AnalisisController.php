<?php

namespace App\Http\Controllers;

use App\catalog_master;
use DB;
use Illuminate\Http\Request;
use View;
use Response;

class AnalisisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $analisis = DB::table('catalog_master AS ope')
          ->leftjoin('relationship_catalogs AS op_act','op_act.rccmfk','=','ope.cmfk')
          ->leftjoin('catalog_master AS act','act.cmtpcat','=','op_act.rctpcat')
          ->leftjoin('relationship_catalogs AS act_pel','act_pel.rccmfk','=','act.cmfk')
          ->leftjoin('catalog_master AS pel','pel.cmtpcat','=','act_pel.rctpcat')
          ->leftjoin('relationship_catalogs AS pel_des','pel_des.rccmfk','=','pel.cmfk')
          ->leftjoin('catalog_master AS des','des.cmtpcat','=','pel_des.rctpcat')
          ->leftjoin('relationship_catalogs AS des_con','des_con.rccmfk','=','des.cmfk')
          ->leftjoin('catalog_master AS con','con.cmtpcat','=','des_con.rctpcat')
          ->select(
            'act.cmfk AS idOperacion',
            'act.cmdesc AS desOperacion',
            'pel.cmfk AS idActividad',
            'pel.cmdesc AS desActividad',
            'des.cmfk AS idDescripcion',
            'des.cmdesc AS desDescripcion',
            'con.cmtpcat AS idCatalogo',
            'con.cmfk AS idConsecuencias',
            'con.cmabbr AS desConsecuencias',
            'con.cmval AS valorConsecuencias'
          )
          ->where('ope.cmfk','=','1')
          // ->where('ope.status','=','1')
          // ->Where('op_act.status','=','1')
          ->Where('act.cmstatus','=','1')
          // ->Where('pel.status','=','1')
          ->where('ope.cmtpquest','=','1')
          // ->where('des.status','=','1')
          ->orderBy('act.cmfk','ASC')
          ->orderBy('pel.cmfk','ASC')
          ->orderBy('des.cmfk','ASC')
          ->orderBy('con.cmfk','ASC')
          ->get();
          $contador = 0;
          while($contador < count($analisis)){
            $arrayIdOperacion[$analisis[$contador]->idOperacion] = $analisis[$contador]->idOperacion;
            $arrayNameOperacion[$analisis[$contador]->idOperacion] = $analisis[$contador]->desOperacion;

            $arrayIdActividad[$analisis[$contador]->idActividad] = $analisis[$contador]->idActividad;
            $arrayNameActividad[$analisis[$contador]->idOperacion][$analisis[$contador]->idActividad] = $analisis[$contador]->desActividad;

            $arrayIdDescripcion[$analisis[$contador]->idDescripcion] = $analisis[$contador]->idDescripcion;
            $arrayNameDescripcion[$analisis[$contador]->idOperacion][$analisis[$contador]->idActividad][$analisis[$contador]->idDescripcion] = $analisis[$contador]->desDescripcion;

            $arrayIdCatalogo[$analisis[$contador]->idCatalogo] = $analisis[$contador]->idCatalogo;
            $arrayNameCatalogo[$analisis[$contador]->idOperacion][$analisis[$contador]->idActividad][$analisis[$contador]->idDescripcion][$analisis[$contador]->idCatalogo]= $analisis[$contador]->idCatalogo;

            $arrayIdConsecuencias[$analisis[$contador]->idConsecuencias] = $analisis[$contador]->idConsecuencias;
            $arrayNameConsecuencias[$analisis[$contador]->idOperacion][$analisis[$contador]->idActividad][$analisis[$contador]->idDescripcion][$analisis[$contador]->idCatalogo][$analisis[$contador]->idConsecuencias]= $analisis[$contador]->desConsecuencias;
            $arrayValorConsecuencias[$analisis[$contador]->idOperacion][$analisis[$contador]->idActividad][$analisis[$contador]->idDescripcion][$analisis[$contador]->idCatalogo][$analisis[$contador]->idConsecuencias]= $analisis[$contador]->valorConsecuencias;

            $contador++;
          }

          return view('analisis.index',compact('arrayIdOperacion','arrayNameOperacion','arrayIdActividad','arrayNameActividad','arrayIdDescripcion',
            'arrayNameDescripcion','arrayIdCatalogo','arrayNameCatalogo','arrayIdConsecuencias','arrayNameConsecuencias','arrayValorConsecuencias'));

      // $analisis = DB::table('catalogo as cat')
      //   ->select('cat.id AS idCatalogo','cat.id AS idCatalogo2','cat.tipo_catalogo','cat.descripcion As produccion')
      //   ->join('relacion as rel','cat.tipo_catalogo','=','rel.id_tipocatalogo')
      //   ->join('catalogo as cat2','rel.id_catalogo','=','cat2.id')
      //   ->where('rel.id_catalogo','=','1')
      //   ->where('cat.tipocuestion','=','1')
      //   ->orderBy('rel.id')
      //   ->get();
      //
      //   $contador = 0 ;
      //   while($contador < count($analisis)){
      //      // Indices Query1
      //      $arrayIdCatalogo[$analisis[$contador]->idCatalogo] = $analisis[$contador]->idCatalogo;
      //
      //      // Descripciones
      //      $arrayNameCatalogo[$analisis[$contador]->idCatalogo] = $analisis[$contador]->produccion;
      //      $arrayIdCatalogo2[$analisis[$contador]->idCatalogo] = $analisis[$contador]->idCatalogo2;
      //
      //      $contador++;
      //   }
      //
      //  // Ibtiene los ID y los acumula en un arreglo para pasarlos al IN de la otra query
      //  foreach($arrayIdCatalogo as $idCatalogo){
      //    $arrayConcarQry1[]=$idCatalogo;
      //  }
      //
      // $analisisQry2 = DB::table('catalogo as cat')
      //    ->select('rel.id_catalogo AS idCatalogo','cat.tipo_catalogo as idTipoCatalogo','cat.id AS idSubs','cat.descripcion As colmn2')
      //    ->join('relacion as rel','cat.tipo_catalogo','=','rel.id_tipocatalogo')
      //    ->join('catalogo as cat2','rel.id_catalogo','=','cat2.id')
      //    ->whereIn('rel.id_catalogo',$arrayConcarQry1)
      //    ->where('cat.tipocuestion','=','1')
      //    ->orderBy('rel.id')
      //    ->get();
      //
      //  $contador2 = 0 ;
      //  while($contador2 < count($analisisQry2)){
      //     // Indices
      //     $arrayIdCatalogo[$analisisQry2[$contador2]->idCatalogo] = $analisisQry2[$contador2]->idCatalogo;
      //     $arrayIdTipoCatalogo[$analisisQry2[$contador2]->idTipoCatalogo] = $analisisQry2[$contador2]->idTipoCatalogo;
      //     $arrayIdSubs[$analisisQry2[$contador2]->idSubs] = $analisisQry2[$contador2]->idSubs;
      //
      //     // Descripciones
      //     $arrayNameProd2[$analisisQry2[$contador2]->idCatalogo][$analisisQry2[$contador2]->idTipoCatalogo] = $analisisQry2[$contador2]->idTipoCatalogo;
      //     $arrayTipoCatId[$analisisQry2[$contador2]->idCatalogo][$analisisQry2[$contador2]->idTipoCatalogo][$analisisQry2[$contador2]->idSubs]
      //                     = $analisisQry2[$contador2]->colmn2;
      //     $contador2++;
      //  }
      //
      //  $table="";
      //  $table.="<table border='1'>";
      //
      //
      //  ###############       INICIO VISTAAAA   ##############
      //  foreach($arrayIdCatalogo as $idCatalogo){
      //       $totaSubs=0;
      //       foreach($arrayIdTipoCatalogo as $idTipoCata){
      //         if(!isset($arrayNameProd2[$idCatalogo][$idTipoCata] ) ){
      //
      //         }else{
      //
      //         }
      //         foreach($arrayIdSubs as $idSubs){
      //            if(!isset($arrayTipoCatId[$idCatalogo][$idTipoCata][$idSubs])){
      //            }else{
      //
      //              $countIdSubs=count($arrayTipoCatId[$idCatalogo][$idTipoCata][$idSubs]);
      //              if(isset($arrayIdCatalogo2[$idTipoCata]) ){}else{
      //                if($arrayIdCatalogo2[$idCatalogo] == $idCatalogo ){
      //                    $totaSubs = $totaSubs + $countIdSubs;
      //                }
      //              }
      //            }
      //         }
      //       }
      //
      //        $table.="<thead>";
      //        $table.="<tr>";
      //        $table.="<th colspan='".$totaSubs."'  bgcolor='red'>---".$idCatalogo."--".$arrayNameCatalogo[$idCatalogo]."</th>";
      //        $table.="</tr>";
      //
      //        $table.="<tr>";
      //
      //        foreach($arrayIdTipoCatalogo as $idTipoCata){
      //          foreach($arrayIdSubs as $idSubs){
      //
      //             if(!isset($arrayTipoCatId[$idCatalogo][$idTipoCata][$idSubs])){
      //             }else{
      //               $table.="<th scope='col'>".$totaSubs."--".$arrayTipoCatId[$idCatalogo][$idTipoCata][$idSubs]."</th>";
      //             }
      //          }
      //        }
      //        $table.="</tr>";
      //        $table.="</thead>";
      //
      //  }
      //  $table.="</table>";
      //  echo $table;
      //

/*



      /*$operacion = DB::table('catalogo AS t1')
        ->join('relacion AS t2','t1.tipo_catalogo','=','t2.id_tipocatalogo')
        ->join('catalogo AS t3','t2.id_catalogo','=','t3.id')
        ->distinct('[t1.idOperacion]')
          ->distinct('[t3.idO]')
          ->distinct('[t2.id_tipocatalogo]')
        ->select(
          't3.id',
          't3.descripcion',
          't1.id AS idOperacion',
          't1.descripcion AS NameOperacion'
          )
        ->where('t2.id_catalogo','=','1')
        ->where('t1.tipocuestion','=','1')
        ->where('t2.status','=','1')
        ->orderBy('t1.id','ASC')
        	->distinct()
        ->get(); */

        /*
        $contador = 0 ;
        while($contador < count($operacion)){
       	 $arrayIdOperacion[$operacion[$contador]->idOperacion] = $operacion[$contador]->idOperacion;
       	 $arrayNameOperacion[$operacion[$contador]->idOperacion] = $operacion[$contador]->NameOperacion;
       	 $contador++;
       }*/
      //return View::make('analisis.index',compact('arrayIdOperacion','arrayNameOperacion'));
      // return view('analisis.index',compact('operacion'));
    }

    public function Valorcatalogo(Request $request, $id){

      $valor = DB::table('catalog_master')->where('cmfk','=',$id)->get();
        return response::json($valor);
     }

     public function GradoRiesgo(Request $request, $id){

       $resultado = DB::table('catalog_master')
       ->select(
         'cmval',
         'cmabbr',
         'cmfk'
         )
       ->where('cmtpcat','=',$id)
       ->get();
         return response::json($resultado);
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo 'esto es el estore';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Analisis  $analisis
     * @return \Illuminate\Http\Response
     */
    public function show(Analisis $analisis)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Analisis  $analisis
     * @return \Illuminate\Http\Response
     */
    public function edit(Analisis $aradsdanger)
    {


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Analisis  $analisis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

      // echo $id;
      if($request->catalogo == '44'){
        DB::table('answered_analysis')
                    ->where('aradsdanger', $id)
                    // ->update(['araconsequen' => $request->descripcion]);

                    ->update([
                      'aratotconsec' => $request->valor1,
                      'araconsequen' => $request->descripcion]
                  );

      }else {
        echo "no es 44";
      }
      if($request->catalogo == '45'){
        DB::table('answered_analysis')
                    ->where('aradsdanger', $id)
                    // ->update(['araconsequen' => $request->descripcion]);

                    ->update([
                      'araexpositio' => $request->descripcion,
                      'aratotexposi' => $request->valor2]
                  );
      }else {
      echo  "no es 45";
      }
      if($request->catalogo == '46'){
        DB::table('answered_analysis')
                    ->where('aradsdanger', $id)
                    // ->update(['araconsequen' => $request->descripcion]);

                    ->update([
                      'araprobabili' => $request->descripcion,
                      'aratotprobab' => $request->valor3]
                  );
                echo  $request->valor3;
      }else {
      echo  "no es 46";
      }
      if($request->consecuencia == $id){
        DB::table('answered_analysis')
                    ->where('aradsdanger', $id)
                    // ->update(['araconsequen' => $request->descripcion]);

                    ->update([
                      'aragrndtotal' => $request->total]
                  );
      }else {
        echo "no es total";
      }

        DB::table('answered_analysis')
                    ->where('ararfk', $request->id2)
                    ->update(['araobservati' => $request->caja] );

         if ($request->conse == $request->conse) {
             return $request->conse.$request->tota;
             $resultado = DB::table('answered_analysis')
                       ->where('ararfk', $request->conse)
                       ->update(['araobservati' => $request->tota]
                     );
                     return $resultado;

                    echo "se guardo";
         }else {
           echo "no se guardo riesgo";
         }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Analisis  $analisis
     * @return \Illuminate\Http\Response
     */
    public function destroy(Analisis $analisis)
    {
        //
    }
}
