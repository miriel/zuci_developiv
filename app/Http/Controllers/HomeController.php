<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(Request $request)
     {
    // $request->user()->authorizeRoles(['user', 'auditor','admin']);

    $folio = Auth::user()->folio;
    $idTipoCompay = Auth::user()->ctfk;

    $cuestionarios = DB::table('company_answers')
      ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
      ->join('answer_type_questionnaires','questionnaire_answers.qaatqfk','=','answer_type_questionnaires.atqbk')
      ->join('questionnaires','answer_type_questionnaires.atqqufk','=','questionnaires.qufk')
      ->join('questionnaries_status','company_answers.casqfk','=','questionnaries_status.sqfk')
      ->select('answer_type_questionnaires.atqqufk AS id','questionnaires.quname as description' ,'questionnaries_status.sqstatus AS status')
      ->where('company_answers.cafoliofk','=',$folio)
      ->groupBy('answer_type_questionnaires.atqqufk' ,'questionnaires.quname' ,'questionnaries_status.sqstatus')
      ->paginate(10);

      $tipoCompania = DB::table('company_types')
        ->select('ctname')
        ->where('ctfk','=',$idTipoCompay)
        ->get();

    // Se inicializa la variable global para cuestionarios asignados  que se agregaran el el archivo: loyout.adm.php
    // Para que se cargue en todos los archivos del menu
    Session::put('cuestionarios', $cuestionarios);
    Session::put('tipoCompania', $tipoCompania);

    return view('home');
}


public function someAdminStuff(Request $request)
{
    $request->user()->authorizeRoles(‘admin’);

    return view(‘some.view’);
}

}
