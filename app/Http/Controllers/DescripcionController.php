<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\catalog_master;
use App\relationship_catalogs;
use Illuminate\Support\Facades\Auth;
use DB;
use View;
use Session;
use Illuminate\Routing\Redirector;
use App\Http\Controllers\user;

class DescripcionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $idoperacion = $request->id;
      $cuestion = $request->id2;
      $status = $request->status;
      if($request->status == ""){ $status = 1; }

      $descripcion = DB::table('catalog_master AS ope')
      ->select(
        'ope.cmfk AS idperacion',
        'act.cmfk AS idOperacion',
        'act.cmdesc AS desOperacion',
        'act.cmstatus AS cmstatus',
        'act.cmtpcat As catalogo'

      )
      ->leftjoin('relationship_catalogs AS op_act','op_act.rccmfk','=','ope.cmfk')
      ->leftjoin('catalog_master AS act','act.cmtpcat','=','op_act.rctpcat')
      ->where('ope.cmfk',$request->id)
      ->Where('act.cmstatus','=','1')
      ->where('ope.cmtpquest',$request->id2)
      ->orderBy('act.cmfk','ASC')
      ->paginate(10,['*'],'actividad1');

      $descripcionbaja = DB::table('catalog_master AS ope')
      ->select(
        'ope.cmfk AS idOperacion',
        'act.cmfk AS idOperacion',
        'act.cmdesc AS desOperacion',
        'act.cmstatus AS cmstatus',
        'act.cmtpcat As catalogo'
      )
      ->leftjoin('relationship_catalogs AS op_act','op_act.rccmfk','=','ope.cmfk')
      ->leftjoin('catalog_master AS act','act.cmtpcat','=','op_act.rctpcat')
      ->where('ope.cmfk',$request->id)
      ->Where('act.cmstatus','=','0')
      ->where('ope.cmtpquest',$request->id2)
      ->orderBy('act.cmfk','ASC')
      ->paginate(10,['*'],'actividad2');

      $cuestionarios = DB::table('company_types')
        ->select('ctfk','ctname','ctstatus')
        ->where('ctfk',$request->id2)
        ->get();

         return view('analisis.descripcion.index',compact('status','descripcion','descripcionbaja','cuestionarios','idoperacion','cuestion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $status = $request->status;
      if($request->status == ""){ $status = 1; }
      // ID de sesion del usuario
        $id = Auth::user()->id;

        // Validacion de campo requerido
        $actividad=  $this->validate($request, [
             'cmdesc' => 'required'
         ]);

       $actividad = catalog_master::create([
         'cmtpcat' => $request->qtfk,
         'cmdesc' => $request->cmdesc,
         'cmtpquest' => $request->cuest,
         'cmuserfk' => $id,
       ]);

          $resultado = $actividad->cmfk;

         $operacion = relationship_catalogs::create([
           'rctpcat' => '50',
           'rccmfk' => $resultado,
           'rcctfk' => $request->cuest,
         ]);
         $operacion = relationship_catalogs::create([
           'rctpcat' => '51',
           'rccmfk' => $resultado,
         ]);
         $operacion = relationship_catalogs::create([
           'rctpcat' => '52',
           'rccmfk' => $resultado,

         ]);


      return response()->json([
        "mensaje" => "creado"
      ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $descipcion = catalog_master::find($cmfk);
      return response()->json($descipcion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $descipcion= $this->validate($request, [
            'cmdesc' => 'required',
       ]);

       $descipcion = catalog_master::find($id);
       $descipcion->cmdesc = $request->cmdesc;
       $descipcion->save();

       return response()->json([
         "mensaje" => "creado"
       ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cmfk, Request $request)
    {
      if($request->estatus == 1){
          $estaus=0;
          $mnsStatus = "baja";
      }else{
          $estaus=1;
          $mnsStatus = "alta";
      }

      $descipcion= catalog_master::find($cmfk);
      /*$this->authorize('delete', $pretunta);*/
      $descipcion->cmstatus = $estaus;
      $descipcion->save();
      Session::flash('message','El registro: '.$request->mcdesc." fue dada de ".$mnsStatus);
      return back();
    }

}
