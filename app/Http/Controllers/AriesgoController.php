<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\catalog_master;
use DB;
use View;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade as PDF;
use App\AnswerResponse;

class AriesgoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

      $folio = $request->folio;
      $empresa = $request->empresa;

      $analisis = DB::table('catalog_master AS ope')
      ->select(
        'ope.cmfk AS idOperacion',
        'ope.cmdesc AS desOperacion'
      )
      ->where('ope.cmtpcat','=','2')
      ->Where('ope.cmstatus','=','1')
      ->where('ope.cmtpquest',$empresa) // traer esto de la secccion
      ->orderBy('ope.cmfk','ASC')
      ->get();
          return view('analisis.principal',compact('analisis','folio','empresa'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo "esto es el store";
    }
    public function pdf(Request $request)
    {

         $id = $request->id;
         $idd = $id;
         $session = $request->folio;
         $idcuestion = $request->empresa;
         $carbon = new Carbon();
         $date = Carbon::now();
         $fechaActual = $date->format('d-m-Y');
         $cuestionario = $request->empresa;

         // Configura sin limite de tiempo de carga
         set_time_limit(0);
         ini_set('memory_limit', '-1'); // Asigna la memoria necesaria para la carga
         $analisis = DB::table('catalog_master AS ope')
           ->leftjoin('relationship_catalogs AS op_act','op_act.rccmfk','=','ope.cmfk')
           ->leftjoin('catalog_master AS act','act.cmtpcat','=','op_act.rctpcat')
           ->leftjoin('relationship_catalogs AS act_pel','act_pel.rccmfk','=','act.cmfk')
           ->leftjoin('catalog_master AS pel','pel.cmtpcat','=','act_pel.rctpcat')
           ->leftjoin('answered_analysis AS res','res.aradsdanger','=','pel.cmfk')
           ->leftjoin('catalog_master AS nombre','nombre.cmfk','=','res.aradsdanger')
           ->leftjoin('catalog_master AS nombre1','nombre1.cmfk','=','araconsequen')
           ->leftjoin('catalog_master AS nombre2','nombre2.cmfk','=','araexpositio')
           ->leftjoin('catalog_master AS nombre3','nombre3.cmfk','=','araprobabili')
           ->select(
             'ope.cmfk as idActividad','ope.cmdesc as NameActividad',
             'act.cmfk AS idOperacion','act.cmdesc AS desOperacion',
             'pel.cmfk AS idActivid','pel.cmdesc AS desActividad',
             'res.aradsdanger AS idRespuesta',
             'res.araconsequen','nombre1.cmabbr AS Consecuenias','res.aratotconsec',
             'res.araexpositio','nombre2.cmabbr AS Exposicion','res.aratotexposi',
             'res.araprobabili','nombre3.cmabbr AS Probabilidad','res.aratotprobab',
             'res.aragrndtotal','res.ararisklevel','res.araobservati'
             )
           ->where('ope.cmfk','=',$id)
           ->Where('act.cmstatus','=','1')
           ->where('pel.cmstatus','=','1')
           ->where('ope.cmtpquest',$cuestionario) //traer tipo de cuestionario en la de la session
           ->where('res.arafoliofk',$session) //trae el folio de la session y l compara en la query
           ->orderBy('act.cmfk','ASC')
           ->orderBy('pel.cmfk','ASC')
           ->get();
           $contador = 0;
           while($contador < count($analisis)){
             $arrayIdAct[$analisis[$contador]->idActividad] = $analisis[$contador]->idActividad;
             $arrayNameAct[$analisis[$contador]->idActividad] = $analisis[$contador]->NameActividad;
             $arrayIdOperacion[$analisis[$contador]->idOperacion] = $analisis[$contador]->idOperacion;
             $arrayNameOperacion[$analisis[$contador]->idOperacion] = $analisis[$contador]->desOperacion;
             $arrayIdActividad[$analisis[$contador]->idActivid] = $analisis[$contador]->idActivid;
             $arrayNameActividad[$analisis[$contador]->idOperacion][$analisis[$contador]->idActivid] = $analisis[$contador]->desActividad;
             $contador++;
           }

        $pdf = PDF::loadView('analisis.pdf.analisis', compact('nom','apellido','area','fechaActual','cuestionario','analisis','id',
        'arrayIdAct','arrayNameAct','arrayIdOperacion','arrayNameOperacion','arrayIdActividad','arrayNameActividad','session',
    'idcuestion'));
          $pdf->setPaper('A4', 'landscape');
          return $pdf->stream();

        // return $pdf->download('listado.pdf');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id, Request $request){

      $idd = $id;
      $session = $request->folio;
      $tipoEmpresaUs =$request->empresa; // Variable que obtiene el id de la empresa del usuario seleccionado
      $cuestionario = $request->empresa;
      $folio = Auth::user()->folio;
      $idUserLogin = Auth::user()->id;

      // Arreglo de grupos para los valores del total del cuestionarios analisis
      $arrayGruppos  = [53,54,55,56,57];

      $analisis = DB::table('catalog_master AS ope')
        ->leftjoin('relationship_catalogs AS op_act','op_act.rccmfk','=','ope.cmfk')
        ->leftjoin('catalog_master AS act','act.cmtpcat','=','op_act.rctpcat')
        ->leftjoin('relationship_catalogs AS act_pel','act_pel.rccmfk','=','act.cmfk')
        ->leftjoin('catalog_master AS pel','pel.cmtpcat','=','act_pel.rctpcat')
        ->leftjoin('relationship_catalogs AS pel_des','pel_des.rccmfk','=','pel.cmfk')
        ->leftjoin('catalog_master AS des','des.cmtpcat','=','pel_des.rctpcat')
        ->select(
          'ope.cmfk as idActividad',
          'ope.cmdesc as NameActividad',
          'act.cmfk AS idOperacion',
          'act.cmdesc AS desOperacion',
          'pel.cmfk AS idActividad',  // Id de respuesta
          'pel.cmdesc AS desActividad', // nombre respuesta
          'des.cmtpcat AS idCatalogo',
          'des.cmfk AS idConsecuencias',
          'des.cmabbr AS desConsecuencias',
          'des.cmval AS valorConsecuencias'
        )
        ->where('ope.cmfk','=',$id)
        ->Where('act.cmstatus','=','1')
        ->where('pel.cmstatus','=','1')
        ->where('des.cmstatus','=','1')
        ->where('ope.cmtpquest',$cuestionario) //traer tipo de cuestionario en la de la session
        ->orderBy('act.cmfk','ASC')
        ->orderBy('pel.cmfk','ASC')
        ->orderBy('des.cmfk','ASC')
        ->get();
        $contador = 0;
        while($contador < count($analisis)){
          // $arrayIdAct[$analisis[$contador]->idActividad] = $analisis[$contador]->idActividad;
          // $arrayNameAct[$analisis[$contador]->idActividad] = $analisis[$contador]->NameActividad;

          $arrayIdOperacion[$analisis[$contador]->idOperacion] = $analisis[$contador]->idOperacion;
          $arrayNameOperacion[$analisis[$contador]->idOperacion] = $analisis[$contador]->desOperacion;

          $arrayIdActividad[$analisis[$contador]->idActividad] = $analisis[$contador]->idActividad;
          $arrayNameActividad[$analisis[$contador]->idOperacion][$analisis[$contador]->idActividad] = $analisis[$contador]->desActividad;

          $arrayIdCatalogo[$analisis[$contador]->idCatalogo] = $analisis[$contador]->idCatalogo;
          $arrayNameCatalogo[$analisis[$contador]->idOperacion][$analisis[$contador]->idActividad][$analisis[$contador]->idCatalogo]= $analisis[$contador]->idCatalogo;

          $arrayIdConsecuencias[$analisis[$contador]->idConsecuencias] = $analisis[$contador]->idConsecuencias;
          $arrayNameConsecuencias[$analisis[$contador]->idOperacion][$analisis[$contador]->idActividad][$analisis[$contador]->idCatalogo][$analisis[$contador]->idConsecuencias]= $analisis[$contador]->desConsecuencias;
          $arrayValorConsecuencias[$analisis[$contador]->idOperacion][$analisis[$contador]->idActividad][$analisis[$contador]->idCatalogo][$analisis[$contador]->idConsecuencias]= $analisis[$contador]->valorConsecuencias;

          $contador++;
        }



        $nombre = DB::table('catalog_master')
        ->select('catalog_master.cmfk',
        'catalog_master.cmdesc')
        ->where('catalog_master.cmfk','=',$id)
        ->get();
        $divisas1 = DB::table('catalog_master')
        ->select('catalog_master.cmfk','catalog_master.cmtpcat','catalog_master.cmdesc','catalog_master.cmabbr','catalog_master.cmstatus','catalog_master.cmmin','catalog_master.cmmax','catalog_master.cmmoneda' )
        ->whereIn('catalog_master.cmtpcat',[50,51,52,53,54,55,56,57])
        ->Where('catalog_master.cmstatus','=','1')
        ->orderBy('catalog_master.cmfk','ASC')
        ->get();
        $moneda =DB::table('catalog_master')
        ->select('catalog_master.cmfk','catalog_master.cmtpcat','catalog_master.cmdesc')
        ->Where('catalog_master.cmtpcat','=','60')
        ->orderBy('catalog_master.cmfk','ASC')
        ->get();

        // Obtiene el numero total de preguntas del cuestionario analisisde riesgo
<<<<<<< HEAD
        $totalPreguntas = DB::table('answered_analysis')
=======
        $totalPreguntas = DB::table('answered_analysis') 
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
            ->select(DB::raw('count(answered_analysis.aractfk) as tot'))
            ->leftjoin('catalog_master','catalog_master.cmfk','=','answered_analysis.aradsdanger')
            ->leftjoin('catalog_master as cata','cata.cmfk','=','answered_analysis.araconsequen')
            ->leftjoin('catalog_master as cata1','cata1.cmfk','=','answered_analysis.araexpositio')
            ->leftjoin('catalog_master as cata2','cata2.cmfk','=','answered_analysis.araprobabili')
            ->where('answered_analysis.arafoliofk','=',$session)
            ->where('answered_analysis.aractfk','=',$tipoEmpresaUs)
            ->groupBy('answered_analysis.aractfk')
            ->get();

        // Resultdo de grupos en JSON mediante un arreglo de grupos y se envian en un arreglo de grupos
        $gruposRes =  $this->GradoRiesgo($arrayGruppos);


        return view('analisis.respuesta',compact('cuestionario','analisis','nombre','idd','arrayIdOperacion','arrayNameOperacion','arrayIdActividad','arrayNameActividad',
        'arrayIdDescripcion','arrayNameDescripcion','arrayIdCatalogo','arrayNameCatalogo','arrayIdConsecuencias','arrayNameConsecuencias','arrayValorConsecuencias'
        ,'fechaActual','session', 'tipoEmpresaUs','divisas1','moneda','session','idcuestion','cuestionario','totalPreguntas','gruposRes','idUserLogin'));
    }



    public function Valorcatalogo(Request $request, $id){

      $valor = DB::table('catalog_master')->where('cmfk','=',$id)->get();
        return response::json($valor);
     }

     public function GradoRiesgo($gruposArray){

       $resultado = DB::table('catalog_master')
       ->select(
         'cmval',
         'cmabbr',
         'cmfk'
         )
       ->whereIn('cmtpcat',$gruposArray)
       ->get();
        return $resultado;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

<<<<<<< HEAD
      $totPreg = $request->totalPreg;  // Variable que obiene el total de preguntas del cuestionario asignado a cada usuario
=======
      $totPreg = $request->totPreg;  // Variable que obiene el total de preguntas del cuestionario asignado a cada usuario
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
      $folio = $request->folio; // Folio del usuario del cuestionarios seleccionado

      $ip = $request->ip();
      $user = $request->user()->id;

      if($request->catalogo == '50'){
        //echo $request->total;
        DB::table('answered_analysis')
                    ->where('aradsdanger', $id)
                    // ->update(['araconsequen' => $request->descripcion]);

                    ->update([
                      'aratotconsec' => $request->valor,
                      'araconsequen' => $request->descripcion]
                  );
          // Actualiza el total general
          DB::table('answered_analysis')
                      ->where('aradsdanger', $id)
                       //->update(['araconsequen' => $request->descripcion]);
                      ->update([
                        'aragrndtotal' => $request->total,
                        'ararisklevel' => $request->gradoT,
                        'arausuerfk' => $request->idUserLogin
                        ]
                    );

      }else
      if($request->catalogo == '51'){
        DB::table('answered_analysis')
                    ->where('aradsdanger', $id)
                    // ->update(['araconsequen' => $request->descripcion]);

                    ->update([
                      'araexpositio' => $request->descripcion,
                      'aratotexposi' => $request->valor]
                  );
                  // Actualiza el total general
        DB::table('answered_analysis')
                    ->where('aradsdanger', $id)
                     //->update(['araconsequen' => $request->descripcion]);
                    ->update([
                      'aragrndtotal' => $request->total,
                      'ararisklevel' => $request->gradoT,
                      'arausuerfk' => $request->idUserLogin
                      ]
                  );
      }else
      if($request->catalogo == '52'){
        DB::table('answered_analysis')
                    ->where('aradsdanger', $id)
                    // ->update(['araconsequen' => $request->descripcion]);

                    ->update([
                      'araprobabili' => $request->descripcion,
                      'aratotprobab' => $request->valor
                    ]
                  );
        // Actualiza el total general y e grado de riesgo
        DB::table('answered_analysis')
                    ->where('aradsdanger', $id)
                     //->update(['araconsequen' => $request->descripcion]);
                    ->update([
                      'aragrndtotal' => $request->total,
                      'ararisklevel' => $request->gradoT,
                      'arausuerfk' => $request->idUserLogin
                      ]
                  );
      }else
      //Actualizar los comentarios
      if( $request->catalogo == 'updtcom'){
        // Actualiza los comentarios
        DB::table('answered_analysis')
                    ->where('ararfk', $request->id2)
                    ->update([
                              'araobservati' => $request->caja,
                              'arausuerfk' => $request->idUserLogin
                             ]
                           );
      }

<<<<<<< HEAD
      $this->validaResps();
=======

      $this->validaResps( $request->totPreg, $request->idConsecuencia, $folio, $ip, $user);
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

<<<<<<< HEAD
  public function validaResps(/*$totPreg, $idPreg, $folio, $ip, $user, $idConsecuencia*/){
      // Id de cuestionario analiside Riesgo en duro con ID (10)  que se encuentra en duro debido a la estructura de las tablas de Analisis de Riesgo
      $idCuest=10;

      // Cuenta los numero
      $actResp = DB::table('answered_response')
        ->select('arcount','arqsfk','arfk')
        ->where('arqufk','=', $idCuest )
        ->where('arfoliofk','=', $folio )
        ->get();

        if( count($actResp) == 0 ){
=======

    // Funcion que valida el cuestionario para cambiar de estatus al contestar el 100% de las preguntas
    public function validaResps($totPreg, $idPreg, $folio, $ip, $user){
      // Id de cuestionario analiside Riesgo en duro con ID (10)  que se encuentra en duro debido a la estructura de las tablas de Analisis de Riesgo
      $idCuest=10;

      //validar si existen las respuestas en la tabla: answered_analysis
      $actResp = DB::table('answered_analysis')
        ->select('araconsequen as consec','araexpositio as exposic','araprobabili as  probab')
        ->where('aradsdanger','=', $idPreg )
        ->where('arafoliofk','=', $folio )
        ->get();

      if( $actResp->consec  == ""){
        echo "consec nula";
      }

        // Valida si existe un registro de pregunta contestada del cuestionario correspondiente
      /*  if( count($actResp) == 0 ){
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003

            AnswerResponse::create([
                'arqufk' => $idCuest,
                'arqsfk' => 'p_'.$idConsecuencia.'='.$idPreg.',',
                'arfoliofk'  => $folio,
                'arip' => $ip,
                'arusuerfk' => $user,
                'arcount' => 1
            ]);

<<<<<<< HEAD
=======
         // Si ya existe algun registro de alguna pregunta contestda
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
         }else{
           $existePregunta=0;

            // Obtiene en un array las preguntas
            $prind = explode("|",$actResp[0]->arqsfk);
            for($i=0; $i<count($prind); $i++){
              $cade = explode("=",$prind[$i]);
              for($h=0; $h<count($cade); $h++){
                //echo $cade[1];
                $preg = explode("p",$cade[$h]);
                for($j=0; $j<count($preg); $j++){
                  $preg1 = explode("=",$preg[$j]);
                  for($x=0;$x<count($preg1);$x++){
                      $preg1=$preg1[0]."_";
                      $preg2 = explode("_",$preg1);
                      for($y=0;$y<count($preg2);$y++){
                          //echo $preg2[1].".";
                          if($preg2[1] == $idConsecuencia){
                            $idPregunta = $preg2[1];
                            break;
                          }
                      }
                  }
                }
              }
            }



            $cade = explode('|',$actResp[0]->arqsfk);
            for( $h=0; $h < count($cade); $h++ ){
              $cade2 = explode('=',$cade[$h]);
              for( $i=0; $i < count($cade2); $i++ ){
                  $idPregunta = $cade2[0] ;
                  $idRespuesta = $cade2[1] ;
                  var_dump($cade2[0]);

                  //echo $idPregunta;
                  /*if($cade2[0]  == $idConsecuencia){
                    echo $cade2[0]."-".$existePregunta=1;
                    break;
                  } **/
<<<<<<< HEAD
              }
            }
=======
        //      }
          //  }
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003

            //echo $idPregunta;

            /*$cadeResp = explode('=',$actResp[0]->arqsfk);
            for( $x=0; $x < count($cadeResp); $x++ ){
                echo $cadeResp[$x];
                $cadeRs1 = explode(',',$cadeResp[$x]);
                for( $y=0; $y < count($cadeRs1[$x]); $y++ ){
                  echo $cadeRs1[$y];
                }

            }*/



            // Existe pregunta
<<<<<<< HEAD
            if( $idPregunta == 1 ){
               //echo "existe";

            // No existe la pregunta
            }else{
              //echo "no existe";
            }
=======
          //  if( $idPregunta == 1 ){
               //echo "existe";

            // No existe la pregunta
          //  }else{
              //echo "no existe";
          //  }
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003



             // Busca una Pregunta en el array
             /*$res = in_array($idPreg,$cade);

             // Validan si existen los datos en el arreglo
             if( $res == 1 ){

             }else{

               $preguntasConcat = $actResp[0]->arqsfk.$idPreg.",";

               $updtResp = AnswerResponse::where('arfoliofk', $folio)
                                         ->where('arqufk', $idCuest)->first();
               $updtResp->arcount = $actResp[0]->arcount + 1;
               $updtResp->arqsfk = $preguntasConcat;
               $updtResp->save();
             }

             $actRespI = DB::table('answered_response')
               ->select('arcount')
               ->where('arqufk','=', $idCuest )
               ->where('arfoliofk','=', $folio )
               ->get();

             // Valida el cambio de estatus al momento de actualizar las respuestas
             if( $actRespI[0]->arcount ==  $totPreg ){
               //echo "cambio de estatus";
               DB::table('questionnaries_status')
               ->where('sqfoliofk', $folio )
               ->where('sqqufk', $idCuest )
               ->update(['sqstatus' => 2]);
             } */
<<<<<<< HEAD
        }
=======
        //}
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
    }

}
