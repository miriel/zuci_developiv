<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EmpresaRequest;
use App\Company_Type;
use Laracasts\Flash\flash;
use Session;
use Illuminate\Routing\Redirector;

class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $status = $request->status;
      if($request->status == ""){ $status = 1; }
        // $empresa = Company_Type::orderBy('id','DESC')->get();
        $empresa = Company_Type::allowed()->where('ctstatus','=','1')->orderBy('ctfk','ASC')->paginate(10,['*'],'empresa1');
        $empresabajas = Company_Type::allowed()->where('ctstatus','=','0')->orderBy('ctfk','ASC')->paginate(10,['*'],'empresa2');

          return view('catalogos.empresa.index', compact('status','empresa','empresabajas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
       $this->authorize('create', new Company_Type);
         return view('catalogos.empresa.create');
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(EmpresaRequest $request)
     {
       // $empre=  $this->validate($request, [
       //      'ctname' => 'required|unique:company_types',
       //  ]);
         $this->authorize('create', new Company_Type);

         $empresa = new Company_Type($request->all());
         $empresa->ctip=$request->ip();
         $empresa->ctuserfk=$request->user()->id;
         // dd($giro);
         $empresa->save();
         return response()->json([
           "mensaje" => "creado"
         ]);
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $cotfk
      * @return \Illuminate\Http\Response
      */
     public function show($cotfk)
     {
         //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $cotfk
      * @return \Illuminate\Http\Response
      */
     public function edit($cotfk)
     {
          $Company_Type = Company_Type::find($cotfk);
          $this->authorize('update', $Company_Type);

          $empresa = Company_Type::find($cotfk);
         return response()->json($empresa);
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $cotfk
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $cotfk)
     {
       $empre= $this->validate($request, [
             'ctname' => 'required',
        ]);
         $Company_Type = Company_Type::find($cotfk);
         $this->authorize('update', $Company_Type);

         $empresa = Company_Type::find($cotfk);
         $empresa->fill($request->all());
         $empresa->save();
         return response()->json([
           "mensaje" => "creado"
         ]);
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $cotfk
      * @return \Illuminate\Http\Response
      */
     public function destroy($cotfk, Request $request)
     {
       $Company_Type = Company_Type::find($cotfk);
       $this->authorize('delete', $Company_Type);

         if($request->status == 1){
             $status=0;
             $mnsgStatus="baja";
         }else{
             $status=1;
             $mnsgStatus="alta";
         }
         // funcion de eliminar
         $empresa = Company_Type::find($cotfk);
         $empresa->ctstatus=$status;
         $empresa->save();
         Session::flash('flash_delete','la Empresa '.$empresa->ctname.' ha sido borrado con exito!');
         // return redirect()->route('empresa.index');
         return back();
     }
}
