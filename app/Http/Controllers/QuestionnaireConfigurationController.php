<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\QuestionnaireConfiguration;
use App\Company_Type;
use App\Questionnaire;
use Session;
use App\Http\Requests\QuestionnaireConfigurationRequest;
use Carbon\Carbon;
use Illuminate\Validation\Rule; // Libreria para la validacaion de RUles

class QuestionnaireConfigurationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $status = $request->status;
      if($request->status == ""){ $status = 1; }
      $configuracionesAl = DB::table('configuration_questionnaire') 
          ->join('questionnaires','configuration_questionnaire.cqqufk','questionnaires.qufk')
          ->join('company_types','configuration_questionnaire.cqctfk','company_types.ctfk')
          ->select('configuration_questionnaire.cqfk AS id','questionnaires.quname AS nameCuest','company_types.ctname AS typeCompany',
              'configuration_questionnaire.cqstatus AS status')
          ->where('configuration_questionnaire.cqstatus','=',1)
          ->orderBy('configuration_questionnaire.cqfk')
          ->paginate(8,['*'],'confg1');
        $configuracionesBa = DB::table('configuration_questionnaire')
            ->join('questionnaires','configuration_questionnaire.cqqufk','questionnaires.qufk')
            ->join('company_types','configuration_questionnaire.cqctfk','company_types.ctfk')
            ->select('configuration_questionnaire.cqfk AS id','questionnaires.quname AS nameCuest','company_types.ctname AS typeCompany',
                'configuration_questionnaire.cqstatus AS status')
            ->where('configuration_questionnaire.cqstatus','=',0)
            ->orderBy('configuration_questionnaire.cqfk')
            ->paginate(8,['*'],'config2');

        $tipoCompania = Company_Type::where('ctstatus','=','1')
            ->pluck('ctname','ctfk');

        $cuestionario = Questionnaire::where('qustatus','=','1')
            ->pluck('quname','qufk');

        return view('cuestionarios.config.index', compact('configuracionesAl','configuracionesBa','status','tipoCompania',
                    'cuestionario'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipoCompania = Company_Type::where('status','=','1')
            ->pluck('description','id');

        $cuestionario = Questionnaire::where('status','=','1')
            ->pluck('description','id');
        return view('cuestionarios.config.create',compact('tipoCompania','cuestionario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionnaireConfigurationRequest $request)
    {
        $carbon = new Carbon();
        $date = Carbon::now();
        $fechaActual = $date->format('Y-m-d H:i:s');

        $config = QuestionnaireConfiguration::create([
          'cqctfk' => $request->cqctfk,
          'cqqufk' => $request->cqqufk,
          'cqeverybody'	 => $request->everybody,
          'cqip' => $request->ip(),
          'cquserfk' => $request->user()->id,
          'cqinsertdt' => $fechaActual,
          'cqupddt' => $fechaActual,
        ]);

        return response()->json([
          "mensaje" => "creado"
        ]);

        /*Session::flash('message','Configuracion de cuestionario: '.$request->description.' creada correctamente');
        return redirect('configcuestionario');*/

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          /*$tipoCompania = Company_Type::where('status','=','1')
              ->pluck('description','id');
          $cuestionario = Questionnaire::where('status','=','1')
              ->pluck('description','id');
          $config = QuestionnaireConfiguration::find($id);*/
          $config = DB::table('configuration_questionnaire')
            ->select('company_types.ctfk AS tipoCompany','questionnaires.qufk AS IdCuestionario','configuration_questionnaire.cqeverybody')
            ->join('company_types','configuration_questionnaire.cqctfk','=','company_types.ctfk')
            ->join('questionnaires','configuration_questionnaire.cqqufk','=','questionnaires.qufk')
            ->where('configuration_questionnaire.cqfk','=',$id)
            ->get();
          return response()->json($config);
          //return view("cuestionarios.config.edit",compact('tipoCompania','cuestionario','config'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $this->cqctfk = $request->cqctfk;
         $this->cqqufk = $request->cqqufk;

          // Valida que sea el campo requerido sin enviar de nuevo a las pimeras validaciones del create
         $empre= $this->validate($request, [
           'cqctfk' => [
                 'required',
                 Rule::unique('configuration_questionnaire')->where(function ($query) {
                     $query->where('cqqufk', $this->cqqufk);
                 }),
           ],
           'cqqufk' => [
                 'required',
                 Rule::unique('configuration_questionnaire')->where(function ($query) {
                     $query->where('cqctfk', $this->cqctfk);
                 }),
           ],
          ]);

          $config = QuestionnaireConfiguration::find($id);
          $config->cqctfk = $request->cqctfk;
          $config->cqqufk = $request->cqqufk;
          $config->cqeverybody = $request->everybody;
          $config->cqip = $request->ip();
          $config->cquserfk = $request->user()->id;
          $config->save();

          return response()->json([
            "mensaje" => "creado"
          ]);

          /*Session::flash('message','La configuracion con ID: '.$id.'. Se ha actualizado');
          return redirect('configcuestionario');*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
          if($request->status == 1){
              $estatus=0;
              $mnsgStatus="baja";
          }else{
              $estatus=1;
              $mnsgStatus="alta";
          }
          $config = QuestionnaireConfiguration::find($id);
          $config->cqstatus = $estatus;
          $config->save();
          Session::flash('message','La configuracion con ID: '.$id."  fue dada de ".$mnsgStatus);
          return back();
    }
}
