<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\catalog_master;
use DB;
use Session;
use Illuminate\Support\Facades\Auth;

class DivisasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $this->authorize('view', new catalog_master);

      $status = $request->cmstatus;
      if($request->cmstatus == ""){ $status = 1; }

      $analisis = DB::table('catalog_master')
      ->select('catalog_master.cmfk','catalog_master.cmdesc','catalog_master.cmabbr','catalog_master.cmstatus')
      ->where('catalog_master.cmtpcat','=','60')
      ->Where('catalog_master.cmstatus','=','1')
      ->orderBy('catalog_master.cmfk','ASC')
      ->paginate(10,['*'],'analisis1');

      $analisisbajas = DB::table('catalog_master')
      ->select('catalog_master.cmfk','catalog_master.cmdesc','catalog_master.cmabbr','catalog_master.cmstatus')
      ->where('catalog_master.cmtpcat','=','60')
      ->Where('catalog_master.cmstatus','=','0')
      ->orderBy('catalog_master.cmfk','ASC')
      ->paginate(10,['*'],'analisis1');

       return view('analisis.divisas.index',compact('status','analisis','analisisbajas'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $divisa=  $this->validate($request, [
           'cmdesc' => 'required',
           'cmabbr' => 'required',
           'cmval' => 'required',
       ]);

      $divisa = new catalog_master($request->all());
      $divisa->cmtpcat = '60';
      $divisa->cmdesc = $request->cmdesc;
      $divisa->cmabbr = $request->cmabbr;
      $divisa->cmval = $request->cmval;
      $divisa->save();

    return response()->json([
      "mensaje" => "creado"
    ]);
<<<<<<< HEAD


=======
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cmfk)
    {
      $divisas = catalog_master::find($cmfk);
      return response()->json($divisas);
      return $divisas;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->authorize('update', new catalog_master);
      $actividad= $this->validate($request, [
            'cmdesc' => 'required',
            'cmabbr' => 'required',
            'cmval' => 'required',
       ]);

       $actividad = catalog_master::find($id);
       $actividad->cmdesc = $request->cmdesc;
       $actividad->cmabbr = $request->cmabbr;
       $actividad->cmval = $request->cmval;
       $actividad->save();

       return response()->json([
         "mensaje" => "creado"
       ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cmfk, Request $request)
    {
      if($request->estatus == 1){
          $estaus=0;
          $mnsStatus = "baja";
      }else{
          $estaus=1;
          $mnsStatus = "alta";
      }

      $operacion= catalog_master::find($cmfk);
      /*$this->authorize('delete', $pretunta);*/
      $operacion->cmstatus = $estaus;
      $operacion->save();
      Session::flash('message','El registro: '.$request->cmdesc." fue dada de ".$mnsStatus);
      return back();

    }

}
