<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\CompanyAnswer;
use App\ConfigCompany;
use Carbon\Carbon;
use App\QuestionnaireNotes;
use App\PaquetesModel;
use App\QuestionnaireAnswerController;
use App\AnswerController;
use App\Question;
use App\QuestionnaireAnswer;

use App\StatusQuestionnarie;

class PruebaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

      $config = DB::table('configuration_questionnaire')
          ->select('configuration_questionnaire.questionnaire AS idCuestionario')
          ->join('questionnaires','configuration_questionnaire.questionnaire','=','questionnaires.id')
          ->where('configuration_questionnaire.typecompany','=', 8 /*$data['tipo']*/ )
          ->where('configuration_questionnaire.status','=', '1' )
          ->orWhere('configuration_questionnaire.everybody','=','true')
          ->get();

       foreach($config as $conf ){
             // Inserta las notas para clientes, compaÃ±ias etc.
             $insertConfComp = QuestionnaireNotes::create([ ]);
             $estatusCuest = StatusQuestionnarie::create([
                 'folio' =>  '23434344'/*$id_company.$folio*/,
                 'questionaire' => $conf->idCuestionario,
             ]);

             // ID que arroja al insertar los estatus de cuestionarios
             $estatusCuestID =  $estatusCuest->id;

            $pregResp = DB::table('answer_type_questionnaires')
                ->select('questionnaire_answers.id AS idRespPregunta')
                ->join('questionnaire_answers','answer_type_questionnaires.agpkid','=','questionnaire_answers.typequest')
                ->where('answer_type_questionnaires.agquestionnaire','=', $conf->idCuestionario  )
                ->get();

            foreach($pregResp as $pregRsp){
              echo $pregRsp->idRespPregunta."<br>";
              CompanyAnswer::create([
                  'idpregresp' => $pregRsp->idRespPregunta,
                  'folio' => '23434344'/*$id_company.$folio*/,
                  'idnotes' => $insertConfComp->id,
                  'statusquestionnaire' => $estatusCuestID,
               ]);
            }
        //End foreach      
       }

    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo "sasas";
          echo $request->pregunta;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        echo "edit";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        echo "update";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
