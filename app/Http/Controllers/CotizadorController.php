<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Company_Type;
use Carbon\Carbon;
use App\catalog_master;
use DB;
use App\quotation_user_profiles;
use App\quotations;
use Response;
use App\quotation_primary_questionnaires;
use App\quotation_secondary_questionnaires;
use App\Company;
use App\Events\cotizador;
use Illuminate\Support\Facades\Mail;
use Session;

class CotizadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $correo = $request->email;
        // canbia el status a revisado por auditor
          DB::table('questionnaries_status')
         ->where('sqfoliofk','=',$request->id)
         ->where('sqqufk','=',$request->idcues)
         ->update(['sqstatus' => 3]);

         $users = User::select('folio','email')
        ->where('folio','=',$request->id)
        ->get();

         foreach($users as $user){
             $correo = $user->email;
         }
         // envia correo al usuairo que ya se reviso el cuestionario
         $dates = array('folio'=> $request->id);
         $email = $correo;
         $this->Email1($dates,$email);

         $idcues = $request->idcues;
         $iduser= $request->id2;
         $folio = $request->id;
         $em =$request->empresa;
         $empresa = Company_Type::select('ctfk','ctname')->where('ctstatus','=','1')->get();
         $carbon = new Carbon();
         $date = Carbon::now();
         $fechaActual = $date->format('Y-m-d H:i:s');

         $user = User::
         select('id','folio','email','phone_fixed','lada_cel')
         ->where('id',$iduser)
         ->where('folio',$folio)
         ->get();
         $divisas = catalog_master::
           select('cmfk','cmtpcat','cmdesc','cmstatus','cmmin','cmmax','cmmoneda')
           ->where('cmtpcat','=','60')
           ->where('cmstatus','=','1')
           ->orderby('cmfk','ASC')
           ->get();
           $roles =  DB::table('roles')
            ->select('id','name','cost')
            ->whereIn('id',['2','3','8','7'])
            ->where('statuscot','=','1')
            ->orderBy('id','ASC')
            ->get();
            $roldefaull =  DB::table('roles')
             ->select('id','name','cost')
             ->where('id','=','5')
             ->where('statuscot','=','1')
             ->orderBy('id','ASC')
             ->get();

            $cuestionarios = DB::table('questionnaires')
            ->select('qufk','quname','qucost','qustatus')
            ->whereIn('qustatus',['1','2'])
            ->whereIn('qufk',['8','7','10','11','12'])
            ->orderBy('qufk','ASC')
            ->get();
            $cuesdefaull = DB::table('questionnaires')
            ->select('qufk','quname','qucost')
            ->where('qustatus','=','1')
            ->where('qufk','=',$idcues)
            ->orderBy('qufk','ASC')
            ->get();
           //
           //  $analisis = DB::table('catalog_master')
           // ->select('cmfk','cmtpcat','cmdesc','cmstatus','cmmin','cmmax','cmmoneda')
           // ->where('cmtpcat','=','1')
           // ->where('cmstatus','=','1')
           // ->orderby('cmfk','ASC')
           // ->get();
           $idcues = $request->idcues;

        return view('cotizador.index',compact('idcues','folio','iduser','em','user','empresa','fechaActual','divisas','roles','cuestionarios','cuesdefaull','roldefaull'));
    }

    // envio de correo
    function Email1($dates,$email){
    Mail::send('emails.revision',$dates, function($message) use ($email){
      $message->subject('Bienvenido a la plataforma');
      $message->to($email);
      $message->from('no-repply@geeklife.com.ve','CIS');
    });
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return "estas en create";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
<<<<<<< HEAD
=======

>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
         // $request->array2;
        $auditor = Auth::user()->id;
        $iduser  = $request->iduser;
        $folio = $request->folio;

        $date = Carbon::now();
        $fechaActual = $date->format('Y-m-d H:i:s');
        $endDate = $date->addDay(30);
        // echo $request->id;
        // echo $request->array2;
<<<<<<< HEAD

=======
        //
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
        $quotations =  quotations::create([
          'qtfolio' => $request->folio,
          'qtcmfk' => $request->moneda,
          'qtdiscount' => $request->auditotal,
          'qtiva' =>$request->iva,
          'qttotal' =>  $request->total,
          'qtuserfk' =>  $iduser,
          'qtusradit' => $auditor,
          'qtdate' => $fechaActual,
          'qtdateend' =>$endDate,
          'qtsubtotal' =>$request->subtotal,
          'qtporcentaje' =>$request->descuento
        ]);
        $idquotations = $quotations->qtfk;
            DB::table('questionnaries_status')
            ->where('sqfoliofk', $folio )
            ->where('sqqufk', $request->idcues )
            ->update(['sqstatus' => 4]);

        if($request->array2 != ""){
          foreach($request->array2 as $array){
            $des = explode(',',$array);
              // var_dump($des);
              $user =  quotation_user_profiles::create([
                'qupid' => $des[0],
                'qupqtfk' =>$idquotations ,
                'qupfolio' => $des[5],
                'qupquantity' =>$des[1],
                'quptotal' =>  $des[4],
                'qupstatus' =>  '1',
                'qupuser' =>$des[6],
                'qupunitprice' => $des[2],
                'qupconversion' => $des[3],
                'qupdate' =>$fechaActual
              ]);
          }
      }
<<<<<<< HEAD

=======
>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
      if($request->array3 != ""){
        foreach($request->array3 as $cues){
          $cuestionarios = explode(',',$cues);
          // var_dump($cuestionarios);
          $Cuestion =  quotation_primary_questionnaires::create([
            'qpqqufk' => $cuestionarios[0] ,
            'qpqqtfk' => $idquotations,
            'qpqfolio' => $cuestionarios[5],
            'qpqquantity' => $cuestionarios[1],
            'qpqtotal' => $cuestionarios[4],
            'qpqdate' =>$fechaActual,
            'qpqstatus' => '1',
            'qpqunitprice' => $cuestionarios[2],
            'qpqconversion' => $cuestionarios[3],
            'qpquser' => $cuestionarios[6]
          ]);
        }
      }
      if($request->arrayanalisis != ""){
        foreach($request->arrayanalisis as $analisis) {
          $ana = explode(',',$analisis);
           // var_dump($ana);
          $analisi = quotation_secondary_questionnaires::create([
            'qsqqtfk' => $ana[0] ,
            'qsqqtfkc' => $idquotations,
            'qsqfolio' => $ana[5],
            'qsqquantity' => $ana[1],
            'qsqtotal' => $ana[4],
            'qsqdate' => $fechaActual,
            'qsqstatus' => '1',
            'qsqunitprice' => $ana[2] ,
            'qsqconversion' => $ana[3],
            'qsquser' => $ana[6]
          ]);
        }
      }
      //  return   $this->show($folio,$iduser,$idquotations);
        return  $idquotations;
    }

    public function getid(Request $request, $id){

      $valor = DB::table('catalog_master')
        ->select('cmfk','cmmax')
        ->where('cmfk','=',$id)
        ->get();

        return response::json($valor);
     }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
         $quotations = quotations::select('qtfk','qtfolio')
         ->where('qtfk',$request->id)
         ->get();
         foreach ($quotations as $cotizacion) {
           $id = $cotizacion->qtfk;
           $folio = $cotizacion->qtfolio;

           $roles = quotations::select('perfiles.qupid','roles.name','perfiles.qupquantity','perfiles.qupunitprice','perfiles.qupconversion','perfiles.quptotal')
           ->join('quotation_user_profiles AS perfiles','perfiles.qupqtfk','=','quotations.qtfk')
           ->join('roles','roles.id','=','perfiles.qupid')
           ->where('quotations.qtfolio',$folio)
           ->where('quotations.qtfk',$id)
            ->get();

            $cuestionarios = quotations::select('questionnaires.qufk','questionnaires.quname','cuestionarios.qpqquantity','cuestionarios.qpqunitprice','cuestionarios.qpqconversion','cuestionarios.qpqtotal')
            ->join('quotation_primary_questionnaires as cuestionarios','cuestionarios.qpqqtfk','=','quotations.qtfk')
            ->join('questionnaires','questionnaires.qufk','=','cuestionarios.qpqqufk')
            ->where('quotations.qtfolio',$folio)
            ->where('quotations.qtfk',$id)
             ->get();

             $analisis = quotations::select('questionnaires.qufk','questionnaires.quname','analisis.qsqquantity','analisis.qsqunitprice','analisis.qsqconversion','analisis.qsqtotal')
             ->join('quotation_secondary_questionnaires as analisis','analisis.qsqqtfkc','=','quotations.qtfk')
             ->join('questionnaires','questionnaires.qufk','=','analisis.qsqqtfk')
             ->where('quotations.qtfolio',$folio)
             ->where('quotations.qtfk',$id)
              ->get();
             $user = User::select('name','apaternal','amaternal','email','folio','wkrkarea','job','phone_fixed','quotations.qtdate','quotations.qtsubtotal','quotations.qtiva','quotations.qttotal','quotations.qtdateend','quotations.qtcmfk','quotations.qtdiscount','quotations.qtporcentaje')
             ->join('quotations','quotations.qtfolio','=','users.folio')
             ->where('quotations.qtfolio',$folio)
             ->where('quotations.qtfk',$id)
              ->get();
              $compania = Company::select('coname','cosocrzn')
              ->join('users','users.id','=','companies.couserfk')
              ->where('users.folio',$folio)
               ->get();
               $divisas = catalog_master::
                 select('cmfk','cmtpcat','cmdesc','cmstatus','cmmin','cmmax','cmmoneda')
                 ->where('cmtpcat','=','60')
                 ->where('cmstatus','=','1')
                 ->orderby('cmfk','ASC')
                 ->get();
                 //enviamos el email
                 foreach ($user as $us) {
                   $correo = $us->email;
                  $dates = array('email'=>$us->email,'roles'=>$roles,'cuestionarios'=>$cuestionarios,'analisis'=>$analisis,'user'=>$user,
                  'compania'=>$compania,'divisas'=>$divisas,'id'=>$id);
                   $this->Email($dates,$correo);
                   }
                   Session::flash('flash_registro','Se ha enviado a su correo y guardado la cotizacion del usuario');
             return view('cotizador.pdf',compact('id','roles','cuestionarios','analisis','user','compania','divisas'))->with('message');

         }
        // return response()->json([
      //   "mensaje" => "creado"
      // ]);
    }
    function Email($dates,$email){
    Mail::send('emails.cotizador',$dates, function($message) use ($email){
      $message->subject('Bienvenido a la plataforma');
      $message->to($email);
      $message->from('no-repply@geeklife.com.ve','CIS');
    });
  }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
