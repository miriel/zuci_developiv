<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use App\QuestionnaireFile;
use Chumper\Zipper\Zipper; // Libreria para poder comprimir archivos

class FileTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $resquest)
    {

      $id = $resquest->id;
      $carbon = new Carbon();
      $date = Carbon::now();
      $fechaActual = $date->format('d-m-Y');

      $catStatus = DB::table('catalog_master')
        ->select('catalog_master.cmdesc as nameStatus','catalog_master.cmval as valStatus')
        ->where('catalog_master.cmtpcat','=',58)
        ->where('catalog_master.cmstatus','=','1')
        ->get();

      $tipoDocumentos = DB::table('catalogs')
        ->where('catnamtabl','=','document_type')
        ->where('cattype','=','file')
        ->pluck('catvalue','catfk');

      $filesQuestionnaries = DB::table('questionnaire_files')
          ->select('questionnaire_files.qffk as idFile','questionnaire_files.qfnamefile','questionnaire_files.qfnopags',
                   'questionnaire_files.qfstatus as status','questionnaire_files.qfnopags','questionnaire_files.qfsize',
                   'catalogs.catvalue AS tipoFile')
          ->join('catalogs','questionnaire_files.qftypfil','=','catalogs.catfk')
          ->where('questionnaire_files.qfqafk','=',$id  )
          ->orderBy('questionnaire_files.qffk','DESC')
          ->get();

      $detalle=DB::table('questionnaire_answers')
          ->join('answer_type_questionnaires','questionnaire_answers.qaatqfk','=','answer_type_questionnaires.atqbk')
          ->join('questionnaires','answer_type_questionnaires.atqqufk','=','questionnaires.qufk')
          ->leftjoin('catalogs','questionnaire_answers.qascafk','=','catalogs.catfk')
          ->leftjoin('catalogs AS cat2','questionnaire_answers.qasbcatfk','=','cat2.catfk')
          ->leftjoin('questions','questionnaire_answers.qaqsfk','=','questions.qsfk')
          ->where('questionnaire_answers.qafk','=',$id)
          ->select('questionnaires.quname AS nameCuestionario','catalogs.catvalue AS nameSeccion','cat2.catvalue AS nameSubSeccion',
                    'questionnaire_answers.qascafk AS idSeccion','questionnaire_answers.qasbcatfk AS idSubsecc',
                    'questionnaire_answers.qaqsfk AS idPreg','questionnaire_answers.qaanfk AS idResp',
                    'questions.qsname')
          ->get();

      return view('cuestionarios.cuestionario.form.fileTemplate',compact('detalle','fechaActual','tipoDocumentos','id','filesQuestionnaries',
                  'catStatus'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $carbon = new Carbon();
      $date = Carbon::now();
      $fechaActual = $date->format('dmYHis');

      //Valida que existan archivos seleccionados
      if($request->fileCheck){

          $fileSep = explode(",",$request->fileCheck);
          for($i=0; $i<count($fileSep); $i++){
            // Se acumula el resultado de las opciones seleccionadas en un Arreglo
            $arrarFilsD[] = $fileSep[$i];
          }

          // Se ejecuta query que obtiene las opciones seleccionadas mediante el arreglo acumulado
          $nameFile=QuestionnaireFile::select('qfnamefile')->whereIn('qffk', $arrarFilsD)->get();

          foreach($nameFile as $filesAddZip){

              // Agrega los archivos seleccionador al archivo temporal
              $files = glob(public_path('archivos/cuestionarios/templates/'.$filesAddZip->qfnamefile));
              // Crear el archivo zip temporal
              \Zipper::make('archivos/tmp/filestmp_'.$fechaActual.'.zip')->add($files)->close();
          }

          // Decarga el archivo temporal en la PC del usuario
          return response()->download(public_path('/archivos/tmp/filestmp_'.$fechaActual.'.zip'));

      }else{
        // Si no existen archivos seleccionados regresa a la pantalla inicial
        return back();
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $carbon = new Carbon();
      $date = Carbon::now();
      $fechaActual = $date->format('Ymd');
      $fechaActual2 = $date->format('Y-m-d H:i:s');

      $rutaLocal = '/archivos/cuestionarios/templates/';
      $path = public_path().$rutaLocal;
      $files = $request->file('file');
      $idSeccion = $request->idSeccion;
      $idSubSeccion = $request->idSubSeccion;
      $tipoFile = $request->tipoFile;
      $idPreg = $request->idPregunta;
      $idResp = $request->idRespuesta;


      $siglaFile = DB::table('catalogs')
          ->select('catacronym')
          ->where('catfk','=',$tipoFile)
          ->get();
      $filename="";
      // Foreach para multimples archivos se modifica la variable por file
      //foreach($files as $file){ $file;
            $fileOriginName = $files->getClientOriginalName(); // Obtiene el nombre original del archivo
            $extension = $files->getClientOriginalExtension(); // Obtiene la extencion del archivo
            $tamañoArchivoByte = filesize($files); // Obtiene el tamaño del archivo
            $filename = pathinfo($fileOriginName, PATHINFO_FILENAME);

            // Valida si existe el nombre del archivo cargado
            $buscaTemplExist = db::table('template_questionnaires')
              ->selectRaw('COUNT(qtfk) as total')
              ->where('qtnamefile','=',$filename.".".strtolower($extension))
              ->where('qtqufk','=',$request->idCuestionario)
              ->get();

            if( $buscaTemplExist[0]->total > 0 ){

              $files->move($path, $filename.".".strtolower($extension));

            }else{

              $fileCreate = QuestionnaireFile::create([
                  'qfqafk' => $request->idQuestionAnswer,
                  'qfnamefile' => $filename.".".strtolower($extension),
                  'qfroute' => $rutaLocal,
                  'qfnopags' => $request->noPaginas,
                  'qfsize' => $tamañoArchivoByte,
                  'qftypfil' => $tipoFile,
                  'qfip' => $request->ip(),
                  'qfuserfk' => $request->user()->id,
                  'qfinsertdt' => $fechaActual2,
                  'qfupddt' => $fechaActual2,
              ]);

              if($fileCreate->qffk > 0){
                $files->move($path, $filename.".".strtolower($extension) );
              }
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
      $sttusUP="";

      if( $request->deleteAllFiles == 1){
          if($request->datos != ""){
            foreach($request->datos as $datos){
              if($datos > 0){
                    //Busca los templates que seleccionaron
                    $qryVal = QuestionnaireFile::find($datos);

                    // Valida si estan en estatus 1 actualiza a cero
                    if( $qryVal->qfstatus == 1 ){ $statsOk=0;
                    // Valida si estan en estatus 0 actualiza a activo
                    }else{ $statsOk=1; }

                    $qryValUP = QuestionnaireFile::find($datos);
                    $qryValUP->qfstatus = $statsOk;
                    $qryValUP->save();

              }
            }
          }

      }else{
        if($request->status == 1){
          $sttusUP=0;
        }else{
          $sttusUP=1;
        }

        $updtFilTmplCuest = QuestionnaireFile::find($id);
        $updtFilTmplCuest->qfstatus	= $sttusUP;
        $updtFilTmplCuest->save();

      }

      return response()->json([
        "mensaje" => "creado"
      ]);
    }


    protected function downloadFile($src){
        if(is_file($src)){
          $finfo = finfo_open(FILEINFO_MIME_TYPE);
          $content_type = finfo_file($finfo, $src);
          finfo_close($finfo);
          $file_name=basename($src).PHP_EOL;
          $size = filesize($src);
          header("Content-Type: $content_type");
          header("Content-Disposition: attachment; filename=$file_name");
          header("Content-Transfer-Encoding: binary");
          header("Content-Length: $size");
          readfile($src);
          return true;
        }else{
          return false;
        }
    }

    protected function download($file){
      if(!$this->downloadFile(public_path()."/archivos/cuestionarios/templates/".$file)){
        return redirect()->back();
      }
    }

    // Envia al siguiente nivel de revision
    public function signiv1(Request $request, $id){
      $sigNivelFile = AnswerFile::find($id);
      $sigNivelFile->afstatus = '2';
      $sigNivelFile->save();

      return response()->json([
        "mensaje" => "creado"
      ]);
    }
}
