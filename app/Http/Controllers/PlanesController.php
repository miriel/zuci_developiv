<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Packages;
use DB;
use Laracasts\Flash\flash;
use Session;
use Illuminate\Support\Collection;
use App\Http\Requests\PlanesRequest;

class PlanesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

      $planes = Packages::where('pgstatus','=','1')->orderBy('pgfk','ASC')->Paginate(10);
      return view('catalogos.planes.index',compact('planes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('catalogos.planes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PlanesRequest $request)
    {
      if($request->ajax())
         {

           $concat="";
           $count= 0;
           $coma="";
          foreach ($request->pgdesc as  $val) {
             if($count> 0){
               $coma= ",";
             }
             $concat.= $coma.$val;
             $count++;
            }

            $plan = Packages::create([
             'pgname' =>$request->pgname,
             'pgdesc' =>$concat,
             'pgprice' =>$request->pgprice,
             'pghowprice'=>$request->pghowprice,
             'pgip' => \Request::ip(),
             'pguserpk' => $request->user()->id,
              ]);
              return $request;
           }

      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $planes = Packages::where('pgstatus','=','1')->orderBy('pgfk','ASC')->get();
      return view('catalogos.planes.planes',compact('planes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $planes = DB::table('packages')
          ->select('pgfk','pgname','pgdesc','pgprice','pgquantity','pghowprice')
          ->where('pgfk','=',$id)
          ->get();


         // return $planes;
          return view('catalogos.planes.edit',compact('planes'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $planes=  $request->validate([
           'pgprice'=>'required|numeric|max:999999',
       ],[
          // 'name.required'=> trans('welcome.home'),llmar desde el lenguaje el mensaje
          'pgprice.max'=>'El precio no debe exeder el 1000000',
        ]);
        $concat="";
        $count= 0;
        $coma="";
       foreach ($request->pgdesc as  $val) {
          if($count> 0){
            $coma= ",";
          }
          $concat .= $coma.$val;
          $count++;
         }
      $planes = Packages::find($id);
      $planes->fill($request->all());
      $planes->pghowprice=$request->showprice;
      $planes->pgdesc=$concat;
      $planes->save();
      Session::flash('flash_success','EL Plan '.$planes->pgname.' ha sido editado con exito!');
      return redirect()->route('planes.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
      if($request->status == 1){
          $status=0;
          $mnsgStatus="baja";
      }else{
          $status=1;
          $mnsgStatus="alta";
      }
      // funcion de eliminar
      $plan = Packages::find($id);
      $plan->pgstatus=$status;
      $plan->save();

      Session::flash('flash_success','EL Plan '.$plan->pgname.' ha sido  con exito!');
      // return redirect()->route('giro.index');
      return back();
    }
    public function baja()
    {
      // $giro = Company_Turn::orderBy('id','DESC')->paginate();
        $planes = Packages::where('pgstatus','=','0')->orderBy('pgfk','ASC')->Paginate(10);
        return view('catalogos.planes.index',compact('planes'));
    }
}
