<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\catalog_master;
use DB;
use View;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Facades\Auth;
use Session;

class CataAnalisisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $this->authorize('view', new catalog_master);
      $status = $request->status;
      if($request->status == ""){ $status = 1; }  $status = $request->cmtpcat;

      $analisis = DB::table('catalog_master')
      ->select('catalog_master.cmfk','catalog_master.cmtpcat','catalog_master.cmdesc','catalog_master.cmabbr','catalog_master.cmstatus')
      ->where('catalog_master.cmtpcat','=','50')
      ->Where('catalog_master.cmstatus','=','1')
      ->orderBy('catalog_master.cmfk','ASC')
      ->paginate(10,['*'],'analisis1');
      $analisisBaja = DB::table('catalog_master')
      ->select('catalog_master.cmfk','catalog_master.cmtpcat','catalog_master.cmdesc','catalog_master.cmabbr','catalog_master.cmstatus')
      ->where('catalog_master.cmtpcat','=','50')
      ->Where('catalog_master.cmstatus','=','0')
      ->orderBy('catalog_master.cmfk','ASC')
      ->paginate(5,['*'],'analisis11');

      $analisis51 = DB::table('catalog_master')
      ->select('catalog_master.cmfk','catalog_master.cmtpcat','catalog_master.cmdesc','catalog_master.cmabbr','catalog_master.cmstatus' )
      ->where('catalog_master.cmtpcat','=','51')
      ->Where('catalog_master.cmstatus','=','1')
      ->orderBy('catalog_master.cmfk','ASC')
      ->paginate(10,['*'],'analisis1');
      $analisis51Baja = DB::table('catalog_master')
      ->select('catalog_master.cmfk','catalog_master.cmtpcat','catalog_master.cmdesc','catalog_master.cmabbr','catalog_master.cmstatus' )
      ->where('catalog_master.cmtpcat','=','51')
      ->Where('catalog_master.cmstatus','=','0')
      ->orderBy('catalog_master.cmfk','ASC')
      ->paginate(10,['*'],'analisis1');

      $analisis52 = DB::table('catalog_master')
      ->select('catalog_master.cmfk','catalog_master.cmtpcat','catalog_master.cmdesc','catalog_master.cmabbr','catalog_master.cmstatus' )
      ->where('catalog_master.cmtpcat','=','52')
      ->Where('catalog_master.cmstatus','=','1')
      ->orderBy('catalog_master.cmfk','ASC')
      ->paginate(10,['*'],'analisis1');

      $analisis52Baja = DB::table('catalog_master')
      ->select('catalog_master.cmfk','catalog_master.cmtpcat','catalog_master.cmdesc','catalog_master.cmabbr','catalog_master.cmstatus' )
      ->where('catalog_master.cmtpcat','=','52')
      ->Where('catalog_master.cmstatus','=','0')
      ->orderBy('catalog_master.cmfk','ASC')
      ->paginate(10,['*'],'analisis1');

      $analisis53 = DB::table('catalog_master')
      ->select('catalog_master.cmfk','catalog_master.cmtpcat','catalog_master.cmdesc','catalog_master.cmabbr','catalog_master.cmstatus' )
      ->whereIn('catalog_master.cmtpcat',[53,54,55,56,57])
      ->Where('catalog_master.cmstatus','=','1')
      ->orderBy('catalog_master.cmfk','ASC')
      ->paginate(10,['*'],'analisis1');

      $divisas = DB::table('catalog_master')
        ->select('cmfk','cmtpcat','cmdesc','cmstatus','cmmin','cmmax','cmmoneda')
        ->where('cmtpcat','=','60')
        ->where('cmstatus','=','1')
        ->get();

       return view('analisis.catalogos.index',compact('status','analisis','analisisBaja','analisis51','analisis51Baja','analisis52','analisis52Baja','analisis53','divisas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $catalogos=  $this->validate($request, [
           'cmdesc' => 'required',
           'cmabbr' => 'required',
           'cmval' => 'required',
           // 'cmmin' => 'required',
           // 'cmmax' => 'required',
           'cmtpcat' => 'required',
       ]);


         $catalogos = catalog_master::create([
           'cmtpcat' => $request->cmtpcat,
           'cmdesc' => $request->cmdesc,
           'cmabbr' => $request->cmabbr,
           'cmval' => $request->cmval,
           'cmmin' => $request->cmmin,
           'cmmax' => $request->cmmax,
           'cmmoneda' => $request->cmmoneda
         ]);

         return response()->json([
           "mensaje" => "creado"
         ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->authorize('update', new catalog_master);
      $actividad= $this->validate($request, [
            'cmdesc' => 'required',
            'cmabbr' => 'required',
            'cmval' => 'required',
       ]);

       $actividad = catalog_master::find($id);
       $actividad->cmdesc = $request->cmdesc;
       $actividad->cmabbr = $request->cmabbr;
       $actividad->cmval = $request->cmval;
       $actividad->cmmax = $request->cmmax;
       $actividad->cmmin = $request->cmmin;
       $actividad->cmmoneda = $request->cmmoneda;
       $actividad->save();

       return response()->json([
         "mensaje" => "creado"
       ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cmfk, Request $request)
    {
      if($request->status == 1){
          $estaus=0;
          $mnsStatus = "baja";
      }else{
          $estaus=1;
          $mnsStatus = "alta";
      }

      $catalogos= catalog_master::find($cmfk);
      $catalogos->cmstatus = $estaus;
      $catalogos->save();
      Session::flash('message','El registro: '.$request->desc." fue dada de ".$mnsStatus);
      return back();

    }
}
