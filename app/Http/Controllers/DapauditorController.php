<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use App\CompanyAnswer;
use App\AnswerResponse;
use App\StatusQuestionnarie;
use App\User;

class DapauditorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
           $folio = Auth::user()->folio;

           $cuestionarios = DB::table('company_answers')
             ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
             ->join('answer_type_questionnaires','questionnaire_answers.qaatqfk','=','answer_type_questionnaires.atqbk')
             ->join('questionnaires','answer_type_questionnaires.atqqufk','=','questionnaires.qufk')
             ->join('questionnaries_status','company_answers.casqfk','=','questionnaries_status.sqfk')
             ->select('answer_type_questionnaires.atqqufk AS id','questionnaires.quname as description' ,'questionnaries_status.sqstatus AS status')
             ->where('company_answers.cafoliofk','=',$folio)
             ->groupBy('answer_type_questionnaires.atqqufk' ,'questionnaires.quname' ,'questionnaries_status.sqstatus')
             ->paginate(10);

           return view('miscuestionarios.index',compact('cuestionarios'));
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
         //
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {
       //
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
         $users = User::get();
           return view('miscuestionarios.users',compact('users'));
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {

         $folio = Auth::user()->folio;

         $cuestPreguntas = DB::table('company_answers')
           ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
           ->join('questions','questionnaire_answers.qaqsfk','=','questions.qsfk')
           ->join('answer_type_questionnaires','questionnaire_answers.qaatqfk','=','answer_type_questionnaires.atqbk')
           ->select('questions.qsfk AS idPregunta','questions.qsname AS pregunta','answer_type_questionnaires.atqqufk AS idCuestion')
           ->where('answer_type_questionnaires.atqqufk','=',$id)
           ->where('company_answers.cafoliofk','=',$folio)
           ->groupBy('questions.qsfk','questions.qsname','answer_type_questionnaires.atqqufk')
           ->orderBy('questions.qsfk')
           ->paginate(100);


           $cuestRespuestas = DB::table('company_answers')
             ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
             ->join('questions','questionnaire_answers.qaqsfk','=','questions.qsfk')
             ->join('answer_type_questionnaires','questionnaire_answers.qaatqfk','=','answer_type_questionnaires.atqbk')
             ->join('answers','questionnaire_answers.qaanfk','=','answers.anfk')
             ->select('questions.qsfk AS idPregunta','questions.qsname AS pregunta','answers.anfk AS idRespuesta',
                 'answers.anname AS respuesta','answers.anatfk AS tipoCaja','company_answers.cafk AS idCA',
                 'company_answers.cavalue AS value','answers.anvalue AS valCheck')
             ->where('answer_type_questionnaires.atqqufk','=',$id)
             ->where('company_answers.cafoliofk','=',$folio)
             ->orderBy('questions.qsfk')
             ->paginate(100);

             $secciones = DB::table('company_answers')
                   ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
                   ->leftjoin('questions','questionnaire_answers.qaqsfk','=','questions.qsfk')
                   ->leftjoin('answers','questionnaire_answers.qaanfk','=','answers.anfk')
                   ->leftjoin('answer_type_questionnaires','questionnaire_answers.qaatqfk','=','answer_type_questionnaires.atqbk')
                   ->leftjoin('questionnaires','answer_type_questionnaires.atqqufk','=','questionnaires.qufk')
                   ->leftjoin('catalogs','questionnaire_answers.qascafk','=','catalogs.catfk')
                   ->leftjoin ('catalogs as catsub','questionnaire_answers.qasbcatfk','=','catsub.catfk')
                   ->select(
                     'questionnaires.qufk AS idCuestionario',
                     'questionnaires.quname AS NameCuestionario',
                     'catalogs.catfk as idSeccion', 'catalogs.catvalue as seccion',
                     'catsub.catfk as idSubSeccion', 'catsub.catvalue as subseccion',
                     'questions.qsfk AS idPregunta', 'questions.qsname AS pregunta',
                     'answers.anfk AS idRespuesta','answers.anname AS respuesta',
                     'answers.anvalue AS valPonder',
                     'answers.anatfk AS tiporesp',
                     'company_answers.cafk as idRespPreg','company_answers.cavalue AS value'
                    )
                   ->where('answer_type_questionnaires.atqqufk','=', $id)
                   ->where('company_answers.cafoliofk','=',$folio)
                   ->orderby('answers.anfk')
                   ->get();
               $cuestionarios = DB::table('company_answers')
                 ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
                 ->join('answer_type_questionnaires','questionnaire_answers.qaatqfk','=','answer_type_questionnaires.atqbk')
                 ->join('questionnaires','answer_type_questionnaires.atqqufk','=','questionnaires.qufk')
                 ->join('questionnaries_status','company_answers.casqfk','=','questionnaries_status.sqfk')
                 ->select('answer_type_questionnaires.atqqufk AS id','questionnaires.quname as description' ,'questionnaries_status.sqstatus AS status')
                 ->where('company_answers.cafoliofk','=',$folio)
                 ->groupBy('answer_type_questionnaires.atqqufk' ,'questionnaires.quname' ,'questionnaries_status.sqstatus')
                 ->paginate(10);

               $contador = 0;
               while ($contador < count($secciones)) { //Extraemos los datos del array de uno en uno mientras haya datos
                 $arrayIdCuestionario[$secciones[$contador]->idCuestionario] = $secciones[$contador]->idCuestionario;
                 $arrayIdSecciones[$secciones[$contador]->idSeccion] = $secciones[$contador]->idSeccion;
                 $arrayIdSubSecciones[$secciones[$contador]->idSubSeccion] = $secciones[$contador]->idSubSeccion;
                 $arrayIdPreguntas[$secciones[$contador]->idPregunta] = $secciones[$contador]->idPregunta;
                 $arrayIdRespuesta[$secciones[$contador]->idRespuesta] = $secciones[$contador]->idRespuesta;
                 //$arrayIdPregResp[$secciones[$contador]->idRespPreg] = $secciones[$contador]->idRespPreg;

                 $arrayNameCuestionario[$secciones[$contador]->idCuestionario] = $secciones[$contador]->NameCuestionario;
                 $arrayNameSeccion[$secciones[$contador]->idSeccion] = $secciones[$contador]->seccion;
                 $arrayNameSubseccion[$secciones[$contador]->idSeccion][$secciones[$contador]->idSubSeccion] = $secciones[$contador]->subseccion;
                 $arrayNamePregunta[$secciones[$contador]->idSeccion][$secciones[$contador]->idSubSeccion][$secciones[$contador]->idPregunta] = $secciones[$contador]->pregunta;
                 $arrayNameRespuesta[$secciones[$contador]->idSeccion][$secciones[$contador]->idSubSeccion][$secciones[$contador]->idPregunta][$secciones[$contador]->idRespuesta] = $secciones[$contador]->respuesta;
                 $arrayTipoRespuesta[$secciones[$contador]->idSeccion][$secciones[$contador]->idSubSeccion][$secciones[$contador]->idPregunta][$secciones[$contador]->idRespuesta] = $secciones[$contador]->tiporesp;
                 // Valor de ponderacion de laa respuesta asignada
                 $arrayValPondRespuesta[$secciones[$contador]->idSeccion][$secciones[$contador]->idSubSeccion][$secciones[$contador]->idPregunta][$secciones[$contador]->idRespuesta] = $secciones[$contador]->valPonder;
                 $arrayIdPregRep[$secciones[$contador]->idSeccion][$secciones[$contador]->idSubSeccion][$secciones[$contador]->idPregunta][$secciones[$contador]->idRespuesta]
                                 = $secciones[$contador]->idRespPreg;
                 $arrayPregRepValue[$secciones[$contador]->idSeccion][$secciones[$contador]->idSubSeccion][$secciones[$contador]->idPregunta][$secciones[$contador]->idRespuesta]
                                 = $secciones[$contador]->value;
                 $contador++;
               }

   /*
       foreach($arrayIdSecciones as $idSeccion){
         echo "ID seccion:" . $idSeccion." Name seccion: ".$arrayNameSeccion[$idSeccion]."<br>";

         foreach($arrayIdSubSecciones as $idSubSeccion){
           if(!isset($arrayNameSubseccion[$idSeccion][$idSubSeccion])) {

           }else{
             echo  "Subseccion".$arrayNameSubseccion[$idSeccion][$idSubSeccion]."<br>";
           }
           foreach($arrayIdPreguntas as $idPregunta){
             if(!isset($arrayNamePregunta[$idSeccion][$idSubSeccion][$idPregunta])) {

             }else{
               echo  "Peguntas: ".$arrayNamePregunta[$idSeccion][$idSubSeccion][$idPregunta]."<br>";
             }

              foreach($arrayIdRespuesta as $idRespuesta){
               if(!isset($arrayNameRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta])) {

               }else{
                 echo  "Respuesta: ".$arrayNameRespuesta[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta];
               }

               if(isset($arrayIdPregRep[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta])) {
                 echo "idRespPregunta: ".$arrayIdPregRep[$idSeccion][$idSubSeccion][$idPregunta][$idRespuesta];
               }

             }

           }
         }
       }
   */

     return view("miscuestionarios.edit" ,
           compact(
                   'arrayIdSecciones','arrayNameSeccion','arrayIdSubSecciones','arrayNameSubseccion','arrayIdPreguntas','arrayNamePregunta',
                   'arrayIdRespuesta','arrayNameRespuesta','arrayTipoRespuesta','arrayIdPregRep','arrayPregRepValue','arrayValPondRespuesta',
                   'id','cuestPreguntas','cuestRespuestas','contador', 'cuestionarios','arrayIdCuestionario','arrayNameCuestionario'
           )
       );

     }


     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {
       $folio = Auth::user()->folio;

       $this->validaPresguntaCuest($request->idCuestion, $request->idPregunta, $folio, $request->totPreguntas, $request->ip(), $request->user()->id);

           // Recibe las respuestas de tipo radiobutton (varible del .js)
          if( $request->tipoRespuesta == 4  ){
            // Define si seleccionaron un radio button con cuestionarios con secciones
            if($request->secciones == 1){ echo "entra con seccion";
                  $actResp = DB::table('company_answers')
                    ->select('company_answers.cafk AS id')
                    ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
                    ->join('answers','questionnaire_answers.qaanfk','=','answers.anfk')
                    ->where('company_answers.cafoliofk','=', $folio )
                    ->where('answers.anatfk','=', $request->tipoRespuesta )
                    ->where('questionnaire_answers.qaqsfk','=', $request->idPregunta )
                    ->get();

                 foreach($actResp as $actR){
                     $seteo1 = CompanyAnswer::find($actR->id);
                     $seteo1->cavalue = '';
                     $seteo1->save();

                     $seteo2 = CompanyAnswer::find($id);
                     $seteo2->fill($request->all());
                     $seteo2->cavalue = $request->value;
                     $seteo2->caip = $request->ip();
                     $seteo2->causerfk = $request->user()->id;
                     $seteo2->save();
                 }


            // Define si seleccionaron un radio button con cuestionarios sin secciones
          }else{
              $actResp = DB::table('company_answers')
                ->select('company_answers.cafk AS id')
                ->join('questionnaire_answers','company_answers.caqafk','=','questionnaire_answers.qafk')
                ->where('company_answers.cafoliofk','=', $folio )
                ->where('questionnaire_answers.qaqsfk','=', $request->idPregunta )
                ->get();

                foreach($actResp as $resp){
                  $seteo1 = CompanyAnswer::find($resp->id);
                  $seteo1->cavalue = '';
                  $seteo1->save();

                  $seteo2 = CompanyAnswer::find($id);
                  $seteo2->fill($request->all());
                  $seteo2->cavalue = $request->value;
                  $seteo2->caip = $request->ip();
                  $seteo2->causerfk = $request->user()->id;
                  $seteo2->save();
               }
            }


         }else{

           // Recibe las respuestas difetentes al radio button
           $respuesta = CompanyAnswer::find($id);
           $respuesta->fill($request->all());
           $respuesta->cavalue = $request->value;
           $respuesta->caip = $request->ip();
           $respuesta->causerfk = $request->user()->id;
           $respuesta->save();

         }

     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
         //
     }

     public function validaPresguntaCuest($idCuest, $idPregunta, $folio, $totPreg, $ip, $user){
         $count=0;
         $insert=1;

         // Cuenta los numero
         $actResp = DB::table('answered_response')
           ->select('arqufk','arqsfk AS idquestion','arfoliofk')
           ->where('arqufk','=', $idCuest )
           ->where('arfoliofk','=', $folio )
           ->get();

             if(count($actResp) == 0){

               AnswerResponse::create([
                   'arqufk' => $idCuest,
                   'arqsfk' => $idPregunta,
                   'arfoliofk'	 => $folio,
                   'arip' => $ip,
                   'arusuerfk' => $user,
               ]);

             }else{
               foreach($actResp as $resp){
                   // En caso de que le pregunta exista
                   if( $resp->idquestion == $idPregunta  ){
                     $insert=0;
                   }
                   $count++;
              }
           }

           if( $insert == 1){
             AnswerResponse::create([
                 'arqufk' => $idCuest,
                 'arqsfk' => $idPregunta,
                 'arfoliofk'	 => $folio,
                 'arip' => $ip,
                 'arusuerfk' => $user,
             ]);
             $count++;
           }

           if($totPreg == $count ){
             // cambia de estaus de cuestionario a contestado
             DB::table('questionnaries_status')
             ->where('sqfoliofk', $folio )
             ->where('sqqufk', $idCuest )
             ->update(['sqstatus' => 2]);
           }

     }
}
