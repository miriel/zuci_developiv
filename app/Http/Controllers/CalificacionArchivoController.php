<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\flash;
use App\Http\Requests\AdminCalification;
use Session;
use DB;
use App\catalog_master;
use Illuminate\Routing\Redirector;

class CalificacionArchivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = $request->status;
        $status = ($status == '') ? 1 : $status;

        $tiposCalificacionActivos = catalog_master::select('cmfk', 'cmtpcat','cmdesc', 'cmabbr', 'cmtpquest', 'cmval', 'cmstatus')->where('cmtpcat','=','59')->where('cmstatus','=','1')->get();
        $tiposCalificacionInactivo = catalog_master::select('cmfk', 'cmtpcat','cmdesc', 'cmabbr', 'cmtpquest', 'cmval', 'cmstatus')->where('cmtpcat','=','59')->where('cmstatus','=','0')->get();

        return view('catalogos/calificacionArchivo/index',compact('tiposCalificacionActivos','tiposCalificacionInactivo','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(AdminCalification $request)
    {
      if($_GET){

        $status = ($_GET['cmstatus'] == 'A') ? 1 : 0;

      //  $this->authorize('create', new catalog_master);
        $calificacion = new catalog_master;
        $calificacion->cmtpcat = 59;
        $calificacion->cmdesc = $_GET['cmdesc'];
        $calificacion->cmabbr = $_GET['cmabbr'];
        $calificacion->cmstatus = $status;
        $calificacion->cmtpquest = 1;
        $result = $calificacion->save();

        if($result){
          Session::flash('flash_success','El registro se creo satisfactoriamente!');
          return response()->json([
            "mensaje" => "creado"
          ]);
        }else{
          Session::flash('flash_delete','Hubo un problema al intentar crear el registro, intente de nuevo por favor!');
          return response()->json([
            "mensaje" => "fallo"
          ]);
        }
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $texto = catalog_master::find($id);
      return response()->json($texto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
             'cmdesc' => 'required',
             'cmabbr' => 'required',
           ]
        );
        $status = ($request->cmstatus == 'A') ? 1 : 0;

        $archivo = catalog_master::find($request->cmfk);
        $archivo->cmdesc = $request->cmdesc;
        $archivo->cmabbr = $request->cmabbr;
        $archivo->cmstatus = $status;
        $archivo->save();

        Session::flash('flash_success','El registro se actualizo satisfactoriamente!');

        return response()->json([
          "mensaje" => "creado"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $status = ($request->status == 1) ? 0 : 1;

        $actualizar = catalog_master::find($id);
        $actualizar->cmstatus = $status;
        $actualizar->save();

        if($status == 1){
          Session::flash('flash_success','El registro se activo satisfactoriamente!');
        }else{
          Session::flash('flash_delete','El tipo de archivo fue eliminado con exito!!');
        }
        return back();
    }
}
