<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\catalog_master;
use DB;
use View;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade as PDF;
use App\AnswerResponse;
use Khill\Lavacharts\Lavacharts;

class AriesgoEjecutivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
          $folio = $request->folio;
          $empresa = $request->empresa;

          $analisis = DB::table('catalog_master AS ope')
          ->select(
            'ope.cmfk AS idOperacion',
            'ope.cmdesc AS desOperacion'
          )
          ->where('ope.cmtpcat','=','2')
          ->Where('ope.cmstatus','=','1')
          ->where('ope.cmtpquest',$empresa) // traer esto de la secccion
          ->orderBy('ope.cmfk','ASC')
          ->get();

          return view('analisis.principalEjec',compact('analisis','folio','empresa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
      $idd = $id;
      $session = $request->folio;
      $tipoEmpresaUs =$request->empresa; // Variable que obtiene el id de la empresa del usuario seleccionado
      $cuestionario = $request->empresa;
      $folio = Auth::user()->folio;
      $idUserLogin = Auth::user()->id;

      // Arreglo de grupos para los valores del total del cuestionarios analisis
      $arrayGruppos  = [53,54,55,56,57];

      $analisis = DB::table('catalog_master AS ope')
        ->leftjoin('relationship_catalogs AS op_act','op_act.rccmfk','=','ope.cmfk')
        ->leftjoin('catalog_master AS act','act.cmtpcat','=','op_act.rctpcat')
        ->leftjoin('relationship_catalogs AS act_pel','act_pel.rccmfk','=','act.cmfk')
        ->leftjoin('catalog_master AS pel','pel.cmtpcat','=','act_pel.rctpcat')
        ->leftjoin('relationship_catalogs AS pel_des','pel_des.rccmfk','=','pel.cmfk')
        ->leftjoin('catalog_master AS des','des.cmtpcat','=','pel_des.rctpcat')
        ->select(
          'ope.cmfk as idActividad',
          'ope.cmdesc as NameActividad',
          'act.cmfk AS idOperacion',
          'act.cmdesc AS desOperacion',
          'pel.cmfk AS idActividad',  // Id de respuesta
          'pel.cmdesc AS desActividad', // nombre respuesta
          'des.cmtpcat AS idCatalogo',
          'des.cmfk AS idConsecuencias',
          'des.cmabbr AS desConsecuencias',
          'des.cmval AS valorConsecuencias'
        )
        ->where('ope.cmfk','=',$id)
        ->Where('act.cmstatus','=','1')
        ->where('pel.cmstatus','=','1')
        ->where('des.cmstatus','=','1')
        ->where('ope.cmtpquest',$cuestionario) //traer tipo de cuestionario en la de la session
        ->orderBy('act.cmfk','ASC')
        ->orderBy('pel.cmfk','ASC')
        ->orderBy('des.cmfk','ASC')
        ->get();
        $contador = 0;
        while($contador < count($analisis)){
          // $arrayIdAct[$analisis[$contador]->idActividad] = $analisis[$contador]->idActividad;
          // $arrayNameAct[$analisis[$contador]->idActividad] = $analisis[$contador]->NameActividad;

          $arrayIdOperacion[$analisis[$contador]->idOperacion] = $analisis[$contador]->idOperacion;
          $arrayNameOperacion[$analisis[$contador]->idOperacion] = $analisis[$contador]->desOperacion;

          $arrayIdActividad[$analisis[$contador]->idActividad] = $analisis[$contador]->idActividad;
          $arrayNameActividad[$analisis[$contador]->idOperacion][$analisis[$contador]->idActividad] = $analisis[$contador]->desActividad;

          $arrayIdCatalogo[$analisis[$contador]->idCatalogo] = $analisis[$contador]->idCatalogo;
          $arrayNameCatalogo[$analisis[$contador]->idOperacion][$analisis[$contador]->idActividad][$analisis[$contador]->idCatalogo]= $analisis[$contador]->idCatalogo;

          $arrayIdConsecuencias[$analisis[$contador]->idConsecuencias] = $analisis[$contador]->idConsecuencias;
          $arrayNameConsecuencias[$analisis[$contador]->idOperacion][$analisis[$contador]->idActividad][$analisis[$contador]->idCatalogo][$analisis[$contador]->idConsecuencias]= $analisis[$contador]->desConsecuencias;
          $arrayValorConsecuencias[$analisis[$contador]->idOperacion][$analisis[$contador]->idActividad][$analisis[$contador]->idCatalogo][$analisis[$contador]->idConsecuencias]= $analisis[$contador]->valorConsecuencias;

          $contador++;
        }



        $nombre = DB::table('catalog_master')
        ->select('catalog_master.cmfk',
        'catalog_master.cmdesc')
        ->where('catalog_master.cmfk','=',$id)
        ->get();
        $divisas1 = DB::table('catalog_master')
        ->select('catalog_master.cmfk','catalog_master.cmtpcat','catalog_master.cmdesc','catalog_master.cmabbr','catalog_master.cmstatus','catalog_master.cmmin','catalog_master.cmmax','catalog_master.cmmoneda' )
        ->whereIn('catalog_master.cmtpcat',[50,51,52,53,54,55,56,57])
        ->Where('catalog_master.cmstatus','=','1')
        ->orderBy('catalog_master.cmfk','ASC')
        ->get();
        $moneda =DB::table('catalog_master')
        ->select('catalog_master.cmfk','catalog_master.cmtpcat','catalog_master.cmdesc')
        ->Where('catalog_master.cmtpcat','=','60')
        ->orderBy('catalog_master.cmfk','ASC')
        ->get();

        // Obtiene el numero total de preguntas del cuestionario analisisde riesgo
        $totalPreguntas = DB::table('answered_analysis')
            ->select(DB::raw('count(answered_analysis.aractfk) as tot'))
            ->leftjoin('catalog_master','catalog_master.cmfk','=','answered_analysis.aradsdanger')
            ->leftjoin('catalog_master as cata','cata.cmfk','=','answered_analysis.araconsequen')
            ->leftjoin('catalog_master as cata1','cata1.cmfk','=','answered_analysis.araexpositio')
            ->leftjoin('catalog_master as cata2','cata2.cmfk','=','answered_analysis.araprobabili')
            ->where('answered_analysis.arafoliofk','=',$session)
            ->where('answered_analysis.aractfk','=',$tipoEmpresaUs)
            ->groupBy('answered_analysis.aractfk')
            ->get();


        return view('analisis.respuestaEjec',compact('cuestionario','analisis','nombre','idd','arrayIdOperacion','arrayNameOperacion','arrayIdActividad','arrayNameActividad',
        'arrayIdDescripcion','arrayNameDescripcion','arrayIdCatalogo','arrayNameCatalogo','arrayIdConsecuencias','arrayNameConsecuencias','arrayValorConsecuencias'
        ,'fechaActual','session', 'tipoEmpresaUs','divisas1','moneda','session','idcuestion','cuestionario','totalPreguntas','gruposRes','idUserLogin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      // Actualiza el: Plan de acción
      if( $request->tipoCaja == 'arplanacc' ){
        DB::table('answered_analysis')->where('ararfk', $request->id2)->update([ 'arplanacc' => $request->caja ]);
      }
      // Actualiza el: Depto. responsable
      if( $request->tipoCaja == 'ardepresp' ){
        DB::table('answered_analysis')->where('ararfk', $request->id2)->update([ 'ardepresp' => $request->caja ]);
      }

      // Actualiza el: Consecuencias para la empresa
      if( $request->tipoCaja == 'arconsemp' ){
        DB::table('answered_analysis')->where('ararfk', $request->id2)->update([ 'arconsemp' => $request->caja ]);
      }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function pdf(Request $request)
    {

      $id = $request->id;
      $idd = $id;
      $session = $request->folio;
      $idcuestion = $request->empresa;
      $carbon = new Carbon();
      $date = Carbon::now();
      $fechaActual = $date->format('d-m-Y');
      $cuestionario = $request->empresa;

      // Muestra el promedio por standar (seccion) por total de respuestas del total general y probabilidad
      $analisis = DB::table('catalog_master as ope')
        ->leftjoin('relationship_catalogs AS op_act','op_act.rccmfk','=','ope.cmfk')
        ->leftjoin('catalog_master AS act','act.cmtpcat','=','op_act.rctpcat')
        ->leftjoin('relationship_catalogs AS rela2','rela2.rccmfk','=','act.cmfk')
        ->leftjoin('catalog_master AS act2','act2.cmtpcat','=','rela2.rctpcat')
        ->leftjoin('answered_analysis AS respu','respu.aradsdanger','=','act2.cmfk')
        ->select(
          'ope.cmfk','ope.cmdesc',
          DB::raw('AVG(respu.aragrndtotal)as promVal'),
          DB::raw('AVG(respu.aratotprobab)as promProb')
          )
        //->where('ope.cmfk','=',$id)
        ->Where('ope.cmtpcat','=','2')
        ->where('ope.cmstatus','=','1')
        ->where('ope.cmtpquest',$cuestionario) //traer tipo de cuestionario en la de la session
        ->where('respu.arafoliofk',$session) //trae el folio de la session y l compara en la query
        ->groupBy('ope.cmfk')
        ->groupBy('ope.cmdesc')
        ->orderBy('ope.cmdesc','ASC')
        ->get();

        $pdf = \PDF::loadView('analisis.pdf.analisisEjec',compact('analisis'));
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 5000);
        $pdf->setOption('enable-smart-shrinking', true);
        $pdf->setOption('no-stop-slow-scripts', true);
        return $pdf->download('AnalisisEjecutivo.pdf');

    }

}
