<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Answer;
use App\AnswerType;
use Session;
use App\Http\Requests\AnswerRequest;
use App\QuestionnaireAnswer;
use Illuminate\Validation\Rule; // Libreria para la validacaion de RUles

class AnswerController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $status = $request->status;
      if($request->status == ""){ $status = 1; }
        $respuestasAl = DB::table('answers')
            ->join('answer_types','answers.anatfk','answer_types.atfk')
            ->select('answers.anfk AS id','answers.anname AS description','answer_types.atname AS tipo','answers.anstatus AS status')
            ->where('answers.anstatus','=',1)
            ->orderBy('answers.anfk')
            ->paginate(6,['*'],'resp1');
        $respuestasBa = DB::table('answers')
            ->join('answer_types','answers.anatfk','answer_types.atfk')
            ->select('answers.anfk AS id','answers.anname AS description','answer_types.atname AS tipo','answers.anstatus AS status')
            ->where('answers.anstatus','=',0)
            ->orderBy('answers.anfk')
            ->paginate(6,['*'],'resp2');
        $tipoRespuesta = AnswerType::where('atstatus','=','1')
              ->pluck('atname','atfk');
        return view("cuestionarios.respuestas.index", compact('respuestasAl','respuestasBa','status','tipoRespuesta'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipoRespuesta = AnswerType::where('atstatus','=','1')
              ->pluck('atname','atfk');
        return view("cuestionarios.respuestas.create", compact('tipoRespuesta'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnswerRequest $request)
    {
          $answers = Answer::create($request->all());
          $answers->anip = $request->ip();
          $answers->arusuerfk = $request->user()->id;
          $answers->save();

          return response()->json([
            "mensaje" => "creado"
          ]);

          /*Session::flash('message','Respuesta: '.$request->anname.' creada correctamente');
          return back();*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $respuesta = DB::table('answers')
          ->select('questionnaire_answers.qafk','answers.anname AS anname','answers.anatfk AS anatfk','answers.anvalue AS anvalue',
                   'questionnaire_answers.qastatus AS status',
                   'questionnaire_answers.qaanfk AS idRespuesta','answer_types.atname AS nameResp' )
          ->join('questionnaire_answers','answers.anfk','=','questionnaire_answers.qaanfk')
          ->join('answer_types','answers.anatfk','=','answer_types.atfk')
          ->where('questionnaire_answers.qafk','=',$id)
          ->get();
        return response()->json($respuesta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // Validacion y actualizacion de respuestas para el modulo cuestionarios
        if($request->modulo == 'cuestionarios'){
            // Validacion de cajas de texto sin enviar al RequestAnswer
            $this->idTipoCuestionario = $request->idTipoCuestionario;
            $this->idPregunta = $request->idPregunta;
            $this->validate($request, [
                  // Valida si que la pregunta sea requerida ademas de que la pregunta no sea repetida pero solamente en el mismo cuestionario
                  'qaanfk' => [
                        'required',
                        /*Rule::unique('questionnaire_answers')->where(function ($query) {
                            $query->where('qaatqfk', $this->idTipoCuestionario)
                                  ->where('qaqsfk', $this->idPregunta);
                        }),*/
                  ],
             ]);

             $pregRespt = QuestionnaireAnswer::find($request->idPregResp);
             $pregRespt->qaanfk = $request->qaanfk;
             $pregRespt->qastatus = $request->status;
             $pregRespt->save();
        }

        if($request->modulo == 'respuestas'){

            $empre= $this->validate($request, [
                 'anatfk' => 'required|numeric|min:1',
            ]);

            // ACtualiza valores del modulo de respuestas
            $respuesta = Answer::find($id);
            $respuesta->anname = $request->anname;
            $respuesta->anvalue = $request->anvalue;
            $respuesta->anatfk = $request->anatfk;
            $respuesta->anip = $request->ip();
            $respuesta->arusuerfk = $request->user()->id;
            $respuesta->save();
        }

        return response()->json([
          "mensaje" => "creado"
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        if($request->estatus == 1){
            $estatus=0;
            $mnsgStatus="baja";
        }else{
            $estatus=1;
            $mnsgStatus="alta";
        }
        $respuesta = Answer::find($id);
        $respuesta->anstatus = $estatus;
        $respuesta->save();
        Session::flash('message','La respuesta: '.$request->anname."  fue dada de ".$mnsgStatus);
        return back();
    }

    public function getAnswer(Request $request){
          if($request->ajax()){
              $preguntas = Answer::answers();
              return response()->json($preguntas);
          }
    }

    public function respuesta($id){
        $respuesta = Answer::find($id);
        return response()->json($respuesta);
    }
}
