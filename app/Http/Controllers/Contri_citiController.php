<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use Laracasts\Flash\flash;
use Session;
use App\Country;
use App\CountrycitiesModel;

class Contri_citiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $this->authorize('create', new CountrycitiesModel);
      $pais = Country::where('cnstatus','=','1')->pluck('cnname','cnfk');
      $ciudad = City::where('cistatus','=','1')->pluck('ciname','cifk');
        $arryVals = array('ciudad'=>$ciudad, 'pais'=> $pais);
      return view('catalogos.ciudad.agregar',compact('arryVals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $paciti = $this->validate($request,[
          'cccnfk' => 'required|numeric|min:1',
          'cccifk' => 'required|numeric|min:1|unique:country_cities',
      ]);
      $paciti = new CountrycitiesModel($request->all());
      $paciti->save();

      Session::flash('flash_success','La ciudad '.$paciti->country. ' ha sido agregado con pais '.$paciti->city);
      return redirect()->route('agrega.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
