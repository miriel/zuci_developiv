<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuestionnaireType;
use DB;
use Session;
use App\Http\Requests\QuestionnaireTypeRequest;

class QuestionnaireTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $status = $request->status;
      if($request->status == ""){ $status = 1; }

        $tipoCuestAl = DB::table('questionnaire_types')
          ->select('qtfk AS id','qtname  AS description','qtstatus AS status')
          ->orderBy('qtfk')
          ->where('qtstatus','=',1)
          ->paginate(6,['*'],'cuest1');

        $tipoCuestBa = DB::table('questionnaire_types')
          ->select('qtfk AS id','qtname  AS description','qtstatus AS status')
          ->orderBy('qtfk')
          ->where('qtstatus','=',0)
          ->paginate(6,['*'],'cuest2');
        return view('cuestionarios.tipo.index',compact('tipoCuestAl','tipoCuestBa','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', new QuestionnaireType);
        return view('cuestionarios.tipo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionnaireTypeRequest $request)
    {
      $this->authorize('create', new QuestionnaireType);
        $tipoCuest = QuestionnaireType::create($request->all());
        $tipoCuest->qtip = $request->ip();
        $tipoCuest->qtuserfk = $request->user()->id;
        $tipoCuest->save();

        return response()->json([
          "mensaje" => "creado"
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipoCuestionario = QuestionnaireType::find($id);
        return response()->json($tipoCuestionario);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      // Valida que sea el campo requerido sin enviar de nuevo a las pimeras validaciones del create
     $empre= $this->validate($request, [
           'qtname' => 'required',
      ]);

      $tipoCuestion = QuestionnaireType::find($id);
        $this->authorize('update', $tipoCuestion);
      $tipoCuestion->qtname = $request->qtname;
      $tipoCuestion->qtip = $request->ip();
      $tipoCuestion->qtuserfk = $request->user()->id;
      $tipoCuestion->save();

      return response()->json([
        "mensaje" => "creado"
      ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
          if($request->estatus == 1){
              $estatus=0;
              $mnsgStatus="baja";
          }else{
              $estatus=1;
              $mnsgStatus="alta";
          }
          $tipoCuest = QuestionnaireType::find($id);
            $this->authorize('delete', $tipoCuest);
          $tipoCuest->qtstatus = $estatus;
          $tipoCuest->save();
          Session::flash('message','El tipo de cuestionario: '.$request->description."  fue dada de ".$mnsgStatus);
           return back();
    }
}
