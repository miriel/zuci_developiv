<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use DB;
use App\AnswerFile;
use Auth;
use Chumper\Zipper\Zipper; // Libreria para poder comprimir archivos
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use App\catalog_master;
use App\CompanyAnswer;
use App\AnswerFileQuestionnaire;

use Illuminate\Http\Request;

class FilesQuestionarieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $resquest)
    {

      //$folio = Auth::user()->folio;
      $folio = $resquest->folio;
      $carbon = new Carbon();
      $date = Carbon::now();
      $fechaActual = $date->format('d-m-Y');

      $name = $resquest->nameCuest;  // Nombre del cuestionario
      $idCuestionario = $resquest->id; // Id de la tabla company answers (respuestas por compañia)

      $tipoDocumentos = DB::table('catalogs')
        ->where('catnamtabl','=','document_type')
        ->where('cattype','=','file')
        ->pluck('catvalue','catfk');

      $answerFiles = DB::table('answer_files_questionnaires')
      ->select('afqfk as idFile','afqnamefile','afqtypfil','afqstatus as status','afqnopags','afqsize','afqcomtry AS coment','afqqualtion as calif')
      ->where('afqqufk','=',$idCuestionario)
      ->where('afqfoliofk','=',$folio)
      ->orderBy('afqfk','DESC')
      ->get();

      $filesTemplates = DB::table('template_questionnaires')
        ->select('template_questionnaires.qtnamefile as nameTemplate','template_questionnaires.qtsize','catalogs.catvalue as tipoArchivo')
        ->join('catalogs','template_questionnaires.qttypfil','=','catalogs.catfk')
        ->where('template_questionnaires.qtqufk','=',$idCuestionario)
        ->where('template_questionnaires.qtstatus','=', 1)
        ->get();

      // Obtiene el catalogo de status de registros
      $catStatus = DB::table('catalog_master')
        ->select('catalog_master.cmdesc as nameStatus','catalog_master.cmval as valStatus')
        ->where('catalog_master.cmtpcat','=',58)
        ->where('catalog_master.cmstatus','=','1')
        ->get();

      $calificacion = catalog_master::where('cmtpcat','=','59')->where('cmstatus','=','1')->pluck('cmdesc','cmfk');

      return view('miscuestionarios/archivos/templates_cuestionario',compact('name','fechaActual','tipoDocumentos','answerFiles',
                  'folio','filesTemplates', 'catStatus','calificacion','idCuestionario') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $carbon = new Carbon();
      $date = Carbon::now();
      $fechaActual = $date->format('dmYHis');

      //Valida que existan archivos seleccionados
      if($request->fileCheck){

          $fileSep = explode(",",$request->fileCheck);
          for($i=0; $i<count($fileSep); $i++){
            // Se acumula el resultado de las opciones seleccionadas en un Arreglo
            $arrarFilsD[] = $fileSep[$i];
          }

          // Se ejecuta query que obtiene las opciones seleccionadas mediante el arreglo acumulado
          $nameFile=AnswerFileQuestionnaire::select('afqnamefile')->whereIn('afqfk', $arrarFilsD)->get();

          foreach($nameFile as $filesAddZip){
              // Agrega los archivos seleccionador al archivo temporal
              $files = glob(public_path('archivos/cuestionarios/'.$filesAddZip->afqnamefile));
              // Crear el archivo zip temporal
              \Zipper::make('archivos/tmp/filestmp_'.$fechaActual.'.zip')->add($files)->close();
          }

          // Decarga el archivo temporal en la PC del usuario
          return response()->download(public_path('/archivos/tmp/filestmp_'.$fechaActual.'.zip'));

      }else{
        // Si no existen archivos seleccionados regresa a la pantalla inicial
        return back();
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $folio = Auth::user()->folio;
      //$folio = $request->folioAudita;
      $nameEmpresa = Auth::user()->wkrkarea;
      $carbon = new Carbon();
      $date = Carbon::now();
      $fechaActual = $date->format('Ymd');
      $fechaActual2 = $date->format('Y-m-d H:i:s');


      $rutaLocal='/archivos/cuestionarios/';
      $path = public_path().$rutaLocal;
      $files = $request->file('file');

      $tamanoArchivoByte = filesize($files); // Obtiene el tamaño del archivo

      // Foreach para multimples archivos se modifica la variable por file
      //foreach($files as $file){ $file;
            $fileOriginName = $files->getClientOriginalName(); // Obtiene el nombre original del archivo
            $extension = $files->getClientOriginalExtension(); // Obtiene la extencion del archivo
            $filename = pathinfo($fileOriginName, PATHINFO_FILENAME);

            // Valida si existe el nombre del archivo cargado
            $buscaTemplExist = db::table('answer_files_questionnaires')
              ->selectRaw('COUNT(afqfk) as total')
              ->where('afqnamefile','=',$filename.".".strtolower($extension))
              ->where('afqqufk','=',$request->idCuestionario)
              ->where('afqfoliofk','=',$folio)
              ->get();

            // VAlida si existe el archivo co el mismo nombre
            if( $buscaTemplExist[0]->total > 0 ){
              $files->move($path, $filename.".".strtolower($extension));
            }else{
              $fileCreate = AnswerFileQuestionnaire::create([
                  'afqqufk' => $request->idCuestionario,
                  'afqnamefile' => $filename.".".strtolower($extension) ,
                  'afqtypfil' => $request->tipoFile,
                  'afqfoliofk' => $folio,
                  'afqroute' => $rutaLocal,
                  'afqnopags' => 0,
                  'afqsize' => $tamanoArchivoByte,
                  'afqip' =>  $request->ip(),
                  'afquserfk' =>  $request->user()->id,
                  'afqinsertdt' => $fechaActual2,
                  'afqupddt' => $fechaActual2,
              ]);

              if( $fileCreate->afqfk > 0 ){
                $files->move($path, $filename.".".strtolower($extension));
              }

            }





    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
      $statsUpd="";
      // Acautiza el estatus de manera masiva
      if($request->deleteAllFiles == 1){
        if($request->datos > 0){
          foreach($request->datos as $idFilesAll){
            if( $idFilesAll > 0 ){
              $fileStatus = AnswerFileQuestionnaire::find($idFilesAll);

              // Rechazo en caso de que que sea el usuario
              if($fileStatus->afqstatus	 == 2){
                $statsUpd = 8;
              }else
              // Rechazo en caso de que sea el auditor
              if($fileStatus->afqstatus	 == 3 ||  $fileStatus->afqstatus	 == 5 ) {
                $statsUpd = 4;
              }

              $fileUpdate = AnswerFileQuestionnaire::find($idFilesAll);
              $fileUpdate->afqstatus = $statsUpd;
              $fileUpdate->save();

            }
          }
        }

      }else{

          // Rechazo en caso de que sea el auditor
          if( $request->status == 3 || $request->status == 5 ){

            $fileUpdateCal = AnswerFileQuestionnaire::find($id);
            $fileUpdateCal->afqcomtry = $request->comentario;
            $fileUpdateCal->afqqualtion = $request->calificacion;
            $fileUpdateCal->save();

            $valDel = 4;

          }else{
              // Cancela en caso de que sea el usuario
              if( $request->status == 2){
                $valDel = 8;
              }
          }

          $fileUpdate = AnswerFileQuestionnaire::find($id);
          $fileUpdate->afqstatus = $valDel;
          $fileUpdate->save();

      }

      return response()->json([
        "mensaje" => "creado"
      ]);

    }


    protected function downloadFile($src){
        if(is_file($src)){
          $finfo = finfo_open(FILEINFO_MIME_TYPE);
          $content_type = finfo_file($finfo, $src);
          finfo_close($finfo);
          $file_name=basename($src).PHP_EOL;
          $file_name = trim($file_name);
          $size = filesize($src);
          header("Content-Type: $content_type");
          header("Content-Disposition: attachment; filename=$file_name");
          header("Content-Transfer-Encoding: binary");
          header("Content-Length: $size");
          readfile($src);
          return true;
        }else{
          return false;
        }
    }

    protected function download($file){
      $date = Carbon::now();
      $fechaActual = $date->format('dmYHis');

      if($file){
          $fileSep = explode(",",$file);

          foreach($fileSep as $filesAddZip){
            if(file_exists(public_path('archivos/cuestionarios/templates/'.$filesAddZip))){
              $files = glob(public_path('archivos/cuestionarios/templates/'.$filesAddZip));

              \Zipper::make('archivos/tmp/filestmp_'.$fechaActual.'.zip')->add($files)->close();
            }elseif(file_exists(public_path('archivos/cuestionarios/'.$filesAddZip))){

              $files = glob(public_path('archivos/cuestionarios/'.$filesAddZip));

              \Zipper::make('archivos/tmp/filestmp_'.$fechaActual.'.zip')->add($files)->close();
            }else{
              return back();
            }
          }

          return response()->download(public_path('/archivos/tmp/filestmp_'.$fechaActual.'.zip'));
      }else{
        return back();
      }
    }

    // Envia al siguiente nivel de revision
    public function signiv1(Request $request, $id){
      $valSigNiv="";
      // En caso de que el usuario envie el archivo al auditor
      if($request->status == 2){
          $valSigNiv = 3;
      }else
      // En caso de que el auditor envie al siguiente nivel
      if( $request->status == 3 ||  $request->status == 5 ){
          $valSigNiv = 6;
      }else
      // En caso de que el auditor halla rechazado el arhivo y se regresa al usuario
      // Y el usuario reenvia el archivo para su revision del auditor
      if( $request->status == 4) {
        $valSigNiv = 5;
      }

      $sigNivelFile = AnswerFileQuestionnaire::find($id);
      $sigNivelFile->afqstatus = $valSigNiv;
      $sigNivelFile->save();

      return response()->json([
        "mensaje" => "creado"
      ]);
    }


}
