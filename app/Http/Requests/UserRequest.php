<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
       public function rules()
       {
           return [
               'password' => 'confirmed|required|min:8|max:15|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[0-9])(?=.*[$#%&]).+$/',
           ];

       }
              // Devuelve un mensaje por cada atributo erróneo
              public function messages()
              {
                return [
                  'password.confirmed' => ' El Password deben ser igual.',
                  'password.regex' => ' El Password debe contener una letra Mayuscula, Un Numero y Un Caracter Especial ($#%&).',
                  'password.min' => ' El Password no debe tener menos de 8 numeros .',
                    'password.max' => ' El Password no debe tener mas de 10 numeros .',
                     'password.required' => ' El Password es requerido .',
                ];
              }


}
