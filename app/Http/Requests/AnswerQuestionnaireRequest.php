<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule; // Libreria para la validacaion de RUles

class AnswerQuestionnaireRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // questionnaire_answers
        return [
            'qtname' => 'required|numeric|min:1',
            'quname' => 'required|numeric|min:1',
            // Valida si que la pregunta sea requerida ademas de que la pregunta exista pero solamente en el mismo cuestionario
            'qaqsfk' => [
                  'required',
                  Rule::unique('questionnaire_answers')->where(function ($query) {
                      $query->where('qaatqfk', $this->get('idTipoCuestionario'));
                  }),
            ],
            'anname' => 'required|numeric|min:1',
            'contPreg' => 'required',
            'contResp' => 'required',
        ];
    }


}
