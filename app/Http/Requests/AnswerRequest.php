<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'anname' => 'required|unique:answers',
          'anatfk' => 'required|numeric|min:1'
        ];
    }

    public function messages(){
      return [
        'anname.required' => 'La respuesta es requerida',
        'anname.unique' => 'La respuesta ya existe, escriba una diferente',
        'anatfk.required' => 'El tipo de respuesta es requerida'
      ];
    }
}
