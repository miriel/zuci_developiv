<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule; // Libreria para la validacaion de RUles

class QuestionnaireConfigurationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'cqctfk' => [
                  'required',
                  Rule::unique('configuration_questionnaire')->where(function ($query) {
                      $query->where('cqqufk', $this->get('cqqufk'));
                  }),
            ],
            'cqqufk' => [
                  'required',
                  Rule::unique('configuration_questionnaire')->where(function ($query) {
                      $query->where('cqctfk', $this->get('cqctfk'));
                  }),
            ],
        ];
    }
}
