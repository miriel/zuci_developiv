<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionnaireTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'qtname' => 'required|unique:questionnaire_types'
        ];
    }

    public function messages()
    {
      return [
          'qtname.required' => 'Nombre es requerido.',
          'qtname.unique' => 'El tipo de cuestionario ya existe.'
      ];
    }
}
