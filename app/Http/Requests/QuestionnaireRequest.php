<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionnaireRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quname' => 'required|unique:questionnaires'
        ];
    }

    public function messages()
    {
    return [
        'quname.required' => 'Nombre de cuestionario es requerido.',
        'quname.unique' =>'El nombre del cuestionario ya existe.'
    ];
}


}
