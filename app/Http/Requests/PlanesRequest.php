<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlanesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'pgname' => 'required|unique:packages',
             'pgdesc[]' => 'required',
             'pgprice'=>'required|numeric',
        ];
    }
   //  public function response(array $errors)
   // {
   //     if ($this->ajax() || $this->wantsJson())
   //     {
   //         return new JsonResponse($errors, 422);
   //     }
   //     return $this->redirector->to($this->getRedirectUrl())
   //                                     ->withInput($this->except($this->dontFlash))
   //                                     ->withErrors($errors, $this->errorBag);
   // }
}
