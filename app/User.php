<?php

namespace App;

use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $table = "users";
     protected $primaryKey = 'id';
    protected $fillable = [
          'name','amaternal','apaternal','email', 'password','code','active','folio','wkrkarea','job','phone_cel','phone_fixed','lada_cel','expiredcode','id_user','ctfk'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

        // public function setPasswordAttributes($password)
        // {
        //   $this->attributes['password']= bcrypt($password);
        // }

        public function giro()
        {
          return $this->hasMany(Company_Turn::class);
        }

        public function owenr()
        {
          return $this->belongsTo(User::class, 'id_user');
        }


        public function scopeAllowed($query)
        {
          // if( auth()->user()->hasRole('Admin'))
           if( auth()->user()->can('view', $this))
          {
              return $query;
          }else
          {
              return  $query->where('id_user', auth()->id());
          }
        }


}
