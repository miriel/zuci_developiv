<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerFile extends Model
{
    const CREATED_AT = "afinsertdt";
    const UPDATED_AT = "afupddt";

    protected $primaryKey="affk";
    protected $table = "answer_files";
    protected $fillable = ['afqafk','afnamefile','aftypfil','afstatus','afip','afuserfk','afinsertdt','afupddt','affoliofk','afroute',
          'afnopags','afsize','afcomtry','afqualtion'];
}
