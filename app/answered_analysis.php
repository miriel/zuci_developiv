<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class answered_analysis extends Model
{
  protected $table ="answered_analysis";
  protected $primaryKey = 'ararfk';
  protected $fillable = ['rctpcat','arafoliofk','aractfk','aradsdanger','araconsequen','aratotconsec','araexpositio','aratotexposi',
  'araprobabili','aratotprobab','aragrndtotal','ararisklevel','araobservati','arastatus','araip','arausuerfk','arainsertdt','araupddt',
  'arplanacc','ardepresp','arconsemp'];
  const CREATED_AT = 'arainsertdt';
  const UPDATED_AT = 'araupddt';

}
