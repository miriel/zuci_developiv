<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
  protected $table = "countries";
  protected $primaryKey = 'cnfk';
  protected $fillable = ['cnname','cnlada','cnstatus','cnip','cnusuerpk','cninsertdt','cnupddt'];
  const CREATED_AT = 'cninsertdt';
  const UPDATED_AT = 'cnupddt';

  public function owenr()
  {
    return $this->belongsTo(User::class, 'cnusuerpk');
  }


  public function scopeAllowed($query)
  {
    // if( auth()->user()->hasRole('Admin'))
    if( auth()->user()->can('view',$this))
    {
     return $query;
    }else
    {
      return  $query->where('cnusuerpk', auth()->id());
    }

  }

}
