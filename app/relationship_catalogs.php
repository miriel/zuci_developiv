<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class relationship_catalogs extends Model
{
  protected $table ="relationship_catalogs";
  protected $primaryKey = 'rcfk';
  protected $fillable = ['rctpcat','rccmfk','rcstatus','rcctfk'];
  public $timestamps = false;
}
