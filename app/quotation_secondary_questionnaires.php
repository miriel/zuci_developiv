<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class quotation_secondary_questionnaires extends Model
{
  protected $table = 'quotation_secondary_questionnaires';
  protected $primaryKey = 'qsqfk';
  protected $fillable = ['qsqqtfk','qsqqtfkc','qsqfolio','qsqquantity','qsqtotal','qsqdate','qsqstatus','qsqunitprice','qsqconversion','qsquser'];
  public $timestamps = false;
  // const CREATED_AT = 'cotinsertdt';
  // const UPDATED_AT = 'cotupddt';
}
