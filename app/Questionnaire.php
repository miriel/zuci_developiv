<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Spatie\Permission\Traits\HasRoles;

class Questionnaire extends Model
{
    use HasRoles;
    protected $primaryKey = 'qufk';
    protected $table="questionnaires";
    protected $fillable=['quname','qustatus','quip','quuserfk','quinsertdt','quupddt'];
    const CREATED_AT = 'quinsertdt';
    const UPDATED_AT = 'quupddt';

    public static function cuestionario($id){
       $tipoCuest = DB::table('answer_type_questionnaires')
        ->join('questionnaires','answer_type_questionnaires.atqqufk','=','questionnaires.qufk')
        ->select('questionnaires.qufk AS idCues','questionnaires.quname AS nameCuest')
        ->where('answer_type_questionnaires.atqqtfk','=',$id)
        ->get();
        return $tipoCuest;
    }

    public function owenr()
    {
      return $this->belongsTo(User::class, 'quuserfk');
    }

    public function scopeAllowed($query)
    {
      // if( auth()->user()->hasRole('Admin'))
      if( auth()->user()->can('view',$this))
      {
       return $query;
      }else
      {
        return  $query->where('quuserfk', auth()->id());
      }
    }
}
