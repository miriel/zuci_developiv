<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    const CREATED_AT = 'qsinsertdt';
    const UPDATED_AT = 'qsupddt';
    protected $primaryKey = "qsfk";
    protected $table = "questions";
    protected $fillable = ['qsname','qhierarchy','qsstatus','qsip','qsuserfk','qsinsertdt','qsupddt'];

    public static function questions(){
        return Question::where('status','=','1')
              ->select('id','description')
              ->get();
    }
    public function owenr()
    {
      return $this->belongsTo(User::class, 'qsuserfk');
    }


    public function scopeAllowed($query)
    {
      // if( auth()->user()->hasRole('Admin'))
      if( auth()->user()->can('view',$this))
      {
       return $query;
      }else
      {
        return  $query->where('qsuserfk', auth()->id());
      }

    }


}
