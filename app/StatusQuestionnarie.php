<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusQuestionnarie extends Model
{
    protected $table ="questionnaries_status";
      protected $primaryKey = 'sqfk';
    protected $fillable = ['sqfoliofk','sqqufk','sqstatus','sqip','squserfk','sqinsertdt','squpddt'];
    const CREATED_AT = 'sqinsertdt';
    const UPDATED_AT = 'squpddt';
}
