<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class quotations extends Model
{
  protected $table = 'quotations';
  protected $primaryKey = 'qtfk';
  protected $fillable = ['qtfolio','qtcmfk','qtdiscount','qtiva','qttotal','qtuserfk','qtusradit','qtdate','qtdateend',
  'qtdateaudit','qtstatus','qtsubtotal','qtporcentaje'];
  public $timestamps = false;

  public function owenr()
  {
    return $this->belongsTo(User::class, 'qtuserfk');
  }


    public function scopeAllowed($query)
    {
      // if( auth()->user()->hasRole('Admin'))
       if( auth()->user()->can('view', $this))
      {
          return $query;
      }else
      {
          return  $query->where('qtuserfk', auth()->id());
      }
    }
}
