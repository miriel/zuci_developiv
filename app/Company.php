<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
  protected $table = "companies";
  protected $primaryKey = 'cofk';
  protected $fillable = ['coname','coemail','cocotfk','coccfk','coctfk','cosocrzn','cowkrkarea','cofoliofk','corfc','costatus','coip','couserfk','coinsertdt','coupddt'];
  const CREATED_AT = 'coinsertdt';
    const UPDATED_AT = 'coupddt';

}
