<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountrycitiesModel extends Model
{
  protected $table = "country_cities";
  protected $primaryKey = 'ccfk';
  protected $fillable = ['cccnfk','cccifk','ccupddt','ccinserdt','ccusuerpk'];
  const CREATED_AT = 'ccinserdt';
  const UPDATED_AT = 'ccupddt';

  public function owenr()
  {
    return $this->belongsTo(User::class, 'ccusuerpk');
  }


  public function scopeAllowed($query)
  {
    // if( auth()->user()->hasRole('Admin'))
    if( auth()->user()->can('view',$this))
    {
     return $query;
    }else
    {
      return  $query->where('ccusuerpk', auth()->id());
    }

  }
}
