<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company_Turn extends Model
{
  protected $table = 'company_turns';
  protected $primaryKey = 'cotfk';
  protected $fillable = ['cotname','cotstatus','cotip','cotuserfk','cotinsertdt','cotupddt'];
  const CREATED_AT = 'cotinsertdt';
  const UPDATED_AT = 'cotupddt';
  public function owenr()
  {
    return $this->belongsTo(User::class, 'cotuserfk');
  }


  public function scopeAllowed($query)
  {
    // if( auth()->user()->hasRole('Admin'))
    if( auth()->user()->can('view',$this))
    {
     return $query;
    }else
    {
      return  $query->where('cotuserfk', auth()->id());
    }

  }


}
