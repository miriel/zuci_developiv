<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerType extends Model
{
    const CREATED_AT = 'atinsertdt';
    const UPDATED_AT = 'atupddt';
    protected $primaryKey = 'atfk'; 
    protected $table = "answer_types";
    protected $fillable = ['description','typefield','ip','iduser','status','created_at','updated_at'];

}
