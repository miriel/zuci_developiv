<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionnaireAnswer extends Model
{
    const CREATED_AT = "qainsertdt";
    const UPDATED_AT = "qaupddt";
    protected $primaryKey="qafk";
    protected $table="questionnaire_answers";
    protected $fillable = ['qaqsfk','qaanfk','qascafk','qasbcatfk','qaatqfk','qastatus','qaip','qauserfk','qainsertdt','qaupddt'];
}
