<?php

namespace App\Listeners;

use App\Events\UserWasCreated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\LoginCredentials;

class SendLoginCredentials
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserWasCreated  $event
     * @return void
     */
    public function handle(UserWasCreated $event)
    {
        // dd($event->user->toArray(), $event->password);
        //enviar el email con las credenciales de login
        Mail::to($event->user)->queue(
          new LoginCredentials($event->user, $event->password)
        );

    }
}
