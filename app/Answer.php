<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    const CREATED_AT = 'arinsertdt';
    const UPDATED_AT = 'arupddt';

    protected $primaryKey = "anfk";
    protected $table = "answers";
    protected $fillable = ['anname','anatfk','anvalue','anstatus','anip','arusuerfk','arinsertdt','arupddt'];

    public static function answers(){
        return Answer::where('status','=','1')
              ->select('id','description')
              ->get();
    }
    public function owenr()
    {
      return $this->belongsTo(User::class, 'arusuerfk');
    }


    public function scopeAllowed($query)
    {
      // if( auth()->user()->hasRole('Admin'))
      if( auth()->user()->can('view',$this))
      {
       return $query;
      }else
      {
        return  $query->where('arusuerfk', auth()->id());
      }

    }
}
