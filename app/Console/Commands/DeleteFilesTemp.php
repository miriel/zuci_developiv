<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;


class DeleteFilesTemp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generator:delete_filesdap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando borra archivos descargados temporales del cuestionario DAP';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        File::delete(File::glob(public_path()."/archivos/tmp/*.zip"));
    }
}
