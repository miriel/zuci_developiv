<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerResponse extends Model
{
    const CREATED_AT = 'arinsertdt';
    const UPDATED_AT = 'arupddt';
    protected $primaryKey = 'arfk';
    protected $table = "answered_response";
    protected $fillable = ['arqufk','arqsfk','arfoliofk','arstatus','arip','arusuerfk','arinsertdt','arupddt',
                           'arcount','arsubstans'];
}
