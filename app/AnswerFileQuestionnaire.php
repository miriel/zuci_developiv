<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerFileQuestionnaire extends Model
{
  const CREATED_AT = "afqinsertdt";
  const UPDATED_AT = "afqupddt";

  protected $primaryKey="afqfk";
  protected $table = "answer_files_questionnaires";
  protected $fillable = ['afqqufk','afqnamefile','afqtypfil','afqfoliofk','afqroute','afqnopags','afqsize','afqcomtry','afqqualtion',
                         'afqstatus','afqip','afquserfk','afqinsertdt','afqupddt'];
}
