<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Model_has_roles extends Model
{
  protected $table = "model_has_roles";
    protected $primaryKey = 'role_id';
  protected $fillable = ['role_id','model_id','model_type'];
  public $timestamps = false;

}
