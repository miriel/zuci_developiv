<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class quotation_primary_questionnaires extends Model
{
  protected $table = 'quotation_primary_questionnaires';
  protected $primaryKey = 'qpqfk';
  protected $fillable = ['qpqqufk','qpquser','qpqqtfk','qpqfolio','qpqquantity','qpqtotal','qpqdate','qpqstatus','qpqconversion','qpqunitprice'];
  public $timestamps = false;
  // const CREATED_AT = 'cotinsertdt';
  // const UPDATED_AT = 'cotupddt';
}
