<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
  protected $table = "cities";
  protected $primaryKey = 'cifk';
  protected $fillable = ['ciname','cilada','cistatus','ciip','ciusuerpk','ciinsertdt','ciupddt'];
  const CREATED_AT = 'ciinsertdt';
  const UPDATED_AT = 'ciupddt';
  public function owenr()
  {
    return $this->belongsTo(User::class, 'ciusuerpk');
  }

  public function scopeAllowed($query)
  {
    // if( auth()->user()->hasRole('Admin'))
    if( auth()->user()->can('view',$this))
    {
     return $query;
    }else
    {
      return  $query->where('ciusuerpk', auth()->id());
    }
  }

  public static function citiess($id){
    return Cities::Where('cifk','=',$id)
    ->get();
  }

}
