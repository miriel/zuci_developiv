<?php

namespace App\Providers;

use App\Company_Turn;
use App\Policies\GiroPolicy;
use App\Policies\RolePolicy;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
          'App\Company_Turn' => 'App\Policies\GiroPolicy',
          'App\User' => 'App\Policies\UserPolicy',
          'Spatie\Permission\Models\Role' => 'App\Policies\RolePolicy',
          'Spatie\Permission\Models\Permission' => 'App\Policies\PermissionPolicy',
          'App\Company_Type' => 'App\Policies\EmpresaPolicy',
          'App\Country' => 'App\Policies\PaisPolicy',
          'App\City' => 'App\Policies\CiudadPolicy',
          'App\CountrycitiesModel' => 'App\Policies\AgrecpPolicy',
          'App\QuestionnaireType' => 'App\Policies\TypecuesPolicy',
          'App\Question' => 'App\Policies\PreguntaPolicy',
          'App\Answer' => 'App\Policies\RespuestaPolicy',
          'App\Questionnaire' => 'App\Policies\CuestionarioPolicy',
          'App\QuestionnaireConfiguration' => 'App\Policies\ConfiguracionPolicy',
          'App\Catalogs' => 'App\Policies\CatalogoPolicy',
          'App\CompanyAnswer' => 'App\Policies\Dap',
          'App\catalog_master' => 'App\Policies\AnalisisPolicy',
          'App\AnswerFile' => 'App\Policies\AuditoriaPolicy',
          'App\quotations' => 'App\Policies\quotationsPolicy',

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
