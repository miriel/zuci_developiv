<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerTypeQuestionnaire extends Model
{  
  protected $primaryKey="atqbk";
  protected $table = "answer_type_questionnaires";
  protected $fillable=['atqqufk','atqqtfk'];
  public $timestamps = false;
}
