<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyAnswer extends Model
{
    const CREATED_AT = 'cainsertfk';
    const UPDATED_AT = 'caupddt';
    protected $table = "company_answers";
    protected $primaryKey = 'cafk';
    protected $fillable = ['cavalue','caqafk','cafoliofk','caqnfk','casqfk','caip','causerfk','cainsertfk','caupddt'];

}
