<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionnaireConfiguration extends Model
{
    const CREATED_AT = 'cqinsertdt';
    const UPDATED_AT = 'cqupddt';
    protected $primaryKey = "cqfk";
    protected $table="configuration_questionnaire";
    protected $fillable=['cqctfk','cqqufk','cqeverybody','cqstatus ','cqip','cquserfk','cqinsertdt','cqupddt'];

    public function owenr()
    {
      return $this->belongsTo(User::class, 'cquserfk');
    }
    public function scopeAllowed($query)
    {
      // if( auth()->user()->hasRole('Admin'))
      if( auth()->user()->can('view',$this))
      {
       return $query;
      }else
      {
        return  $query->where('cquserfk', auth()->id());
      }
    }


}
