<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaquetesModel extends Model
{
  protected $table = "packages";
  protected $primaryKey = 'id';
  protected $fillable = ['nombre','description','quantity','price','period','showprice'];

  }
