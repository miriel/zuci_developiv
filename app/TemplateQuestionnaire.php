<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplateQuestionnaire extends Model
{
    const CREATED_AT = "qtinsertdt";
    const UPDATED_AT = "qtupddt";
    protected $primaryKey = "qtfk";
    protected $table = "template_questionnaires";
    protected $fillable = ['qtqufk','qtnamefile','qtroute','qtnopags','qtsize','qttypfil','qtstatus','qtip','qtuserfk','qtinsertdt','qtupddt'];

}
