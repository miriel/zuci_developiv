<?php

namespace App\Policies;

use App\User;
use App\catalog_master;

use Illuminate\Auth\Access\HandlesAuthorization;

class AnalisisPolicy
{
    use HandlesAuthorization;
    public function before($user)
  {
    if($user->hasRole('Admin'))
    {
      return true;
    }
  }

  public function view(User $user)
  {
    return  $user->hasPermissionTo('View Analisis');
  }

  public function update(User $user)
  {
    return $user->hasPermissionTo('Update Analisis');
  }
  public function viewdivisa(User $user)
  {
    return  $user->hasPermissionTo('View Divisas');
  }

  public function createdivisa(User $user)
  {
        return  $user->hasPermissionTo('Create Divisas');
  }

  public function updatedivisa(User $user)
  {
    return $user->hasPermissionTo('Update Divisas');
  }

  public function deletedivisa(User $user)
  {
    return $user->hasPermissionTo('Delete Divisas');
  }
  // public function viewauditoria(User $user)
  // {
  //   return  $user->hasPermissionTo('Ver Auditoria');
  // }


}
