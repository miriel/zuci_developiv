<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AuditoriaPolicy
{
    use HandlesAuthorization;
    public function before($user)
  {
    if($user->hasRole('Admin'))
    {
      return true;
    }
  }

  public function view(User $user)
  {
    return  $user->hasPermissionTo('Ver Auditoria');
  }

  public function calificarArchivo(User $user)
  {
    return  $user->hasPermissionTo('calificaFile');
  }
  public function comentFileAudit(User $user)
  {
    return  $user->hasPermissionTo('addComentFileAudit');
  }
  public function viewTemplatesQuestions(User $user)
  {
    return  $user->hasPermissionTo('Ver templates por pregunta');
  }
  public function auditoria(User $user){
    return $user->hasPermissionTo('Auditoria Auditor');
  }




}
