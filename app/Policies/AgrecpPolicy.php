<?php

namespace App\Policies;

use App\User;
use App\CountrycitiesModel;
use Illuminate\Auth\Access\HandlesAuthorization;

class AgrecpPolicy
{
    use HandlesAuthorization;

    public function before($user)
  {
    if($user->hasRole('Admin'))
    {
      return true;
    }
  }

    /**
     * Determine whether the user can view the countrycitiesModel.
     *
     * @param  \App\User  $user
     * @param  \App\CountrycitiesModel  $countrycitiesModel
     * @return mixed
     */
    public function view(User $user, CountrycitiesModel $countrycitiesModel)
    {
      // return $user->id == $countrycitiesModel->ccusuerpk
      // || $user->hasPermissionTo('View CountrycitiesModel');

    }

    /**
     * Determine whether the user can create countrycitiesModels.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return  $user->hasPermissionTo('Create CountrycitiesModel');
    }

    /**
     * Determine whether the user can update the countrycitiesModel.
     *
     * @param  \App\User  $user
     * @param  \App\CountrycitiesModel  $countrycitiesModel
     * @return mixed
     */
    public function update(User $user, CountrycitiesModel $countrycitiesModel)
    {
        //
    }

    /**
     * Determine whether the user can delete the countrycitiesModel.
     *
     * @param  \App\User  $user
     * @param  \App\CountrycitiesModel  $countrycitiesModel
     * @return mixed
     */
    public function delete(User $user, CountrycitiesModel $countrycitiesModel)
    {
        //
    }
}
