<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function before($user)
    {
      if($user->hasRole('Admin'))
      {
        return true;
      }
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function view(User $user, User $authUser)
    {
      return $user->id === $authUser->id
      || $user->hasPermissionTo('View user');

    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return  $user->hasPermissionTo('Create user');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function update(User $user, User $authUser)
    {
      return $user->id === $authUser->id
        || $user->hasPermissionTo('Update user');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function delete(User $user, User $authUser)
    {
      return $user->id === $authUser->id
        || $user->hasPermissionTo('Delete user');
    }

    public function catalogo(User $user, User $authUser)
    {
      return $user->id === $authUser->id
        || $user->hasPermissionTo('view Catalogos');
    }
    public function ver(User $user)
    {
      return  $user->hasPermissionTo('Ver Auditoria');
    }
}
