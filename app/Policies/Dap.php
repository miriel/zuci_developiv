<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\CompanyAnswer;

class Dap
{
    use HandlesAuthorization;

    public function before($user)
    {
      if($user->hasRole('Admin'))
      {
        return true;
      }
    }

    public function dapTxtAudit(User $user){
      return $user->hasPermissionTo('view_calif_dap');
    }
    public function cuestionarioRadio(User $user){
      return $user->hasPermissionTo('Habilitar radio');
    }
    public function cuestionarioTextArea(User $user){
      return $user->hasPermissionTo('Habilitar text area');
    }
    public function cuestionarioText(User $user){
      return $user->hasPermissionTo('Habilitar text');
    }
    public function cuestionarioButton(User $user){
      return $user->hasPermissionTo('Habilitar buttom');
    }
    public function cuestionarioSelect(User $user){
      return $user->hasPermissionTo('Habilitar select');
    }
    public function view(User $user)
    {
      return $user->hasPermissionTo('Ver templates');
    }
    public function boton(User $user){
      return $user->hasPermissionTo('Ver templates');
    }
    public function regresar(User $user){
      return $user->hasPermissionTo('Regresar');
    }
}
