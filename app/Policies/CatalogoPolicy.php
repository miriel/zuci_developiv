<?php

namespace App\Policies;

use App\User;
use App\Catalogs;
use Illuminate\Auth\Access\HandlesAuthorization;

class CatalogoPolicy
{
    use HandlesAuthorization;

    public function before($user)
  {
    if($user->hasRole('Admin'))
    {
      return true;
    }
  }

    /**
     * Determine whether the user can view the catalogs.
     *
     * @param  \App\User  $user
     * @param  \App\Catalogs  $catalogs
     * @return mixed
     */
    public function view(User $user, Catalogs $catalogs)
    {
      return $user->id == $catalogs->catuserfk
      || $user->hasPermissionTo('View Secciones');
    }

    /**
     * Determine whether the user can create catalogs.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return  $user->hasPermissionTo('Create Secciones');
    }

    /**
     * Determine whether the user can update the catalogs.
     *
     * @param  \App\User  $user
     * @param  \App\Catalogs  $catalogs
     * @return mixed
     */
    public function update(User $user, Catalogs $catalogo)
    {
      return $user->id == $catalogo->catuserfk
      || $user->hasPermissionTo('Update Secciones');
    }

    /**
     * Determine whether the user can delete the catalogs.
     *
     * @param  \App\User  $user
     * @param  \App\Catalogs  $catalogs
     * @return mixed
     */
    public function delete(User $user, Catalogs $catalogs)
    {
      return $user->id == $catalogs->catuserfk
      || $user->hasPermissionTo('Delete Secciones');
    }

    public function secciones(User $user, Catalogs $catalogs)
    {
      return $user->id == $catalogs->catuserfk
      || $user->hasPermissionTo('Mostar Secciones');
    }
}
