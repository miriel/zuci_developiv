<?php

namespace App\Policies;

use App\User;
use App\QuestionnaireType;
use Illuminate\Auth\Access\HandlesAuthorization;

class TypecuesPolicy
{
    use HandlesAuthorization;

          public function before($user)
        {
          if($user->hasRole('Admin'))
          {
            return true;
          }
        }

    /**
     * Determine whether the user can view the questionnaireType.
     *
     * @param  \App\User  $user
     * @param  \App\QuestionnaireType  $questionnaireType
     * @return mixed
     */
    public function view(User $user, QuestionnaireType $questionnaireType)
    {
      return $user->id == $questionnaireType->qtuserfk
      || $user->hasPermissionTo('View QuestionnaireType');
    }

    /**
     * Determine whether the user can create questionnaireTypes.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return  $user->hasPermissionTo('Create QuestionnaireType');
    }

    /**
     * Determine whether the user can update the questionnaireType.
     *
     * @param  \App\User  $user
     * @param  \App\QuestionnaireType  $questionnaireType
     * @return mixed
     */
    public function update(User $user, QuestionnaireType $questionnaireType)
    {
      return $user->id == $questionnaireType->qtuserfk
      || $user->hasPermissionTo('Update QuestionnaireType');
    }

    /**
     * Determine whether the user can delete the questionnaireType.
     *
     * @param  \App\User  $user
     * @param  \App\QuestionnaireType  $questionnaireType
     * @return mixed
     */
    public function delete(User $user, QuestionnaireType $tipoCuest)
    {
      return $user->id == $tipoCuest->qtuserfk
      || $user->hasPermissionTo('Delete QuestionnaireType');
    }

    public function evaluaciones(User $user, QuestionnaireType $tipoCuest)
    {
      return $user->id == $tipoCuest->qtuserfk
      || $user->hasPermissionTo('Mostar evaluaciones');
    }
}
