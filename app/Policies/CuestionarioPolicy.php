<?php

namespace App\Policies;

use App\User;
use App\Questionnaire;
use Illuminate\Auth\Access\HandlesAuthorization;

class CuestionarioPolicy
{
    use HandlesAuthorization;

        public function before($user)
      {
        if($user->hasRole('Admin'))
        {
          return true;
        }
      }

    /**
     * Determine whether the user can view the questionnaire.
     *
     * @param  \App\User  $user
     * @param  \App\Questionnaire  $questionnaire
     * @return mixed
     */
    public function view(User $user, Questionnaire $questionnaire)
    {
      return $user->id === $questionnaire->quuserfk
      || $user->hasPermissionTo('View Questionnaire');

    }

    /**
     * Determine whether the user can create questionnaires.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
      return  $user->hasPermissionTo('Create Questionnaire');
    }

    /**
     * Determine whether the user can update the questionnaire.
     *
     * @param  \App\User  $user
     * @param  \App\Questionnaire  $questionnaire
     * @return mixed
     */
    public function update(User $user, Questionnaire $questionnaire)
    {
      return $user->id === $questionnaire->quuserfk
      || $user->hasPermissionTo('Update Questionnaire');
    }

    /**
     * Determine whether the user can delete the questionnaire.
     *
     * @param  \App\User  $user
     * @param  \App\Questionnaire  $questionnaire
     * @return mixed
     */
    public function delete(User $user, Questionnaire $questionnaire)
    {
      return $user->id === $questionnaire->quuserfk
      || $user->hasPermissionTo('Delete Questionnaire');
    }

    public function question(User $user, Questionnaire $questionnaire)
    {
      return $user->id === $questionnaire->quuserfk
      || $user->hasPermissionTo('Edit Questionnaire');
    }

    public function pregres(User $user, Questionnaire $questionnaire)
    {
      return $user->id === $questionnaire->quuserfk
      || $user->hasPermissionTo('Pregres Questionnaire');
    }

    public function pregunta(User $user, Questionnaire $questionnaire)
    {
      return $user->id === $questionnaire->quuserfk
      || $user->hasPermissionTo('Pregunta Questionnaire');
    }

    public function respuesta(User $user, Questionnaire $questionnaire)
    {
      return $user->id === $questionnaire->quuserfk
      || $user->hasPermissionTo('Respuesta Questionnaire');
    }
    public function miscuestionarios(User $user){
        return  $user->hasPermissionTo('Mis Cuestionarios');
    }
}
