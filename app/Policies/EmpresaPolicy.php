<?php

namespace App\Policies;

use App\User;
use App\Company_Type;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmpresaPolicy
{
    use HandlesAuthorization;

        public function before($user)
      {
        if($user->hasRole('Admin'))
        {
          return true;
        }
      }

    /**
     * Determine whether the user can view the companyType.
     *
     * @param  \App\User  $user
     * @param  \App\Company_Type  $companyType
     * @return mixed
     */
    public function view(User $user, Company_Type $Company_Type)
    {
      return $user->id === $Company_Type->ctuserfk
      || $user->hasPermissionTo('View company_types');
    }

    /**
     * Determine whether the user can create companyTypes.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return  $user->hasPermissionTo('Create company_types');
    }

    /**
     * Determine whether the user can update the companyType.
     *
     * @param  \App\User  $user
     * @param  \App\Company_Type  $companyType
     * @return mixed
     */
    public function update(User $user, Company_Type $Company_Type)
    {
      return $user->id === $Company_Type->ctuserfk
      || $user->hasPermissionTo('Update company_types');
    }

    /**
     * Determine whether the user can delete the companyType.
     *
     * @param  \App\User  $user
     * @param  \App\Company_Type  $companyType
     * @return mixed
     */
    public function delete(User $user, Company_Type $Company_Type)
    {
      return $user->id === $Company_Type->ctuserfk
      || $user->hasPermissionTo('Delete company_types');
    }
}
