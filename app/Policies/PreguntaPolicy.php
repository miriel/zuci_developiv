<?php

namespace App\Policies;

use App\User;
use App\Question;
use Illuminate\Auth\Access\HandlesAuthorization;

class PreguntaPolicy
{
    use HandlesAuthorization;
    public function before($user)
  {
    if($user->hasRole('Admin'))
    {
      return true;
    }
  }

    /**
     * Determine whether the user can view the question.
     *
     * @param  \App\User  $user
     * @param  \App\Question  $question
     * @return mixed
     */
    public function view(User $user, Question $question)
    {
      return $user->id == $question->qsuserfk
      || $user->hasPermissionTo('View question');
    }

    /**
     * Determine whether the user can create questions.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
          return  $user->hasPermissionTo('Create question');
    }

    /**
     * Determine whether the user can update the question.
     *
     * @param  \App\User  $user
     * @param  \App\Question  $question
     * @return mixed
     */
    public function update(User $user, Question $preguntasFind)
    {
      return $user->id == $preguntasFind->qsuserfk
      || $user->hasPermissionTo('Update question');
    }

    /**
     * Determine whether the user can delete the question.
     *
     * @param  \App\User  $user
     * @param  \App\Question  $question
     * @return mixed
     */
    public function delete(User $user, Question $pretunta)
    {
      return $user->id == $pretunta->qsuserfk
      || $user->hasPermissionTo('Delete question');
    }
}
