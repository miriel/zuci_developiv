<?php

namespace App\Policies;

use App\User;
use App\City;
use Illuminate\Auth\Access\HandlesAuthorization;

class CiudadPolicy
{
    use HandlesAuthorization;

    public function before($user)
  {
    if($user->hasRole('Admin'))
    {
      return true;
    }
  }
    /**
     * Determine whether the user can view the city.
     *
     * @param  \App\User  $user
     * @param  \App\City  $city
     * @return mixed
     */
    public function view(User $user, City $ciudad)
    {
      return $user->id == $ciudad->ciusuerpk
      || $user->hasPermissionTo('View city');
    }

    /**
     * Determine whether the user can create cities.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return  $user->hasPermissionTo('Create city');
    }

    /**
     * Determine whether the user can update the city.
     *
     * @param  \App\User  $user
     * @param  \App\City  $city
     * @return mixed
     */
    public function update(User $user, City $ciudad)
    {
      return $user->id == $ciudad->ciusuerpk
      || $user->hasPermissionTo('Update city');
    }

    /**
     * Determine whether the user can delete the city.
     *
     * @param  \App\User  $user
     * @param  \App\City  $city
     * @return mixed
     */
    public function delete(User $user, City $ciudad)
    {
      return $user->id == $ciudad->ciusuerpk
      || $user->hasPermissionTo('Delete city');
    }
}
