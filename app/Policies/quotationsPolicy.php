<?php

namespace App\Policies;

use App\User;
use App\quotations;
use Illuminate\Auth\Access\HandlesAuthorization;

class quotationsPolicy
{
    use HandlesAuthorization;

    public function before($user)
    {
      if($user->hasRole('Admin'))
      {
        return true;
      }
    }


    public function view(User $user, quotations $quotations)
    {
      return $user->id === $quotations->qtuserfk
      || $user->hasPermissionTo('Cotizacion');
    }

    /**
     * Determine whether the user can create quotations.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
      return $user->hasPermissionTo('Crear Cotizacion');
    }

    /**
     * Determine whether the user can update the quotations.
     *
     * @param  \App\User  $user
     * @param  \App\quotations  $quotations
     * @return mixed
     */
    public function update(User $user, quotations $quotations)
    {
        //
    }

    /**
     * Determine whether the user can delete the quotations.
     *
     * @param  \App\User  $user
     * @param  \App\quotations  $quotations
     * @return mixed
     */
    public function delete(User $user, quotations $quotations)
    {
        //
    }

    //Aqui ponemos el permiso viewMenu es como lo voy a mandar llamar en el codigo
    //Visualizar menu cotizador es el nombre que yo le asigne al crearlo
    public function viewMenu(User $user){
      return $user->hasPermissionTo('Visualizar menu cotizador');
    }
}
