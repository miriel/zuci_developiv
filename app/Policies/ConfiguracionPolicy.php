<?php

namespace App\Policies;

use App\User;
use App\QuestionnaireConfiguration;
use Illuminate\Auth\Access\HandlesAuthorization;

class ConfiguracionPolicy
{
    use HandlesAuthorization;

          public function before($user)
        {
          if($user->hasRole('Admin'))
          {
            return true;
          }
        }

    /**
     * Determine whether the user can view the questionnaireConfiguration.
     *
     * @param  \App\User  $user
     * @param  \App\QuestionnaireConfiguration  $questionnaireConfiguration
     * @return mixed
     */
    public function view(User $user, QuestionnaireConfiguration $questionnaireConfiguration)
    {
      return $user->id === $questionnaireConfiguration->cquserfk
      || $user->hasPermissionTo('View configuracioncues');
    }

    /**
     * Determine whether the user can create questionnaireConfigurations.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return  $user->hasPermissionTo('Create configuracioncues');
    }

    /**
     * Determine whether the user can update the questionnaireConfiguration.
     *
     * @param  \App\User  $user
     * @param  \App\QuestionnaireConfiguration  $questionnaireConfiguration
     * @return mixed
     */
    public function update(User $user, QuestionnaireConfiguration $questionnaireConfiguration)
    {
      return $user->id === $questionnaireConfiguration->cquserfk
      || $user->hasPermissionTo('Update configuracioncues');
    }

    /**
     * Determine whether the user can delete the questionnaireConfiguration.
     *
     * @param  \App\User  $user
     * @param  \App\QuestionnaireConfiguration  $questionnaireConfiguration
     * @return mixed
     */
    public function delete(User $user, QuestionnaireConfiguration $questionnaireConfiguration)
    {
      return $user->id === $questionnaireConfiguration->cquserfk
      || $user->hasPermissionTo('Delete configuracioncues');
    }
}
