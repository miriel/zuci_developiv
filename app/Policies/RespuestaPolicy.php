<?php

namespace App\Policies;

use App\User;
use App\Answer;
use Illuminate\Auth\Access\HandlesAuthorization;

class RespuestaPolicy
{
    use HandlesAuthorization;

          public function before($user)
        {
          if($user->hasRole('Admin'))
          {
            return true;
          }
        }


    /**
     * Determine whether the user can view the answer.
     *
     * @param  \App\User  $user
     * @param  \App\Answer  $answer
     * @return mixed
     */
    public function view(User $user, Answer $answer)
    {
      return $user->id == $answer->arusuerfk
      || $user->hasPermissionTo('View Answer');
    }

    /**
     * Determine whether the user can create answers.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return  $user->hasPermissionTo('Create Answer');
    }

    /**
     * Determine whether the user can update the answer.
     *
     * @param  \App\User  $user
     * @param  \App\Answer  $answer
     * @return mixed
     */
    public function update(User $user, Answer $answer)
    {
      return $user->id == $answer->arusuerfk
      || $user->hasPermissionTo('Update Answer');
    }

    /**
     * Determine whether the user can delete the answer.
     *
     * @param  \App\User  $user
     * @param  \App\Answer  $answer
     * @return mixed
     */
    public function delete(User $user, Answer $answer)
    {
      return $user->id == $answer->arusuerfk
      || $user->hasPermissionTo('Delete Answer');
    }

    
}
