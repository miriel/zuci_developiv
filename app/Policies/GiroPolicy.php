<?php

namespace App\Policies;

use App\User;
use App\Company_Turn;
use Illuminate\Auth\Access\HandlesAuthorization;

class GiroPolicy
{
    use HandlesAuthorization;

          public function before($user)
        {
          if($user->hasRole('Admin'))
          {
            return true;
          }
        }

    /**
     * Determine whether the user can view the companyTurn.
     *
     * @param  \App\User  $user
     * @param  \App\Company_Turn  $companyTurn
     * @return mixed
     */
    public function view(User $user, Company_Turn $companyTurn)
    {
      return $user->id == $companyTurn->ctuserfk
      || $user->hasPermissionTo('View company_turn');

    }

    /**
     * Determine whether the user can create companyTurns.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
          return  $user->hasPermissionTo('Create company_turn');
    }

    /**
     * Determine whether the user can update the companyTurn.
     *
     * @param  \App\User  $user
     * @param  \App\Company_Turn  $companyTurn
     * @return mixed
     */
    public function update(User $user, Company_Turn $companyTurn)
    {
      return $user->id === $companyTurn->cotuserfk
      || $user->hasPermissionTo('Update company_turn');
    }

    /**
     * Determine whether the user can delete the companyTurn.
     *
     * @param  \App\User  $user
     * @param  \App\Company_Turn  $companyTurn
     * @return mixed
     */
    public function delete(User $user, Company_Turn $companyTurn)
    {
      return $user->id === $companyTurn->cotuserfk
      || $user->hasPermissionTo('Delete company_turn');
    }
}
