<?php

namespace App\Policies;

use App\User;
use App\CompanyAnswer;
use Illuminate\Auth\Access\HandlesAuthorization;

class templates
{
    use HandlesAuthorization;

    public function before($user)
  {
    if($user->hasRole('Admin'))
    {
      return true;
    }
  }


    /**
     * Determine whether the user can view the companyAnswer.
     *
     * @param  \App\User  $user
     * @param  \App\CompanyAnswer  $companyAnswer
     * @return mixed
     */
    public function view(User $user)
    {
      return $user->hasPermissionTo('Ver templates');
    }

    /**
     * Determine whether the user can create companyAnswers.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the companyAnswer.
     *
     * @param  \App\User  $user
     * @param  \App\CompanyAnswer  $companyAnswer
     * @return mixed
     */
    public function update(User $user, city $city)
    {
        //
    }

    /**
     * Determine whether the user can delete the companyAnswer.
     *
     * @param  \App\User  $user
     * @param  \App\CompanyAnswer  $companyAnswer
     * @return mixed
     */
    public function delete(User $user, CompanyAnswer $companyAnswer)
    {
        //
    }
}
