<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company_Type extends Model
{
  protected $table = 'company_types';
  protected $primaryKey = 'ctfk';
  protected $fillable = ['ctname','ctstatus','ctip','ctuserfk','ctinsertdt','ctupddt'];
  const CREATED_AT = 'ctinsertdt';
  const UPDATED_AT = 'ctupddt';

  public function owenr()
  {
    return $this->belongsTo(User::class, 'ctuserfk');
  }

  public function scopeAllowed($query)
  {
    // if( auth()->user()->hasRole('Admin'))
    if( auth()->user()->can('view',$this))
    {
     return $query;
    }else
    {
      return  $query->where('ctuserfk', auth()->id());
    }
  }

}
