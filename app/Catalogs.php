<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catalogs extends Model
{
    const CREATED_AT = 'catinsertdt';
    const UPDATED_AT = 'catupddt';
    protected $primaryKey="catfk";
    protected $table="catalogs";
    protected $fillable = ['catnamtabl','catvalue','catstatus','catip','catuserfk','catinsertdt','catupddt','cattype','catacronym'];

    public function owenr()
    {
      return $this->belongsTo(User::class, 'catuserfk');
    }

    public function scopeAllowed($query)
    {
      // if( auth()->user()->hasRole('Admin'))
      if( auth()->user()->can('view',$this))
      {
       return $query;
      }else
      {
        return  $query->where('catuserfk', auth()->id());
      }
    }
}
