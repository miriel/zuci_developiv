<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* vista de idiomas  */
    Route::group(['middleware' => ['web']], function () {
  	    Route::resource('/','AdminController');

		Route::group(['middleware' => ['web']], function () {
			Route::get('lang/{lang}', function ($lang) {
			     App::setLocale($lang);
			        session(['lang' => $lang]);
				          return \Redirect::back();
			             })->where([
				                 'lang' => 'en|es|fr|pt'
         ]);
        });
       });
    /* fin de ruta de idiomas  */
Auth::routes();
Route::get('/respuesta',function(){
  return view('response');
});
Route::get('activacion/{code}','UserController@activate');
Route::post('complete/{id}','UserController@complete');
// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('cities/{id}','Auth\RegisterController@getCities');
Route::get('lada/{id}','Auth\RegisterController@getlada');

Route::get('reactivacion',function(){
  return view('auth.reactivacion');
});
  /* vista de validador de correo si existe */
  Route::post('validacorreor','UserController@validar');
  /* fin de ruta de validaro de correo */

Route::group(['middleware'=>'auth'],function(){
          Route::get('/home', 'HomeController@index')->name('home');
          Route::resource('micuestionarios','DapauditorController');
          //  Cuestionarios asignados al cliente
          Route::resource('miscuestionarios','MisCuestionariosController');
          Route::get('subsecciones/{id}','MisCuestionariosController@getSubsection')->name('subsecciones.inicio');
          Route::get('preguntas/{seccion}/{subseccion}','MisCuestionariosController@getQuestions')->name('preguntas.inicio');

          // Administrador de Cuestionarios
          Route::get('respuesta/respuesta/{val1}','AnswerController@respuesta');
          Route::resource('respuesta','AnswerController');
          Route::resource('pregunta','QuestionController');
          Route::resource('cuestionario','QuestionnaireController');
          Route::get('cuestionariostatus/{st}','QuestionnaireController@muestraActivInac');
          Route::resource('asingapregunta','QuestionnaireAnswerController');
          Route::get('tipocuest/{id}','QuestionnaireAnswerController@getCuestionarios');
          Route::resource('tipocuestionario','QuestionnaireTypeController');
          Route::resource('configcuestionario','QuestionnaireConfigurationController');
          Route::get('respuestaselect','AnswerController@getAnswer');
          Route::get('preguntaselect','QuestionController@getQuestion');
          Route::post('savetemplate','FilesController@guardaTemplate'); // Ruta para guardar los templates(Archivos) para cada respuesta
          Route::resource('file','FilesController');
          Route::resource('fileTemplate','FileTemplateController'); // Ruta que envia a la pestaña para cargar los templates por pregunta/respuesta(Archivos)
          Route::resource('tempcuest','FileTemplateCuestionarioController'); // Ruta que envia a la pestaña para cargar los templates por cuestionario(Archivos)
          Route::resource('filecuest','FilesQuestionarieController'); // Ruta que envia a la pestaña para cargar los templates por pregunta/respuesta(Archivos)
          Route::get('download/{file}','FilesController@download');
          Route::get('downloadCuest/{file}','FilesQuestionarieController@download');
          Route::get('downloadTemplate/{file}','FileTemplateController@download');
          Route::get('downloadTemplateCues/{file}','FilesQuestionarieController@download');
          Route::get('signiv1/{id}','FilesController@signiv1'); // Enviar al siguiente nivel os archivos al perfil auditor desde el perfil usuario
          Route::get('signiv1Cuest/{id}','FilesQuestionarieController@signiv1'); // Enviar al siguiente nivel os archivos al perfil auditor desde el perfil usuario
          Route::get('signiv1Temp/{id}','FileTemplateController@signiv1');

          Route::resource('catalogo','CatalogoController');
          // Administrador alta de tipo de giro
<<<<<<< HEAD
 
=======

>>>>>>> 8f4e320ea0510daec4932c459c7ba8e038e10003
          Route::resource('giro','GiroController');
          Route::resource('empresa','EmpresaController');
          Route::resource('planes','PlanesController');
          Route::resource('dap','DapController');
          Route::resource('pais','PaisController');
          Route::resource('ciudad','CiudadController');
          Route::resource('agrega','Contri_citiController');
          Route::resource('tipoArchivo','TipoArchivoController');
          Route::resource('calificacionArchivo','CalificacionArchivoController');


      Route::resource('user','UserController');

      Route::middleware('role:Admin')->put('user/{user}/roles','UsersRolesController@update')->name('user.roles.update');

      Route::middleware('role:Admin')->put('user/{user}/permissions','UsersPermissionsController@update')->name('user.permissions.update');

      Route::resource('roles','RoleController');

      Route::resource('permissions','PermissionsController');

      /* ruta para ver el email la vista */
      Route::get('email', function(){
        return new App\Mail\LoginCredentials(App\User::first(),'123456');
      });
      Route::resource('auditFile','AuditoriaArchivosController');
      Route::resource('analisis','AnalisController');
      Route::get('valor/{id}','AnalisController@Valorcatalogo');
      Route::get('grado/{id}','AnalisController@GradoRiesgo');
      Route::get('val/{id}','AriesgoController@Valorcatalogo');
      Route::get('grad/{id}','AriesgoController@GradoRiesgo');
      Route::resource('ariesgo','AriesgoController');
      Route::resource('ariesgoEjec','AriesgoEjecutivoController');
      Route::resource('operacion','catalog_masterController');
      Route::resource('actividad','ActividadController');
      Route::resource('descripcion','DescripcionController');
      Route::resource('catalogos','CataAnalisisController');

      Route::get('descargar-productos', 'GiroController@pdf')->name('products.pdf');
      Route::get('descargar-producto', 'GiroController@excel')->name('products.excel');
      // Route::get('pdf', 'PdfController@invoice');
      Route::get('analisis', 'AriesgoController@pdf')->name('alanisis.pdf');
      Route::get('analisisEjec', 'AriesgoEjecutivoController@pdf')->name('analisisEjec.pdf');
      Route::get('analisisEjecGr', 'AriesgoEjecutivoController@muestraContenido')->name('analisisEjecGr.muestraContenido'); // Ruta para guardar la imagen generada de la grfica
      Route::resource('divisas', 'DivisasController');
      Route::resource('cotizador', 'CotizadorController');
      Route::get('div/{id}','CotizadorController@getid');
      Route::resource('cotizaciones', 'CotizadorUser');
      Route::get('pdfcotizacion', 'CotizadorUser@pdf')->name('pdfcotizacion.pdf');
      Route::resource('admincotizador', 'AdminCotizadorController');

});

Route::resource('prueba','DapController');
